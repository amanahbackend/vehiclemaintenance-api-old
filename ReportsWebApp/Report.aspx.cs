﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using ReportsWebApp.Models;
using ReportsWebApp.Utils;

namespace ReportsWebApp
{
    public partial class Report : Page
    {
        private string ReportsJsonFilePath => Server.MapPath("App_Data/Reports.json");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DisplayReportData();
            }
        }

        #region Methods

        private void DisplayReportData()
        {
            if (Request.QueryString["id"] == null)
            {
                return;
            }

            int reportId = Convert.ToInt32(Request.QueryString["id"]);

            if (reportId <= 0)
            {
                BindDataToReportViewer(new DataSet(), "Reports/TestReport.rdl");

                return;
            }

            ReportMetaData reportMetaData = GetReportMetaDataFromJson(reportId);

            if (reportMetaData == null)
            {
                lblErrorMessage.Text = "Error reading report meta data.";

                return;
            }

            DataSet reportDataSet = GetDataTableOfReport(reportMetaData);

            if (reportDataSet == null)
            {
                lblErrorMessage.Text = "Sorry. An exception occurred while getting the report data.";
            }
            else if (reportDataSet.Tables.Count == 0)
            {
                lblErrorMessage.Text = "No data source found in the report.";
            }
            else
            {
                BindDataToReportViewer(reportDataSet, reportMetaData.RelativePath);
            }
        }

        private ReportMetaData GetReportMetaDataFromJson(int reportId)
        {
            string jsonString = FileUtil.ReadFile(ReportsJsonFilePath);

            List<ReportMetaData> lstReportMetaDatas =
                new JavaScriptSerializer().Deserialize<List<ReportMetaData>>(jsonString);

            ReportMetaData reportMetaData = lstReportMetaDatas.FirstOrDefault(obj => obj.Id == reportId);

            return reportMetaData;
        }

        private DataSet GetDataTableOfReport(ReportMetaData reportMetaData)
        {
            DataSet dataSet = new DataSet();

            foreach (ReportDataSourceData dataSource in reportMetaData.DataSources)
            {
                List<SqlParameter> lstSqlParameters = new List<SqlParameter>(dataSource.ReportParameters.Count);

                foreach (ReportParameterData reportParameter in dataSource.ReportParameters)
                {
                    string parameterValue = reportParameter.ParameterValue ??
                                            Request.QueryString[reportParameter.QueryStringParameterName];

                    if (!string.IsNullOrWhiteSpace(parameterValue))
                    {
                        lstSqlParameters.Add(new SqlParameter(reportParameter.ParameterName, parameterValue));
                    }
                }

                DataTable dtReportData = DbAccessManager.GetTable(dataSource.CommandText,
                    dataSource.DataSetName, lstSqlParameters);

                dataSet.Tables.Add(dtReportData);
            }

            return dataSet;
        }

        /// <summary>
        /// Bind Data To Report Viewer.
        /// </summary>
        /// <param name="dataSet">Set of tables. Each table name is "DataSourceName".</param>
        /// <param name="relativeReportPath">"Reports/Report1.rdl"</param>
        private void BindDataToReportViewer(DataSet dataSet, string relativeReportPath)
        {
            reportViewer1.ProcessingMode = ProcessingMode.Local;

            string websitePhysicalPath = WebConfigManager.WebsitePhysicalPath;
            reportViewer1.LocalReport.ReportPath = $"{websitePhysicalPath}\\{relativeReportPath}";

            reportViewer1.LocalReport.DataSources.Clear();

            foreach (DataTable dataTable in dataSet.Tables)
            {
                ReportDataSource dataSource = new ReportDataSource
                {
                    Name = dataTable.TableName,
                    Value = dataTable
                };

                reportViewer1.LocalReport.DataSources.Add(dataSource);
            }

            reportViewer1.LocalReport.Refresh();
        }

        #endregion Methods
    }
}