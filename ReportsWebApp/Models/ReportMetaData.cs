﻿using System.Collections.Generic;

namespace ReportsWebApp.Models
{
    public class ReportMetaData
    {
        public int? Id { get; set; }

        public string ReportTitle { get; set; }

        /// <summary>
        /// The value like "Reports/Report1.rdl".
        /// </summary>
        public string RelativePath { get; set; }
        
        public List<ReportDataSourceData> DataSources { get; set; }

        public ReportMetaData()
        {
            DataSources = new List<ReportDataSourceData>();
        }
    }

    public class ReportDataSourceData
    {
        /// <summary>
        /// DataSetName or DataTable Name
        /// </summary>
        public string DataSetName { get; set; }

        public string CommandText { get; set; }

        public string CommandType { get; set; }

        /// <summary>
        /// Your own description.
        /// </summary>
        public string Description { get; set; }

        public List<ReportParameterData> ReportParameters { get; set; }

        public ReportDataSourceData()
        {
            CommandType = System.Data.CommandType.StoredProcedure.ToString();
            Description = string.Empty;

            ReportParameters = new List<ReportParameterData>();
        }
    }

    public class ReportParameterData
    {
        /// <summary>
        /// Parameter name like "@id"
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// Query string parameter name.
        /// Setting this property ignores "ParameterValue" property.
        /// </summary>
        public string QueryStringParameterName { get; set; }

        /// <summary>
        /// Set this property if the value of the parameter is fixed.
        /// Setting this property ignores "QueryStringParameterName" property.
        /// </summary>
        public string ParameterValue { get; set; }

        /// <summary>
        /// Your own description.
        /// </summary>
        public string Description { get; set; }

        public ReportParameterData()
        {
            Description = string.Empty;
        }
    }
}