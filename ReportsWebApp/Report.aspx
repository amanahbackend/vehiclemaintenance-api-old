﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="ReportsWebApp.Report" %>

<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View report</title>
</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="reportViewer1" runat="server" Width="900px" Height="700px"></rsweb:ReportViewer>
        <div>
            <asp:Label runat="server" ID="lblErrorMessage"></asp:Label>
        </div>
    </form>

</body>
</html>
