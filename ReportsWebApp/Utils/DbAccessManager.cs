﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ReportsWebApp.Utils
{
    public static class DbAccessManager
    {
        public static DataTable GetTable(string commandText, string tableName, List<SqlParameter> lstSqlParameters = null)
        {
            DataTable dataTable1 = new DataTable(tableName);

            SqlConnection sqlConnection = new SqlConnection(WebConfigManager.ConnectionString);

            try
            {
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = sqlConnection,
                    CommandText = commandText,
                    CommandType = CommandType.StoredProcedure
                };

                if (lstSqlParameters != null && lstSqlParameters.Count > 0)
                {
                    selectCommand.Parameters.AddRange(lstSqlParameters.ToArray());
                }

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter
                {
                    SelectCommand = selectCommand
                };

                sqlDataAdapter.Fill(dataTable1);
            }
            catch (SqlException ex)
            {
                dataTable1 = null;
            }
            finally
            {
                sqlConnection.Close();
            }

            return dataTable1;
        }
    }
}