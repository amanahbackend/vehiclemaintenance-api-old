﻿using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using ReportsWebApp.Models;

namespace ReportsWebApp.Utils
{
    public static class FileUtil
    {
        public static string ReadFile(string filePath)
        {
            StreamReader streamReader = new StreamReader(filePath);
            string content = streamReader.ReadToEnd();
            streamReader.Close();

            return content;
        }

        public static void WriteFile(string filePath, string content)
        {
            StreamWriter streamWriter = new StreamWriter(filePath);
            streamWriter.Write(content);
            streamWriter.Close();
        }

        public static void SaveJsonFile(string filePath, List<ReportMetaData> lstReportMetaDatas)
        {
            var json = new JavaScriptSerializer().Serialize(lstReportMetaDatas);

            WriteFile(filePath, json);
        }
    }
}