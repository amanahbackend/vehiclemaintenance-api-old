﻿using System.Web.Configuration;

namespace ReportsWebApp.Utils
{
    public static class WebConfigManager
    {
        private static string _connectionString;
        private static string _websitePhysicalPath;

        public static string ConnectionString =>
            _connectionString ?? (_connectionString = WebConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString);

        public static string WebsitePhysicalPath =>
            _websitePhysicalPath ?? (_websitePhysicalPath = WebConfigurationManager.AppSettings["WebsitePhysicalPath"]);
    }
}