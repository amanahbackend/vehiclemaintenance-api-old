﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 27/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_GetSpareParts @jobCardId = 343;
-- EXEC dbo.stp_JobCard_GetSpareParts @jobCardId = 0;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_GetSpareParts] (@jobCardId INT)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@jobCardId <> 0)
    BEGIN

        SELECT RequestSpareParts.[Id] AS RequestSparePartId,
               Inventory.Name_EN AS SparePartName,
               RequestSpareParts.Quantity
        FROM [dbo].[SparePartsRequest] AS RequestSpareParts
            INNER JOIN dbo.RepairRequest AS RepairRequest
                ON RepairRequest.Id = RequestSpareParts.FK_RepairRequest_ID
            INNER JOIN dbo.Inventory AS Inventory
                ON Inventory.Id = RequestSpareParts.FK_Inventory_ID
        WHERE RepairRequest.FK_JobCard_ID = @jobCardId;

    END;
    ELSE
    BEGIN

        DECLARE @rowsCount INT = 7;

        DECLARE @returnedTable TABLE
        (
            RequestSparePartId INT NULL,
            SparePartName NVARCHAR(50) NULL,
            Quantity INT NULL
        );

        DECLARE @index INT = 0;

        WHILE (@index <= @rowsCount)
        BEGIN

            INSERT INTO @returnedTable
            (
                RequestSparePartId,
                SparePartName,
                Quantity
            )
            VALUES
            (   NULL, -- RequestSparePartId - int
                N'',  -- SparePartName - nvarchar(50)
                NULL  -- Quantity - int
                );

            SET @index += 1;

        END;

        SELECT RequestSparePartId,
               SparePartName,
               Quantity
        FROM @returnedTable;

    END;
END;
