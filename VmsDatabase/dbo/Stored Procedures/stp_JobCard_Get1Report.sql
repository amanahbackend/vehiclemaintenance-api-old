﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 27/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_Get1Report @jobCardId = 346;
-- EXEC dbo.stp_JobCard_Get1Report @vehicleId = 811, @jobCardId = 0;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_Get1Report]
(
    @jobCardId INT = 0,
    @vehicleId INT = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@jobCardId > 0)
    BEGIN

        SELECT @vehicleId = JobCard.FK_Vehicle_ID
        FROM dbo.JobCard
        WHERE Id = @jobCardId;

    END;

    DECLARE @vehiclePlateNo NVARCHAR(50);
    DECLARE @vehicleFleetNo NVARCHAR(50);
    DECLARE @vehicleType NVARCHAR(50);

    SELECT @vehiclePlateNo = Vehicle.VehicleNumberPlate,
           @vehicleFleetNo = Vehicle.FleetNumber,
           @vehicleType = VehicleType.[Name]
    FROM dbo.Vehicle
        INNER JOIN dbo.VehicleType
            ON VehicleType.Id = Vehicle.VehicleTypeId
    WHERE Vehicle.Id = @vehicleId;

    IF (@jobCardId > 0)
    BEGIN

        DECLARE @createdByUserId VARCHAR(36);
        SELECT @createdByUserId = JobCard.FK_User_ID
        FROM dbo.JobCard
        WHERE JobCard.Id = @jobCardId;

        DECLARE @requestedByUserName NVARCHAR(50);
        SELECT @requestedByUserName = FirstName + N' ' + LastName
        FROM [dbo].[AspNetUsers]
        WHERE Id = @createdByUserId;

        SELECT Id AS JobCardId,
               @vehicleId AS VehicleId,
               @vehiclePlateNo AS VehiclePlateNo,
               @vehicleFleetNo AS VehicleFleetNo,
               @vehicleType AS VehicleType,
               CreatedDate AS CreatedDate,
               Customer_Type AS CustomerType,
               @requestedByUserName AS RequestedByUserName,
               Date_In AS DateIn,
               Date_Out AS DateOut,
               Hourmeter,
               Odometer,
               Maintenance_Type AS MaintenanceType
        FROM dbo.JobCard
        WHERE Id = @jobCardId;

    END;
    ELSE
    BEGIN

        SELECT 0 AS JobCardId,
               @vehicleId AS VehicleId,
               @vehiclePlateNo AS VehiclePlateNo,
               @vehicleFleetNo AS VehicleFleetNo,
               @vehicleType AS VehicleType,
               SYSDATETIME() AS CreatedDate,
               N'CustomerType' AS CustomerType,
               'requestedByUserName' AS RequestedByUserName,
               SYSDATETIME() AS DateIn,
               SYSDATETIME() AS DateOut,
               0 AS Hourmeter,
               0 AS Odometer,
               N'Maintenance_Type' AS MaintenanceType;

    END;
END;
