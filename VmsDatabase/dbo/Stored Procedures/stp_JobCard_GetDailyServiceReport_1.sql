﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 28/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_GetDailyServiceReport;
-- EXEC dbo.stp_JobCard_GetDailyServiceReport @vehiclePlateNo=N'4-20657';
-- EXEC dbo.stp_JobCard_GetDailyServiceReport @startDate = '12/9/2018';
-- EXEC dbo.stp_JobCard_GetDailyServiceReport @startDate = '12/9/2018', @endDate = '12/9/2018';
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_GetDailyServiceReport]
(
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL,
    @vehiclePlateNo NVARCHAR(50) = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@startDate IS NOT NULL)
    BEGIN
        SELECT @startDate = [dbo].[GetDateWithFirstTime](@startDate);
    END;

    IF (@endDate IS NOT NULL)
    BEGIN
        SELECT @endDate = [dbo].[GetDateWithLastTime](@endDate);
    END;

    DECLARE @vehicleId INT = NULL;

    IF (@vehiclePlateNo IS NOT NULL)
    BEGIN
        
		SELECT @vehicleId = Vehicle.Id
        FROM dbo.Vehicle
        WHERE VehicleNumberPlate = @vehiclePlateNo
              AND IsDeleted = 0; -- Not deleted vehicle.

    END;

    SELECT ROW_NUMBER() OVER (PARTITION BY DailyServiceDesc ORDER BY JobCardId DESC) AS RowNumber,
           JobCardsTable.DailyServiceDesc,
           JobCardsTable.JobCardId,
           JobCardsTable.CreatedDate,
           JobCardsTable.VehicleId,
           JobCardsTable.VehiclePlateNo,
           JobCardsTable.VehicleFleetNo,
           JobCardsTable.CustomerType,
           JobCardsTable.ServiceType,
           JobCardsTable.DateIn,
           JobCardsTable.DateOut,
           JobCardsTable.DateInShiftId,
           JobCardsTable.DateOutShiftId,
           JobCardsTable.JobCardDescription,
           JobCardsTable.Comment
    FROM
    (
        SELECT CASE
                   WHEN JobCard.DateInShiftId = JobCard.DateOutShiftId THEN
                       'Daily Receipt'
                   ELSE
                       'Brought Forward'
               END AS DailyServiceDesc,
               JobCard.Id AS JobCardId,
               JobCard.CreatedDate,
               Vehicle.Id AS VehicleId,
               Vehicle.VehicleNumberPlate AS VehiclePlateNo,
               Vehicle.FleetNumber AS VehicleFleetNo,
               JobCard.Customer_Type AS CustomerType,
               ServiceType.ServiceTypeName_EN AS ServiceType,
               JobCard.Date_In AS DateIn,
               JobCard.Date_Out AS DateOut,
               JobCard.DateInShiftId,
               JobCard.DateOutShiftId,
               JobCard.[Desc] AS JobCardDescription,
               JobCard.Comment
        FROM dbo.JobCard
            INNER JOIN dbo.Vehicle
                ON Vehicle.Id = JobCard.FK_Vehicle_ID
            INNER JOIN dbo.ServiceType
                ON ServiceType.Id = JobCard.FK_ServiceType_ID
        WHERE (
                  Vehicle.Id = @vehicleId
                  OR @vehicleId IS NULL
              )
              AND
              (
                  JobCard.Date_In >= @startDate
                  OR @startDate IS NULL
              )
              AND
              (
                  JobCard.Date_In <= @endDate
                  OR @endDate IS NULL
              )
    ) AS JobCardsTable;

END;