﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 28/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_GetDailyServiceReport;
-- =============================================
CREATE PROCEDURE dbo.stp_JobCard_GetDailyServiceReport
(
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT ROW_NUMBER() OVER (ORDER BY JobCard.Id DESC) AS RowNumber,
           (CASE
                WHEN (ROW_NUMBER() OVER (ORDER BY JobCard.Id DESC)) < 10 THEN
                    1
                ELSE
                    2
            END
           ) AS ShiftId,
           JobCard.[Id],
           JobCard.[CreatedDate],
           Vehicle.VehicleNumberPlate AS VehiclePlateNo,
           Vehicle.FleetNumber AS VehicleFleetNo,
           JobCard.[Customer_Type] AS CustomerType,
           ServiceType.ServiceTypeName_EN AS ServiceType,
           JobCard.[Date_In],
           JobCard.[Date_Out],
           JobCard.[Desc],
           JobCard.[Comment]
    FROM [dbo].[JobCard]
        INNER JOIN dbo.Vehicle
            ON Vehicle.Id = JobCard.FK_Vehicle_ID
        INNER JOIN dbo.ServiceType
            ON ServiceType.Id = JobCard.FK_ServiceType_ID;
END;