﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 12/12/2018
-- Description:	
-- EXEC dbo.stp_JobCards_GetRepairRequests @jobCardId = 333;
-- EXEC dbo.stp_JobCards_GetRepairRequests @jobCardId = 0;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCards_GetRepairRequests]
(
	@jobCardId INT -- Pass 0 to get empty data for the blank job card report.
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@jobCardId <> 0)
    BEGIN

        SELECT ROW_NUMBER() OVER (ORDER BY RepairRequest.Id) AS RowNumber,
               RepairRequest.Id AS RepairRequestId,
               RepairRequest.FK_JobCard_ID AS JobCardId,
               ServiceType.ServiceTypeName_EN AS ServiceTypeName
        FROM dbo.RepairRequest
            INNER JOIN dbo.ServiceType
                ON ServiceType.Id = RepairRequest.FK_ServiceType_ID
        WHERE RepairRequest.FK_JobCard_ID = @jobCardId
              AND RepairRequest.IsDeleted = 0
              AND RepairRequest.FK_RepairRequestStatus_ID <> 1; -- Not "Needs Approval".
    END;
    ELSE
    BEGIN

        DECLARE @rowsCount INT = 5;

        DECLARE @returnedTable TABLE
        (
            RowNumber INT NOT NULL,
            RepairRequestId INT NULL,
            JobCardId INT NULL,
            ServiceName NVARCHAR(50) NULL
        );

        DECLARE @index INT = 1;

        WHILE (@index <= @rowsCount)
        BEGIN

            INSERT INTO @returnedTable
            (
                RowNumber,
                RepairRequestId,
                JobCardId,
                ServiceName
            )
            VALUES
            (   @index, -- RowNumber - int
                0,      -- RepairRequestId - int
                0,      -- JobCardId - int
                N''     -- ServiceName - nvarchar(50)
                );

            SET @index += 1;

        END;

        SELECT RowNumber,
               RepairRequestId,
               JobCardId,
               ServiceName
        FROM @returnedTable;

    END;

END;