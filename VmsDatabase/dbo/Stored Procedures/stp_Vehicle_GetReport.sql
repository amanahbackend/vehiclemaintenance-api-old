﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 25/11/2018
-- Description:	
-- =============================================
CREATE PROCEDURE stp_Vehicle_GetReport
(
    @vehicleId INT = NULL,
    @model NVARCHAR(50) = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT *
    FROM dbo.Vehicle
    WHERE (
              Id = @vehicleId
              OR @vehicleId IS NULL
          )
          AND
          (
              Model LIKE @model + '%'
              OR @model IS NULL
          );
END;
