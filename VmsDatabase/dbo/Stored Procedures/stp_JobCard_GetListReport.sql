﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 25/11/2018
-- Description:	
-- EXEC [dbo].[stp_JobCard_GetListReport];
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_GetListReport]
(
    @customerType NVARCHAR(50) = NULL,
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL,
    @serviceTypeId INT = NULL,
    @jobCardStatusId INT = NULL,
    @vehiclePlateNo NVARCHAR(50) = NULL,
    @vehicleFleetNo NVARCHAR(50) = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @vehicleId INT = NULL;

    IF (@vehiclePlateNo IS NOT NULL)
    BEGIN

        SELECT TOP (1)
               @vehicleId = Vehicle.Id
        FROM dbo.Vehicle
        WHERE VehicleNumberPlate = @vehiclePlateNo
        ORDER BY Id DESC;

    END;

    IF (@vehicleFleetNo IS NOT NULL)
    BEGIN
        SELECT TOP (1)
               @vehicleId = Vehicle.Id
        FROM dbo.Vehicle
        WHERE @vehicleFleetNo = @vehicleFleetNo
        ORDER BY Id DESC;
    END;

    SELECT JobCard.Id AS JobCardId,
           JobCard.Customer_Type AS CustomerType,
           JobCard.[Date_In] AS DateIn,
           JobCard.[Date_Out] AS DateOut,
           Vehicle.VehicleNumberPlate AS VehiclePlateNo,
           Vehicle.FleetNumber AS VehicleFleetNo,
           Driver.FirstName + ' ' + Driver.LastName AS DriverFullName,
           [Status].StatusName_EN AS JobCardStatus,
           ServiceType.ServiceTypeName_EN AS ServiceTypeName
    FROM [dbo].[JobCard]
        INNER JOIN dbo.Vehicle
            ON Vehicle.Id = JobCard.FK_Vehicle_ID
        INNER JOIN dbo.Driver
            ON JobCard.FK_Driver_ID = Driver.Id
        INNER JOIN dbo.[Status]
            ON [Status].Id = JobCard.FK_Status_ID
        INNER JOIN dbo.ServiceType
            ON ServiceType.Id = JobCard.FK_ServiceType_ID
    WHERE (
              JobCard.FK_Vehicle_ID = @vehicleId
              OR @vehicleId IS NULL
          )
          AND
          (
              JobCard.Customer_Type = @customerType
              OR @customerType IS NULL
          )
          AND
          (
              JobCard.Date_In >= @startDate
              OR @startDate IS NULL
          )
          AND
          (
              JobCard.Date_Out <= @endDate
              OR @endDate IS NULL
          )
          AND
          (
              JobCard.FK_ServiceType_ID = @serviceTypeId
              OR @serviceTypeId IS NULL
          )
          AND
          (
              JobCard.FK_Status_ID = @jobCardStatusId
              OR @jobCardStatusId IS NULL
          );

END;
