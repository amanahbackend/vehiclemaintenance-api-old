﻿CREATE TABLE [dbo].[ServiceType] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]        DATETIME2 (7)  NOT NULL,
    [DeletedDate]        DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]    NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]    NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]          BIT            NOT NULL,
    [Maintenance_Type]   BIT            DEFAULT ((0)) NOT NULL,
    [Need_Approve]       BIT            DEFAULT ((0)) NOT NULL,
    [Period]             INT            NOT NULL,
    [ServiceTypeName_AR] NVARCHAR (100) NOT NULL,
    [ServiceTypeName_EN] NVARCHAR (100) NOT NULL,
    [UpdatedDate]        DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_ServiceType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

