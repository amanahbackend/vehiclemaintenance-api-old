﻿CREATE TABLE [dbo].[Shift] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (50)  NULL,
    [StartTime]       TIME (7)       NULL,
    [EndTime]         TIME (7)       NULL,
    [FK_CreatedBy_Id] NVARCHAR (MAX) NULL,
    [CreatedDate]     DATETIME2 (7)  NULL,
    [FK_UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedDate]     DATETIME2 (7)  NULL,
    [FK_DeletedBy_Id] NVARCHAR (MAX) NULL,
    [IsDeleted]       BIT            CONSTRAINT [DF_Shift_IsDeleted] DEFAULT ((0)) NULL,
    [DeletedDate]     DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED ([Id] ASC)
);

