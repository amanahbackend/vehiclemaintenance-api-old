﻿CREATE TABLE [dbo].[VehicleType] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]     DATETIME2 (7)  NOT NULL,
    [CurrentUserId]   NVARCHAR (MAX) NULL,
    [DeletedDate]     DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id] NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id] NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [IsDeleted]       BIT            NOT NULL,
    [Name]            NVARCHAR (100) NULL,
    [UpdatedDate]     DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

