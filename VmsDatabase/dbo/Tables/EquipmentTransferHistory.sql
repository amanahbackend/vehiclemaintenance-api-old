﻿CREATE TABLE [dbo].[EquipmentTransferHistory] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]         DATETIME2 (7)  NOT NULL,
    [DeletedDate]         DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_RepairRequest_ID] INT            NULL,
    [FK_UpdatedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_User_ID]          INT            NULL,
    [IsDeleted]           BIT            NOT NULL,
    [UpdatedDate]         DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_EquipmentTransferHistory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

