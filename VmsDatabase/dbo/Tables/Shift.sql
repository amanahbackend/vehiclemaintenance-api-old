﻿CREATE TABLE [dbo].[Shift] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [ShiftName] NVARCHAR (50) NULL,
    [StartTime] TIME (7)      NULL,
    [EndTime]   TIME (7)      NULL,
    CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED ([Id] ASC)
);

