﻿CREATE TABLE [dbo].[Status] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]     DATETIME2 (7)  NOT NULL,
    [DeletedDate]     DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id] NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id] NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [IsDeleted]       BIT            NOT NULL,
    [StatusName_AR]   NVARCHAR (50)  NOT NULL,
    [StatusName_EN]   NVARCHAR (50)  NOT NULL,
    [UpdatedDate]     DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED ([Id] ASC)
);

