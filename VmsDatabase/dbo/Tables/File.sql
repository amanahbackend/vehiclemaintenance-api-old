﻿CREATE TABLE [dbo].[File] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]         DATETIME2 (7)  NOT NULL,
    [DeletedDate]         DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_JobCard_ID]       INT            NULL,
    [FK_RepairRequest_ID] INT            NULL,
    [FK_UpdatedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_Vendor_ID]        INT            NOT NULL,
    [FileName]            NVARCHAR (50)  NULL,
    [FilePath]            NVARCHAR (100) NULL,
    [FileURL]             NVARCHAR (100) NULL,
    [IsDeleted]           BIT            NOT NULL,
    [Selected]            BIT            DEFAULT ((0)) NOT NULL,
    [UpdatedDate]         DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED ([Id] ASC)
);

