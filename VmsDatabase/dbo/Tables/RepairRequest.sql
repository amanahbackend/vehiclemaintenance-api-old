﻿CREATE TABLE [dbo].[RepairRequest] (
    [Id]                        INT            IDENTITY (1, 1) NOT NULL,
    [FK_JobCard_ID]             INT            NULL,
    [Maintenance_Type]          BIT            CONSTRAINT [DF__RepairReq__Maint__52593CB8] DEFAULT ((0)) NOT NULL,
    [Cost]                      FLOAT (53)     NOT NULL,
    [Duration]                  FLOAT (53)     NOT NULL,
    [External]                  BIT            CONSTRAINT [DF__RepairReq__Exter__5165187F] DEFAULT ((0)) NOT NULL,
    [FK_RepairRequestStatus_ID] INT            NULL,
    [FK_RepairRequestType_ID]   INT            NULL,
    [FK_ServiceType_ID]         INT            NULL,
    [FK_User_ID]                NVARCHAR (50)  NULL,
    [Comment]                   NVARCHAR (200) NULL,
    [CreatedDate]               DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]           NVARCHAR (MAX) NULL,
    [UpdatedDate]               DATETIME2 (7)  NOT NULL,
    [FK_UpdatedBy_Id]           NVARCHAR (MAX) NULL,
    [IsDeleted]                 BIT            NOT NULL,
    [FK_DeletedBy_Id]           NVARCHAR (MAX) NULL,
    [DeletedDate]               DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_RepairRequest] PRIMARY KEY CLUSTERED ([Id] ASC)
);





