﻿CREATE TABLE [dbo].[RepairRequestTechnician] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]         DATETIME2 (7)  NOT NULL,
    [DeletedDate]         DATETIME2 (7)  NOT NULL,
    [EndTime]             DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]     NVARCHAR (MAX) NULL,
    [FK_RepairRequest_ID] INT            NULL,
    [FK_Technician_ID]    INT            NULL,
    [FK_UpdatedBy_Id]     NVARCHAR (MAX) NULL,
    [IsDeleted]           BIT            NOT NULL,
    [Removed]             BIT            DEFAULT ((0)) NOT NULL,
    [StartTime]           DATETIME2 (7)  NOT NULL,
    [UpdatedDate]         DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_RepairRequestTechnician] PRIMARY KEY CLUSTERED ([Id] ASC)
);

