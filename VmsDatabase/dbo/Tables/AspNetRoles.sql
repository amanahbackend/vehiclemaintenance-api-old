﻿CREATE TABLE [dbo].[AspNetRoles] (
    [Id]               NVARCHAR (450) NOT NULL,
    [ConcurrencyStamp] NVARCHAR (MAX) NULL,
    [CreatedDate]      DATETIME2 (7)  NOT NULL,
    [DeletedDate]      DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]  NVARCHAR (50)  NULL,
    [FK_DeletedBy_Id]  NVARCHAR (50)  NULL,
    [FK_UpdatedBy_Id]  NVARCHAR (50)  NULL,
    [IsDeleted]        BIT            NOT NULL,
    [Name]             NVARCHAR (256) NULL,
    [NormalizedName]   NVARCHAR (256) NULL,
    [UpdatedDate]      DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[AspNetRoles]([NormalizedName] ASC) WHERE ([NormalizedName] IS NOT NULL);

