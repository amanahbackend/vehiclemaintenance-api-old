﻿CREATE TABLE [dbo].[SparePartsRequest] (
    [Id]                  INT             IDENTITY (1, 1) NOT NULL,
    [Amount]              DECIMAL (18, 2) NULL,
    [Comments]            NVARCHAR (200)  NULL,
    [CreatedDate]         DATETIME2 (7)   NOT NULL,
    [DeletedDate]         DATETIME2 (7)   NOT NULL,
    [EndDate]             DATETIME2 (7)   NOT NULL,
    [FK_CreatedBy_Id]     NVARCHAR (MAX)  NULL,
    [FK_DeletedBy_Id]     NVARCHAR (MAX)  NULL,
    [FK_Inventory_ID]     INT             NULL,
    [FK_RepairRequest_ID] INT             NULL,
    [FK_Status_ID]        INT             NULL,
    [FK_Technician_ID]    INT             NULL,
    [FK_UpdatedBy_Id]     NVARCHAR (MAX)  NULL,
    [IsDeleted]           BIT             NOT NULL,
    [Source]              NVARCHAR (100)  NULL,
    [StartDate]           DATETIME2 (7)   NOT NULL,
    [UpdatedDate]         DATETIME2 (7)   NOT NULL,
    [Quantity]            INT             NULL,
    CONSTRAINT [PK_SparePartsRequest] PRIMARY KEY CLUSTERED ([Id] ASC)
);

