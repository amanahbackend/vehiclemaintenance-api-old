﻿CREATE TABLE [dbo].[Technician] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Address]         NVARCHAR (200) NOT NULL,
    [Available]       BIT            DEFAULT ((0)) NOT NULL,
    [CreatedDate]     DATETIME2 (7)  NOT NULL,
    [DeletedDate]     DATETIME2 (7)  NOT NULL,
    [Employe_Id]      NVARCHAR (50)  NOT NULL,
    [FK_CreatedBy_Id] NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id] NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [FirstName]       NVARCHAR (50)  NOT NULL,
    [HourRating]      FLOAT (53)     NOT NULL,
    [IsDeleted]       BIT            NOT NULL,
    [LastName]        NVARCHAR (50)  NOT NULL,
    [PhoneNumber]     NVARCHAR (50)  NOT NULL,
    [Specialty]       NVARCHAR (50)  NOT NULL,
    [UpdatedDate]     DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Technician] PRIMARY KEY CLUSTERED ([Id] ASC)
);

