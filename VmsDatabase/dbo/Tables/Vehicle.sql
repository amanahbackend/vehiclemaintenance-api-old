﻿CREATE TABLE [dbo].[Vehicle] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [VehicleNumberPlate] NVARCHAR (50)  NULL,
    [FleetNumber]        NVARCHAR (50)  NOT NULL,
    [Model]              NVARCHAR (100) NOT NULL,
    [Make]               NVARCHAR (100) NULL,
    [VehicleTypeId]      INT            NULL,
    [ChassisNumber]      NVARCHAR (50)  NULL,
    [Odometer]           FLOAT (53)     NULL,
    [KiloMeters]         NVARCHAR (50)  NULL,
    [VIN]                NVARCHAR (50)  NULL,
    [PurchaseDate]       DATETIME2 (7)  NULL,
    [Year]               NVARCHAR (20)  NULL,
    [Color]              NVARCHAR (50)  NULL,
    [FuelConsumption]    INT            NULL,
    [ToolDescription]    NVARCHAR (500) NULL,
    [WorkIdleTime]       NVARCHAR (50)  NULL,
    [Status]             NVARCHAR (50)  NULL,
    [Remarks]            NVARCHAR (500) NULL,
    [CreatedDate]        DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]    NVARCHAR (MAX) NULL,
    [UpdatedDate]        DATETIME2 (7)  NOT NULL,
    [FK_UpdatedBy_Id]    NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]          BIT            NOT NULL,
    [DeletedDate]        DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle_VehicleType_VehicleTypeId] FOREIGN KEY ([VehicleTypeId]) REFERENCES [dbo].[VehicleType] ([Id])
);






GO
CREATE NONCLUSTERED INDEX [IX_Vehicle_VehicleTypeId]
    ON [dbo].[Vehicle]([VehicleTypeId] ASC);

