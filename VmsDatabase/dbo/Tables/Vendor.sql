﻿CREATE TABLE [dbo].[Vendor] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Address]         NVARCHAR (MAX) NULL,
    [CreatedDate]     DATETIME2 (7)  NOT NULL,
    [DeletedDate]     DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id] NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id] NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [IsDeleted]       BIT            NOT NULL,
    [Name]            NVARCHAR (100) NOT NULL,
    [UpdatedDate]     DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED ([Id] ASC)
);

