﻿CREATE TABLE [dbo].[UsedMaterials] (
    [Id]                       INT            IDENTITY (1, 1) NOT NULL,
    [Amount]                   INT            NOT NULL,
    [Capacity]                 NVARCHAR (50)  NULL,
    [CreatedDate]              DATETIME2 (7)  NOT NULL,
    [DeletedDate]              DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]          NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]          NVARCHAR (MAX) NULL,
    [FK_Inventory_ID]          INT            NULL,
    [FK_JobCard_ID]            INT            NULL,
    [FK_RepairRequest_ID]      INT            NULL,
    [FK_SparePartStatus_ID]    INT            NULL,
    [FK_Technician_ID]         INT            NULL,
    [FK_TranferFromVehicle_Id] INT            NULL,
    [FK_UpdatedBy_Id]          NVARCHAR (MAX) NULL,
    [FleetNumber]              NVARCHAR (50)  NULL,
    [IsDeleted]                BIT            NOT NULL,
    [UpdatedDate]              DATETIME2 (7)  NOT NULL,
    [cost]                     FLOAT (53)     DEFAULT ((2.0000000000000000e+001)) NOT NULL,
    CONSTRAINT [PK_UsedMaterials] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UsedMaterials_Vehicle_FK_TranferFromVehicle_Id] FOREIGN KEY ([FK_TranferFromVehicle_Id]) REFERENCES [dbo].[Vehicle] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_UsedMaterials_FK_TranferFromVehicle_Id]
    ON [dbo].[UsedMaterials]([FK_TranferFromVehicle_Id] ASC);

