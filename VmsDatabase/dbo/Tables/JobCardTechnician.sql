﻿CREATE TABLE [dbo].[JobCardTechnician] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [CreatedDate]      DATETIME2 (7)  NOT NULL,
    [DeletedDate]      DATETIME2 (7)  NOT NULL,
    [EndTime]          DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]  NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]  NVARCHAR (MAX) NULL,
    [FK_JobCard_ID]    INT            NULL,
    [FK_Technician_ID] INT            NULL,
    [FK_UpdatedBy_Id]  NVARCHAR (MAX) NULL,
    [IsDeleted]        BIT            NOT NULL,
    [StartTime]        DATETIME2 (7)  NOT NULL,
    [UpdatedDate]      DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_JobCardTechnician] PRIMARY KEY CLUSTERED ([Id] ASC)
);

