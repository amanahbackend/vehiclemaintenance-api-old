﻿CREATE TABLE [dbo].[JobCard] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Comment]           NVARCHAR (200) NULL,
    [CreatedDate]       DATETIME2 (7)  NOT NULL,
    [Customer_Type]     NVARCHAR (100) NOT NULL,
    [Date_In]           DATETIME2 (7)  NOT NULL,
    [Date_Out]          DATETIME2 (7)  NOT NULL,
    [DeletedDate]       DATETIME2 (7)  NOT NULL,
    [Desc]              NVARCHAR (200) NULL,
    [FK_CreatedBy_Id]   NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id]   NVARCHAR (MAX) NULL,
    [FK_Driver_ID]      INT            NULL,
    [FK_ServiceType_ID] INT            NULL,
    [FK_Status_ID]      INT            NULL,
    [FK_UpdatedBy_Id]   NVARCHAR (MAX) NULL,
    [FK_User_ID]        NVARCHAR (50)  NULL,
    [FK_Vehicle_ID]     INT            NOT NULL,
    [Hourmeter]         FLOAT (53)     NULL,
    [IsDeleted]         BIT            NOT NULL,
    [Maintenance_Type]  BIT            DEFAULT ((0)) NOT NULL,
    [Odometer]          FLOAT (53)     NULL,
    [UpdatedDate]       DATETIME2 (7)  NOT NULL,
    [DateInShiftId]     INT            NULL,
    [DateOutShiftId]    INT            NULL,
    CONSTRAINT [PK_JobCard] PRIMARY KEY CLUSTERED ([Id] ASC)
);



