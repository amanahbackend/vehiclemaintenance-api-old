﻿CREATE TABLE [dbo].[Driver] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]          NVARCHAR (50)  NOT NULL,
    [LastName]           NVARCHAR (50)  NOT NULL,
    [Title]              NVARCHAR (100) NOT NULL,
    [Employe_Id]         NVARCHAR (50)  NOT NULL,
    [DateOfBirth]        NVARCHAR (50)  NOT NULL,
    [Gender]             NVARCHAR (20)  NULL,
    [Active]             BIT            NOT NULL,
    [PhoneNumber]        NVARCHAR (50)  NOT NULL,
    [Address]            NVARCHAR (200) NOT NULL,
    [SSN]                NVARCHAR (50)  NOT NULL,
    [LicenseNumber]      NVARCHAR (50)  NOT NULL,
    [LicenseIssuedDate]  DATETIME2 (7)  NOT NULL,
    [LicenseIssuedState] NVARCHAR (50)  NOT NULL,
    [MaritalStatus]      NVARCHAR (50)  NULL,
    [CreatedDate]        DATETIME2 (7)  NOT NULL,
    [FK_CreatedBy_Id]    NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id]    NVARCHAR (MAX) NULL,
    [UpdatedDate]        DATETIME2 (7)  NOT NULL,
    [FK_DeletedBy_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]          BIT            NOT NULL,
    [DeletedDate]        DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Driver] PRIMARY KEY CLUSTERED ([Id] ASC)
);



