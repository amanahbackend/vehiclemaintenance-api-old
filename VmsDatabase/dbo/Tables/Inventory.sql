﻿CREATE TABLE [dbo].[Inventory] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Case_AR]         NVARCHAR (100) NOT NULL,
    [Case_EN]         NVARCHAR (100) NOT NULL,
    [Cost]            INT            DEFAULT ((10)) NOT NULL,
    [CreatedDate]     DATETIME2 (7)  NOT NULL,
    [DeletedDate]     DATETIME2 (7)  NOT NULL,
    [Description]     NVARCHAR (200) NULL,
    [FK_CreatedBy_Id] NVARCHAR (MAX) NULL,
    [FK_DeletedBy_Id] NVARCHAR (MAX) NULL,
    [FK_UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [IsDeleted]       BIT            NOT NULL,
    [Make]            NVARCHAR (50)  NOT NULL,
    [Model]           NVARCHAR (50)  NOT NULL,
    [Name_EN]         NVARCHAR (100) NOT NULL,
    [Name_ER]         NVARCHAR (100) NOT NULL,
    [SerialNumber]    NVARCHAR (50)  NOT NULL,
    [Unit]            NVARCHAR (50)  NULL,
    [UpdatedDate]     DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

