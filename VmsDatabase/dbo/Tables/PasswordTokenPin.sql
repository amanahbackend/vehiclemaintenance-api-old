﻿CREATE TABLE [dbo].[PasswordTokenPin] (
    [Id]    INT            IDENTITY (1, 1) NOT NULL,
    [Pin]   NVARCHAR (50)  NOT NULL,
    [Token] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_PasswordTokenPin] PRIMARY KEY CLUSTERED ([Id] ASC)
);

