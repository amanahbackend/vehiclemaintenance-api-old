﻿CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (450)     NOT NULL,
    [AccessFailedCount]    INT                NOT NULL,
    [ConcurrencyStamp]     NVARCHAR (MAX)     NULL,
    [CreatedDate]          DATETIME2 (7)      NOT NULL,
    [Deactivated]          BIT                NOT NULL,
    [DeletedDate]          DATETIME2 (7)      NOT NULL,
    [Email]                NVARCHAR (256)     NULL,
    [EmailConfirmed]       BIT                NOT NULL,
    [FK_CreatedBy_Id]      NVARCHAR (MAX)     NULL,
    [FK_DeletedBy_Id]      NVARCHAR (MAX)     NULL,
    [FK_UpdatedBy_Id]      NVARCHAR (MAX)     NULL,
    [FirstName]            NVARCHAR (MAX)     NOT NULL,
    [IsDeleted]            BIT                NOT NULL,
    [LastName]             NVARCHAR (MAX)     NOT NULL,
    [LockoutEnabled]       BIT                NOT NULL,
    [LockoutEnd]           DATETIMEOFFSET (7) NULL,
    [NormalizedEmail]      NVARCHAR (256)     NULL,
    [NormalizedUserName]   NVARCHAR (256)     NULL,
    [PasswordHash]         NVARCHAR (MAX)     NULL,
    [Phone1]               NVARCHAR (MAX)     NOT NULL,
    [Phone2]               NVARCHAR (MAX)     NULL,
    [PhoneNumber]          NVARCHAR (MAX)     NULL,
    [PhoneNumberConfirmed] BIT                NOT NULL,
    [PicturePath]          NVARCHAR (MAX)     NULL,
    [SecurityStamp]        NVARCHAR (MAX)     NULL,
    [TwoFactorEnabled]     BIT                NOT NULL,
    [UpdatedDate]          DATETIME2 (7)      NOT NULL,
    [UserName]             NVARCHAR (256)     NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [EmailIndex]
    ON [dbo].[AspNetUsers]([NormalizedEmail] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AspNetUsers]([NormalizedUserName] ASC) WHERE ([NormalizedUserName] IS NOT NULL);

