﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 07/08/2017
-- Description:	
-- SELECT dbo.GetDateWithFirstTime ('8/25/2017');
-- =============================================
CREATE FUNCTION [dbo].[GetDateWithFirstTime]
(
    @date DATETIME
)
RETURNS DATETIME
AS
BEGIN

    DECLARE @dateWzFirstTime DATETIME;

    SET @dateWzFirstTime = CAST(CONVERT(VARCHAR(10), @date, 110) AS DATETIME);

    RETURN @dateWzFirstTime;

END;