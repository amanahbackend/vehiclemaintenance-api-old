﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 07/08/2017
-- Description:	
-- SELECT dbo.GetDateWithLastTime ('8/25/2017');
-- =============================================
CREATE FUNCTION [dbo].[GetDateWithLastTime]
(
    @date DATETIME
)
RETURNS DATETIME
AS
BEGIN

    SET @date = DATEADD(DAY, 1, @date);

    SELECT @date = [dbo].[GetDateWithFirstTime](@date);

    RETURN DATEADD(ms, -3, @date);

END;