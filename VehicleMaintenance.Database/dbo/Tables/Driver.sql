﻿CREATE TABLE [dbo].[Driver] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [Title]              NVARCHAR (50)  NULL,
    [FirstName]          NVARCHAR (50)  NULL,
    [LastName]           NVARCHAR (50)  NULL,
    [PhoneNumber]        NVARCHAR (20)  NULL,
    [Address]            NVARCHAR (200) NULL,
    [BirthDate]          DATETIME2 (7)  NULL,
    [Ssn]                NVARCHAR (50)  NULL,
    [LicenseIssuedDate]  DATETIME2 (7)  NULL,
    [LicenseIssuedState] NVARCHAR (50)  NULL,
    [LicenseNumber]      NVARCHAR (20)  NULL,
    [Gender]             NVARCHAR (10)  NULL,
    [MaritalStatus]      NVARCHAR (20)  NULL,
    [EmployeeId]         NVARCHAR (36)  NULL,
    [RowStatusId]        INT            NULL,
    [CreatedByUserId]    NVARCHAR (36)  NULL,
    [CreationDate]       DATETIME2 (7)  NULL,
    [ModifiedByUserId]   NVARCHAR (36)  NULL,
    [ModificationDate]   DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Driver] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Driver_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Driver_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [IX_Driver_ModifiedByUserId]
    ON [dbo].[Driver]([ModifiedByUserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Driver_CreatedByUserId]
    ON [dbo].[Driver]([CreatedByUserId] ASC);

