﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 
-- Description:	
-- EXEC dbo.stp_RepairRequestSparePart_GetTechnicianWorkReport;
-- EXEC dbo.stp_RepairRequestSparePart_GetTechnicianWorkReport @technicianId = 3;
-- =============================================
CREATE PROCEDURE [dbo].[stp_RepairRequestSparePart_GetTechnicianWorkReport]
(
    @technicianId INT = NULL,
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@technicianId = 0)
    BEGIN
        SET @technicianId = NULL;
    END;

    IF (@startDate IS NOT NULL)
    BEGIN
        SELECT @startDate = [dbo].[GetDateWithFirstTime](@startDate);
    END;

    IF (@endDate IS NOT NULL)
    BEGIN
        SELECT @endDate = [dbo].[GetDateWithLastTime](@endDate);
    END;

    SELECT RRSP.Id AS RRSPId,
           RRSP.RepairRequestId,
           RRSP.TechnicianId,
           Technician.EmployeeId AS TechEmployeeId,
           Technician.LastName + N' ' + Technician.LastName AS FullName,
           CONVERT(DATE, RRSP.TechnicianStartDate) AS [Date],
           RRSP.TechnicianStartDate,
           RRSP.TechnicianEndDate,
           DATEDIFF(HOUR, RRSP.TechnicianStartDate, RRSP.TechnicianEndDate) AS TechnicianWorkHours,
           @startDate AS StartDateParameter,
           @endDate AS EndDateParameter
    FROM dbo.RepairRequestSparePart AS RRSP
        INNER JOIN dbo.Technician
            ON Technician.Id = RRSP.TechnicianId
    WHERE RRSP.RowStatusId = 1
          AND
          (
              RRSP.TechnicianId = @technicianId
              OR @technicianId IS NULL
          )
          AND
          (
              RRSP.TechnicianStartDate >= @startDate
              OR @startDate IS NULL
          )
          AND
          (
              RRSP.TechnicianEndDate <= @endDate
              OR @endDate IS NULL
          )
		  ORDER BY [Date] DESC;
END;