USE [VMSDatabaseDev2]
GO
SET IDENTITY_INSERT [dbo].[VehicleType] ON 

INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'Tool', 1, NULL, CAST(N'2018-12-24T13:59:02.0433134' AS DateTime2), NULL, NULL)
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'Truck', 1, NULL, CAST(N'2018-12-24T13:59:02.0433134' AS DateTime2), NULL, NULL)
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'Car', 1, NULL, CAST(N'2018-12-24T13:59:02.0433134' AS DateTime2), NULL, NULL)
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (4, N'555uuu', -1, NULL, CAST(N'2018-12-30T13:56:49.5862634' AS DateTime2), NULL, CAST(N'2018-12-30T14:05:02.3775124' AS DateTime2))
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (5, N'yyyee', -1, NULL, CAST(N'2018-12-30T14:06:06.3217963' AS DateTime2), NULL, CAST(N'2018-12-30T14:09:49.4541900' AS DateTime2))
SET IDENTITY_INSERT [dbo].[VehicleType] OFF
