USE [VMSDatabaseDev2]
GO
SET IDENTITY_INSERT [dbo].[Vendor] ON 

INSERT [dbo].[Vendor] ([Id], [Name], [Address], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'Foad Foad', N'Cairo, Egypt', 1, N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T16:41:59.7549463' AS DateTime2), N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T18:02:40.6528234' AS DateTime2))
INSERT [dbo].[Vendor] ([Id], [Name], [Address], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'Khalil Khalil', N'Cairo, Egypt.', 1, N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T16:41:59.7549463' AS DateTime2), N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T18:02:47.5062838' AS DateTime2))
INSERT [dbo].[Vendor] ([Id], [Name], [Address], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'Nader', N'Cairo, Egypt.', 1, N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T17:01:19.2471058' AS DateTime2), N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T17:09:46.3799944' AS DateTime2))
INSERT [dbo].[Vendor] ([Id], [Name], [Address], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (4, N'Mostafa', N'Cairo, Egypt.', 1, N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T17:08:21.5441878' AS DateTime2), N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T17:09:27.3094732' AS DateTime2))
INSERT [dbo].[Vendor] ([Id], [Name], [Address], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (5, N'Mohammed', N'Cairo, Egypt.', 1, N'3a1bcaab-4d5d-43ee-b2be-a57953ee4ea7', CAST(N'2019-01-21T17:09:03.4200613' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Vendor] OFF
