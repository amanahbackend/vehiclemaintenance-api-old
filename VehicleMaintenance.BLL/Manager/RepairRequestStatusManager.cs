﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.Manager
{
   public class RepairRequestStatusManager : Repository<RepairRequestStatus>, IRepairRequestStatusManager
    {
        public RepairRequestStatusManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
    }
}
