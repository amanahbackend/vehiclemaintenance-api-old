﻿using System;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
    public class StatusManager : Repository<Status>, IStatusManager
    {
        public StatusManager(VehicleMaintenanceDbContext context)
         : base(context)
        {
        }
        public ProcessResult<Status> GetByNameEN(string nameEN)
        {
            var status = GetAllQuerable().Data.FirstOrDefault(r => r.StatusName_EN.ToLower() == nameEN.ToLower());

            if (status != null)
            {
                return ProcessResultHelper.Succedded(status);
            }

            return ProcessResultHelper.Failed(status, new Exception($"No status with NameEN {nameEN}"));
        }
    }
}
