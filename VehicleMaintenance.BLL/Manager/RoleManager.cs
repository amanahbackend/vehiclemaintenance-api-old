﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace Dispatching.Identity.BLL.Managers
{
    public class RoleManager : IRoleManager
    {
        RoleManager<Role> _identityRoleManager;

        public RoleManager(RoleManager<Role> identityRoleManager)
        {
            _identityRoleManager = identityRoleManager;
        }

        public async Task<Role> GetByName(string roleName)
        {
            var role = await _identityRoleManager.FindByNameAsync(roleName);
            return role;
        }

        public async Task<Role> AddRoleAsync(Role Role)
        {
            IdentityResult result = await _identityRoleManager.CreateAsync(Role);
            if (result.Succeeded)
            {
                return Role;
            }
            else
            {
                return null;
            }
        }

        public async Task AddRolesAsync(List<Role> Roles)
        {
            foreach (var role in Roles)
            {
                await AddRoleAsync(role);
            }
        }

        public async Task<bool> Delete(string roleName)
        {
            var role = await _identityRoleManager.FindByNameAsync(roleName);
            IdentityResult identityResult = await _identityRoleManager.DeleteAsync(role);
            return identityResult.Succeeded;
        }

        public async Task<Role> Update(Role role)
        {
            var dbRole = await _identityRoleManager.FindByIdAsync(role.Id);
            dbRole.Name = role.Name;
            IdentityResult identityResult = await _identityRoleManager.UpdateAsync(dbRole);
            return identityResult.Succeeded ? dbRole : null;
        }

        public List<Role> GetAll()
        {
            var roles = _identityRoleManager.Roles.ToList();
            return roles;
        }
    }
}
