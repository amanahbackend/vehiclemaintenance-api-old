﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.Manager
{
   public class RepairRequest_TechnicianManager : Repository<RepairRequestTechnician>, IRepairRequest_TechnicianManager
    {
        public RepairRequest_TechnicianManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
        public ProcessResult<List<RepairRequestTechnician>> GetByRepairRequestID(int repairrequestId)
        {
            List<RepairRequestTechnician> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairrequestId).ToList();

                return ProcessResultHelper.Succedded<List<RepairRequestTechnician>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByRepairRequestID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequestTechnician>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByRepairRequestID");
            }
        }

        public ProcessResult<List<RepairRequestTechnician>> GetRepairRequestsByTechniciansAndDate(int? TechnicianId, DateTime? startDate, DateTime? endDate)
        {
            List<RepairRequestTechnician> input = null;
            try
            {
                if (TechnicianId != null && startDate==null && endDate==null)
                {
                    input = GetAllQuerable().Data.Where(x => x.FK_Technician_ID == TechnicianId ).ToList();
                }
                else if (TechnicianId != null && startDate != null && endDate == null)
                {
                    input = GetAllQuerable().Data.Where(x => x.FK_Technician_ID == TechnicianId && x.StartTime >= startDate).ToList();
                }
                else if (TechnicianId != null && startDate == null && endDate != null)
                {
                    input = GetAllQuerable().Data.Where(x => x.FK_Technician_ID == TechnicianId && x.EndTime <= endDate).ToList();
                }
                else if (TechnicianId != null && startDate != null && endDate != null)
                {
                    input = GetAllQuerable().Data.Where(x => x.FK_Technician_ID == TechnicianId && x.StartTime >= startDate && x.EndTime <= endDate).ToList();
                }
                else if (TechnicianId == null && startDate != null && endDate == null)
                {
                    input = GetAllQuerable().Data.Where(x=>x.StartTime >= startDate).ToList();
                }
                else if (TechnicianId == null && startDate != null && endDate!= null)
                {
                    input = GetAllQuerable().Data.Where(x=>x.StartTime >= startDate && x.EndTime <= endDate).ToList();
                }
                else if (TechnicianId == null && startDate == null && endDate != null)
                {
                    input = GetAllQuerable().Data.Where(x => x.EndTime <= endDate).ToList();
                }
                return ProcessResultHelper.Succedded<List<RepairRequestTechnician>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetRepairRequestsByTechniciansAndDate");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequestTechnician>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetRepairRequestsByTechniciansAndDate");
            }
        }

    }
}
