﻿using System.Linq;
using DispatchProduct.RepositoryModule;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
    public class SparePartStatusManager : Repository<SparePartStatus>, ISparePartStatusManager
    {
        public SparePartStatusManager(VehicleMaintenanceDbContext context)
         : base(context)
        {
        }

        public SparePartStatus GetByName(string name)
        {
            var status = GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.Equals(name));
            return status;
        }
    }
}
