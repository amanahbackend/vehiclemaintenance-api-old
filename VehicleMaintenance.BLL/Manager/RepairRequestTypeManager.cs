﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.Manager
{
    public class RepairRequestTypeManager: Repository<RepairRequestType>, IRepairRequestTypeManager
    {
        public RepairRequestTypeManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
    }
}
