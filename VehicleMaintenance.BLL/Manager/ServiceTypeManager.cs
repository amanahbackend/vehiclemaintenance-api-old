﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.Models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
    public class ServiceTypeManager : Repository<ServiceType>, IServiceTypeManager
    {
        public ServiceTypeManager(VehicleMaintenanceDbContext context)
            : base(context)
        {
        }

        public ProcessResult<List<ServiceType>> GetByMaintenanceType(bool type)
        {
            List<ServiceType> result = null;

            try
            {
                result = GetAllQuerable().Data.Where(x => x.Maintenance_Type == type).ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }
    }
}
