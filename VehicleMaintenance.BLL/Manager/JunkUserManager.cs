using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.BLL.IManager;

namespace Dispatching.Identity.BLL.Managers
{
    public class JunkUserManager : IdentityRepository<JunkUser>, IJunkUserManager
    {
        private VehicleMaintenanceDbContext _context;

        public JunkUserManager(VehicleMaintenanceDbContext context) : base(context)
        {
            _context = context;
        }


        public override ProcessResult<bool> Delete(JunkUser entity)
        {
            try
            {
                if (entity != null)
                {
                    _context.JunkUsers.Remove(entity);
                    var result = _context.SaveChanges() > 0;
                    return ProcessResultHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultHelper.Failed(false, new ArgumentNullException(), "Argument is null");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }

        public override ProcessResult<bool> Delete(List<JunkUser> entitylst)
        {
            try
            {
                var data = false;
                foreach (var item in entitylst)
                {
                    data = Delete(item).Data;
                }
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }

        public override ProcessResult<bool> DeleteById(params object[] id)
        {
            try
            {
                var entity = _context.JunkUsers.Find(id);
                return Delete(entity);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }

        public ProcessResult<bool> DeleteByUsername(string username)
        {
            try
            {
                var user = _context.JunkUsers.FirstOrDefault(x => x.UserName.Equals(username));
                return Delete(user);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }

        public ProcessResult<JunkUser> GetJunkUser(User applicationUser)
        {
            try
            {
                JunkUser junkUser = new JunkUser()
                {
                    AccessFailedCount = applicationUser.AccessFailedCount,
                    ConcurrencyStamp = applicationUser.ConcurrencyStamp,
                    CreatedDate = applicationUser.CreatedDate,
                    Deactivated = applicationUser.Deactivated,
                    DeletedDate = applicationUser.DeletedDate,
                    Email = applicationUser.Email,
                    EmailConfirmed = applicationUser.EmailConfirmed,
                    FirstName = applicationUser.FirstName,
                    FK_CreatedBy_Id = applicationUser.FK_CreatedBy_Id,
                    FK_DeletedBy_Id = applicationUser.FK_DeletedBy_Id,
                    FK_UpdatedBy_Id = applicationUser.FK_UpdatedBy_Id,
                    IsDeleted = applicationUser.IsDeleted,
                    LastName = applicationUser.LastName,
                    LockoutEnabled = applicationUser.LockoutEnabled,
                    LockoutEnd = applicationUser.LockoutEnd,
                    NormalizedEmail = applicationUser.NormalizedEmail,
                    NormalizedUserName = applicationUser.NormalizedUserName,
                    PasswordHash = applicationUser.PasswordHash,
                    Phone1 = applicationUser.Phone1,
                    Phone2 = applicationUser.Phone2,
                    PhoneNumber = applicationUser.PhoneNumber,
                    PhoneNumberConfirmed = applicationUser.PhoneNumberConfirmed,
                    SecurityStamp = applicationUser.SecurityStamp,
                    TwoFactorEnabled = applicationUser.TwoFactorEnabled,
                    UpdatedDate = applicationUser.UpdatedDate,
                    UserName = applicationUser.UserName
                };
                return ProcessResultHelper.Succedded(junkUser);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<JunkUser>(null, ex);
            }
        }
    }
}
