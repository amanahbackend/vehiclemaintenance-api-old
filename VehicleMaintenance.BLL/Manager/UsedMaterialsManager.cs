﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VehicleMaintenance.models.Settings;

namespace VehicleMaintenance.BLL.Manager
{
    public class UsedMaterialsManager : Repository<UsedMaterials>, IUsedMaterialsManager
    {
        IServiceProvider _serviceprovider;
        private readonly VehicleAppSettings _appSettings;
        public UsedMaterialsManager(IServiceProvider serviceprovider, VehicleMaintenanceDbContext context,
            IOptions<VehicleAppSettings> appSettings)
         : base(context)
        {
            _serviceprovider = serviceprovider;
            _appSettings = appSettings.Value;
        }
        private IInventoryManager InventoryManager
        {
            get
            {
                return _serviceprovider.GetService<IInventoryManager>();
            }
        }
        private IJobCardManager JobCardManager
        {
            get
            {
                return _serviceprovider.GetService<IJobCardManager>();
            }
        }
        private IVehicleManager VehicleManager => _serviceprovider.GetService<IVehicleManager>();
        private IRepairRequestManager RepairRequestManager => _serviceprovider.GetService<IRepairRequestManager>();

        private ISparePartStatusManager SparePartStatusManager => _serviceprovider.GetService<ISparePartStatusManager>();
        public ProcessResult<List<UsedMaterials>> GetByJobCardID(int jobcardId)
        {
            List<UsedMaterials> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_JobCard_ID == jobcardId).ToList();

                return ProcessResultHelper.Succedded<List<UsedMaterials>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByJobCardID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<UsedMaterials>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByJobCardID");
            }
        }
        public ProcessResult<List<UsedMaterials>> GetByRepairRequestID(int repairrequestId)
        {
            List<UsedMaterials> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairrequestId).ToList();

                return ProcessResultHelper.Succedded<List<UsedMaterials>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByRepairRequestID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<UsedMaterials>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByRepairRequestID");
            }
        }
        public ProcessResult<List<UsedMaterials>> GetByRepairRequestIDs(List<int?> repairrequestIds)
        {

            List<UsedMaterials> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(r => repairrequestIds.Contains(r.FK_RepairRequest_ID)).ToList();
                BindInventoryItems(input);
                BindjobCardItems(input);
                return ProcessResultHelper.Succedded<List<UsedMaterials>>(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<UsedMaterials>>(input, ex);
            }
        }
        public List<UsedMaterials> BindInventoryItems(List<UsedMaterials> data)
        {
            foreach (var item in data)
            {
                var invItm = InventoryManager.Get(item.FK_Inventory_ID);
                if (invItm.IsSucceeded && invItm.Data != null)
                {
                    item.Inventory = invItm.Data;
                }
            }
            return data;
        }
        public List<UsedMaterials> BindjobCardItems(List<UsedMaterials> data)
        {
            foreach (var item in data)
            {
                var RRItm = RepairRequestManager.Get(item.FK_RepairRequest_ID);
                if (RRItm.IsSucceeded && RRItm.Data != null)
                {
                    var jobcardItm = JobCardManager.Get(RRItm.Data.FK_JobCard_ID);
                    if (jobcardItm.IsSucceeded && jobcardItm.Data != null)
                    {
                        var vehicleItm = VehicleManager.Get(jobcardItm.Data.FK_Vehicle_ID);
                        if (vehicleItm.IsSucceeded && vehicleItm.Data != null)
                        {
                            item.FleetNumber = vehicleItm.Data.FleetNumber;
                        }
                    }
                }
            }
            return data;
        }
        public ProcessResult<List<UsedMaterials>> SearchBysparePartSerialNumber_cost(int repairrequestId,string word)
        {

            List<UsedMaterials> input = null;
            List<UsedMaterials> input2 = null;

            try
            {
                if (repairrequestId != null || repairrequestId != 0)
                {

                    if (word != null && word != "")
                    {
                        var sparePartRes = InventoryManager.GetAllQuerable().Data.Where(x => x.SerialNumber == word).FirstOrDefault();
                        input = GetAllQuerable().Data.Where(x => x.FK_Inventory_ID.ToString().StartsWith(sparePartRes.Id.ToString()) && x.FK_RepairRequest_ID == repairrequestId).ToList();
                        input2 = GetAllQuerable().Data.Where(x => x.cost.ToString().StartsWith(word.ToString()) && x.FK_RepairRequest_ID == repairrequestId).ToList();
                        if (input2.Count() > 0)
                        {
                            foreach (var item in input2)
                            {
                                input.Add(item);
                            }
                        }
                       
                    }
                }
                return ProcessResultHelper.Succedded<List<UsedMaterials>>(input, (string)null, ProcessResultStatusCode.Succeded, "SearchBysparePartSerialNumber_cost");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<UsedMaterials>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "SearchBysparePartSerialNumber_cost");
            }
        }

        public ProcessResult<List<EquipmentTransfer>> GetAllEquipmentTransfer()
        {
            var transferedStatus = SparePartStatusManager.GetByName(_appSettings.TransferdSparePartStatus);
            var usedTransferedMaterials = GetAllQuerable().Data
                .Where(u => u.FK_SparePartStatus_ID == transferedStatus.Id)
                .ToList();
            var resultData = GetEqiuipmentTransferFromUsedMaterial(usedTransferedMaterials);
            var result = ProcessResultHelper.Succedded(resultData);
            return result;
        }

        public ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferByDate(DateTime date)
        {
            var transferedStatus = SparePartStatusManager.GetByName(_appSettings.TransferdSparePartStatus);
            var usedTransferedMaterials = GetAllQuerable().Data
                .Where(u => u.FK_SparePartStatus_ID == transferedStatus.Id && u.CreatedDate.Date == date.Date)
                .ToList();
            var resultData = GetEqiuipmentTransferFromUsedMaterial(usedTransferedMaterials);
            var result = ProcessResultHelper.Succedded(resultData);
            return result;
        }

        public ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferByVehicleFrom(string fleetNumber)
        {
            var transferedStatus = SparePartStatusManager.GetByName(_appSettings.TransferdSparePartStatus);
            var usedTransferedMaterials = GetAllQuerable().Data
                .Include(x=>x.TransferFromVehicle)
                .Where(u => u.FK_SparePartStatus_ID == transferedStatus.Id && u.TransferFromVehicle.FleetNumber.Equals(fleetNumber))
                .ToList();
            var resultData = GetEqiuipmentTransferFromUsedMaterial(usedTransferedMaterials);
            var result = ProcessResultHelper.Succedded(resultData);
            return result;
        }

        public ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferByVehicleTo(string fleetNumber)
        {
            var transferedStatus = SparePartStatusManager.GetByName(_appSettings.TransferdSparePartStatus);
            var usedTransferedMaterials = GetAllQuerable().Data
                .Where(u => u.FK_SparePartStatus_ID == transferedStatus.Id && u.FleetNumber.Equals(fleetNumber))
                .ToList();
            var resultData = GetEqiuipmentTransferFromUsedMaterial(usedTransferedMaterials);
            var result = ProcessResultHelper.Succedded(resultData);
            return result;
        }

        public ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferBySparePart(int sparePartId)
        {
            var transferedStatus = SparePartStatusManager.GetByName(_appSettings.TransferdSparePartStatus);
            var usedTransferedMaterials = GetAllQuerable().Data
                .Where(u => u.FK_SparePartStatus_ID == transferedStatus.Id && u.FK_Inventory_ID == sparePartId)
                .ToList();
            var resultData = GetEqiuipmentTransferFromUsedMaterial(usedTransferedMaterials);
            var result = ProcessResultHelper.Succedded(resultData);
            return result;
        }

        private List<EquipmentTransfer> GetEqiuipmentTransferFromUsedMaterial(IEnumerable<UsedMaterials> usedMaterials)
        {
            var equipmentTransferLst = new List<EquipmentTransfer>();
            foreach (var item in usedMaterials)
            {
                var inventory = InventoryManager.Get(item.FK_Inventory_ID).Data;
                var vehicle = VehicleManager.Get(item.FK_TranferFromVehicle_Id).Data;
                var equipmentTransfer = new EquipmentTransfer()
                {
                    Date = item.CreatedDate,
                    Description = inventory.Description,
                    JobCardId = item.FK_JobCard_ID,
                    Make = inventory.Make,
                    Model = inventory.Model,
                    NameEn = inventory.Name_EN,
                    NameAr = inventory.Name_ER,
                    RepairRequestId = item.FK_RepairRequest_ID,
                    SerialNumber = inventory.SerialNumber,
                    TransferFromFleetNumber = vehicle.FleetNumber,
                    TransferToFleetNumber = item.FleetNumber,
                    Capacity = item.Capacity
                };
                equipmentTransferLst.Add(equipmentTransfer);
            }
            return equipmentTransferLst;
        }
    }
}
