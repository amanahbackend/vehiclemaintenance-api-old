﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using Utilites.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.BLL.Manager
{
    public class VehicleTypeManager : Repository<VehicleType>, IVehicleTypeManager
    {
        public VehicleTypeManager(DbContext context) : base(context)
        {
        }

        public ProcessResult<List<VehicleType>> SearchByName(string word)
        {
            List<VehicleType> result = null;

            try
            {
                result = GetAllQuerable().Data.Where(obj => obj.Name.Contains(word)).ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }
    }
}
