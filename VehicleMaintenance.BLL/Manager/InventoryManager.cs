﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
   public class InventoryManager : Repository<Inventory>, IInventoryManager
    {
        public InventoryManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
        public ProcessResult<List<Inventory>> GetBySparePartName(string Name)
        {
            List<Inventory> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Name_EN== Name).ToList();

                return ProcessResultHelper.Succedded<List<Inventory>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByJobCardID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Inventory>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByJobCardID");
            }
        }
        public ProcessResult<List<Inventory>> Search(string word)
        {
            List<Inventory> input = null;
            try
            {
                if (word != null && word != "" )
                {
                    input = GetAllQuerable().Data.Where(x => x.SerialNumber.StartsWith(word)).Take(15).ToList();
                }
                return ProcessResultHelper.Succedded<List<Inventory>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByJobCardID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Inventory>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByJobCardID");
            }
        }
        public ProcessResult<List<Inventory>> SearchByName_SerialNum_Desc(string word)
        {
            List<Inventory> input = null;
            List<Inventory> input2 = null;
            List<Inventory> input3 = null;
            try
            {      
                    if (word != null && word != "")
                    {
                    input = GetAllQuerable().Data.Where(x => x.Name_EN.StartsWith(word) || x.Name_ER.StartsWith(word)).ToList();
                    input2 = GetAllQuerable().Data.Where(x => x.SerialNumber.StartsWith(word)).ToList();
                    input3 = GetAllQuerable().Data.Where(x => x.Description.StartsWith(word)).ToList();
                    if (input2.Count() > 0)
                        {
                            foreach (var item in input2)
                            {
                                input.Add(item);
                            }
                        }
                        if (input3.Count() > 0)
                        {
                            foreach (var item in input3)
                            {
                                input.Add(item);
                            }
                        }
                    }
                
                return ProcessResultHelper.Succedded<List<Inventory>>(input, (string)null, ProcessResultStatusCode.Succeded, "SearchByName_SerialNum_Desc");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Inventory>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "SearchByName_SerialNum_Desc");
            }
        }

    }
}
