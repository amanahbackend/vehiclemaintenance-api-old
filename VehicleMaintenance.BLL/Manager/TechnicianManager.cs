﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
    public class TechnicianManager : Repository<Technician>, ITechnicianManager
    {
        public TechnicianManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
        public ProcessResult<List<Technician>> SearchByName_EmployeId(string word)
        {
            List<Technician> input = null;
            List<Technician> input2 = null;

            try
            {           
                    if (word != null && word != "")
                    {
                    input = GetAllQuerable().Data.Where(x => x.FirstName.StartsWith(word) || x.LastName.StartsWith(word)).ToList();
                    input2 = GetAllQuerable().Data.Where(x => x.Employe_Id.StartsWith(word)).ToList();
                        if (input2.Count() > 0)
                        {
                            foreach (var item in input2)
                            {
                                input.Add(item);
                            }
                        } 
                    }
                return ProcessResultHelper.Succedded<List<Technician>>(input, (string)null, ProcessResultStatusCode.Succeded, "SearchByName_EmployeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Technician>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "SearchByName_EmployeId");
            }
        }

    }
}
