﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
   public class EquipmentTransferStatusManager : Repository<EquipmentTransferStatus>, IEquipmentTransferStatusManager
    {
        public EquipmentTransferStatusManager(VehicleMaintenanceDbContext context)
           : base(context)
        {
        }
    }
}
