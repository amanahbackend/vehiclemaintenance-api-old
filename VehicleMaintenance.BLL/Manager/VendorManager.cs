﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
    public class VendorManager : Repository<Vendor>, IVendorManager
    {
        public VendorManager(VehicleMaintenanceDbContext context)
            : base(context)
        {
        }       
    }
}
