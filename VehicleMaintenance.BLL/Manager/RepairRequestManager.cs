﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.Manager
{
    public class RepairRequestManager : Repository<RepairRequest>, IRepairRequestManager
    {
        IServiceProvider _serviceprovider;
        private IRepairRequestStatusManager RepairRequestStatusManager
        {
            get
            {
                return _serviceprovider.GetService<IRepairRequestStatusManager>();
            }
        }
        public RepairRequestManager(IServiceProvider serviceprovider,VehicleMaintenanceDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        public ProcessResult<List<RepairRequest>> GetByJobCardID(int jobcardId)
        {
            List<RepairRequest> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_JobCard_ID == jobcardId).ToList();

                return ProcessResultHelper.Succedded<List<RepairRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByJobCardID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByJobCardID");
            }
        }
        public ProcessResult<List<RepairRequest>> GetByJobCardIDs(List<int> jobcardIds)
        {
            List<RepairRequest> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => jobcardIds.Contains(x.FK_JobCard_ID.Value)).ToList();
                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input,ex);
            }
        }
        public ProcessResult<List<RepairRequest>> GetByMaintenanceType(bool type)
        {
            List<RepairRequest> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Maintenance_Type == type).ToList();

                return ProcessResultHelper.Succedded<List<RepairRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMaintenanceType");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByMaintenanceType");
            }
        }
        public ProcessResult<List<RepairRequest>> GetByexternalandMaintenanceType(bool external, bool type)
        {
            List<RepairRequest> input = null;
            try
            {
                var repairRequestStatusRes = RepairRequestStatusManager.GetAllQuerable().Data.Where(x => x.RepairRequestStatusName_EN.ToLower() == "In Progress").FirstOrDefault();

                input = GetAllQuerable().Data.Where(x => x.Maintenance_Type == type && x.External == external && x.FK_RepairRequestStatus_ID != repairRequestStatusRes.Id).ToList();

                return ProcessResultHelper.Succedded<List<RepairRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMaintenanceType");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByMaintenanceType");
            }
        }
        public ProcessResult<List<RepairRequest>> GetNonApprovedrepairrequests()
        {
            List<RepairRequest> input = null;
            try
            {
                var repairRequestStatusRes = RepairRequestStatusManager.GetAllQuerable().Data.Where(x => x.RepairRequestStatusName_EN.ToLower() == "Need Approval").FirstOrDefault();

                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequestStatus_ID == repairRequestStatusRes.Id).ToList();

                return ProcessResultHelper.Succedded<List<RepairRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetNonApprovedJobcards");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetNonApprovedJobcards");
            }
        }
        public ProcessResult<List<RepairRequest>> SearchByID_JobcardID(string word)
        {
                List<RepairRequest> input = null;
                List<RepairRequest> input2 = null;

                try
                {     
                        if (word != null && word != "")
                        {
                        input = GetAllQuerable().Data.Where(x => x.Id.ToString().StartsWith(word)).ToList();
                        input2 = GetAllQuerable().Data.Where(x => x.FK_JobCard_ID.ToString().StartsWith(word)).ToList();
                            if (input2.Count() > 0)
                            {
                                foreach (var item in input2)
                                {
                                    input.Add(item);
                                }
                            }
                        }

                return ProcessResultHelper.Succedded<List<RepairRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "SearchByID_JobcardID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<RepairRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "SearchByID_JobcardID");
            }
        }
        
        public ProcessResult<List<ChartViewModel>> GetChart()
        {
            try
            {
                var data = GetAll().Data.GroupBy(r => r.FK_RepairRequestStatus_ID, (key, g) => new ChartViewModel
                {
                    Id = key.Value,
                    Count = g.ToList().Count
                }).ToList();
                foreach (var item in data)
                {
                    var statusRes = RepairRequestStatusManager.Get(item.Id);
                    if (statusRes.IsSucceeded && statusRes.Data != null)
                    {
                        item.NameAR = statusRes.Data.RepairRequestStatusName_AR;
                        item.NameEN = statusRes.Data.RepairRequestStatusName_EN;
                    }
                }

                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<ChartViewModel>>(null, ex);
            }
        }

        public new ProcessResult<int> Count()
        {
            try
            {
                var count = GetAll().Data.Count();
                return ProcessResultHelper.Succedded(count);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(0, ex);
            }
        }
        public ProcessResult<bool> ApproveRepairRequest(int repairRequestId, bool Approve)
        {
            ProcessResult<bool> input = null;
            try
            {
                if (Approve)
                {
                    var repairRequestStatusRes = RepairRequestStatusManager.GetAllQuerable().Data.Where(x => x.RepairRequestStatusName_EN.ToLower() == "In Progress").FirstOrDefault();

                    RepairRequest repairRequestRes = GetAllQuerable().Data.Where(x => x.Id == repairRequestId).ToList().FirstOrDefault();

                    repairRequestRes.FK_RepairRequestStatus_ID = repairRequestStatusRes.Id;

                    input = base.Update(repairRequestRes);
                }
                return input;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, new Exception("Cann't approve Repair Request "));
            }
        }
    }
}
