﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using Utilites.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.Models.Entities.Common;

namespace VehicleMaintenance.BLL.Manager
{
    public class ShiftManager : Repository<Shift>, IShiftManager
    {
        public ShiftManager(DbContext context) : base(context)
        {
        }

        public ProcessResult<List<Shift>> GetList()
        {
            List<Shift> result = null;

            try
            {
                result = GetAllQuerable().Data.Where(obj => obj.IsDeleted == false).ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }
    }
}
