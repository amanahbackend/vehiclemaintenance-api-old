﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
   public class FileManager : Repository<File>, IFileManager
    {
        public FileManager(VehicleMaintenanceDbContext context)
           : base(context)
        {
        }
        public ProcessResult<List<File>> GetByRepairRequestID(int repairId)
        {
            List<File> input = null;
            try
            {
               // IEnumerable<IGrouping<int, File >> inputGrouping = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairId).GroupBy(p => p.FK_Vendor_ID);
                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairId).ToList();
                

             return ProcessResultHelper.Succedded<List<File>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByRepairRequestID");
            }
			catch (Exception ex) { 
				return ProcessResultHelper.Failed<List<File>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByRepairRequestID");
			}
        }
        public ProcessResult<List<File>> GetByVendorID(int vendorId) {
            List<File> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Vendor_ID == vendorId && x.Selected == true).ToList();
                return ProcessResultHelper.Succedded<List<File>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByRepairRequestIDandChoosedVendor");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<File>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByRepairRequestIDandChoosedVendor");
            }
        }
        public ProcessResult<List<File>> GetByRepairRequestIDandChoosedVendor(int repairId)
        {
            List<File> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairId && x.Selected==true).ToList();
                return ProcessResultHelper.Succedded<List<File>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByRepairRequestIDandChoosedVendor");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<File>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByRepairRequestIDandChoosedVendor");
            }
        }
        public ProcessResult<bool> UpdateChoosedVendor(int repairId,int FK_Vendor_ID)
        {
            List<File> input = null;
            List<File> other_input = null;

            bool result = false;
            bool other_result = false;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairId&&x.FK_Vendor_ID==FK_Vendor_ID).ToList();
                other_input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairId && x.FK_Vendor_ID != FK_Vendor_ID).ToList();

                foreach (var item in input)
                {
                    item.Selected = true;
                }
                foreach (var item in other_input)
                {
                    item.Selected = false;

                }
                if (input.Count>0)
                {
                    result = Update(input).Data;
                }
                if (other_input.Count > 0)
                {
                    other_result = Update(other_input).Data;
                    if (!other_result)
                    {
                        result = false;
                    }
                } 

                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "Update");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "Update");
            }
        }
    }
}
