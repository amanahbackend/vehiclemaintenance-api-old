﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
   public class EquipmentTransferHistoryManager : Repository<EquipmentTransferHistory>, IEquipmentTransferHistoryManager
    {
        public EquipmentTransferHistoryManager(VehicleMaintenanceDbContext context)
           : base(context)
        {
        }
    }
}
