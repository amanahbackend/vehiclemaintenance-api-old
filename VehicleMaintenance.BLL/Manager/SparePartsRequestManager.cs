﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace VehicleMaintenance.BLL.Manager
{
    public class SparePartsRequestManager : Repository<SparePartsRequest>, ISparePartsRequestManager
    {
        IServiceProvider _serviceprovider;

        public SparePartsRequestManager(VehicleMaintenanceDbContext context, IServiceProvider serviceprovider)
         : base(context)
        {
            _serviceprovider = serviceprovider;
        }

        private IStatusManager StatusManager
        {
            get
            {
                return _serviceprovider.GetService<IStatusManager>();
            }
        }

        public ProcessResult<List<SparePartsRequest>> GetByRepairRequestID(int repairRequestId)
        {
            List<SparePartsRequest> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.Where(x => x.StatusName_EN.ToLower() == "in progress").FirstOrDefault();

                input = GetAllQuerable().Data.Where(x => x.FK_RepairRequest_ID == repairRequestId && x.FK_Status_ID == statusRes.Id).ToList();

                return ProcessResultHelper.Succedded<List<SparePartsRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByRepairRequestID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<SparePartsRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByRepairRequestID");
            }
        }
        public ProcessResult<List<SparePartsRequest>> GetByRepairRequestIDs(List<int?> repairrequestIds)
        {

            List<SparePartsRequest> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.Where(x => x.StatusName_EN.ToLower() == "in progress").FirstOrDefault();
                input = GetAllQuerable().Data.Where(r => repairrequestIds.Contains(r.FK_RepairRequest_ID) && r.FK_Status_ID == statusRes.Id).ToList();
                return ProcessResultHelper.Succedded<List<SparePartsRequest>>(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<SparePartsRequest>>(input, ex);
            }
        }
        public ProcessResult<List<SparePartsRequest>> GetNonApproved()
        {
            List<SparePartsRequest> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.Where(x => x.StatusName_EN.ToLower() == "pending approved").FirstOrDefault();

                input = GetAllQuerable().Data.Where(x => x.FK_Status_ID == statusRes.Id).ToList();

                return ProcessResultHelper.Succedded<List<SparePartsRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetNonApproved");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<SparePartsRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetNonApproved");
            }
        }

        public ProcessResult<List<SparePartsRequest>> GetApproved()
        {
            List<SparePartsRequest> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.Where(x => x.StatusName_EN.ToLower() == "pending approved").FirstOrDefault();

                input = GetAllQuerable().Data.Where(x => x.FK_Status_ID != statusRes.Id).ToList();

                return ProcessResultHelper.Succedded<List<SparePartsRequest>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetApproved");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<SparePartsRequest>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetApproved");
            }
        }
        public ProcessResult<bool> ApproveSparePart(int sparePartId, bool Approve)
        {
            ProcessResult<bool> input = null;
            try
            {
                if (Approve)
                {
                    var statusRes = StatusManager.GetAllQuerable().Data.Where(x => x.StatusName_EN.ToLower() == "In progress").FirstOrDefault();

                    SparePartsRequest sparePartRes = GetAllQuerable().Data.Where(x => x.Id == sparePartId).ToList().FirstOrDefault();

                    sparePartRes.FK_Status_ID = statusRes.Id;

                    input = base.Update(sparePartRes);
                }
                return input;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, new Exception("Cann't approve Spare Part "));
            }
        }
        public ProcessResult<List<SparePartsRequest>> SearchSparePartsRequests(int? inventoryId, DateTime? startDate, DateTime? endDate)
        {
            List<SparePartsRequest> result = null;

            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "in progress");

                var querySparePartsRequests = GetAllQuerable().Data.Where(obj => obj.FK_Status_ID == statusRes.Id);

                if (inventoryId != null && inventoryId.Value > 0)
                {
                    querySparePartsRequests = querySparePartsRequests.Where(obj => obj.FK_Inventory_ID == inventoryId);
                }

                if (startDate != null)
                {
                    querySparePartsRequests = querySparePartsRequests.Where(obj => obj.StartDate.Date >= startDate);
                }

                if (endDate != null)
                {
                    querySparePartsRequests = querySparePartsRequests.Where(obj => obj.EndDate.Date <= endDate);
                }

                result = querySparePartsRequests.ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }
    }
}
