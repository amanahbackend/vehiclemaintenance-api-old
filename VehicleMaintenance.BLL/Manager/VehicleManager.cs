﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using Utilites.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.BLL.Utilities;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.BLL.Manager
{
    public class VehicleManager : Repository<Vehicle>, IVehicleManager
    {
        public VehicleManager(VehicleMaintenanceDbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Get vehicles per page.
        /// </summary>
        /// <param name="pageNo">Min value = 1</param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ProcessResult<List<Vehicle>> GetVehiclesPerPage(int pageNo, int pageSize)
        {
            int rowsCountToSkip = Pagination.GetRowsCountToSkip(pageNo, ref pageSize);

            List<Vehicle> result = null;

            try
            {
                IQueryable<Vehicle> queryGetVehicles = GetAllQuerable().Data.Include("VehicleType");

                result = queryGetVehicles.Skip(rowsCountToSkip).Take(pageSize).ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        public ProcessResult<List<Vehicle>> Search(string word)
        {
            List<Vehicle> result = null;

            try
            {
                if (!string.IsNullOrEmpty(word))
                {
                    result = GetAllQuerable().Data.Where(x => x.FleetNumber.StartsWith(word)).Take(15).ToList();
                }

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        public ProcessResult<List<Vehicle>> GetByType(string type)
        {
            List<Vehicle> result = null;
            try
            {
                // result = GetAllQuerable().Data.Where(x => x.Type=="Tool").ToList();
                int vehicleTypeId = 5;
                result = GetAllQuerable().Data.Where(x => x.VehicleTypeId == vehicleTypeId).ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        public ProcessResult<List<Vehicle>> SearchVehicles(string plateNo, string fleetNo, string model,
            string vehicleStatus, int pageNo, int pageSize)
        {
            List<Vehicle> result = null;

            int rowsCountToSkip = Pagination.GetRowsCountToSkip(pageNo, ref pageSize);

            try
            {
                IQueryable<Vehicle> queryable = GetQueryableSearchVehicles(plateNo, fleetNo, model, vehicleStatus);

                result = queryable.Skip(rowsCountToSkip).Take(pageSize).ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        public ProcessResult<int> SearchVehiclesRowsCount(string plateNo, string fleetNo, string model,
            string vehicleStatus)
        {
            int result = 0;

            try
            {
                IQueryable<Vehicle> querySearchVehicles = GetQueryableSearchVehicles(plateNo, fleetNo, model, vehicleStatus);

                result = querySearchVehicles.Count();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        private IQueryable<Vehicle> GetQueryableSearchVehicles(string plateNo, string fleetNo, string model,
            string vehicleStatus)
        {
            IQueryable<Vehicle> querySearchVehicles = GetAllQuerable().Data.Include("VehicleType");

            if (!string.IsNullOrWhiteSpace(plateNo))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.VehicleNumberPlate.StartsWith(plateNo));
            }

            if (!string.IsNullOrWhiteSpace(fleetNo))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.FleetNumber.StartsWith(fleetNo));
            }

            if (!string.IsNullOrWhiteSpace(model))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.Model.StartsWith(model));
            }

            if (!string.IsNullOrWhiteSpace(vehicleStatus))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.Status == vehicleStatus);
            }

            return querySearchVehicles;
        }

        public ProcessResult<bool> IsPlateNoExists(string plateNo, int? vehicleIdToSkipCheck)
        {
            var vehicle = GetAllQuerable().Data.FirstOrDefault(v =>
                v.VehicleNumberPlate == plateNo && (v.Id != vehicleIdToSkipCheck ||
                                                    vehicleIdToSkipCheck == null));

            ProcessResult<bool> result = vehicle != null
                ? ProcessResultHelper.Succedded(true)
                : ProcessResultHelper.Failed(false, new Exception("There is no vehicle with this plate number"));

            return result;
        }

        public ProcessResult<bool> IsFleetExists(string fleetNo, int? vehicleIdToSkipCheck)
        {
            var vehicle = GetAllQuerable().Data.FirstOrDefault(v =>
                v.FleetNumber == fleetNo && (v.Id != vehicleIdToSkipCheck ||
                                             vehicleIdToSkipCheck == null));

            ProcessResult<bool> result = vehicle != null
                ? ProcessResultHelper.Succedded(true)
                : ProcessResultHelper.Failed(false, new Exception("There is no vehicle with this fleet number"));

            return result;
        }
    }
}
