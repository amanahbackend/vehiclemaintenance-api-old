﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
   public class JobCard_TechnicianManager : Repository<JobCardTechnician>, IJobCard_TechnicianManager
    {
        public JobCard_TechnicianManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
        public ProcessResult<List<JobCardTechnician>> GetByJobCardID(int jobcardId)
        {
            List<JobCardTechnician> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_JobCard_ID == jobcardId).ToList();

                return ProcessResultHelper.Succedded<List<JobCardTechnician>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByJobCardID");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<JobCardTechnician>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByJobCardID");
            }
        }

    }
}
