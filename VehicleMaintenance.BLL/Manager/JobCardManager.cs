﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VehicleMaintenance.BLL.Utilities;
using VehicleMaintenance.models.Settings;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.BLL.Manager
{
    public class JobCardManager : Repository<JobCard>, IJobCardManager
    {
        private readonly IServiceProvider _serviceprovider;
        private readonly VehicleAppSettings _appSettings;

        public JobCardManager(IOptions<VehicleAppSettings> appSettings, IServiceProvider serviceprovider,
            VehicleMaintenanceDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
            _appSettings = appSettings.Value;
        }

        private IStatusManager StatusManager => _serviceprovider.GetService<IStatusManager>();

        private IVehicleManager VehicleManager => _serviceprovider.GetService<IVehicleManager>();

        public ProcessResult<List<ChartViewModel>> GetChart()
        {
            try
            {
                var data = GetAll().Data.GroupBy(r => r.FK_Status_ID, (key, g) =>
                    new ChartViewModel
                    {
                        Id = key.Value,
                        Count = g.ToList().Count
                    }).ToList();

                foreach (var item in data)
                {
                    var statusRes = StatusManager.Get(item.Id);
                    if (statusRes.IsSucceeded && statusRes.Data != null)
                    {
                        item.NameAR = statusRes.Data.StatusName_AR;
                        item.NameEN = statusRes.Data.StatusName_EN;
                    }
                }

                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<ChartViewModel>>(null, ex);
            }
        }

        public override ProcessResult<bool> Update(JobCard entity)
        {
            ProcessResult<bool> result;

            if (entity.Odometer == null)
            {
                result = ProcessResultHelper.Failed(false,
                    new Exception("OdoMeter can't be null. please set odoMeter value."));
            }
            else
            {
                result = IsValidVehicleOdoMeter((double)entity.Odometer, entity.FK_Vehicle_ID.Value);

                if (result.IsSucceeded)
                {
                    result = base.Update(entity);
                }
            }

            return result;
        }

        public ProcessResult<List<JobCard>> GetByMaintenanceType(bool type)
        {
            List<JobCard> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "pending approved");
                input = GetAllQuerable().Data.Where(x => x.Maintenance_Type == type && x.FK_Status_ID != statusRes.Id)
                    .ToList();

                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<List<JobCard>> GetByCreatedDate(DateTime createdDate, int vehicleId)
        {
            List<JobCard> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "pending approved");

                input = GetAllQuerable().Data.Where(x => x.CreatedDate.AddDays(1) > createdDate &&
                                                         x.FK_Vehicle_ID == vehicleId && x.FK_Status_ID != statusRes.Id)
                    .ToList();
                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<List<JobCard>> GetByMaintenanceAndserviceAndvehicleId(int serviceType,
            bool maintenanceType, int vehicleId)
        {
            List<JobCard> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Maintenance_Type == maintenanceType &&
                                                         x.FK_ServiceType_ID == serviceType &&
                                                         x.FK_Vehicle_ID == vehicleId).ToList();

                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<List<JobCard>> GetNonApprovedJobcards()
        {
            List<JobCard> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "pending approved");

                input = GetAllQuerable().Data.Where(x => x.FK_Status_ID == statusRes.Id).ToList();

                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<List<JobCard>> GetByIdsAndMaintenanceType(List<int> ids, bool maintenanceType)
        {
            List<JobCard> input = null;
            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "pending approved");

                input = GetByIds(ids).Data.Where(x => x.Maintenance_Type == maintenanceType
                                                      && x.FK_Status_ID != statusRes.Id).ToList();

                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<List<JobCard>> GetByVehicle_IDandOpen(int vehicleId)
        {
            List<JobCard> input = null;
            try
            {
                var statusResClosed = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "closed");

                var statusResCancelled = StatusManager.GetAllQuerable().Data.FirstOrDefault(x => x.StatusName_EN.ToLower() == "cancelled");

                input = GetAllQuerable().Data.Where(x => x.FK_Vehicle_ID == vehicleId &&
                                                         x.FK_Status_ID != statusResClosed.Id &&
                                                         x.FK_Status_ID != statusResCancelled.Id).ToList();
                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<List<JobCard>> GetByVehicle_ID(int vehicleId)
        {
            List<JobCard> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Vehicle_ID == vehicleId).ToList();
                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<PaginatedItems<JobCard>> GetDailyReceipt(DateTime? date,
            PaginatedItems<JobCard> paginatedItems)
        {
            List<string> dates = new List<string>();
            Expression<Func<JobCard, bool>> predicate = PredicateBuilder.True<JobCard>();
            if (date == null)
            {
                date = DateTime.Now.Date;
            }
            //first date
            dates.Add(date.ToString());
            //second date
            dates.Add(date.ToString());

            predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<JobCard>(
                nameof(JobCard.Date_In), dates));

            //predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<JobCard>(nameof(JobCard.Date_In), date));
            var paginatedResult = GetAllPaginated(paginatedItems, predicate);
            if (paginatedResult.IsSucceeded && paginatedResult.Data != null)
            {
                paginatedResult.Data.Data = BindRepairRequests(paginatedResult.Data.Data);
            }
            return paginatedResult;
        }

        public ProcessResult<PaginatedItems<JobCard>> GetDailyReceiptExceptBrought(DateTime? date,
            PaginatedItems<JobCard> paginatedItems)
        {
            List<string> dates = new List<string>();
            Expression<Func<JobCard, bool>> predicate = PredicateBuilder.True<JobCard>();

            if (date == null)
            {
                date = DateTime.Now.Date;
            }

            //first date
            dates.Add(date.ToString());

            //second date
            dates.Add(date.ToString());

            predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<JobCard>(
                    nameof(JobCard.Date_In), dates));

            var status = StatusManager.GetByNameEN(_appSettings.ExistInGarageStatus);

            if (status.IsSucceeded && status.Data != null)
            {
                var statusExp = PredicateBuilder.CreateNotEqualSingleExpression<JobCard>(
                    nameof(JobCard.FK_Status_ID), status.Data.Id);

                predicate = PredicateBuilder.And(predicate, statusExp);
            }

            var paginatedResult = GetAllPaginated(paginatedItems, predicate);

            if (paginatedResult.IsSucceeded && paginatedResult.Data != null)
            {
                paginatedResult.Data.Data = BindRepairRequests(paginatedResult.Data.Data);
            }

            return paginatedResult;
        }

        public ProcessResult<PaginatedItems<JobCard>> GetAllPaginatedExceptClosed(int vehicleId = 0,
            PaginatedItems<JobCard> paginatedItems = null)
        {
            Expression<Func<JobCard, bool>> predicate = PredicateBuilder.True<JobCard>();

            //second date
            var status = StatusManager.GetByNameEN(_appSettings.ClosedStatus);
            if (status.IsSucceeded && status.Data != null)
            {
                var statusExp = PredicateBuilder.CreateNotEqualSingleExpression<JobCard>(
                        nameof(JobCard.FK_Status_ID), status.Data.Id);

                predicate = PredicateBuilder.And(predicate, statusExp);
            }
            if (vehicleId > 0)
            {
                var vehicleExp = PredicateBuilder.CreateEqualSingleExpression<JobCard>(
                        nameof(JobCard.FK_Vehicle_ID), vehicleId);

                predicate = PredicateBuilder.And(predicate, vehicleExp);
            }
            var paginatedResult = GetAllPaginated(paginatedItems, predicate);
            if (paginatedResult.IsSucceeded && paginatedResult.Data != null)
            {
                paginatedResult.Data.Data = BindRepairRequests(paginatedResult.Data.Data);
            }
            return paginatedResult;
        }

        public ProcessResult<PaginatedItems<JobCard>> GetAllPaginatedClosed(int vehicleId = 0,
            PaginatedItems<JobCard> paginatedItems = null)
        {
            Expression<Func<JobCard, bool>> predicate = PredicateBuilder.True<JobCard>();
            var status = StatusManager.GetByNameEN(_appSettings.ClosedStatus);
            if (status.IsSucceeded && status.Data != null)
            {
                var statusExp =
                    PredicateBuilder.CreateEqualSingleExpression<JobCard>(nameof(JobCard.FK_Status_ID), status.Data.Id);
                predicate = PredicateBuilder.And(predicate, statusExp);
            }
            if (vehicleId > 0)
            {
                var vehicleExp =
                    PredicateBuilder.CreateEqualSingleExpression<JobCard>(nameof(JobCard.FK_Vehicle_ID), vehicleId);
                predicate = PredicateBuilder.And(predicate, vehicleExp);
            }
            var paginatedResult = GetAllPaginated(paginatedItems, predicate);
            if (paginatedResult.IsSucceeded && paginatedResult.Data != null)
            {
                paginatedResult.Data.Data = BindRepairRequests(paginatedResult.Data.Data);
            }
            return paginatedResult;
        }

        public ProcessResult<List<UsedMaterials>> GetDailyWorkOfGarage(DateTime? date)
        {
            ProcessResult<List<UsedMaterials>> result = null;
            //Expression<Func<JobCard, bool>> predicate = PredicateBuilder.True<JobCard>();

            if (date == null)
            {
                date = DateTime.Now.Date;
            }

            var data = GetAllQuerable().Data.Where(um => um.Date_In.Date == date).ToList();

            if (data != null)
            {
                var jobIds = data.Select(job => job.Id).ToList();
                var repairRequestsRes = RepairRequestManager.GetByJobCardIDs(jobIds);
                if (repairRequestsRes.IsSucceeded && repairRequestsRes.Data != null)
                {
                    var repairReqIds = repairRequestsRes.Data.Select(repReq => repReq.Id).ToList();
                    var repairReqIdsNullable = repairReqIds.Select(i => (int?)i).ToList();
                    result = UsedMaterialsManager.GetByRepairRequestIDs(repairReqIdsNullable);
                    //var xx=usedMaterialRes.Data.GroupBy(gp => gp.FleetNumber, (key, g) => new { PersonId = key, Cars = g.ToList() }););

                }
                else if (!repairRequestsRes.IsSucceeded)
                {
                    return ProcessResultHelper.Failed<List<UsedMaterials>>(null, repairRequestsRes.Exception);
                }
                else if (repairRequestsRes.Data == null)
                {
                    return ProcessResultHelper.Failed<List<UsedMaterials>>(null,
                        new Exception($"No SpareParts Used In This date {date}"));
                }

                data = BindRepairRequests(data);
            }
            else
            {
                return ProcessResultHelper.Failed<List<UsedMaterials>>(null,
                    new Exception($"no Job Cards for this date {date}"));
            }

            return result;
        }

        public ProcessResult<bool> CanAddJobCard(int vehicleId)
        {
            const int closedStatus = 3; // "Closed" AppSettings.json
            const int cancelledStatus = 4; // "Cancelled" AppSettings.json

            var jobCard = GetAllQuerable().Data.Where(jc => jc.FK_Vehicle_ID == vehicleId &&
                                                            jc.FK_Status_ID != closedStatus &&
                                                            jc.FK_Status_ID != cancelledStatus)
                .OrderByDescending(r => r.Id).FirstOrDefault();

            ProcessResult<bool> result;

            if (jobCard != null)
            {
                const string message = "Can't add new job card for this vehicle before closing the exist one.";

                result = ProcessResultHelper.Failed(false, new Exception(message));
            }
            else
            {
                result = ProcessResultHelper.Succedded(true);
            }

            return result;
        }

        //public ProcessResult<bool> CanAddJobCard(int vehicleId)
        //{
        //    int statusId = 0;
        //    var statusRes = StatusManager.GetByNameEN(_appSettings.ExistInGarageStatus);

        //    if (statusRes.IsSucceeded && statusRes.Data != null)
        //    {
        //        statusId = statusRes.Data.Id;
        //    }

        //    var jobCard = GetAllQuerable().Data.Where(jc => jc.FK_Vehicle_ID == vehicleId).Where(jc => jc.FK_Status_ID == statusId).OrderByDescending(r => r.CreatedDate).FirstOrDefault();

        //    ProcessResult<bool> result = jobCard == null
        //        ? ProcessResultHelper.Succedded(true)
        //        : ProcessResultHelper.Failed(false,
        //            new Exception(
        //                $"Can't add new job card for this vehicle {vehicleId} before closing the exist one with status {statusRes.Data.StatusName_EN}"));

        //    return result;
        //}

        public ProcessResult<bool> IsValidVehicleOdoMeter(double currentAudoMeter, int vehicleId)
        {
            ProcessResult<bool> result;
            bool data = true;
            int statusId = 0;
            int approvedstatusId = 0;
            var status = StatusManager.GetByNameEN(_appSettings.ExistInGarageStatus);

            if (status.IsSucceeded && status.Data != null)
            {
                statusId = status.Data.Id;
            }

            var jobCard = GetAllQuerable().Data.Where(jc => jc.FK_Vehicle_ID == vehicleId)
                .Where(jc => jc.FK_Status_ID == statusId).OrderByDescending(r => r.CreatedDate).FirstOrDefault();

            if (jobCard != null)
            {
                var allowedAudoMeter = _appSettings.AudoMeterFactorChange + jobCard.Odometer;

                if (allowedAudoMeter < currentAudoMeter)
                {
                    data = false;
                    var approvedstatusRes = StatusManager.GetByNameEN(_appSettings.PendingApprovedStatus);
                    if (approvedstatusRes.IsSucceeded && approvedstatusRes.Data != null)
                    {
                        approvedstatusId = approvedstatusRes.Data.Id;
                    }
                    jobCard.FK_Status_ID = approvedstatusId;
                    Update(jobCard);

                    result = ProcessResultHelper.Failed(data, new Exception(
                        $"{_appSettings.AudoMeterInvalidMessage}.value increased by {currentAudoMeter - jobCard.Odometer}"));
                }
                else
                {
                    result = ProcessResultHelper.Succedded(data);
                }
            }
            else
            {
                result = ProcessResultHelper.Succedded(data);
            }
            return result;
        }

        public ProcessResult<PaginatedItems<JobCard>> GetDailyBroughtReceipt(DateTime? date,
            PaginatedItems<JobCard> paginatedItems)
        {
            List<string> dates = new List<string>();
            Expression<Func<JobCard, bool>> predicate = PredicateBuilder.True<JobCard>();
            if (date == null)
            {
                date = DateTime.Now.Date;
            }
            //first date
            dates.Add(date.ToString());
            //second date
            dates.Add(date.ToString());

            predicate = predicate.And(
                PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<JobCard>(
                    nameof(JobCard.Date_In), dates));

            var status = StatusManager.GetByNameEN(_appSettings.ExistInGarageStatus);
            if (status.IsSucceeded && status.Data != null)
            {
                var statusExp =
                    PredicateBuilder.CreateEqualSingleExpression<JobCard>(nameof(JobCard.FK_Status_ID), status.Data.Id);
                predicate = PredicateBuilder.And(predicate, statusExp);
            }
            var paginatedResult = GetAllPaginated(paginatedItems, predicate);
            if (paginatedResult.IsSucceeded && paginatedResult.Data != null)
            {
                paginatedResult.Data.Data = BindRepairRequests(paginatedResult.Data.Data);
            }
            return paginatedResult;
        }

        public ProcessResult<List<JobCard>> SearchByID_CustomerType_Desc(int vehicleId, string word)
        {
            List<JobCard> input = null;

            try
            {
                if (vehicleId != 0)
                {

                    if (!string.IsNullOrEmpty(word))
                    {
                        input = GetAllQuerable().Data
                            .Where(x => x.Id.ToString().StartsWith(word) && x.FK_Vehicle_ID == vehicleId).ToList();
                        var input2 = GetAllQuerable().Data
                            .Where(x => x.Customer_Type.StartsWith(word) && x.FK_Vehicle_ID == vehicleId).ToList();
                        var input3 = GetAllQuerable().Data
                            .Where(x => x.Desc.StartsWith(word) && x.FK_Vehicle_ID == vehicleId).ToList();
                        if (input2.Any())
                        {
                            foreach (var item in input2)
                            {
                                input.Add(item);
                            }
                        }
                        if (input3.Any())
                        {
                            foreach (var item in input3)
                            {
                                input.Add(item);
                            }
                        }
                    }
                }

                return ProcessResultHelper.Succedded(input, null,
                    ProcessResultStatusCode.Succeded, "SearchByName_SerialNum_Desc");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex, null,
                    ProcessResultStatusCode.Failed, "SearchByName_SerialNum_Desc");
            }
        }

        public ProcessResult<List<JobCard>> SearchActiveJobCards(int vehicleId, string searchKey)
        {
            try
            {
                int statusId = 0;
                var status = StatusManager.GetByNameEN(_appSettings.ClosedStatus);
                if (status.IsSucceeded && status.Data != null)
                {
                    statusId = status.Data.Id;
                }
                var query = GetAllQuerable().Data.Where(x => x.FK_Vehicle_ID == vehicleId);
                if (statusId > 0)
                {
                    query = query.Where(x => x.FK_Status_ID != statusId);
                }
                query = query.Where(r =>
                    r.Customer_Type.ToLower().StartsWith(searchKey)
                    ||
                    r.Desc.ToLower().StartsWith(searchKey)
                    ||
                    r.Id.ToString() == searchKey.ToLower()
                );
                var data = query.ToList();
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<JobCard>>(null, ex);
            }
        }

        public ProcessResult<List<JobCard>> SearchDeActiveJobCards(int vehicleId, string searchKey)
        {
            try
            {
                int statusId = 0;
                var status = StatusManager.GetByNameEN(_appSettings.ClosedStatus);
                if (status.IsSucceeded && status.Data != null)
                {
                    statusId = status.Data.Id;
                }
                var query = GetAllQuerable().Data.Where(x => x.FK_Vehicle_ID == vehicleId);
                if (statusId > 0)
                {
                    query = query.Where(x => x.FK_Status_ID == statusId);
                }
                query = query.Where(r =>
                    r.Customer_Type.ToLower().StartsWith(searchKey.ToLower())
                    ||
                    r.Desc.ToLower().StartsWith(searchKey.ToLower())
                    ||
                    r.Id.ToString() == searchKey
                );
                var data = query.ToList();
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<JobCard>>(null, ex);
            }
        }

        private IRepairRequestManager RepairRequestManager => _serviceprovider.GetService<IRepairRequestManager>();

        private IUsedMaterialsManager UsedMaterialsManager => _serviceprovider.GetService<IUsedMaterialsManager>();

        public ProcessResult<List<JobCard>> GetAllClosed(int vehicleId = 0)
        {
            var closedStatus = StatusManager.GetByNameEN(_appSettings.ClosedStatus);
            const int cancelledStatusId = 4;

            ProcessResult<List<JobCard>> result;

            if (closedStatus.IsSucceeded && closedStatus.Data != null)
            {
                var dataQry = GetAllQuerable().Data.Where(r =>
                r.FK_Status_ID == closedStatus.Data.Id || r.FK_Status_ID == cancelledStatusId);

                if (vehicleId > 0)
                {
                    dataQry = dataQry.Where(v => v.FK_Vehicle_ID == vehicleId);
                }

                List<JobCard> data = dataQry.ToList();

                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultHelper.Failed<List<JobCard>>(null,
                    new Exception("There is no configuration for 'Closed' or 'Cancelled' statuses."));
            }

            return result;
        }

        public ProcessResult<List<JobCard>> GetAllExceptClosed(int vehicleId = 0)
        {
            ProcessResult<List<JobCard>> result;

            var closedStatus = StatusManager.GetByNameEN(_appSettings.ClosedStatus);
            const int cancelledStatusId = 4;

            if (closedStatus.IsSucceeded && closedStatus.Data != null)
            {
                var dataQry = GetAllQuerable().Data.Where(r =>
                r.FK_Status_ID != closedStatus.Data.Id && r.FK_Status_ID != cancelledStatusId);

                if (vehicleId > 0)
                {
                    dataQry = dataQry.Where(v => v.FK_Vehicle_ID == vehicleId);
                }

                List<JobCard> data = dataQry.ToList();
                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = GetAll();
            }

            return result;
        }

        private List<JobCard> BindRepairRequests(List<JobCard> data)
        {
            foreach (var obj in data)
            {
                var repairReqRes = GetRepairRequests(obj.Id);
                if (repairReqRes.IsSucceeded)
                {
                    obj.RepairRequests = repairReqRes.Data;
                }
            }
            return data;
        }

        public ProcessResult<List<RepairRequest>> GetRepairRequests(int jobCardId)
        {
            return RepairRequestManager.GetByJobCardID(jobCardId);
        }

        public ProcessResult<List<JobCard>> GetByOpenStatus()
        {
            List<JobCard> input = null;

            try
            {
                var statusRes = StatusManager.GetAllQuerable().Data
                    .FirstOrDefault(x => x.StatusName_EN.ToLower() == "in progress" ||
                                         x.StatusName_EN.ToLower() == "pending approved");
                input = GetAllQuerable().Data.Where(x => x.FK_Status_ID == statusRes.Id).ToList();

                return ProcessResultHelper.Succedded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<int> Count()
        {
            try
            {
                var count = GetAll().Data.Count();
                return ProcessResultHelper.Succedded(count);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(0, ex);
            }
        }

        public ProcessResult<bool> ApproveJobCard(int jobCardId, bool approve)
        {
            ProcessResult<bool> input = null;
            try
            {
                if (approve)
                {
                    var statusRes = StatusManager.GetAllQuerable().Data
                        .FirstOrDefault(x => x.StatusName_EN.ToLower() == "In progress");

                    JobCard jobCardRes = GetAllQuerable().Data.Where(x => x.Id == jobCardId).ToList().FirstOrDefault();

                    jobCardRes.FK_Status_ID = statusRes.Id;

                    input = base.Update(jobCardRes);
                }
                return input;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, new Exception("Can't approve Job Card"));
            }
        }

        public ProcessResult<List<JobCard>> SearchJobCards(string customerType,
            DateTime? dateIn, DateTime? dateOut, int? serviceTypeId, int? jobCardStatusId,
            string vehiclePlateNo, string vehicleFleetNo, int? jobCardId)
        {
            if (serviceTypeId <= 0)
            {
                serviceTypeId = null;
            }

            if (jobCardStatusId <= 0)
            {
                jobCardStatusId = null;
            }

            if (jobCardId <= 0)
            {
                jobCardId = null;
            }

            int? vehicleId = null;

            if (!string.IsNullOrWhiteSpace(vehiclePlateNo))
            {
                vehicleId = VehicleManager.GetAllQuerable().Data.FirstOrDefault(
                    obj => obj.VehicleNumberPlate == vehiclePlateNo).Id;
            }

            if (!string.IsNullOrWhiteSpace(vehicleFleetNo))
            {
                vehicleId = VehicleManager.GetAllQuerable().Data.FirstOrDefault(
                    obj => obj.FleetNumber == vehicleFleetNo).Id;
            }

            List<JobCard> result = null;

            try
            {
                IQueryable<JobCard> iqJobCards = GetAllQuerable().Data;

                if (!string.IsNullOrWhiteSpace(customerType))
                {
                    iqJobCards = iqJobCards.Where(x => x.Customer_Type == customerType);
                }

                if (dateIn != null)
                {
                    iqJobCards = iqJobCards.Where(x => x.Date_In.Date >= dateIn);
                }

                if (dateOut != null)
                {
                    iqJobCards = iqJobCards.Where(x => x.Date_Out.Date <= dateOut);
                }

                if (serviceTypeId != null)
                {
                    iqJobCards = iqJobCards.Where(x => x.FK_ServiceType_ID == serviceTypeId);
                }

                if (jobCardStatusId != null)
                {
                    iqJobCards = iqJobCards.Where(x => x.FK_Status_ID == jobCardStatusId);
                }

                if (vehicleId != null)
                {
                    iqJobCards = iqJobCards.Where(x => x.FK_Vehicle_ID == vehicleId);
                }

                if (jobCardId != null)
                {
                    iqJobCards = iqJobCards.Where(x => x.Id == jobCardId);
                }

                result = iqJobCards.ToList();

                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        // Mohammed Osman.
        public ProcessResult<List<JobCard>> GetDailyReceiptReport(string vehiclePlateNo, DateTime? startDate, DateTime? endDate)
        {
            string connectionString = Context.Database.GetDbConnection().ConnectionString;

            SqlConnection sqlConnection;

            const string commandText = "dbo.stp_JobCard_GetDailyServiceReport";

            List<SqlParameter> lstSqlParameters = new List<SqlParameter>();

            if (startDate.HasValue)
            {
                lstSqlParameters.Add(new SqlParameter("@startDate", startDate));
            }

            if (endDate.HasValue)
            {
                lstSqlParameters.Add(new SqlParameter("@endDate", endDate));
            }

            if (!string.IsNullOrWhiteSpace(vehiclePlateNo))
            {
                lstSqlParameters.Add(new SqlParameter("@vehiclePlateNo", vehiclePlateNo));
            }

            IDataReader dataReader = DbAccessManager.GetDataReader(connectionString, commandText, lstSqlParameters, out sqlConnection);

            List<JobCard> lstJobCards = new List<JobCard>();

            try
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        JobCard jobCard = GetJobCardFromDataReader(dataReader);

                        lstJobCards.Add(jobCard);
                    }

                    if (!dataReader.IsClosed)
                    {
                        dataReader.Close();
                    }
                }
            }
            finally
            {
                sqlConnection.Close();
            }

            ProcessResult<List<JobCard>> processResult = new ProcessResult<List<JobCard>>
            {
                Data = lstJobCards
            };

            return processResult;
        }

        private static JobCard GetJobCardFromDataReader(IDataRecord dataReader)
        {
            JobCard jobCard = new JobCard();
            jobCard.Sr = Convert.ToInt32(dataReader["RowNumber"]);
            jobCard.DailyServiceDesc = Convert.ToString(dataReader["DailyServiceDesc"]);
            jobCard.Id = Convert.ToInt32(dataReader["JobCardId"]);
            jobCard.CreatedDate = Convert.ToDateTime(dataReader["CreatedDate"]);
            jobCard.Customer_Type = Convert.ToString(dataReader["CustomerType"]);
            jobCard.Date_In = Convert.ToDateTime(dataReader["DateIn"]);
            jobCard.Date_Out = Convert.ToDateTime(dataReader["DateOut"]);

            jobCard.DateInShiftId = dataReader["DateInShiftId"] != DBNull.Value
                ? Convert.ToInt32(dataReader["DateInShiftId"])
                : (int?)null;

            jobCard.DateOutShiftId = dataReader["DateOutShiftId"] != DBNull.Value
                ? Convert.ToInt32(dataReader["DateOutShiftId"])
                : (int?)null;

            jobCard.Desc = Convert.ToString(dataReader["JobCardDescription"]);
            jobCard.Comment = Convert.ToString(dataReader["Comment"]);

            jobCard.vehicle = new Vehicle();
            jobCard.vehicle.VehicleNumberPlate = Convert.ToString(dataReader["VehiclePlateNo"]);
            jobCard.vehicle.FleetNumber = Convert.ToString(dataReader["VehicleFleetNo"]);

            jobCard.ServiceType = new ServiceType();
            jobCard.ServiceType.ServiceTypeName_EN = Convert.ToString(dataReader["ServiceType"]);

            return jobCard;
        }
    }
}