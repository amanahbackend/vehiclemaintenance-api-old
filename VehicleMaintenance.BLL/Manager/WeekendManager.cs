﻿using System;
using System.Collections.Generic;
using System.Linq;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.Manager
{
   public class WeekendManager : Repository<Weekend>, IWeekendManager
    {
        public WeekendManager(VehicleMaintenanceDbContext context)
          : base(context)
        {
        }
        public ProcessResult<List<Weekend>> GetLast(DateTime date)
        {
            List<Weekend> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.StartDate <= date && x.EndDate >= date).ToList();
                return ProcessResultHelper.Succedded<List<Weekend>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetLast");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Weekend>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetLast");
            }
        }
    }
}
