﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IJobCard_TechnicianManager : IRepository<JobCardTechnician>
    {
        ProcessResult<List<JobCardTechnician>> GetByJobCardID(int jobcardId);

    }
}
