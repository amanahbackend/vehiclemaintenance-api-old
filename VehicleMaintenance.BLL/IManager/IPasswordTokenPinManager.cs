using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IPasswordTokenPinManager 
    {
        PasswordTokenPin Add(PasswordTokenPin entity);
        PasswordTokenPin GetByToken(string token);
        PasswordTokenPin GetByPin(string pin);
        bool Delete(PasswordTokenPin entity);
        bool IsPinExist(string pin);
    }
}
