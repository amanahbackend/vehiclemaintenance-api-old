﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IRepairRequestStatusManager : IRepository<RepairRequestStatus>
    {
    }
}
