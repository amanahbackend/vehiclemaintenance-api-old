﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
   public interface IUsedMaterialsManager : IRepository<UsedMaterials>
    {
        ProcessResult<List<UsedMaterials>> GetByJobCardID(int jobcardId);
        ProcessResult<List<UsedMaterials>> GetByRepairRequestID(int repairrequestId);
        ProcessResult<List<UsedMaterials>> GetByRepairRequestIDs(List<int?> repairrequestIds);
        ProcessResult<List<UsedMaterials>> SearchBysparePartSerialNumber_cost(int repairrequestId, string word);
        ProcessResult<List<EquipmentTransfer>> GetAllEquipmentTransfer();
        ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferByDate(DateTime date);
        ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferByVehicleFrom(string fleetNumber);
        ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferByVehicleTo(string fleetNumber);
        ProcessResult<List<EquipmentTransfer>> GetEquipmentTransferBySparePart(int sparePartId);
    }
}
