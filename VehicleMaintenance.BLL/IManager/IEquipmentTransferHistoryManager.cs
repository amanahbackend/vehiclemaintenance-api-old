﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IEquipmentTransferHistoryManager : IRepository<EquipmentTransferHistory>
    {
    }
}
