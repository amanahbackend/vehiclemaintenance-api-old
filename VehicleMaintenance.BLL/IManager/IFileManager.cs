﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IFileManager : IRepository<File>
    {
        ProcessResult<List<File>> GetByRepairRequestID(int repairId);
        ProcessResult<List<File>> GetByRepairRequestIDandChoosedVendor(int repairId);
        ProcessResult<List<File>> GetByVendorID(int vendorId);
        ProcessResult<bool> UpdateChoosedVendor(int repairId, int FK_Vendor_ID);
    }
}
