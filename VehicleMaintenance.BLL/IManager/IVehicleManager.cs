﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IVehicleManager : IRepository<Vehicle>
    {
        ProcessResult<List<Vehicle>> GetVehiclesPerPage(int pageNo, int pageSize);

        ProcessResult<List<Vehicle>> Search(string word);

        ProcessResult<List<Vehicle>> GetByType(string type);

        ProcessResult<List<Vehicle>> SearchVehicles(string plateNo, string fleetNo, string model,
            string vehicleStatus, int pageNo, int pageSize);

        ProcessResult<int> SearchVehiclesRowsCount(string plateNo, string fleetNo, string model,
            string vehicleStatus);

        ProcessResult<bool> IsPlateNoExists(string plateNo, int? vehicleIdToSkipCheck);

        ProcessResult<bool> IsFleetExists(string fleetNo, int? vehicleIdToSkipCheck);
    }
}
