﻿using System;
using System.Collections.Generic;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;
using DispatchProduct.RepositoryModule;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IJobCardManager : IRepository<JobCard>
    {
        ProcessResult<List<JobCard>> GetByMaintenanceType(bool type);
        ProcessResult<List<JobCard>> GetByCreatedDate(DateTime createdDate, int vehicleId);
        ProcessResult<List<JobCard>> GetByMaintenanceAndserviceAndvehicleId(int serviceType, bool maintenanceType, int vehicleId);
        ProcessResult<List<JobCard>> GetNonApprovedJobcards();
        ProcessResult<List<JobCard>> GetByOpenStatus();
        ProcessResult<List<JobCard>> GetByIdsAndMaintenanceType(List<int> ids, bool maintenanceType);
        ProcessResult<List<JobCard>> GetByVehicle_IDandOpen(int vehicleId);
        ProcessResult<List<JobCard>> GetByVehicle_ID(int vehicleId);
        ProcessResult<List<UsedMaterials>> GetDailyWorkOfGarage(DateTime? date);
        ProcessResult<PaginatedItems<JobCard>> GetDailyReceipt(DateTime? date, PaginatedItems<JobCard> paginatedItems);
        ProcessResult<PaginatedItems<JobCard>> GetDailyBroughtReceipt(DateTime? date, PaginatedItems<JobCard> paginatedItems);
        ProcessResult<PaginatedItems<JobCard>> GetDailyReceiptExceptBrought(DateTime? date, PaginatedItems<JobCard> paginatedItems);
        ProcessResult<List<JobCard>> SearchByID_CustomerType_Desc(int vehicleId, string word);
        ProcessResult<bool> IsValidVehicleOdoMeter(double currentAudoMeter, int vehicleId);
        ProcessResult<bool> CanAddJobCard(int vehicleId);
        ProcessResult<List<ChartViewModel>> GetChart();
        ProcessResult<List<JobCard>> GetAllClosed(int vehicleId = 0);
        ProcessResult<List<JobCard>> GetAllExceptClosed(int vehicleId = 0);
        ProcessResult<PaginatedItems<JobCard>> GetAllPaginatedExceptClosed(int vehicleId = 0, PaginatedItems<JobCard> paginatedItems = null);
        ProcessResult<PaginatedItems<JobCard>> GetAllPaginatedClosed(int vehicleId = 0, PaginatedItems<JobCard> paginatedItems = null);
        ProcessResult<List<JobCard>> SearchActiveJobCards(int vehicleId, string searchKey);
        ProcessResult<List<JobCard>> SearchDeActiveJobCards(int vehicleId, string searchKey);
        ProcessResult<int> Count();
        ProcessResult<bool> ApproveJobCard(int jobCardId, bool approve);

        // Mohammed Osman
        ProcessResult<List<JobCard>> SearchJobCards(string customerType, DateTime? dateIn, DateTime? dateOut,
            int? serviceTypeId, int? jobCardStatusId, string vehiclePlateNo, string vehicleFleetNo, int? jobCardId);

        // Mohammed Osman
        ProcessResult<List<JobCard>> GetDailyReceiptReport(string vehiclePlateNo, DateTime? startDate, DateTime? endDate);
    }
}
