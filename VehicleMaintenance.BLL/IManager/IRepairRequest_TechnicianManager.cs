﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IRepairRequest_TechnicianManager : IRepository<RepairRequestTechnician>
    {
        ProcessResult<List<RepairRequestTechnician>> GetByRepairRequestID(int jobcardId);
        ProcessResult<List<RepairRequestTechnician>> GetRepairRequestsByTechniciansAndDate(int? TechnicianId,DateTime? startDate,DateTime? endDate);

    }
}
