﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IUserManager
    {
        Task<User> AddUserAsync(User user, string password);
        Task<bool> AddUserToRolesAsync(User user);
        Task AddUsersAsync(List<Tuple<User, string>> users);
        Task<User> GetBy(string username);
        Task<IList<string>> GetRolesAsync(User user);
        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsEmailExistAsync(string email);
        Task<IList<User>> GetUsersInRole(string roleName);
        Task<bool> IsUserInRole(string userName, string roleName);
        Task<List<User>> GetAll();
        ProcessResult<int> Count();
        Task<bool> DeleteAsync(User user);
        Task<bool> DeleteByIdAsunc(string id);
        Task<User> UpdateUserAsync(User user);
        Task<bool> DeleteAsync(string username);
        bool IsPhoneExist(string phone);
        Task<List<User>> Search(string searchToken, string[] searchFields);
        Task<User> Get(string id);
        Task<IList<Claim>> GetClaimsAsync(User user);

        Task<bool> Deactivate(string username);
        Task<bool> IsUserDeactivated(string username);
        Task<bool> Activate(string username);

        Task<string> GenerateForgetPasswordToken(string username);
        Task<bool> ChangePassword(string username, string newPassword, string changePasswordToken);
        Task<bool> ResetPassword(string username, string newPassword);
        Task<bool> UpdateUserPassword(string userId, string newPassword);
        Task<string> GeneratePhoneNumberToken(string username, string phone);
        Task<bool> CheckPhoneValidationToken(string username, string phone, string token);
        Task<bool> IsPhoneConfirmed(string username);
        Task<bool> ConfirmPhone(string username, string phone, string token);

        Task<bool> SetAuthentiacationToken(User user, string token);
        Task<bool> RemoveAuthenticationToken(User user);
        Task<string> GetAuthenticationToken(User user);
    }
}
