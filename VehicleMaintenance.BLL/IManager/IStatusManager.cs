﻿using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IStatusManager : IRepository<Status>
    {
        ProcessResult<Status> GetByNameEN(string nameEN);
    }
}
