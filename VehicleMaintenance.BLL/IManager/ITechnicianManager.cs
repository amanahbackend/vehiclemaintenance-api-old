﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface ITechnicianManager : IRepository<Technician>
    {
        ProcessResult<List<Technician>> SearchByName_EmployeId(string word);

    }
}
