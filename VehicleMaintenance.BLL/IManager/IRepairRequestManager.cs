﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IRepairRequestManager : IRepository<RepairRequest>
    {
        ProcessResult<List<RepairRequest>> GetByJobCardID(int jobcardId);
        ProcessResult<List<RepairRequest>> GetByJobCardIDs(List<int> jobcardIds);
        ProcessResult<List<RepairRequest>> GetByMaintenanceType(bool type);
        ProcessResult<List<RepairRequest>> GetByexternalandMaintenanceType(bool external, bool type);
        ProcessResult<List<RepairRequest>> GetNonApprovedrepairrequests();
        ProcessResult<List<RepairRequest>> SearchByID_JobcardID(string word);
        ProcessResult<List<ChartViewModel>> GetChart();
        ProcessResult<int> Count();
        ProcessResult<bool> ApproveRepairRequest(int repairRequestId, bool Approve);
    }
}
