﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
   public interface IWeekendManager : IRepository<Weekend>
    {
        ProcessResult<List<Weekend>> GetLast(DateTime date);
    }
}
