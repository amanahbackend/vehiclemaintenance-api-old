﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IRoleManager
    {
        Task<Role> AddRoleAsync(Role Role);
        Task AddRolesAsync(List<Role> Roles);
        Task<bool> Delete(string roleName);
        Task<Role> Update(Role role);
        List<Role> GetAll();
        Task<Role> GetByName(string roleName);
    }
}
