using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IJunkUserManager : IIdentityRepository<JunkUser>
    {
        ProcessResult<bool> DeleteByUsername(string username);
        ProcessResult<JunkUser> GetJunkUser(User applicationUser);
    }
}
