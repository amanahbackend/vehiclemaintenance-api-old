﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IVendorManager : IRepository<Vendor>
    {
    }
}
