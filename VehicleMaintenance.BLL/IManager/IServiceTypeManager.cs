﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.Models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IServiceTypeManager : IRepository<ServiceType>
    {
        ProcessResult<List<ServiceType>> GetByMaintenanceType(bool type);
    }
}
