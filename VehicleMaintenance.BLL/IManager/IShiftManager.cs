﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.Models.Entities.Common;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IShiftManager : IRepository<Shift>
    {
        ProcessResult<List<Shift>> GetList();
    }
}
