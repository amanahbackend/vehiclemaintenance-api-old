﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
   public interface IDriverManager: IRepository<Driver>
    {
        ProcessResult<List<Driver>> SearchByName_EmployeeNum(string word);
    }
}
