﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.BLL.IManager
{
    public interface IVehicleTypeManager : IRepository<VehicleType>
    {
        ProcessResult<List<VehicleType>> SearchByName(string word);
    }
}
