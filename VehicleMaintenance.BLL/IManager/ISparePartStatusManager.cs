﻿using DispatchProduct.RepositoryModule;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface ISparePartStatusManager : IRepository<SparePartStatus>
    {
        SparePartStatus GetByName(string name);
    }
}
