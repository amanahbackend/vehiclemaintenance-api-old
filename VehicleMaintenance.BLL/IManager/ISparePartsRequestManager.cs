﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
    public interface ISparePartsRequestManager : IRepository<SparePartsRequest>
    {
        ProcessResult<List<SparePartsRequest>> GetByRepairRequestID(int repairRequestId);

        ProcessResult<List<SparePartsRequest>> SearchSparePartsRequests(int? inventoryId, DateTime? startDate, DateTime? endDate);

        ProcessResult<List<SparePartsRequest>> GetByRepairRequestIDs(List<int?> repairrequestIds);

        ProcessResult<List<SparePartsRequest>> GetNonApproved();

        ProcessResult<List<SparePartsRequest>> GetApproved();

        ProcessResult<bool> ApproveSparePart(int sparePartId, bool Approve);
    }
}
