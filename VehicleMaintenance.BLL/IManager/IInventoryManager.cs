﻿using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.BLL.IManager
{
   public interface IInventoryManager : IRepository<Inventory>
    {
        ProcessResult<List<Inventory>> GetBySparePartName(string Name);
        ProcessResult<List<Inventory>> Search(string word);
        ProcessResult<List<Inventory>> SearchByName_SerialNum_Desc(string word);
    }
}
