﻿namespace VehicleMaintenance.BLL.Utilities
{
    public static class Pagination
    {
        public static int GetRowsCountToSkip(int pageNo, ref int pageSize)
        {
            if (pageNo <= 0)
            {
                pageNo = 1;
            }

            if (pageSize <= 0)
            {
                pageSize = 20;
            }

            int rowsCountToSkip = (pageNo - 1) * pageSize;

            return rowsCountToSkip;
        }
    }
}
