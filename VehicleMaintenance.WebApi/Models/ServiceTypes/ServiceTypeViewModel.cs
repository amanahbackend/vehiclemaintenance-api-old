﻿namespace VehicleMaintenance.WebApi.Models.ServiceTypes
{
    public class ServiceTypeViewModel : BaseViewModel
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }
        
        public int Period { get; set; }
    }
}
