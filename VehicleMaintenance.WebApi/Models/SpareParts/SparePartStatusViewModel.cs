﻿namespace VehicleMaintenance.WebApi.Models.SpareParts
{
    public class SparePartStatusViewModel : BaseViewModel
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }
    }
}
