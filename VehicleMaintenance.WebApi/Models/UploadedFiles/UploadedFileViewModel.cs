﻿namespace VehicleMaintenance.WebApi.Models.UploadedFiles
{
    public class UploadedFileViewModel
    {
        /// <summary>
        /// Value like "82c19b61-ef64-41dc-b3f1-fd1b63be6cba.jpg".
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Value like "C:/WebsiteService/wwwroot/Data/JobCards/82c19b61-ef64-41dc-b3f1-fd1b63be6cba.jpg".
        /// </summary>
        public string FileFullPath { get; set; }

        /// <summary>
        /// Value like "http://localhost:2730/Data/JobCards/82c19b61-ef64-41dc-b3f1-fd1b63be6cba.jpg".
        /// </summary>
        public string FileUrl { get; set; }

        /// <summary>
        /// Value like "/Data/JobCards/82c19b61-ef64-41dc-b3f1-fd1b63be6cba.jpg".
        /// </summary>
        public string FileRelativePath { get; set; }

        /// <summary>
        /// Value like "image/jpeg"
        /// </summary>
        public string FileType { get; set; }

        public bool? IsDefault { get; set; }

        // Foreign keys.
        public int? JobCardId { get; set; }
        public int? RepairRequestVendorId { get; set; }
    }
}
