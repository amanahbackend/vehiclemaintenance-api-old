﻿using System;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Security.Users;

namespace VehicleMaintenance.WebApi.Models
{
    public class BaseViewModel
    {
        public int Id { get; set; }

        public int? RowStatusId { get; set; }

        public string RowStatus
        {
            get
            {
                if (!RowStatusId.HasValue)
                {
                    return string.Empty;
                }

                string rowStatus;

                switch (RowStatusId.Value)
                {
                    case 1:
                        rowStatus = EnumRowStatus.Active.ToString();
                        break;

                    case 0:
                        rowStatus = EnumRowStatus.Inactive.ToString();
                        break;

                    case -1:
                        rowStatus = EnumRowStatus.Deleted.ToString();
                        break;

                    default:
                        rowStatus = "Unknown Status.";
                        break;
                }

                return rowStatus;
            }
        }

        public bool Active => RowStatusId.HasValue && RowStatusId.Value == 1;

        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }

        public DateTime? CreationDate { get; set; }

        public string ModifiedByUserId { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }

        public DateTime? ModificationDate { get; set; }
    }
}
