﻿using System;

namespace VehicleMaintenance.WebApi.Models.Common
{
    public class ShiftViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }
    }
}
