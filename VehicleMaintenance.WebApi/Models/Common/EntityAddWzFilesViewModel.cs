﻿using VehicleMaintenance.WebApi.Models.UploadedFiles;

namespace VehicleMaintenance.WebApi.Models.Common
{
    public class EntityAddWzFilesViewModel
    {
        public int Id { get; set; }

        public FileListViewModel FileList { get; set; }
    }
}
