﻿namespace VehicleMaintenance.WebApi.Models.Common
{
    public class VendorViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public string Address { get; set; }
    }
}
