﻿using System;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.WebApi.Models.RepairRequests
{
    public class RepairRequestSparePartViewModel : BaseViewModel
    {
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }

        public int? JobCardId { get; set; }
        public virtual JobCard JobCard { get; set; }

        public int? VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }

        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }

        public int? DeliveredQuantity { get; set; }
        public int? Quantity { get; set; }

        public decimal? Amount { get; set; }
        public string Source { get; set; }
        public int? KiloMetersChangeSparePart { get; set; }

        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }
        public DateTime? TechnicianStartDate { get; set; }
        public DateTime? TechnicianEndDate { get; set; }

        /// <summary>
        /// Technician work in hours.
        /// </summary>
        public double TechnicianWorkHours
        {
            get
            {
                if (TechnicianStartDate == null || TechnicianEndDate == null)
                {
                    return 0;
                }

                TimeSpan timeSpan = TechnicianEndDate.Value - TechnicianStartDate.Value;

                return Math.Round(timeSpan.TotalHours, 2);
            }
        }

        public string Comments { get; set; }
    }
}
