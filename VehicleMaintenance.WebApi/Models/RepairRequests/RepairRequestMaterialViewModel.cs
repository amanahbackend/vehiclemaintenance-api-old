﻿using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.WebApi.Models.RepairRequests
{
    public class RepairRequestMaterialViewModel : BaseViewModel
    {
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }
        
        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }

        public int? SparePartStatusId { get; set; }
        public virtual SparePartStatus SparePartStatus { get; set; }

        public int? Quantity { get; set; }

        public decimal? Cost { get; set; }
        
        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }
    }
}
