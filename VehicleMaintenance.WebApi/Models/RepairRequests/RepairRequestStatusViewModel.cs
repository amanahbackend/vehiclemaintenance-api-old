﻿namespace VehicleMaintenance.WebApi.Models.RepairRequests
{
    public class RepairRequestStatusViewModel : BaseViewModel
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }
    }
}
