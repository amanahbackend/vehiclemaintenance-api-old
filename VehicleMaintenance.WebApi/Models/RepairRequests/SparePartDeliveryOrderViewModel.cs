﻿using System.Collections.Generic;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails;

namespace VehicleMaintenance.WebApi.Models.RepairRequests
{
    public class SparePartDeliveryOrderViewModel : BaseViewModel
    {
        public string RequestedBy { get; set; }

        public List<SparePartDeliveryOrderDetails> SparePartDeliveryOrderDetails { get; set; }
    }
}
