﻿namespace VehicleMaintenance.WebApi.Models.Vehicles
{
    public class VehicleTypeViewModel : BaseViewModel
    {
        public string Name { get; set; }
    }
}
