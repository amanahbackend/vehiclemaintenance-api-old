﻿using System;
using System.Collections.Generic;

namespace VehicleMaintenance.WebApi.Models.Security
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => Convert.ToString($"{FirstName} {LastName}");

        public string PhoneNumber { get; set; }
        
        public string Password { get; set; }

        public List<string> RoleNames { get; set; }

        public string CreatedByUserId { get; set; }
        public DateTime? CreationDate { get; set; }

        public string ModifiedByUserId { get; set; }
        public DateTime? ModificationDate { get; set; }
    }
}
