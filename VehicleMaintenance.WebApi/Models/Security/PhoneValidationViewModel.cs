﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.WebApi.Models.Security
{
    public class PhoneValidationViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        public string Token { get; set; }
        public string CountryCode { get; set; }
    }
}
