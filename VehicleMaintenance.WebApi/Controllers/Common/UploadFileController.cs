﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.UploadedFiles;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.WebApi.Utilities;

namespace VehicleMaintenance.WebApi.Controllers.Common
{
    [Route("api/UploadFile")]
    [Produces("application/json")]
    public class UploadFileController : BaseController
    {
        public UploadFileController(UnitOfWork unitOfWork, IMapper mapper, IHostingEnvironment hostingEnvironment) : base(unitOfWork, mapper, hostingEnvironment)
        {
        }

        // POST: api/UploadFile/UploadFiles
        [HttpPost, Route("UploadFiles")]
        public async Task<ResponseResult<bool>> UploadFiles([FromForm] FileListViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                if (!ValidateFileListViewModel(model))
                {
                    responseResult.StatusCode = -1;
                    responseResult.Message = "Invalid 'FileListViewModel' model values.";

                    return responseResult;
                }

                // string directoryPath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");

                string directoryPath = model.FilePaths[0];
                directoryPath = Directory.GetParent(directoryPath).FullName;

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                for (var index = 0; index < model.FormFiles.Count; index++)
                {
                    IFormFile formFile = model.FormFiles[index];

                    //if (formFile.Length == 0)
                    //{
                    //    continue; // Skip this file with length Zero.
                    //}

                    string filePath = model.FilePaths[index];

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(fileStream);
                    }
                }
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        private static bool ValidateFileListViewModel(FileListViewModel model)
        {
            if (model?.FormFiles == null ||
                model.FormFiles.Count == 0 ||
                model.FilePaths == null ||
                model.FormFiles.Count != model.FilePaths.Count)
            {
                return false;
            }

            return true;
        }


        [HttpGet, Route("GetUploadedFilesByJobCard/{jobCardId}")]
        public ResponseResult<List<UploadedFileViewModel>> GetUploadedFilesByJobCard([FromRoute] int jobCardId)
        {
            var responseResult = new ResponseResult<List<UploadedFileViewModel>>();

            try
            {
                List<UploadedFile> lstUploadedFiles = UnitOfWork.UploadedFiles.GetUploadedFilesByJobCard(jobCardId);
                var lstUploadedFileVm = Mapper.Map<List<UploadedFileViewModel>>(lstUploadedFiles);

                string baseUrl = UnitOfWork.Settings.ApiServiceUrl;

                foreach (UploadedFileViewModel uploadedFileViewModel in lstUploadedFileVm)
                {
                    uploadedFileViewModel.FileUrl = $"{baseUrl}{uploadedFileViewModel.FileRelativePath}";
                }

                responseResult.Data = lstUploadedFileVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}")]
        public ResponseResult<bool> Delete(int id)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                UploadedFile uploadedFile = UnitOfWork.UploadedFiles.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);

                CommonMethods.DeleteFile(UnitOfWork.Settings, uploadedFile.FileRelativePath);

                UnitOfWork.UploadedFiles.Remove(uploadedFile);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
