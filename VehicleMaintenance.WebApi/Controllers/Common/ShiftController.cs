﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Common;

namespace VehicleMaintenance.WebApi.Controllers.Common
{
    [Route("api/Shift")]
    [Produces("application/json")]
    public class ShiftController : BaseController
    {
        public ShiftController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<ShiftViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<ShiftViewModel>>();

            try
            {
                List<Shift> lstDrivers = UnitOfWork.Shifts.GetList();
                var lstShiftVm = Mapper.Map<List<ShiftViewModel>>(lstDrivers);

                responseResult.Data = lstShiftVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
