﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.ServiceTypes;

namespace VehicleMaintenance.WebApi.Controllers.ServiceTypes
{
    [Route("api/ServiceType")]
    [Produces("application/json")]
    public class ServiceTypeController : BaseController
    {
        public ServiceTypeController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [HttpGet, Route("Get")]
        public ResponseResult<List<ServiceTypeViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<ServiceTypeViewModel>>();

            try
            {
                List<ServiceType> lstServiceTypes = UnitOfWork.ServiceTypes.Find(obj => obj.RowStatusId != -1).ToList();
                var lstServiceTypeVm = Mapper.Map<List<ServiceTypeViewModel>>(lstServiceTypes);

                responseResult.Data = lstServiceTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("Get/{id}")]
        public ResponseResult<ServiceTypeViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<ServiceTypeViewModel>();

            try
            {
                ServiceType serviceType = UnitOfWork.ServiceTypes.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                ServiceTypeViewModel serviceTypeVm = Mapper.Map<ServiceTypeViewModel>(serviceType);
                responseResult.Data = serviceTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<ServiceTypeViewModel> Add([FromBody] ServiceTypeViewModel model)
        {
            var responseResult = new ResponseResult<ServiceTypeViewModel>();

            try
            {
                ServiceType serviceType = Mapper.Map<ServiceType>(model);
                serviceType.PrepareEntityForAdding();

                UnitOfWork.ServiceTypes.Add(serviceType);

                // Re-map the added model again to update Id.
                var serviceTypeVm = Mapper.Map<ServiceTypeViewModel>(serviceType);
                responseResult.Data = serviceTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] ServiceTypeViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                ServiceType serviceType = Mapper.Map<ServiceType>(model);
                serviceType.PrepareEntityForEditing(model);

                UnitOfWork.ServiceTypes.Update(serviceType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                ServiceType serviceType = UnitOfWork.ServiceTypes.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                serviceType.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.ServiceTypes.Update(serviceType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
