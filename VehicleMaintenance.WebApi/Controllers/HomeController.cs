﻿using Microsoft.AspNetCore.Mvc;

namespace VehicleMaintenance.WebApi.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
