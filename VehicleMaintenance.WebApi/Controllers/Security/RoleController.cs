﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt.Security.Roles;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Security;

namespace VehicleMaintenance.WebApi.Controllers.Security
{
    [Route("api/Role")]
    public class RoleController : Controller
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;

        public RoleController(RoleManager<ApplicationRole> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
            _mapper = mapper;
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<RoleViewModel>> Get()
        {
            ResponseResult<List<RoleViewModel>> responseResult;

            try
            {
                List<ApplicationRole> lstApplicationRoles = _roleManager.Roles.ToList();
                List<RoleViewModel> lstRoleViewModels = _mapper.Map<List<RoleViewModel>>(lstApplicationRoles);

                responseResult = new ResponseResult<List<RoleViewModel>>
                {
                    Data = lstRoleViewModels
                };
            }
            catch (Exception ex)
            {
                responseResult = new ResponseResult<List<RoleViewModel>>
                {
                    Message = ex.Message,
                    StatusCode = 0
                };
            }

            return responseResult;
        }

        [HttpPost, Route("Add")]
        public async Task<ResponseResult<RoleViewModel>> Add([FromBody] RoleViewModel roleViewModel)
        {
            ResponseResult<RoleViewModel> responseResult;

            try
            {
                ApplicationRole applicationRole = _mapper.Map<ApplicationRole>(roleViewModel);
                IdentityResult identityResult = await _roleManager.CreateAsync(applicationRole);

                if (identityResult.Succeeded)
                {
                    roleViewModel = _mapper.Map<RoleViewModel>(applicationRole);

                    responseResult = new ResponseResult<RoleViewModel>
                    {
                        Data = roleViewModel,
                        StatusCode = 1
                    };
                }
                else
                {
                    List<IdentityError> identityErrors = identityResult.Errors.ToList();
                    string error = identityErrors.Count > 0 ? identityErrors[0].Description : string.Empty;

                    responseResult = new ResponseResult<RoleViewModel>
                    {
                        Message = "An error occured while adding role. " + error,
                        StatusCode = -1
                    };
                }
            }
            catch (Exception ex)
            {
                responseResult = new ResponseResult<RoleViewModel>
                {
                    Message = ex.Message,
                    StatusCode = 0
                };
            }

            return responseResult;
        }

    }
}
