﻿using System;
using System.Collections.Generic;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.RepairRequests;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/SparePartDeliveryOrder")]
    [Produces("application/json")]
    public class SparePartDeliveryOrderController : BaseController
    {
        public SparePartDeliveryOrderController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [HttpGet, Route("Search")]
        public ResponseResult<List<SparePartDeliveryOrderViewModel>> Search(int? deliveryOrderId, DateTime? startDate, DateTime? endDate)
        {
            var responseResult = new ResponseResult<List<SparePartDeliveryOrderViewModel>>();

            try
            {
                List<SparePartDeliveryOrder> lstSparePartDeliveryOrders = UnitOfWork.SparePartDeliveryOrders.Search(deliveryOrderId, startDate, endDate);
                var lstSparePartDeliveryOrdersVm = Mapper.Map<List<SparePartDeliveryOrderViewModel>>(lstSparePartDeliveryOrders);

                responseResult.Data = lstSparePartDeliveryOrdersVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<SparePartDeliveryOrderViewModel> Add([FromBody] SparePartDeliveryOrderViewModel model)
        {
            var responseResult = new ResponseResult<SparePartDeliveryOrderViewModel>();

            try
            {
                SparePartDeliveryOrder sparePartDeliveryOrder = Mapper.Map<SparePartDeliveryOrder>(model);
                sparePartDeliveryOrder.PrepareEntityForAdding();

                if (model.SparePartDeliveryOrderDetails != null)
                {
                    foreach (var sparePartDetail in model.SparePartDeliveryOrderDetails)
                    {
                        sparePartDetail.RowStatusId = (int)EnumRowStatus.Active;
                        sparePartDetail.CreatedByUserId = sparePartDeliveryOrder.CreatedByUserId;
                        sparePartDetail.CreationDate = sparePartDeliveryOrder.CreationDate;
                    }
                }

                UnitOfWork.SparePartDeliveryOrders.Add(sparePartDeliveryOrder);

                if (model.SparePartDeliveryOrderDetails != null)
                {
                    foreach (var sparePartDetail in model.SparePartDeliveryOrderDetails)
                    {
                        var repairRequestSparePart = UnitOfWork.RepairRequestSpareParts.Get(sparePartDetail.RepairRequestSparePartId.Value);

                        if (repairRequestSparePart.DeliveredQuantity == null)
                        {
                            repairRequestSparePart.DeliveredQuantity = 0;
                        }

                        repairRequestSparePart.DeliveredQuantity += sparePartDetail.DeliveredQty;

                        UnitOfWork.RepairRequestSpareParts.Update(repairRequestSparePart);
                    }
                }

                // Re-map the added model again to update Id.
                var sparePartDeliveryOrderVm = Mapper.Map<SparePartDeliveryOrderViewModel>(sparePartDeliveryOrder);
                sparePartDeliveryOrderVm.SparePartDeliveryOrderDetails = null;

                responseResult.Data = sparePartDeliveryOrderVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
