﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.RepairRequests;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/RepairRequest")]
    [Produces("application/json")]
    public class RepairRequestController : BaseController
    {
        public RepairRequestController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [HttpGet, Route("GetRepairRequest/{id}")]
        public ResponseResult<RepairRequestViewModel> GetRepairRequest(int id)
        {
            var responseResult = new ResponseResult<RepairRequestViewModel>();

            try
            {
                RepairRequest repairRequest = UnitOfWork.RepairRequests.GetRepairRequest(id);
                RepairRequestViewModel repairRequestVm = Mapper.Map<RepairRequestViewModel>(repairRequest);
                responseResult.Data = repairRequestVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetByJobCardId/{jobCardId}")]
        public ResponseResult<List<RepairRequestViewModel>> GetByJobCardId([FromRoute] int jobCardId)
        {
            var responseResult = new ResponseResult<List<RepairRequestViewModel>>();

            try
            {
                List<RepairRequest> lstRepairRequests = UnitOfWork.RepairRequests.GetByJobCardId(jobCardId);
                var lstRepairRequestVm = Mapper.Map<List<RepairRequestViewModel>>(lstRepairRequests);

                responseResult.Data = lstRepairRequestVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetPendingRepairRequests")]
        public ResponseResult<List<RepairRequestViewModel>> GetPendingRepairRequests()
        {
            var responseResult = new ResponseResult<List<RepairRequestViewModel>>();

            try
            {
                List<RepairRequest> lstRepairRequests = UnitOfWork.RepairRequests.GetPendingRepairRequests();
                var lstRepairRequestVm = Mapper.Map<List<RepairRequestViewModel>>(lstRepairRequests);

                responseResult.Data = lstRepairRequestVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("OpenRepairRequestsCount")]
        public ResponseResult<int> OpenRepairRequestsCount()
        {
            var responseResult = new ResponseResult<int>();

            try
            {
                int repairRequestsCount = UnitOfWork.RepairRequests.GetOpenRepairRequestsCount();
                responseResult.Data = repairRequestsCount;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<RepairRequestViewModel> Add([FromBody] RepairRequestViewModel model)
        {
            var responseResult = new ResponseResult<RepairRequestViewModel>();

            try
            {
                RepairRequest repairRequest = Mapper.Map<RepairRequest>(model);
                repairRequest.PrepareEntityForAdding();
                
                bool isRepairRequestNeedsApprove = UnitOfWork.RepairRequestTypes.Get(model.RepairRequestTypeId.Value).NeedApprove ?? false;

                if (isRepairRequestNeedsApprove)
                {
                    repairRequest.RepairRequestStatusId = (int)EnumRepairRequestStatus.Pending;
                }

                UnitOfWork.RepairRequests.Add(repairRequest);

                // Re-map the added model again to update Id.
                var repairRequestVm = Mapper.Map<RepairRequestViewModel>(repairRequest);
                responseResult.Data = repairRequestVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] RepairRequestViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequest repairRequest = UnitOfWork.RepairRequests.Get(model.Id);

                // Set fields to be updated.
                repairRequest.PrepareEntityForEditing(model);
                repairRequest.RepairRequestStatusId = model.RepairRequestStatusId;

                if (model.ServiceTypeId.HasValue)
                {
                    repairRequest.ServiceTypeId = model.ServiceTypeId;
                }

                if (model.RepairRequestTypeId.HasValue)
                {
                    repairRequest.RepairRequestTypeId = model.RepairRequestTypeId;
                }

                if (!string.IsNullOrWhiteSpace(model.Comment))
                {
                    repairRequest.Comment = model.Comment.Trim();
                }

                UnitOfWork.RepairRequests.Update(repairRequest);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                var entity = UnitOfWork.RepairRequests.Get(id);
                entity.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.RepairRequests.Update(entity);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
