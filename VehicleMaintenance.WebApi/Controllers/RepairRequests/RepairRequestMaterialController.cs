﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.RepairRequests;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/RepairRequestMaterial")]
    [Produces("application/json")]
    public class RepairRequestMaterialController : BaseController
    {
        public RepairRequestMaterialController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("GetByRepairRequestId/{repairRequestId}")]
        public ResponseResult<List<RepairRequestMaterialViewModel>> GetByRepairRequestId([FromRoute] int repairRequestId)
        {
            var responseResult = new ResponseResult<List<RepairRequestMaterialViewModel>>();

            try
            {
                List<RepairRequestMaterial> lstRepairRequestMaterials = UnitOfWork.RepairRequestMaterials.GetByRepairRequestId(repairRequestId);
                var lstRepairRequestMaterialsVm = Mapper.Map<List<RepairRequestMaterialViewModel>>(lstRepairRequestMaterials);

                responseResult.Data = lstRepairRequestMaterialsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPost, Route("Add")]
        public ResponseResult<RepairRequestMaterialViewModel> Add([FromBody] RepairRequestMaterialViewModel model)
        {
            var responseResult = new ResponseResult<RepairRequestMaterialViewModel>();

            try
            {
                RepairRequestMaterial repairRequestMaterial = Mapper.Map<RepairRequestMaterial>(model);
                repairRequestMaterial.PrepareEntityForAdding();

                UnitOfWork.RepairRequestMaterials.Add(repairRequestMaterial);

                // Re-map the added model again to update Id.
                var repairRequestMaterialVm = Mapper.Map<RepairRequestMaterialViewModel>(repairRequestMaterial);
                responseResult.Data = repairRequestMaterialVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] RepairRequestMaterialViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestMaterial repairRequestMaterial = UnitOfWork.RepairRequestMaterials.Get(model.Id);

                // Set fields to be updated.
                repairRequestMaterial.PrepareEntityForEditing(model);

                if (model.Cost.HasValue)
                {
                    repairRequestMaterial.Cost = model.Cost;
                }

                // Technician.
                if (model.TechnicianId.HasValue)
                {
                    repairRequestMaterial.TechnicianId = model.TechnicianId;
                }

                if (model.SparePartStatusId.HasValue)
                {
                    repairRequestMaterial.SparePartStatusId = model.SparePartStatusId;
                }

                // Note: RepairRequestMaterial.SparePartId shouldn't be modified.

                UnitOfWork.RepairRequestMaterials.Update(repairRequestMaterial);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                var entity = UnitOfWork.RepairRequestMaterials.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                entity.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.RepairRequestMaterials.Update(entity);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
