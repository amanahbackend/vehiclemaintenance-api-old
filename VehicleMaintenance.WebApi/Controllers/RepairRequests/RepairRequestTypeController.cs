﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.RepairRequests;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/RepairRequestType")]
    [Produces("application/json")]
    public class RepairRequestTypeController : BaseController
    {
        public RepairRequestTypeController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<RepairRequestTypeViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<RepairRequestTypeViewModel>>();

            try
            {
                List<RepairRequestType> lstRepairRequestTypes = UnitOfWork.RepairRequestTypes.Find(obj => obj.RowStatusId != -1).ToList();
                var lstRepairRequestTypeVm = Mapper.Map<List<RepairRequestTypeViewModel>>(lstRepairRequestTypes);

                responseResult.Data = lstRepairRequestTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<RepairRequestTypeViewModel> Add([FromBody] RepairRequestTypeViewModel model)
        {
            var responseResult = new ResponseResult<RepairRequestTypeViewModel>();

            try
            {
                RepairRequestType repairRequestType = Mapper.Map<RepairRequestType>(model);
                repairRequestType.PrepareEntityForAdding();

                UnitOfWork.RepairRequestTypes.Add(repairRequestType);

                // Re-map the added model again to update Id.
                var repairRequestTypeVm = Mapper.Map<RepairRequestTypeViewModel>(repairRequestType);
                responseResult.Data = repairRequestTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] RepairRequestTypeViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestType repairRequestType = Mapper.Map<RepairRequestType>(model);
                repairRequestType.PrepareEntityForEditing(model);

                UnitOfWork.RepairRequestTypes.Update(repairRequestType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestType repairRequestType = UnitOfWork.RepairRequestTypes.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                repairRequestType.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.RepairRequestTypes.Update(repairRequestType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
