﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Common;
using VehicleMaintenance.WebApi.Models.RepairRequests;
using VehicleMaintenance.WebApi.Utilities;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/RepairRequestVendor")]
    [Produces("application/json")]
    public class RepairRequestVendorController : BaseController
    {
        public RepairRequestVendorController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [Route("GetByRepairRequest/{repairRequestId}")]
        public ResponseResult<List<RepairRequestVendorViewModel>> GetByRepairRequest([FromRoute] int repairRequestId)
        {
            var responseResult = new ResponseResult<List<RepairRequestVendorViewModel>>();

            try
            {
                List<RepairRequestVendor> lstRepairRequestVendors = UnitOfWork.RepairRequestVendors.GetByRepairRequest(repairRequestId);

                // Set the urls of the uploaded files.
                string baseUrl = UnitOfWork.Settings.ApiServiceUrl;

                foreach (RepairRequestVendor repairRequestVendor in lstRepairRequestVendors)
                {
                    foreach (UploadedFile uploadedFile in repairRequestVendor.Files)
                    {
                        uploadedFile.FileUrl = $"{baseUrl}{uploadedFile.FileRelativePath}";
                    }
                }
                
                var lstRepairRequestVendorVm = Mapper.Map<List<RepairRequestVendorViewModel>>(lstRepairRequestVendors);

                responseResult.Data = lstRepairRequestVendorVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<EntityAddWzFilesViewModel> Add([FromBody] RepairRequestVendorViewModel model)
        {
            var responseResult = new ResponseResult<EntityAddWzFilesViewModel>();

            try
            {
                const string dirRelativePath = "/Data/RepairRequests";
                CommonMethods.PrepareUploadedFilesData(UnitOfWork.Settings, dirRelativePath, model.Files, model.CreatedByUserId);

                RepairRequestVendor repairRequestVendor = Mapper.Map<RepairRequestVendor>(model);
                repairRequestVendor.PrepareEntityForAdding();

                UnitOfWork.RepairRequestVendors.Add(repairRequestVendor);

                EntityAddWzFilesViewModel entityAddWzFilesViewModel = CommonMethods.GetEntityAddWzFilesViewModel(repairRequestVendor.Id, model.Files);

                responseResult.Data = entityAddWzFilesViewModel;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}")]
        public ResponseResult<bool> Delete(int id)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestVendor repairRequestVendor = UnitOfWork.RepairRequestVendors.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);

                UnitOfWork.RepairRequestVendors.Remove(repairRequestVendor);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
