﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.SpareParts;

namespace VehicleMaintenance.WebApi.Controllers.SpareParts
{
    [Route("api/SparePartStatus")]
    [Produces("application/json")]
    public class SparePartStatusController : BaseController
    {
        public SparePartStatusController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<SparePartStatusViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<SparePartStatusViewModel>>();

            try
            {
                List<SparePartStatus> lstSparePartStatuss = UnitOfWork.SparePartStatuses.Find(obj => obj.RowStatusId != -1).ToList();
                var lstSparePartStatusVm = Mapper.Map<List<SparePartStatusViewModel>>(lstSparePartStatuss);

                responseResult.Data = lstSparePartStatusVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
