﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.SpareParts;

namespace VehicleMaintenance.WebApi.Controllers.SpareParts
{
    [Route("api/SparePart")]
    [Produces("application/json")]
    public class SparePartController : BaseController
    {
        public SparePartController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [HttpGet, Route("Get")]
        public ResponseResult<List<SparePartViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<SparePartViewModel>>();

            try
            {
                List<SparePart> lstSpareParts = UnitOfWork.SpareParts.Find(obj => obj.RowStatusId != -1).ToList();
                var lstSparePartVm = Mapper.Map<List<SparePartViewModel>>(lstSpareParts);

                responseResult.Data = lstSparePartVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("Get/{id}")]
        public ResponseResult<SparePartViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<SparePartViewModel>();

            try
            {
                SparePart sparePart = UnitOfWork.SpareParts.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                SparePartViewModel sparePartVm = Mapper.Map<SparePartViewModel>(sparePart);
                responseResult.Data = sparePartVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("Search")]
        public ResponseResult<List<SparePartViewModel>> Search(string name, string serialNo, string category)
        {
            var responseResult = new ResponseResult<List<SparePartViewModel>>();

            try
            {
                List<SparePart> lstSpareParts = UnitOfWork.SpareParts.Search(name, serialNo, category);
                var lstSparePartVm = Mapper.Map<List<SparePartViewModel>>(lstSpareParts);

                responseResult.Data = lstSparePartVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
        

        [HttpPost, Route("Add")]
        public ResponseResult<SparePartViewModel> Add([FromBody] SparePartViewModel model)
        {
            var responseResult = new ResponseResult<SparePartViewModel>();

            try
            {
                SparePart sparePart = Mapper.Map<SparePart>(model);
                sparePart.PrepareEntityForAdding();

                UnitOfWork.SpareParts.Add(sparePart);

                // Re-map the added model again to update Id.
                var sparePartVm = Mapper.Map<SparePartViewModel>(sparePart);
                responseResult.Data = sparePartVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] SparePartViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                SparePart sparePart = Mapper.Map<SparePart>(model);
                sparePart.PrepareEntityForEditing(model);

                UnitOfWork.SpareParts.Update(sparePart);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                SparePart sparePart = UnitOfWork.SpareParts.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                sparePart.PrepareEntityForDeleting(deletedByUserId);
                
                UnitOfWork.SpareParts.Update(sparePart);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
