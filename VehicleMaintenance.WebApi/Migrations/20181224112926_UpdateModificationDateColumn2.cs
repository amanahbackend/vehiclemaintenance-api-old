﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class UpdateModificationDateColumn2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "AspNetUsers",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "AspNetRoles",
                newName: "ModificationDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "AspNetUsers",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "AspNetRoles",
                newName: "ModificaitonDate");
        }
    }
}
