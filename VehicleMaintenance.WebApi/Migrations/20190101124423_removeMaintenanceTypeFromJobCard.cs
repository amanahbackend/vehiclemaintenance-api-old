﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class removeMaintenanceTypeFromJobCard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobCard_Vehicle_VehicleId",
                table: "JobCard");

            migrationBuilder.DropColumn(
                name: "MaintenanceType",
                table: "JobCard");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleId",
                table: "JobCard",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCard_Vehicle_VehicleId",
                table: "JobCard",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobCard_Vehicle_VehicleId",
                table: "JobCard");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleId",
                table: "JobCard",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "MaintenanceType",
                table: "JobCard",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCard_Vehicle_VehicleId",
                table: "JobCard",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
