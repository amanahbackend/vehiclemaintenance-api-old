﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class AddColCreatedByUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobCard_AspNetUsers_UserId",
                table: "JobCard");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequest_AspNetUsers_UserId",
                table: "RepairRequest");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RepairRequest");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "JobCard");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "Weekend",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Weekend",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "Vendor",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Vendor",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "VehicleType",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "VehicleType",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "Vehicle",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Vehicle",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "Technician",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Technician",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "SparePartStatus",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "SparePartStatus",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "SparePart",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "SparePart",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "Shift",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Shift",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "ServiceType",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "ServiceType",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "RepairRequestType",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "RepairRequestType",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "RepairRequestTechnician",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "RepairRequestTechnician",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "RepairRequestStatus",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "RepairRequestStatus",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "RepairRequestSparePart",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "RepairRequestSparePart",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "RepairRequestMaterial",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "RepairRequestMaterial",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "RepairRequest",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "RepairRequest",
                newName: "CreatedByUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RepairRequest_UserId",
                table: "RepairRequest",
                newName: "IX_RepairRequest_ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "PasswordTokenPin",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "PasswordTokenPin",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "JobCardStatus",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "JobCardStatus",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "JobCard",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "JobCard",
                newName: "CreatedByUserId");

            migrationBuilder.RenameIndex(
                name: "IX_JobCard_UserId",
                table: "JobCard",
                newName: "IX_JobCard_ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "File",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "File",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "Driver",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Driver",
                newName: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Weekend_CreatedByUserId",
                table: "Weekend",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Weekend_ModifiedByUserId",
                table: "Weekend",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendor_CreatedByUserId",
                table: "Vendor",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendor_ModifiedByUserId",
                table: "Vendor",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_CreatedByUserId",
                table: "VehicleType",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_ModifiedByUserId",
                table: "VehicleType",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_CreatedByUserId",
                table: "Vehicle",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_ModifiedByUserId",
                table: "Vehicle",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Technician_CreatedByUserId",
                table: "Technician",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Technician_ModifiedByUserId",
                table: "Technician",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartStatus_CreatedByUserId",
                table: "SparePartStatus",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartStatus_ModifiedByUserId",
                table: "SparePartStatus",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePart_CreatedByUserId",
                table: "SparePart",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePart_ModifiedByUserId",
                table: "SparePart",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Shift_CreatedByUserId",
                table: "Shift",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Shift_ModifiedByUserId",
                table: "Shift",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceType_CreatedByUserId",
                table: "ServiceType",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceType_ModifiedByUserId",
                table: "ServiceType",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestType_CreatedByUserId",
                table: "RepairRequestType",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestType_ModifiedByUserId",
                table: "RepairRequestType",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestTechnician_CreatedByUserId",
                table: "RepairRequestTechnician",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestTechnician_ModifiedByUserId",
                table: "RepairRequestTechnician",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestStatus_CreatedByUserId",
                table: "RepairRequestStatus",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestStatus_ModifiedByUserId",
                table: "RepairRequestStatus",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_CreatedByUserId",
                table: "RepairRequestSparePart",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_ModifiedByUserId",
                table: "RepairRequestSparePart",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_CreatedByUserId",
                table: "RepairRequestMaterial",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_ModifiedByUserId",
                table: "RepairRequestMaterial",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_CreatedByUserId",
                table: "RepairRequest",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordTokenPin_CreatedByUserId",
                table: "PasswordTokenPin",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordTokenPin_ModifiedByUserId",
                table: "PasswordTokenPin",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCardStatus_CreatedByUserId",
                table: "JobCardStatus",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCardStatus_ModifiedByUserId",
                table: "JobCardStatus",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_CreatedByUserId",
                table: "JobCard",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_File_CreatedByUserId",
                table: "File",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_File_ModifiedByUserId",
                table: "File",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_CreatedByUserId",
                table: "Driver",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_ModifiedByUserId",
                table: "Driver",
                column: "ModifiedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_AspNetUsers_CreatedByUserId",
                table: "Driver",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_AspNetUsers_ModifiedByUserId",
                table: "Driver",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_File_AspNetUsers_CreatedByUserId",
                table: "File",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_File_AspNetUsers_ModifiedByUserId",
                table: "File",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCard_AspNetUsers_CreatedByUserId",
                table: "JobCard",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCard_AspNetUsers_ModifiedByUserId",
                table: "JobCard",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCardStatus_AspNetUsers_CreatedByUserId",
                table: "JobCardStatus",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCardStatus_AspNetUsers_ModifiedByUserId",
                table: "JobCardStatus",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PasswordTokenPin_AspNetUsers_CreatedByUserId",
                table: "PasswordTokenPin",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PasswordTokenPin_AspNetUsers_ModifiedByUserId",
                table: "PasswordTokenPin",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequest_AspNetUsers_CreatedByUserId",
                table: "RepairRequest",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequest_AspNetUsers_ModifiedByUserId",
                table: "RepairRequest",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestMaterial_AspNetUsers_CreatedByUserId",
                table: "RepairRequestMaterial",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestMaterial_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestMaterial",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestSparePart_AspNetUsers_CreatedByUserId",
                table: "RepairRequestSparePart",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestSparePart_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestSparePart",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestStatus_AspNetUsers_CreatedByUserId",
                table: "RepairRequestStatus",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestStatus_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestStatus",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestTechnician_AspNetUsers_CreatedByUserId",
                table: "RepairRequestTechnician",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestTechnician_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestTechnician",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestType_AspNetUsers_CreatedByUserId",
                table: "RepairRequestType",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestType_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestType",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceType_AspNetUsers_CreatedByUserId",
                table: "ServiceType",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceType_AspNetUsers_ModifiedByUserId",
                table: "ServiceType",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_AspNetUsers_CreatedByUserId",
                table: "Shift",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_AspNetUsers_ModifiedByUserId",
                table: "Shift",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SparePart_AspNetUsers_CreatedByUserId",
                table: "SparePart",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SparePart_AspNetUsers_ModifiedByUserId",
                table: "SparePart",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SparePartStatus_AspNetUsers_CreatedByUserId",
                table: "SparePartStatus",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SparePartStatus_AspNetUsers_ModifiedByUserId",
                table: "SparePartStatus",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Technician_AspNetUsers_CreatedByUserId",
                table: "Technician",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Technician_AspNetUsers_ModifiedByUserId",
                table: "Technician",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_AspNetUsers_CreatedByUserId",
                table: "Vehicle",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_AspNetUsers_ModifiedByUserId",
                table: "Vehicle",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleType_AspNetUsers_CreatedByUserId",
                table: "VehicleType",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleType_AspNetUsers_ModifiedByUserId",
                table: "VehicleType",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vendor_AspNetUsers_CreatedByUserId",
                table: "Vendor",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vendor_AspNetUsers_ModifiedByUserId",
                table: "Vendor",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Weekend_AspNetUsers_CreatedByUserId",
                table: "Weekend",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Weekend_AspNetUsers_ModifiedByUserId",
                table: "Weekend",
                column: "ModifiedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Driver_AspNetUsers_CreatedByUserId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_AspNetUsers_ModifiedByUserId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_File_AspNetUsers_CreatedByUserId",
                table: "File");

            migrationBuilder.DropForeignKey(
                name: "FK_File_AspNetUsers_ModifiedByUserId",
                table: "File");

            migrationBuilder.DropForeignKey(
                name: "FK_JobCard_AspNetUsers_CreatedByUserId",
                table: "JobCard");

            migrationBuilder.DropForeignKey(
                name: "FK_JobCard_AspNetUsers_ModifiedByUserId",
                table: "JobCard");

            migrationBuilder.DropForeignKey(
                name: "FK_JobCardStatus_AspNetUsers_CreatedByUserId",
                table: "JobCardStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_JobCardStatus_AspNetUsers_ModifiedByUserId",
                table: "JobCardStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_PasswordTokenPin_AspNetUsers_CreatedByUserId",
                table: "PasswordTokenPin");

            migrationBuilder.DropForeignKey(
                name: "FK_PasswordTokenPin_AspNetUsers_ModifiedByUserId",
                table: "PasswordTokenPin");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequest_AspNetUsers_CreatedByUserId",
                table: "RepairRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequest_AspNetUsers_ModifiedByUserId",
                table: "RepairRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestMaterial_AspNetUsers_CreatedByUserId",
                table: "RepairRequestMaterial");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestMaterial_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestMaterial");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestSparePart_AspNetUsers_CreatedByUserId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestSparePart_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestStatus_AspNetUsers_CreatedByUserId",
                table: "RepairRequestStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestStatus_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestTechnician_AspNetUsers_CreatedByUserId",
                table: "RepairRequestTechnician");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestTechnician_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestTechnician");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestType_AspNetUsers_CreatedByUserId",
                table: "RepairRequestType");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestType_AspNetUsers_ModifiedByUserId",
                table: "RepairRequestType");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceType_AspNetUsers_CreatedByUserId",
                table: "ServiceType");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceType_AspNetUsers_ModifiedByUserId",
                table: "ServiceType");

            migrationBuilder.DropForeignKey(
                name: "FK_Shift_AspNetUsers_CreatedByUserId",
                table: "Shift");

            migrationBuilder.DropForeignKey(
                name: "FK_Shift_AspNetUsers_ModifiedByUserId",
                table: "Shift");

            migrationBuilder.DropForeignKey(
                name: "FK_SparePart_AspNetUsers_CreatedByUserId",
                table: "SparePart");

            migrationBuilder.DropForeignKey(
                name: "FK_SparePart_AspNetUsers_ModifiedByUserId",
                table: "SparePart");

            migrationBuilder.DropForeignKey(
                name: "FK_SparePartStatus_AspNetUsers_CreatedByUserId",
                table: "SparePartStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_SparePartStatus_AspNetUsers_ModifiedByUserId",
                table: "SparePartStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_Technician_AspNetUsers_CreatedByUserId",
                table: "Technician");

            migrationBuilder.DropForeignKey(
                name: "FK_Technician_AspNetUsers_ModifiedByUserId",
                table: "Technician");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_AspNetUsers_CreatedByUserId",
                table: "Vehicle");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_AspNetUsers_ModifiedByUserId",
                table: "Vehicle");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleType_AspNetUsers_CreatedByUserId",
                table: "VehicleType");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleType_AspNetUsers_ModifiedByUserId",
                table: "VehicleType");

            migrationBuilder.DropForeignKey(
                name: "FK_Vendor_AspNetUsers_CreatedByUserId",
                table: "Vendor");

            migrationBuilder.DropForeignKey(
                name: "FK_Vendor_AspNetUsers_ModifiedByUserId",
                table: "Vendor");

            migrationBuilder.DropForeignKey(
                name: "FK_Weekend_AspNetUsers_CreatedByUserId",
                table: "Weekend");

            migrationBuilder.DropForeignKey(
                name: "FK_Weekend_AspNetUsers_ModifiedByUserId",
                table: "Weekend");

            migrationBuilder.DropIndex(
                name: "IX_Weekend_CreatedByUserId",
                table: "Weekend");

            migrationBuilder.DropIndex(
                name: "IX_Weekend_ModifiedByUserId",
                table: "Weekend");

            migrationBuilder.DropIndex(
                name: "IX_Vendor_CreatedByUserId",
                table: "Vendor");

            migrationBuilder.DropIndex(
                name: "IX_Vendor_ModifiedByUserId",
                table: "Vendor");

            migrationBuilder.DropIndex(
                name: "IX_VehicleType_CreatedByUserId",
                table: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_VehicleType_ModifiedByUserId",
                table: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_CreatedByUserId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_ModifiedByUserId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Technician_CreatedByUserId",
                table: "Technician");

            migrationBuilder.DropIndex(
                name: "IX_Technician_ModifiedByUserId",
                table: "Technician");

            migrationBuilder.DropIndex(
                name: "IX_SparePartStatus_CreatedByUserId",
                table: "SparePartStatus");

            migrationBuilder.DropIndex(
                name: "IX_SparePartStatus_ModifiedByUserId",
                table: "SparePartStatus");

            migrationBuilder.DropIndex(
                name: "IX_SparePart_CreatedByUserId",
                table: "SparePart");

            migrationBuilder.DropIndex(
                name: "IX_SparePart_ModifiedByUserId",
                table: "SparePart");

            migrationBuilder.DropIndex(
                name: "IX_Shift_CreatedByUserId",
                table: "Shift");

            migrationBuilder.DropIndex(
                name: "IX_Shift_ModifiedByUserId",
                table: "Shift");

            migrationBuilder.DropIndex(
                name: "IX_ServiceType_CreatedByUserId",
                table: "ServiceType");

            migrationBuilder.DropIndex(
                name: "IX_ServiceType_ModifiedByUserId",
                table: "ServiceType");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestType_CreatedByUserId",
                table: "RepairRequestType");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestType_ModifiedByUserId",
                table: "RepairRequestType");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestTechnician_CreatedByUserId",
                table: "RepairRequestTechnician");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestTechnician_ModifiedByUserId",
                table: "RepairRequestTechnician");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestStatus_CreatedByUserId",
                table: "RepairRequestStatus");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestStatus_ModifiedByUserId",
                table: "RepairRequestStatus");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestSparePart_CreatedByUserId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestSparePart_ModifiedByUserId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestMaterial_CreatedByUserId",
                table: "RepairRequestMaterial");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestMaterial_ModifiedByUserId",
                table: "RepairRequestMaterial");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequest_CreatedByUserId",
                table: "RepairRequest");

            migrationBuilder.DropIndex(
                name: "IX_PasswordTokenPin_CreatedByUserId",
                table: "PasswordTokenPin");

            migrationBuilder.DropIndex(
                name: "IX_PasswordTokenPin_ModifiedByUserId",
                table: "PasswordTokenPin");

            migrationBuilder.DropIndex(
                name: "IX_JobCardStatus_CreatedByUserId",
                table: "JobCardStatus");

            migrationBuilder.DropIndex(
                name: "IX_JobCardStatus_ModifiedByUserId",
                table: "JobCardStatus");

            migrationBuilder.DropIndex(
                name: "IX_JobCard_CreatedByUserId",
                table: "JobCard");

            migrationBuilder.DropIndex(
                name: "IX_File_CreatedByUserId",
                table: "File");

            migrationBuilder.DropIndex(
                name: "IX_File_ModifiedByUserId",
                table: "File");

            migrationBuilder.DropIndex(
                name: "IX_Driver_CreatedByUserId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_ModifiedByUserId",
                table: "Driver");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Weekend",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Weekend",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Vendor",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Vendor",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "VehicleType",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "VehicleType",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Vehicle",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Vehicle",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Technician",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Technician",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "SparePartStatus",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "SparePartStatus",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "SparePart",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "SparePart",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Shift",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Shift",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "ServiceType",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "ServiceType",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "RepairRequestType",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "RepairRequestType",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "RepairRequestTechnician",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "RepairRequestTechnician",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "RepairRequestStatus",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "RepairRequestStatus",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "RepairRequestSparePart",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "RepairRequestSparePart",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "RepairRequestMaterial",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "RepairRequestMaterial",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "RepairRequest",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "RepairRequest",
                newName: "ModifiedBy");

            migrationBuilder.RenameIndex(
                name: "IX_RepairRequest_ModifiedByUserId",
                table: "RepairRequest",
                newName: "IX_RepairRequest_UserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "PasswordTokenPin",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "PasswordTokenPin",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "JobCardStatus",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "JobCardStatus",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "JobCard",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "JobCard",
                newName: "ModifiedBy");

            migrationBuilder.RenameIndex(
                name: "IX_JobCard_ModifiedByUserId",
                table: "JobCard",
                newName: "IX_JobCard_UserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "File",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "File",
                newName: "CreatedBy");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Driver",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Driver",
                newName: "CreatedBy");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "RepairRequest",
                maxLength: 36,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "JobCard",
                maxLength: 36,
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_JobCard_AspNetUsers_UserId",
                table: "JobCard",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequest_AspNetUsers_UserId",
                table: "RepairRequest",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
