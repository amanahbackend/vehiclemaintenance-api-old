﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RepairRequestUploadFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UploadedFile_Vendor_VendorId",
                table: "UploadedFile");

            migrationBuilder.RenameColumn(
                name: "VendorId",
                table: "UploadedFile",
                newName: "RepairRequestVendorId");

            migrationBuilder.RenameColumn(
                name: "Selected",
                table: "UploadedFile",
                newName: "IsDefault");

            migrationBuilder.RenameIndex(
                name: "IX_UploadedFile_VendorId",
                table: "UploadedFile",
                newName: "IX_UploadedFile_RepairRequestVendorId");

            migrationBuilder.CreateTable(
                name: "RepairRequestVendor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    VendorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestVendor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_Vendor_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_CreatedByUserId",
                table: "RepairRequestVendor",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_ModifiedByUserId",
                table: "RepairRequestVendor",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_VendorId",
                table: "RepairRequestVendor",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_UploadedFile_RepairRequestVendor_RepairRequestVendorId",
                table: "UploadedFile",
                column: "RepairRequestVendorId",
                principalTable: "RepairRequestVendor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UploadedFile_RepairRequestVendor_RepairRequestVendorId",
                table: "UploadedFile");

            migrationBuilder.DropTable(
                name: "RepairRequestVendor");

            migrationBuilder.RenameColumn(
                name: "RepairRequestVendorId",
                table: "UploadedFile",
                newName: "VendorId");

            migrationBuilder.RenameColumn(
                name: "IsDefault",
                table: "UploadedFile",
                newName: "Selected");

            migrationBuilder.RenameIndex(
                name: "IX_UploadedFile_RepairRequestVendorId",
                table: "UploadedFile",
                newName: "IX_UploadedFile_VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_UploadedFile_Vendor_VendorId",
                table: "UploadedFile",
                column: "VendorId",
                principalTable: "Vendor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
