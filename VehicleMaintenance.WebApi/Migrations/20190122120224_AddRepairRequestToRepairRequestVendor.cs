﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class AddRepairRequestToRepairRequestVendor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UploadedFile_RepairRequest_RepairRequestId",
                table: "UploadedFile");

            migrationBuilder.DropIndex(
                name: "IX_UploadedFile_RepairRequestId",
                table: "UploadedFile");

            migrationBuilder.DropColumn(
                name: "RepairRequestId",
                table: "UploadedFile");

            migrationBuilder.AddColumn<int>(
                name: "RepairRequestId",
                table: "RepairRequestVendor",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_RepairRequestId",
                table: "RepairRequestVendor",
                column: "RepairRequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestVendor_RepairRequest_RepairRequestId",
                table: "RepairRequestVendor",
                column: "RepairRequestId",
                principalTable: "RepairRequest",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestVendor_RepairRequest_RepairRequestId",
                table: "RepairRequestVendor");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestVendor_RepairRequestId",
                table: "RepairRequestVendor");

            migrationBuilder.DropColumn(
                name: "RepairRequestId",
                table: "RepairRequestVendor");

            migrationBuilder.AddColumn<int>(
                name: "RepairRequestId",
                table: "UploadedFile",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_RepairRequestId",
                table: "UploadedFile",
                column: "RepairRequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_UploadedFile_RepairRequest_RepairRequestId",
                table: "UploadedFile",
                column: "RepairRequestId",
                principalTable: "RepairRequest",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
