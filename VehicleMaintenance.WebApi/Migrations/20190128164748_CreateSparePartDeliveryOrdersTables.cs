﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class CreateSparePartDeliveryOrdersTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeliveredQuantity",
                table: "RepairRequestSparePart",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SparePartDeliveryOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RequestedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SparePartDeliveryOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrder_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrder_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SparePartDeliveryOrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RepairRequestSparePartId = table.Column<int>(nullable: true),
                    DeliveredQty = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SparePartDeliveryOrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_RepairRequestSparePart_RepairRequestSparePartId",
                        column: x => x.RepairRequestSparePartId,
                        principalTable: "RepairRequestSparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrder_CreatedByUserId",
                table: "SparePartDeliveryOrder",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrder_ModifiedByUserId",
                table: "SparePartDeliveryOrder",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_CreatedByUserId",
                table: "SparePartDeliveryOrderDetails",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_ModifiedByUserId",
                table: "SparePartDeliveryOrderDetails",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_RepairRequestSparePartId",
                table: "SparePartDeliveryOrderDetails",
                column: "RepairRequestSparePartId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SparePartDeliveryOrder");

            migrationBuilder.DropTable(
                name: "SparePartDeliveryOrderDetails");

            migrationBuilder.DropColumn(
                name: "DeliveredQuantity",
                table: "RepairRequestSparePart");
        }
    }
}
