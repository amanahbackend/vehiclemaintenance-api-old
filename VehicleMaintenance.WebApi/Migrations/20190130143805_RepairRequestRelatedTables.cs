﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RepairRequestRelatedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RepairRequestTechnician");

            migrationBuilder.DropColumn(
                name: "Capacity",
                table: "RepairRequestMaterial");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "RepairRequestSparePart",
                newName: "TechnicianStartDate");

            migrationBuilder.RenameColumn(
                name: "EndDate",
                table: "RepairRequestSparePart",
                newName: "TechnicianEndDate");

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "RepairRequestMaterial",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "RepairRequestMaterial");

            migrationBuilder.RenameColumn(
                name: "TechnicianStartDate",
                table: "RepairRequestSparePart",
                newName: "StartDate");

            migrationBuilder.RenameColumn(
                name: "TechnicianEndDate",
                table: "RepairRequestSparePart",
                newName: "EndDate");

            migrationBuilder.AddColumn<string>(
                name: "Capacity",
                table: "RepairRequestMaterial",
                maxLength: 20,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RepairRequestTechnician",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    EndTime = table.Column<DateTime>(nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    RowStatusId = table.Column<int>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: true),
                    TechnicianId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestTechnician", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestTechnician_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestTechnician_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestTechnician_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestTechnician_Technician_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestTechnician_CreatedByUserId",
                table: "RepairRequestTechnician",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestTechnician_ModifiedByUserId",
                table: "RepairRequestTechnician",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestTechnician_RepairRequestId",
                table: "RepairRequestTechnician",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestTechnician_TechnicianId",
                table: "RepairRequestTechnician",
                column: "TechnicianId");
        }
    }
}
