﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RenameUsedMaterialToMaterials : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RepairRequestUsedMaterial");

            migrationBuilder.CreateTable(
                name: "RepairRequestMaterial",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatus = table.Column<int>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    TechnicianId = table.Column<int>(nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    SparePartId = table.Column<int>(nullable: true),
                    SparePartStatusId = table.Column<int>(nullable: true),
                    TransferFromVehicleId = table.Column<int>(nullable: true),
                    Capacity = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestMaterial", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_SparePart_SparePartId",
                        column: x => x.SparePartId,
                        principalTable: "SparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_SparePartStatus_SparePartStatusId",
                        column: x => x.SparePartStatusId,
                        principalTable: "SparePartStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_Technician_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_Vehicle_TransferFromVehicleId",
                        column: x => x.TransferFromVehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_RepairRequestId",
                table: "RepairRequestMaterial",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_SparePartId",
                table: "RepairRequestMaterial",
                column: "SparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_SparePartStatusId",
                table: "RepairRequestMaterial",
                column: "SparePartStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_TechnicianId",
                table: "RepairRequestMaterial",
                column: "TechnicianId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_TransferFromVehicleId",
                table: "RepairRequestMaterial",
                column: "TransferFromVehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RepairRequestMaterial");

            migrationBuilder.CreateTable(
                name: "RepairRequestUsedMaterial",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Capacity = table.Column<string>(maxLength: 20, nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 36, nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    RowStatus = table.Column<int>(nullable: true),
                    SparePartId = table.Column<int>(nullable: true),
                    SparePartStatusId = table.Column<int>(nullable: true),
                    TechnicianId = table.Column<int>(nullable: true),
                    TransferFromVehicleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestUsedMaterial", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestUsedMaterial_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestUsedMaterial_SparePart_SparePartId",
                        column: x => x.SparePartId,
                        principalTable: "SparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestUsedMaterial_SparePartStatus_SparePartStatusId",
                        column: x => x.SparePartStatusId,
                        principalTable: "SparePartStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestUsedMaterial_Technician_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestUsedMaterial_Vehicle_TransferFromVehicleId",
                        column: x => x.TransferFromVehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestUsedMaterial_RepairRequestId",
                table: "RepairRequestUsedMaterial",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestUsedMaterial_SparePartId",
                table: "RepairRequestUsedMaterial",
                column: "SparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestUsedMaterial_SparePartStatusId",
                table: "RepairRequestUsedMaterial",
                column: "SparePartStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestUsedMaterial_TechnicianId",
                table: "RepairRequestUsedMaterial",
                column: "TechnicianId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestUsedMaterial_TransferFromVehicleId",
                table: "RepairRequestUsedMaterial",
                column: "TransferFromVehicleId");
        }
    }
}
