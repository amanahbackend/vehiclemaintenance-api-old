﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RemoveServiceTypeFromJobCard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobCard_ServiceType_ServiceTypeId",
                table: "JobCard");

            migrationBuilder.DropIndex(
                name: "IX_JobCard_ServiceTypeId",
                table: "JobCard");

            migrationBuilder.DropColumn(
                name: "ServiceTypeId",
                table: "JobCard");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceTypeId",
                table: "JobCard",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_ServiceTypeId",
                table: "JobCard",
                column: "ServiceTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobCard_ServiceType_ServiceTypeId",
                table: "JobCard",
                column: "ServiceTypeId",
                principalTable: "ServiceType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
