﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class UpdateFileTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FK_JobCard_ID",
                table: "File");

            migrationBuilder.DropColumn(
                name: "FK_RepairRequest_ID",
                table: "File");

            migrationBuilder.RenameColumn(
                name: "FileURL",
                table: "File",
                newName: "FileUrl");

            migrationBuilder.RenameColumn(
                name: "FK_Vendor_ID",
                table: "File",
                newName: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_File_VendorId",
                table: "File",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_File_Vendor_VendorId",
                table: "File",
                column: "VendorId",
                principalTable: "Vendor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_File_Vendor_VendorId",
                table: "File");

            migrationBuilder.DropIndex(
                name: "IX_File_VendorId",
                table: "File");

            migrationBuilder.RenameColumn(
                name: "FileUrl",
                table: "File",
                newName: "FileURL");

            migrationBuilder.RenameColumn(
                name: "VendorId",
                table: "File",
                newName: "FK_Vendor_ID");

            migrationBuilder.AddColumn<int>(
                name: "FK_JobCard_ID",
                table: "File",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FK_RepairRequest_ID",
                table: "File",
                nullable: true);
        }
    }
}
