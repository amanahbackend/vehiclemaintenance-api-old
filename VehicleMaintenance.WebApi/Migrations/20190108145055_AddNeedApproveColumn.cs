﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class AddNeedApproveColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "NeedApprove",
                table: "SparePart",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NeedApprove",
                table: "RepairRequestType",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NeedApprove",
                table: "SparePart");

            migrationBuilder.DropColumn(
                name: "NeedApprove",
                table: "RepairRequestType");
        }
    }
}
