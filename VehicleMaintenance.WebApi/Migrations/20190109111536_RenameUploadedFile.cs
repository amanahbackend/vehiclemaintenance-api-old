﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RenameUploadedFile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "File");

            migrationBuilder.DropTable(
                name: "PasswordTokenPin");

            migrationBuilder.DropTable(
                name: "Weekend");

            migrationBuilder.CreateTable(
                name: "UploadedFile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    FileName = table.Column<string>(maxLength: 50, nullable: true),
                    FilePath = table.Column<string>(maxLength: 100, nullable: true),
                    FileUrl = table.Column<string>(maxLength: 100, nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    JobCardId = table.Column<int>(nullable: true),
                    VendorId = table.Column<int>(nullable: true),
                    Selected = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadedFile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UploadedFile_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_JobCard_JobCardId",
                        column: x => x.JobCardId,
                        principalTable: "JobCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_Vendor_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_CreatedByUserId",
                table: "UploadedFile",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_JobCardId",
                table: "UploadedFile",
                column: "JobCardId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_ModifiedByUserId",
                table: "UploadedFile",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_RepairRequestId",
                table: "UploadedFile",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_VendorId",
                table: "UploadedFile",
                column: "VendorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UploadedFile");

            migrationBuilder.CreateTable(
                name: "File",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    FileName = table.Column<string>(maxLength: 50, nullable: true),
                    FilePath = table.Column<string>(maxLength: 100, nullable: true),
                    FileUrl = table.Column<string>(maxLength: 100, nullable: true),
                    JobCardId = table.Column<int>(nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    RowStatusId = table.Column<int>(nullable: true),
                    Selected = table.Column<bool>(nullable: true),
                    VendorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File", x => x.Id);
                    table.ForeignKey(
                        name: "FK_File_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_JobCard_JobCardId",
                        column: x => x.JobCardId,
                        principalTable: "JobCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_Vendor_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PasswordTokenPin",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    Pin = table.Column<string>(maxLength: 50, nullable: true),
                    RowStatusId = table.Column<int>(nullable: true),
                    Token = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PasswordTokenPin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PasswordTokenPin_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PasswordTokenPin_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Weekend",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    RowStatusId = table.Column<int>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weekend", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Weekend_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Weekend_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_File_CreatedByUserId",
                table: "File",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_File_JobCardId",
                table: "File",
                column: "JobCardId");

            migrationBuilder.CreateIndex(
                name: "IX_File_ModifiedByUserId",
                table: "File",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_File_RepairRequestId",
                table: "File",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_File_VendorId",
                table: "File",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordTokenPin_CreatedByUserId",
                table: "PasswordTokenPin",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordTokenPin_ModifiedByUserId",
                table: "PasswordTokenPin",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Weekend_CreatedByUserId",
                table: "Weekend",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Weekend_ModifiedByUserId",
                table: "Weekend",
                column: "ModifiedByUserId");
        }
    }
}
