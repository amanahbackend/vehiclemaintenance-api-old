﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RemoveColTransferredFromVehicle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestMaterial_Vehicle_TransferFromVehicleId",
                table: "RepairRequestMaterial");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestMaterial_TransferFromVehicleId",
                table: "RepairRequestMaterial");

            migrationBuilder.DropColumn(
                name: "TransferFromVehicleId",
                table: "RepairRequestMaterial");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TransferFromVehicleId",
                table: "RepairRequestMaterial",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_TransferFromVehicleId",
                table: "RepairRequestMaterial",
                column: "TransferFromVehicleId");

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestMaterial_Vehicle_TransferFromVehicleId",
                table: "RepairRequestMaterial",
                column: "TransferFromVehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
