﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RemovePhone1FromAspNetUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Phone1",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                table: "AspNetUsers",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "AspNetUsers",
                newName: "CreatedByUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "AspNetUsers",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "AspNetUsers",
                newName: "CreatedBy");

            migrationBuilder.AddColumn<string>(
                name: "Phone1",
                table: "AspNetUsers",
                maxLength: 20,
                nullable: true);
        }
    }
}
