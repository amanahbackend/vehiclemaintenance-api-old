﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RemoveUnusedColumnsInAspNetUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deactivated",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Phone2",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PicturePath",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "SparePartId",
                table: "SparePartDeliveryOrderDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_SparePartId",
                table: "SparePartDeliveryOrderDetails",
                column: "SparePartId");

            migrationBuilder.AddForeignKey(
                name: "FK_SparePartDeliveryOrderDetails_SparePart_SparePartId",
                table: "SparePartDeliveryOrderDetails",
                column: "SparePartId",
                principalTable: "SparePart",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SparePartDeliveryOrderDetails_SparePart_SparePartId",
                table: "SparePartDeliveryOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_SparePartDeliveryOrderDetails_SparePartId",
                table: "SparePartDeliveryOrderDetails");

            migrationBuilder.DropColumn(
                name: "SparePartId",
                table: "SparePartDeliveryOrderDetails");

            migrationBuilder.AddColumn<bool>(
                name: "Deactivated",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone2",
                table: "AspNetUsers",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PicturePath",
                table: "AspNetUsers",
                maxLength: 100,
                nullable: true);
        }
    }
}
