﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RepairRequestSparePart : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JobCardId",
                table: "RepairRequestSparePart",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "KiloMetersChangeSparePart",
                table: "RepairRequestSparePart",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VehicleId",
                table: "RepairRequestSparePart",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_JobCardId",
                table: "RepairRequestSparePart",
                column: "JobCardId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_VehicleId",
                table: "RepairRequestSparePart",
                column: "VehicleId");

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestSparePart_JobCard_JobCardId",
                table: "RepairRequestSparePart",
                column: "JobCardId",
                principalTable: "JobCard",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RepairRequestSparePart_Vehicle_VehicleId",
                table: "RepairRequestSparePart",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestSparePart_JobCard_JobCardId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropForeignKey(
                name: "FK_RepairRequestSparePart_Vehicle_VehicleId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestSparePart_JobCardId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropIndex(
                name: "IX_RepairRequestSparePart_VehicleId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropColumn(
                name: "JobCardId",
                table: "RepairRequestSparePart");

            migrationBuilder.DropColumn(
                name: "KiloMetersChangeSparePart",
                table: "RepairRequestSparePart");

            migrationBuilder.DropColumn(
                name: "VehicleId",
                table: "RepairRequestSparePart");
        }
    }
}
