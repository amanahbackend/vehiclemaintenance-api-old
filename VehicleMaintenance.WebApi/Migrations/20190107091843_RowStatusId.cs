﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RowStatusId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "Weekend",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "Vendor",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "VehicleType",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "Vehicle",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "Technician",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "SparePartStatus",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "SparePart",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "Shift",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "ServiceType",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "RepairRequestType",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "RepairRequestTechnician",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "RepairRequestStatus",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "RepairRequestSparePart",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "RepairRequestMaterial",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "RepairRequest",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "PasswordTokenPin",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "JobCardStatus",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "JobCard",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "File",
                newName: "RowStatusId");

            migrationBuilder.RenameColumn(
                name: "RowStatus",
                table: "Driver",
                newName: "RowStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "Weekend",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "Vendor",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "VehicleType",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "Vehicle",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "Technician",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "SparePartStatus",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "SparePart",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "Shift",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "ServiceType",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "RepairRequestType",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "RepairRequestTechnician",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "RepairRequestStatus",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "RepairRequestSparePart",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "RepairRequestMaterial",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "RepairRequest",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "PasswordTokenPin",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "JobCardStatus",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "JobCard",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "File",
                newName: "RowStatus");

            migrationBuilder.RenameColumn(
                name: "RowStatusId",
                table: "Driver",
                newName: "RowStatus");
        }
    }
}
