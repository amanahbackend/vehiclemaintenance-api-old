﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class AddSettingsTableModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SettingValue",
                table: "Setting",
                newName: "Value");

            migrationBuilder.RenameColumn(
                name: "SettingName",
                table: "Setting",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "SettingCode",
                table: "Setting",
                newName: "Code");

            migrationBuilder.AddColumn<string>(
                name: "Category",
                table: "Setting",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Category",
                table: "Setting");

            migrationBuilder.RenameColumn(
                name: "Value",
                table: "Setting",
                newName: "SettingValue");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Setting",
                newName: "SettingName");

            migrationBuilder.RenameColumn(
                name: "Code",
                table: "Setting",
                newName: "SettingCode");
        }
    }
}
