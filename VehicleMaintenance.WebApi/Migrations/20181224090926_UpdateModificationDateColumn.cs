﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class UpdateModificationDateColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "Weekend",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "Vendor",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "VehicleType",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "Vehicle",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "Technician",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "SparePartStatus",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "SparePart",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "Shift",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "ServiceType",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "RepairRequestUsedMaterial",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "RepairRequestType",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "RepairRequestTechnician",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "RepairRequestStatus",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "RepairRequestSparePart",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "RepairRequest",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "PasswordTokenPin",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "JobCardStatus",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "JobCard",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "File",
                newName: "ModificationDate");

            migrationBuilder.RenameColumn(
                name: "ModificaitonDate",
                table: "Driver",
                newName: "ModificationDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "Weekend",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "Vendor",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "VehicleType",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "Vehicle",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "Technician",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "SparePartStatus",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "SparePart",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "Shift",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "ServiceType",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "RepairRequestUsedMaterial",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "RepairRequestType",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "RepairRequestTechnician",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "RepairRequestStatus",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "RepairRequestSparePart",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "RepairRequest",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "PasswordTokenPin",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "JobCardStatus",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "JobCard",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "File",
                newName: "ModificaitonDate");

            migrationBuilder.RenameColumn(
                name: "ModificationDate",
                table: "Driver",
                newName: "ModificaitonDate");
        }
    }
}
