﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class RenFilePathToFileRelativePath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FilePath",
                table: "UploadedFile",
                newName: "FileRelativePath");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FileRelativePath",
                table: "UploadedFile",
                newName: "FilePath");
        }
    }
}
