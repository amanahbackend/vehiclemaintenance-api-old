﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class AddColumnSparePartDeliveryOrderId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails",
                column: "SparePartDeliveryOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_SparePartDeliveryOrderDetails_SparePartDeliveryOrder_SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails",
                column: "SparePartDeliveryOrderId",
                principalTable: "SparePartDeliveryOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SparePartDeliveryOrderDetails_SparePartDeliveryOrder_SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_SparePartDeliveryOrderDetails_SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails");

            migrationBuilder.DropColumn(
                name: "SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails");
        }
    }
}
