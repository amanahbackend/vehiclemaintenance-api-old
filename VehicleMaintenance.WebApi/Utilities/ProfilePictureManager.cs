﻿using System;
using System.IO;
using System.Linq;
//using Utilites;
//using Utilites.UploadFile;
//using Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Security;

namespace VehicleMaintenance.WebApi.Utilities
{
    public static class ProfilePictureManager
    {
        //public static string GetProfilePictureBase64(string username, IHostingEnvironment hostingEnv)
        //{
        //    string result = null;
        //    string path = $"/ProfilePictures/{username}";
        //    path = hostingEnv.WebRootPath + path;
        //    DirectoryInfo directoryInfo = new DirectoryInfo(path);
        //    if (directoryInfo.Exists)
        //    {
        //        result = FileUtilities.GetFileBase64(directoryInfo.GetFiles().FirstOrDefault()?.FullName);
        //    }
        //    return result;
        //}

        public static UserViewModel BindFileURL(UserViewModel model,
            IConfigurationRoot configuration)
        {
            var baseUrl = configuration["ApiGatewayBaseUrl"];

            var identityServiceName = configuration["ServiceDiscovery:ServiceName"];

            var basePath = $"{baseUrl}/{identityServiceName}";

            //if (model.PicturePath != null)
            //{
            //    model.PicturePath = model.PicturePath.Replace('\\', '/');
            //    model.PictureUrl = $"{basePath}/{model.PicturePath}";
            //}

            return model;
        }

        //public static string SaveProfilePicture(string base64File, string username, IHostingEnvironment hostingEnv)
        //{
        //    var fileExt = StringUtilities.GetFileExtension(base64File);
        //    string fileName = $"{username}.{fileExt}";
        //    string path = $"/ProfilePictures/{username}";
        //    path = hostingEnv.WebRootPath + path;
        //    FileUtilities.CreateIfMissing(path);
        //    FileUtilities.EmptyDirectory(path);
        //    string filePath = $"{path}/{fileName}";
        //    FileUtilities.WriteFileToPath(base64File, filePath);
        //    return filePath;
        //}

        //public static string SaveProfilePicture(string base64File, string username, IUploadImageFileManager imageManager)
        //{
        //    var fileExt = StringUtilities.GetFileExtension(base64File);
        //    string fileName = $"{username}.{fileExt}";
        //    string path = $"ProfilePictures/{username}";
        //    var imageResult = imageManager.AddFile(new UploadFile
        //    {
        //        FileName = fileName,
        //        FileContent = base64File
        //    }, path);

        //    if (imageResult.IsSucceeded)
        //    {
        //        return $"{imageResult.Data}/{fileName}";
        //    }
        //    return String.Empty;
        //}

        public static void DeleteProfilePicture(string username, IHostingEnvironment hostingEnv)
        {
            string path = $"ProfilePictures/{username}";
            Directory.Delete(path, true);
        }
    }
}
