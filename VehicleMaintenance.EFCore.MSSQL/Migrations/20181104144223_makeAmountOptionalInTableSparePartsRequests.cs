﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VehicleMaintenance.EFCore.MSSQL.Migrations
{
    public partial class makeAmountOptionalInTableSparePartsRequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "SparePartsRequest",
                nullable: true,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "SparePartsRequest",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
