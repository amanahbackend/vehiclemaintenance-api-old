﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VehicleMaintenance.EFCore.MSSQL.Migrations
{
    public partial class addQuantityForJobRequestSpareParts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "SparePartsRequest",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "SparePartsRequest",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "SparePartsRequest");

            migrationBuilder.AlterColumn<int>(
                name: "Amount",
                table: "SparePartsRequest",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
