﻿
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Settings;

namespace VehicleMaintenance.API.Seed
{
    public class VehicleMaintenanceDbContextSeed : ContextSeed
    {
        VehicleAppSettings settings = null;
        IServiceProvider serviceProvider;
        public async Task SeedAsync(IServiceProvider _serviceProvider, IOptions<VehicleAppSettings> _settings, VehicleMaintenanceDbContext context, IHostingEnvironment env,
            ILogger<VehicleMaintenanceDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            serviceProvider = _serviceProvider;
            settings = _settings.Value;
            try
            {


                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<Status> lstOrderStatus = new List<Status>();
                lstOrderStatus = GetDefaultStatus();
                List<SparePartStatus> lstSparePartsStatus = new List<SparePartStatus>();
                lstSparePartsStatus = GetDefaultSparePartStatus();
                using (var transaction = context.Database.BeginTransaction())
                {
                    context.Database.OpenConnection();
                    await SeedEntityAsync(context, lstOrderStatus);
                    await SeedEntityAsync(context, lstSparePartsStatus);
                    //context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Status ON");
                    context.SaveChanges();
                    //context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Status OFF");
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(_serviceProvider,_settings, context, env, logger, retryForAvaiability);
                }
            }
        }
        private List<Status> GetDefaultStatus()
        {
            return settings.JobCardStatus;
        }

        private List<SparePartStatus> GetDefaultSparePartStatus()
        {
            return settings.SparePartStatus;
        }
    }
}

