﻿using AutoMapper;
using Dispatching.Identity.BLL.Managers;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using VehicleMaintenance.API.Controllers;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.BLL.Manager;
using VehicleMaintenance.models.Context;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.models.IEntities;
using VehicleMaintenance.models.Settings;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.Common;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;
using VehicleMaintenance.Models.IEntities;
using VehicleMaintenance.Models.IEntities.Common;
using VehicleMaintenance.Models.IEntities.RepairRequests;
using VehicleMaintenance.Models.IEntities.Vehicles;

namespace VehicleMaintenance.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            // Password configurations.
            services.Configure<IdentityOptions>(options =>
            {
                options.User.RequireUniqueEmail = true;

                // Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;
            });

            services.AddSingleton(provider => Configuration);
            // Add framework services.
            services.AddDbContext<VehicleMaintenanceDbContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("VehicleMaintenance.EFCore.MSSQL")));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<VehicleMaintenanceDbContext>()
                .AddDefaultTokenProviders();

            services.AddOptions();
            services.AddMvc();
            services.AddIdentityServer();
            services.Configure<VehicleAppSettings>(Configuration);

            services.AddAutoMapper(typeof(Startup)); //to take mapper configuration from startup
            Mapper.AssertConfigurationIsValid(); // to check configuration of map is valid

            services.AddScoped<DbContext, VehicleMaintenanceDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            services.AddScoped(typeof(IDriver), typeof(Driver));
            services.AddScoped(typeof(IEquipmentTransferHistory), typeof(EquipmentTransferHistory));
            services.AddScoped(typeof(IEquipmentTransferStatus), typeof(EquipmentTransferStatus));
            services.AddScoped(typeof(IFile), typeof(File));
            services.AddScoped(typeof(IJobCard), typeof(JobCard));
            services.AddScoped(typeof(IJunkUser), typeof(JunkUser));
            services.AddScoped(typeof(IPasswordTokenPin), typeof(PasswordTokenPin));
            services.AddScoped(typeof(IRepairRequest), typeof(RepairRequest));
            services.AddScoped(typeof(IRepairRequestStatus), typeof(RepairRequestStatus));
            services.AddScoped(typeof(IRepairRequestType), typeof(RepairRequestType));
            services.AddScoped(typeof(IRole), typeof(Role));
            services.AddScoped(typeof(IServiceType), typeof(ServiceType));
            services.AddScoped(typeof(ITechnician), typeof(Technician));
            services.AddScoped(typeof(IUser), typeof(User));
            services.AddScoped(typeof(IVehicle), typeof(Vehicle));
            services.AddScoped(typeof(IVehicleType), typeof(VehicleType));
            services.AddScoped(typeof(IShift), typeof(Shift));
            services.AddScoped(typeof(ISparePartsRequest), typeof(SparePartsRequest));
            services.AddScoped(typeof(IStatus), typeof(Status));
            services.AddScoped(typeof(IInventory), typeof(Inventory));
            services.AddScoped(typeof(IJobCardTechnician), typeof(JobCardTechnician));
            services.AddScoped(typeof(IRepairRequestTechnician), typeof(RepairRequestTechnician));
            services.AddScoped(typeof(IUsedMaterials), typeof(UsedMaterials));
            services.AddScoped(typeof(IVendor), typeof(Vendor));
            services.AddScoped(typeof(ISparePartStatus), typeof(SparePartStatus));
            services.AddScoped(typeof(IWeekend), typeof(Weekend));
            

            services.AddScoped(typeof(IDriverManager), typeof(DriverManager));
            services.AddScoped(typeof(IInventoryManager), typeof(InventoryManager));
            services.AddScoped(typeof(ISparePartsRequestManager), typeof(SparePartsRequestManager));
            services.AddScoped(typeof(IStatusManager), typeof(StatusManager));
            services.AddScoped(typeof(IJobCard_TechnicianManager), typeof(JobCard_TechnicianManager));
            services.AddScoped(typeof(IRepairRequest_TechnicianManager), typeof(RepairRequest_TechnicianManager));
            services.AddScoped(typeof(IEquipmentTransferHistoryManager), typeof(EquipmentTransferHistoryManager));
            services.AddScoped(typeof(IEquipmentTransferStatusManager), typeof(EquipmentTransferStatusManager));
            services.AddScoped(typeof(IFileManager), typeof(FileManager));
            services.AddScoped(typeof(IJobCardManager), typeof(JobCardManager));
            services.AddScoped(typeof(IJunkUserManager), typeof(JunkUserManager));
            services.AddScoped(typeof(IRepairRequestManager), typeof(RepairRequestManager));
            services.AddScoped(typeof(IRepairRequestStatusManager), typeof(RepairRequestStatusManager));
            services.AddScoped(typeof(IRepairRequestTypeManager), typeof(RepairRequestTypeManager));
            services.AddScoped(typeof(IRoleManager), typeof(RoleManager));
            services.AddScoped(typeof(IServiceTypeManager), typeof(ServiceTypeManager));
            services.AddScoped(typeof(ITechnicianManager), typeof(TechnicianManager));
            services.AddScoped(typeof(IUserManager), typeof(UserManager));
            services.AddScoped(typeof(IVehicleManager), typeof(VehicleManager));
            services.AddScoped(typeof(IVehicleTypeManager), typeof(VehicleTypeManager));
            services.AddScoped(typeof(IShiftManager), typeof(ShiftManager));
            services.AddScoped(typeof(IPasswordTokenPinManager), typeof(PasswordTokenPinManager));
            services.AddScoped(typeof(IUploadImageFileManager), typeof(UploadImageFileManager));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddScoped(typeof(IUsedMaterialsManager), typeof(UsedMaterialsManager));
            services.AddScoped(typeof(IVendorManager), typeof(VendorManager));
            services.AddScoped(typeof(ISparePartStatusManager), typeof(SparePartStatusManager));
            services.AddScoped(typeof(IWeekendManager), typeof(WeekendManager));

            services.AddScoped(typeof(FileController), typeof(FileController));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseStaticFiles();
        }
    }
}
