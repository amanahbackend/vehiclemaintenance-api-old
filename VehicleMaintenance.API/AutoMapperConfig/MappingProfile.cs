﻿using AutoMapper;
using Dispatching.Identity.API.ViewModels;
using Utilities.Utilites;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.Common;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Driver, DriverViewModel>();
            CreateMap<DriverViewModel, Driver>().IgnoreBaseEntityProperties();

            CreateMap<VehicleType, VehicleTypeViewModel>();
            CreateMap<VehicleTypeViewModel, VehicleType>().IgnoreBaseEntityProperties();

            CreateMap<Shift, ShiftViewModel>();
            CreateMap<ShiftViewModel, Shift>().IgnoreBaseEntityProperties();

            CreateMap<EquipmentTransferHistory, EquipmentTransferHistoryViewModel>();
            CreateMap<EquipmentTransferHistoryViewModel, EquipmentTransferHistory>().IgnoreBaseEntityProperties();

            CreateMap<EquipmentTransferStatus, EquipmentTransferStatusViewModel>();
            CreateMap<EquipmentTransferStatusViewModel, EquipmentTransferStatus>().IgnoreBaseEntityProperties();

            CreateMap<File, FileViewModel>()
                .ForMember(dest => dest.Vendor_Name, opt => opt.Ignore())
                .ForMember(dest => dest.Vendor_Address, opt => opt.Ignore())
                .ForMember(dest => dest.FleetNumber, opt => opt.Ignore())
                .ForMember(dest => dest.VehicleNumberPlate, opt => opt.Ignore())
                .ForMember(dest => dest.RepairRequest_Cost, opt => opt.Ignore())
                .ForMember(dest => dest.File, opt => opt.Ignore());

            CreateMap<FileViewModel, File>().IgnoreBaseEntityProperties();

            CreateMap<JobCard, JobCardViewModel>()
                .ForMember(dest => dest.FleetNumber, opt => opt.Ignore())
                .ForMember(dest => dest.DriverEmploye_Id, opt => opt.Ignore())
                .ForMember(dest => dest.ServiceTypeName, opt => opt.Ignore())
                .ForMember(dest => dest.VehicleNumberPlate, opt => opt.Ignore())
                .ForMember(dest => dest.User_Name, opt => opt.Ignore())
                .ForMember(dest => dest.FK_File_URLs, opt => opt.Ignore())
                .ForMember(dest => dest.DriverName, opt => opt.Ignore())
                .ForMember(dest => dest.StatusName, opt => opt.Ignore());

            CreateMap<JobCardViewModel, JobCard>()
                .ForMember(e => e.CreatedDate, c => c.MapFrom(src => src.CreatedDate))
                .ForMember(e => e.FK_CreatedBy_Id, c => c.Ignore())
                .ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore())
                .ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore())
                .ForMember(e => e.UpdatedDate, c => c.Ignore())
                .ForMember(e => e.DeletedDate, c => c.Ignore())
                .ForMember(e => e.IsDeleted, c => c.Ignore())
                .ForMember(e => e.CurrentUserId, c => c.Ignore());


            CreateMap<RepairRequest, RepairRequestViewModel>()
                .ForMember(dest => dest.RepairRequestStatus_Name, opt => opt.Ignore())
                .ForMember(dest => dest.User_Name, opt => opt.Ignore())
                .ForMember(dest => dest.FK_File_URLs, opt => opt.Ignore())
                .ForMember(dest => dest.ServiceType_Name, opt => opt.Ignore())
                .ForMember(dest => dest.RepairRequestType_Name, opt => opt.Ignore());

            CreateMap<RepairRequestViewModel, RepairRequest>()
                .ForMember(e => e.CreatedDate, c => c.Ignore())
                .ForMember(e => e.FK_CreatedBy_Id, c => c.MapFrom(src => src.FK_CreatedBy_Id))
                .ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore())
                .ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore())
                .ForMember(e => e.UpdatedDate, c => c.Ignore())
                .ForMember(e => e.DeletedDate, c => c.Ignore())
                .ForMember(e => e.IsDeleted, c => c.Ignore())
                .ForMember(e => e.CurrentUserId, c => c.Ignore());

            CreateMap<RepairRequestStatus, RepairRequestStatusViewModel>();
            CreateMap<RepairRequestStatusViewModel, RepairRequestStatus>().IgnoreBaseEntityProperties();

            CreateMap<RepairRequestType, RepairRequestTypeViewModel>();
            CreateMap<RepairRequestTypeViewModel, RepairRequestType>().IgnoreBaseEntityProperties();

            CreateMap<Role, RoleViewModel>();

            CreateMap<RoleViewModel, Role>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedName, opt => opt.Ignore());

            CreateMap<ServiceType, ServiceTypeViewModel>();
            CreateMap<ServiceTypeViewModel, ServiceType>().IgnoreBaseEntityProperties();

            CreateMap<Technician, TechnicianViewModel>();
            CreateMap<TechnicianViewModel, Technician>().IgnoreBaseEntityProperties();

            CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.PictureUrl, opt => opt.Ignore());

            CreateMap<UserViewModel, User>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.Deactivated, opt => opt.Ignore());

            CreateMap<JunkUserViewModel, JunkUser>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.Deactivated, opt => opt.Ignore());

            CreateMap<JunkUser, JunkUserViewModel>();

            CreateMap<Vehicle, VehicleViewModel>();
            CreateMap<VehicleViewModel, Vehicle>().IgnoreBaseEntityProperties();

            CreateMap<Inventory, InventoryViewModel>();
            CreateMap<InventoryViewModel, Inventory>().IgnoreBaseEntityProperties();

            CreateMap<Status, StatusViewModel>();
            CreateMap<StatusViewModel, Status>().IgnoreBaseEntityProperties();

            CreateMap<SparePartStatus, SparePartStatusViewModel>();
            CreateMap<SparePartStatusViewModel, SparePartStatus>().IgnoreBaseEntityProperties();

            CreateMap<Weekend, WeekendViewModel>();
            CreateMap<WeekendViewModel, Weekend>().IgnoreBaseEntityProperties();

            CreateMap<SparePartsRequest, SparePartsRequestViewModel>()
                .ForMember(dest => dest.TechnicianEmploye_Id, opt => opt.Ignore())
                .ForMember(dest => dest.SerialNumber, opt => opt.Ignore())
                .ForMember(dest => dest.Cost, opt => opt.Ignore())
                .ForMember(dest => dest.Description, opt => opt.Ignore())
                .ForMember(dest => dest.FleetNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SparePart_Name, opt => opt.Ignore())
                .ForMember(dest => dest.Unit, opt => opt.Ignore());

            CreateMap<SparePartsRequestViewModel, SparePartsRequest>()
                .ForMember(e => e.CreatedDate, c => c.MapFrom(src => src.CreatedDate))
                .ForMember(e => e.FK_CreatedBy_Id, c => c.MapFrom(src => src.FK_CreatedBy_Id))
                .ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore())
                .ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore())
                .ForMember(e => e.UpdatedDate, c => c.Ignore())
                .ForMember(e => e.DeletedDate, c => c.Ignore())
                .ForMember(e => e.IsDeleted, c => c.Ignore())
                .ForMember(e => e.CurrentUserId, c => c.Ignore());

            CreateMap<JobCardTechnician, JobCard_TechnicianViewModel>()
                .ForMember(dest => dest.TechnicianEmploye_Id, opt => opt.Ignore());

            CreateMap<JobCard_TechnicianViewModel, JobCardTechnician>().IgnoreBaseEntityProperties();

            CreateMap<RepairRequestTechnician, RepairRequest_TechnicianViewModel>()
                .ForMember(dest => dest.TechnicianEmploye_Id, opt => opt.Ignore());

            CreateMap<RepairRequest_TechnicianViewModel, RepairRequestTechnician>()
                .ForMember(e => e.CreatedDate, c => c.Ignore())
                .ForMember(e => e.FK_CreatedBy_Id, c => c.Ignore())
                .ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore())
                .ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore())
                .ForMember(e => e.UpdatedDate, c => c.Ignore())
                .ForMember(e => e.DeletedDate, c => c.MapFrom(src => src.DeletedDate))
                .ForMember(e => e.IsDeleted, c => c.Ignore())
                .ForMember(e => e.CurrentUserId, c => c.Ignore());

            CreateMap<UsedMaterials, UsedMaterialsViewModel>()
                .ForMember(dest => dest.TechnicianEmploye_Id, opt => opt.Ignore())
                .ForMember(dest => dest.SparePart_SerialNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SparePartStatus_Name, opt => opt.Ignore());

            CreateMap<UsedMaterialsViewModel, UsedMaterials>().IgnoreBaseEntityProperties();

            CreateMap<Vendor, VendorViewModel>()
                .ForMember(dest => dest.Files, opt => opt.Ignore());

            CreateMap<VendorViewModel, Vendor>().IgnoreBaseEntityProperties();

            CreateMap<EquipmentTransfer, EquipmentTransferViewModel>();
            CreateMap<EquipmentTransferViewModel, EquipmentTransfer>();

        }
    }
}
