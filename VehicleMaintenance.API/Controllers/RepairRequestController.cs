﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/RepairRequest")]
    public class RepairRequestController : BaseController<IRepairRequestManager, RepairRequest, RepairRequestViewModel>
    {
        IServiceProvider _serviceprovider;
        IProcessResultMapper processResultMapper;
        IRepairRequestManager manager;

        public RepairRequestController(IServiceProvider serviceprovider, IRepairRequestManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            processResultMapper = _processResultMapper;
            _serviceprovider = serviceprovider;
            manager = _manager;

        }
        private FileController FileController
        {
            get
            {
                return _serviceprovider.GetService<FileController>();
            }
        }
        private IJobCardManager JobCardManager
        {
            get
            {
                return _serviceprovider.GetService<IJobCardManager>();
            }
        }
        private IFileManager Filemanager
        {
            get
            {
                return _serviceprovider.GetService<IFileManager>();
            }
        }
        private IServiceTypeManager ServiceTypemanager
        {
            get
            {
                return _serviceprovider.GetService<IServiceTypeManager>();
            }
        }
        private IRepairRequestTypeManager RepairRequestTypemanager
        {
            get
            {
                return _serviceprovider.GetService<IRepairRequestTypeManager>();
            }
        }
        private IRepairRequestStatusManager RepairRequestStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IRepairRequestStatusManager>();
            }
        }
        private IRepairRequest_TechnicianManager RepairRequest_Technicianmanager
        {
            get
            {
                return _serviceprovider.GetService<IRepairRequest_TechnicianManager>();
            }
        }
        private ITechnicianManager Technicianmanager
        {
            get
            {
                return _serviceprovider.GetService<ITechnicianManager>();
            }
        }
        private IUserManager Usermanager
        {
            get
            {
                return _serviceprovider.GetService<IUserManager>();
            }
        }
        private IUsedMaterialsManager UsedMaterialsmanager
        {
            get
            {
                return _serviceprovider.GetService<IUsedMaterialsManager>();
            }
        }
        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<RepairRequestViewModel> Post([FromBody]RepairRequestViewModel model)
        {
            if (model.FK_ServiceType_ID == 0)
            {
                var jobCard= JobCardManager.Get(model.FK_JobCard_ID);
                model.FK_ServiceType_ID = jobCard.Data.FK_ServiceType_ID.Value;
            }
            //if(model.external&&model.FK_RepairRequestType_ID==)
            //{
            //    model.FK_RepairRequestStatus_ID=
            //}
            var repairRes = base.Post(model);
            if (repairRes.IsSucceeded && repairRes.Data != null)
            {
                if (model.Files!=null)
                {
                    model.Files.ForEach(x => x.FK_RepairRequest_ID = repairRes.Data.Id);
                    var fileRes = FileController.SubmitFile(model.Files.ToList());
                    if (fileRes.IsSucceeded && fileRes.Data != null)
                    {
                        repairRes.Data.Files = fileRes.Data;
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.MapToProcessResultViewModel<List<FileViewModel>, RepairRequestViewModel>(fileRes);
                    }
                }
            }
             
            return repairRes;
        }
        public override ProcessResultViewModel<List<RepairRequestViewModel>> Get()
        {
            var result = base.Get();
            if (result.Data != null)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data);
                result.IsSucceeded = bindFiles(result.Data);
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }
        public override ProcessResultViewModel<RepairRequestViewModel> Get(int Id)
        {
            var result = base.Get(Id);
            if (result.Data != null)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data);
                result.IsSucceeded = bindFiles(result.Data);
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }
      
        public bool bindServiceType_Name(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var ServiceType_NameResult = ServiceTypemanager.Get(item.FK_ServiceType_ID);
                    if (ServiceType_NameResult.IsSucceeded && ServiceType_NameResult.Data != null)
                    {
                        item.ServiceType_Name = ServiceType_NameResult.Data.ServiceTypeName_EN;
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindServiceType_Name(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var ServiceType_NameResult = ServiceTypemanager.Get(data.FK_ServiceType_ID);
                if (ServiceType_NameResult.IsSucceeded && ServiceType_NameResult.Data != null)
                {
                    data.ServiceType_Name = ServiceType_NameResult.Data.ServiceTypeName_EN;
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        [Route("GetByMaintenanceType/{type}")]
        [HttpGet()]
        public ProcessResultViewModel<List<RepairRequestViewModel>> GetByMaintenanceType([FromRoute]bool type)
        {
            var entityResult = manager.GetByMaintenanceType(type);
            var result = processResultMapper.Map<List<RepairRequest>, List<RepairRequestViewModel>>(entityResult);
            if (result.Data != null)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data);
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }

        [Route("GetByExternalandMaintenanceType")]
        [HttpGet()]
        public ProcessResultViewModel<List<RepairRequestViewModel>> GetByExternalandMaintenanceType([FromQuery]bool external, bool type)
        {
            var entityResult = manager.GetByexternalandMaintenanceType(external , type);
            var result = processResultMapper.Map<List<RepairRequest>, List<RepairRequestViewModel>>(entityResult);
            if (result.Data != null)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data);
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }

        [HttpGet]
        [Route("GetByJobCardID/{id}")]
        public  ProcessResultViewModel<List<RepairRequestViewModel>> GetByJobCardID(int id)
        {
            var Result = manger.GetByJobCardID(id);
            var result = processResultMapper.Map<List<RepairRequest>, List<RepairRequestViewModel>>(Result);
            if (result.Data != null)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data);
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }

        [Route("GetNonApprovedrepairrequests")]
        [HttpGet()]
        public ProcessResultViewModel<List<RepairRequestViewModel>> GetNonApprovedrepairrequests()
        {

            var entityResult = manager.GetNonApprovedrepairrequests();
            var result = processResultMapper.Map<List<RepairRequest>, List<RepairRequestViewModel>>(entityResult);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data); 
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }

        [Route("SearchByID_JobcardID")]
        [HttpGet()]
        public ProcessResultViewModel<List<RepairRequestViewModel>> SearchByID_JobcardID([FromQuery]string word)
        {
             var entityResult = manager.SearchByID_JobcardID(word);
            var result = processResultMapper.Map<List<RepairRequest>, List<RepairRequestViewModel>>(entityResult);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindRepairRequestType_Name(result.Data);
                result.IsSucceeded = bindRepairRequestStatus_Name(result.Data);
                result.IsSucceeded = bindUser_Name(result.Data);
                result.IsSucceeded = bindFileURLs(result.Data);
                result.IsSucceeded = bindFileIDs(result.Data);
                result.IsSucceeded = bindTechnicianCost(result.Data);
                result.IsSucceeded = bindUsedMaterialsCost(result.Data);

            }
            return result;
        }

        [HttpGet()]
        [Route("GetChart")]
        public ProcessResultViewModel<List<ChartViewModel>> GetChart()
        {
            var entityRes = manager.GetChart();
            return processResultMapper.Map<List<ChartViewModel>, List<ChartViewModel>>(entityRes);
        }

        [Route("ApproveRepairRequest")]
        [HttpPut()]
        public ProcessResultViewModel<bool> ApproveRepairRequest([FromQuery]int repairRequestId, bool Approve)
        {
            var entityResult = manager.ApproveRepairRequest(repairRequestId, Approve);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);           
            return result;
        }

        public bool bindRepairRequestType_Name(List<RepairRequestViewModel> data)
        {
           bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var repairRequestTypeResult = RepairRequestTypemanager.Get(item.FK_RepairRequestType_ID);
                    if (repairRequestTypeResult.IsSucceeded && repairRequestTypeResult.Data != null)
                    {
                        item.RepairRequestType_Name = repairRequestTypeResult.Data.RepairRequestTypeName_EN;
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindRepairRequestStatus_Name(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var RepairRequestStatus_NameResult = RepairRequestStatusmanager.Get(item.FK_RepairRequestStatus_ID);
                    if (RepairRequestStatus_NameResult.IsSucceeded && RepairRequestStatus_NameResult.Data != null)
                    {
                        item.RepairRequestStatus_Name = RepairRequestStatus_NameResult.Data.RepairRequestStatusName_EN;
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool bindUser_Name(List<RepairRequestViewModel> data)
        {
            bool result = false;

            if (data.Any())
            {
                foreach (var item in data)
                {
                    var userNameResult = Usermanager.Get(item.FK_User_ID);
                    if (userNameResult.Result != null)
                    {
                        item.User_Name = $"{userNameResult.Result.FirstName} {userNameResult.Result.LastName}";
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        public bool bindFileURLs(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var FilesResult = Filemanager.GetByRepairRequestID(item.Id);
                    if (FilesResult.IsSucceeded && FilesResult.Data != null)
                    {
                        List<string> tempURLs = new List<string>();
                        foreach (var urlitem in FilesResult.Data)
                        {
                            tempURLs.Add(urlitem.FileURL);
                            result = true;
                        }
                        item.FK_File_URLs = tempURLs;
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }

            return result;
        }
        public bool bindFileIDs(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var FilesResult = Filemanager.GetByRepairRequestID(item.Id);
                    if (FilesResult.IsSucceeded && FilesResult.Data != null)
                    {
                        List<int> tempIDs = new List<int>();
                        foreach (var iditem in FilesResult.Data)
                        {
                            tempIDs.Add(iditem.Id);
                            result = true;
                        }
                        item.FK_File_IDs = tempIDs;
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindFiles(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var FilesResult = FileController.GetByRepairRequestID(item.Id);
                    if (FilesResult.IsSucceeded && FilesResult.Data != null)
                    {
                        item.Files = FilesResult.Data.ToList();
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindTechnicianCost(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    
                    var RepairRequest_TechnicianResult = RepairRequest_Technicianmanager.GetAll().Data.Where(X => X.FK_RepairRequest_ID == item.Id && X.Removed==false);
                    if (RepairRequest_TechnicianResult.Count() > 0)
                    {
                        foreach (var item2 in RepairRequest_TechnicianResult)
                        {
                            TimeSpan diff = item2.EndTime - item2.StartTime;
                            double hours = diff.TotalHours;
                            var TechnicianResult = Technicianmanager.Get(item2.FK_Technician_ID);
                            if (TechnicianResult.IsSucceeded && TechnicianResult.Data != null)
                            {
                                item.Cost = hours * TechnicianResult.Data.HourRating;
                            }
                            result = true;
                        }
                    }
                }
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindUsedMaterialsCost(List<RepairRequestViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var UsedMaterialsResult = UsedMaterialsmanager.GetAll().Data.Where(X => X.FK_RepairRequest_ID == item.Id);
                    if (UsedMaterialsResult.Count() > 0)
                    {
                        foreach (var item2 in UsedMaterialsResult)
                        {
                            item.Cost += item2.cost * item2.Amount; ;
                            result = true;
                        }
                    }
                }
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindRepairRequestType_Name(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var repairRequestTypeResult = RepairRequestTypemanager.Get(data.FK_RepairRequestType_ID);
                if (repairRequestTypeResult.IsSucceeded && repairRequestTypeResult.Data != null)
                {
                    data.RepairRequestType_Name = repairRequestTypeResult.Data.RepairRequestTypeName_EN;
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindRepairRequestStatus_Name(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var RepairRequestStatus_NameResult = RepairRequestStatusmanager.Get(data.FK_RepairRequestStatus_ID);
                if (RepairRequestStatus_NameResult.IsSucceeded && RepairRequestStatus_NameResult.Data != null)
                {
                    data.RepairRequestStatus_Name = RepairRequestStatus_NameResult.Data.RepairRequestStatusName_EN;
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool bindUser_Name(RepairRequestViewModel data)
        {
            bool result = false;

            if (data != null)
            {
                var userNameResult = Usermanager.Get(data.FK_User_ID);
                if (userNameResult.Result != null)
                {
                    data.User_Name = userNameResult.Result.FirstName + " " + userNameResult.Result.LastName;
                    result = true;
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        public bool bindFileURLs(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var FilesResult = Filemanager.GetByRepairRequestID(data.Id);
                if (FilesResult.IsSucceeded && FilesResult.Data != null)
                {
                    List<string> tempURLs = new List<string>();
                    foreach (var urlitem in FilesResult.Data)
                    {
                        tempURLs.Add(urlitem.FileURL);
                        result = true;
                    }
                    data.FK_File_URLs = tempURLs;
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindFileIDs(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var FilesResult = Filemanager.GetByRepairRequestID(data.Id);
                if (FilesResult.IsSucceeded && FilesResult.Data != null)
                {
                    List<int> tempIDs = new List<int>();
                    foreach (var iditem in FilesResult.Data)
                    {
                        tempIDs.Add(iditem.Id);
                        result = true;
                    }
                    data.FK_File_IDs = tempIDs;
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindFiles(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var FilesResult = FileController.GetByRepairRequestID(data.Id);
                if (FilesResult.IsSucceeded && FilesResult.Data != null)
                {
                    data.Files = FilesResult.Data.ToList();
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindTechnicianCost(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var RepairRequest_TechnicianResult = RepairRequest_Technicianmanager.GetAll().Data.Where(X=>X.FK_RepairRequest_ID==data.Id && X.Removed == false);
                if (RepairRequest_TechnicianResult.Count()>0) 
                {
                    foreach (var item in RepairRequest_TechnicianResult) {

                        TimeSpan diff = item.EndTime - item.StartTime;
                        double hours = diff.TotalHours;
                        var TechnicianResult = Technicianmanager.Get(item.FK_Technician_ID);
                        if (TechnicianResult.IsSucceeded && TechnicianResult.Data != null)
                        {
                            data.Cost += hours * TechnicianResult.Data.HourRating;
                        }
                        result = true;
                    }
                }
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindUsedMaterialsCost(RepairRequestViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var UsedMaterialsResult = UsedMaterialsmanager.GetAll().Data.Where(X => X.FK_RepairRequest_ID == data.Id);
                if (UsedMaterialsResult.Count() > 0)
                {
                    foreach (var item in UsedMaterialsResult)
                    {
                        data.Cost += item.cost * item.Amount;
                        result = true;
                    }
                }
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        [HttpGet, Route("Count")]
        public ProcessResultViewModel<int> Count()
        {
            var entityResult = manager.Count();
            if (entityResult.IsSucceeded)
            {
                return ProcessResultViewModelHelper.Succedded(entityResult.Data);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<int>(0, entityResult.Exception.Message);
            }
        }
    }
}