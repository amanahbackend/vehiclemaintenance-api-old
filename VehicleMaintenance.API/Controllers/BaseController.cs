﻿using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace VehicleMaintenance.API.Controllers
{
    public class BaseController : Controller
    {
        protected IProcessResultMapper ProcessResultMapper;
    }
}
