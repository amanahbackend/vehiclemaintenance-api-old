﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/RepairRequest_Technician")]
    public class RepairRequest_TechnicianController : BaseController<IRepairRequest_TechnicianManager, RepairRequestTechnician, RepairRequest_TechnicianViewModel>
    {
        IServiceProvider _serviceprovider;
        IRepairRequest_TechnicianManager manager;
        IProcessResultMapper processResultMapper;
        public RepairRequest_TechnicianController(IServiceProvider serviceprovider, IRepairRequest_TechnicianManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) 
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        private ITechnicianManager TechnicianManager
        {
            get
            {
                return _serviceprovider.GetService<ITechnicianManager>();
            }
        }
        public override ProcessResultViewModel<List<RepairRequest_TechnicianViewModel>> Get()
        {
            var result = base.Get();
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
            }
            return result;
        }
        public bool bindTechnicianName(List<RepairRequest_TechnicianViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var technicianResult = TechnicianManager.Get(item.FK_Technician_ID);
                if (technicianResult.IsSucceeded && technicianResult.Data != null)
                {
                    item.TechnicianEmploye_Id = technicianResult.Data.Employe_Id;
                    result = true;
                }
            } 
            return result;
        }
        [HttpGet]
        [Route("GetByRepairRequestID/{id}")]
        public ProcessResultViewModel<List<RepairRequest_TechnicianViewModel>> GetByRepairRequestID(int id)
       {
            var Result = manger.GetByRepairRequestID(id);
            var result = processResultMapper.Map<List<RepairRequestTechnician>, List<RepairRequest_TechnicianViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
            }
            return result;
        }

        [HttpGet]
        [Route("GetRepairRequestsByTechniciansAndDate")]
        public ProcessResultViewModel<List<RepairRequest_TechnicianViewModel>> GetRepairRequestsByTechniciansAndDate([FromQuery]int? technicianId, DateTime? startDate, DateTime? endDate)
        {
            var Result = manger.GetRepairRequestsByTechniciansAndDate(technicianId, startDate, endDate);
            var result = processResultMapper.Map<List<RepairRequestTechnician>, List<RepairRequest_TechnicianViewModel>>(Result);
            return result;
        }
    }
}