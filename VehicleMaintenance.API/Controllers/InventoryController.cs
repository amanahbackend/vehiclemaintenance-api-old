﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Inventory")]
    public class InventoryController : BaseController<IInventoryManager, Inventory, InventoryViewModel>
    {
        IProcessResultMapper processResultMapper;
        IInventoryManager manager;
        public InventoryController(IInventoryManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            processResultMapper = _processResultMapper;
            manager = _manager;
        }

        [Route("GetBySparePartName/{name}")]
        [HttpGet()]
        public ProcessResultViewModel<List<InventoryViewModel>> GetBySparePartName([FromRoute]string Name)
        {
            var entityResult = manager.GetBySparePartName(Name);
            return processResultMapper.Map<List<Inventory>, List<InventoryViewModel>>(entityResult);
        } 

        [Route("Search/{Word}")]
        [HttpGet()]
        public ProcessResultViewModel<List<InventoryViewModel>> Search([FromRoute]string Word)
        {
            var entityResult = manager.Search(Word);
            return processResultMapper.Map<List<Inventory>, List<InventoryViewModel>>(entityResult);
        }

        [Route("SearchByName_SerialNum_Desc")]
        [HttpGet()]
        public ProcessResultViewModel<List<InventoryViewModel>> SearchByName_SerialNum_Desc([FromQuery]string word)
        {
            var entityResult = manager.SearchByName_SerialNum_Desc(word);
            return processResultMapper.Map<List<Inventory>, List<InventoryViewModel>>(entityResult);
        }

    }
}