﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/SparePartsRequest")]
    public class SparePartsRequestController : BaseController<ISparePartsRequestManager, SparePartsRequest,
        SparePartsRequestViewModel>
    {
        readonly IServiceProvider _serviceprovider;
        readonly ISparePartsRequestManager _manager;
        readonly IProcessResultMapper _processResultMapper;

        public SparePartsRequestController(IServiceProvider serviceprovider, ISparePartsRequestManager manager,
            IMapper mapper, IProcessResultMapper processResultMapper,
            IProcessResultPaginatedMapper processResultPaginatedMapper)
            : base(manager, mapper, processResultMapper, processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            _manager = manager;
            _processResultMapper = processResultMapper;
        }

        private ITechnicianManager TechnicianManager => _serviceprovider.GetService<ITechnicianManager>();

        private IRepairRequestManager RepairRequestManager => _serviceprovider.GetService<IRepairRequestManager>();

        private IJobCardManager JobCardManager => _serviceprovider.GetService<IJobCardManager>();

        private IVehicleManager VehicleManager => _serviceprovider.GetService<IVehicleManager>();

        private IUserManager UserManager => _serviceprovider.GetService<IUserManager>();

        private IInventoryManager InventoryManager => _serviceprovider.GetService<IInventoryManager>();

        public override ProcessResultViewModel<List<SparePartsRequestViewModel>> Get()
        {
            var result = base.Get();
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }
            return result;
        }

        public override ProcessResultViewModel<SparePartsRequestViewModel> Get(int id)
        {
            var result = base.Get(id);
            if (result.Data != null)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }
            return result;
        }

        [HttpGet]
        [Route("GetByRepairRequestID/{id}")]
        public ProcessResultViewModel<List<SparePartsRequestViewModel>> GetByRepairRequestID(int id)
        {
            var Result = manger.GetByRepairRequestID(id);
            var result = _processResultMapper.Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(Result);
            if (result.Data.Any())
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }
            return result;
        }

        [HttpGet]
        [Route("GetByRepairRequestID2/{id}")]
        public async Task<ProcessResultViewModel<List<MaterialRequisitionViewModel>>> GetByRepairRequestID2(int id)
        {
            var repairRequestRes = RepairRequestManager.Get(id);
            var jobCardRes = JobCardManager.Get(repairRequestRes.Data.FK_JobCard_ID);
            var vehicleRes = VehicleManager.Get(jobCardRes.Data.FK_Vehicle_ID);
            var supervisorRes = (await UserManager.Get(repairRequestRes.Data.FK_User_ID));
            var Result = manger.GetByRepairRequestID(id);
            var resulttest = _processResultMapper
                .Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(Result);

            var result = resulttest.Data.GroupBy(gp => gp.FK_RepairRequest_ID,
                (key, g) => new MaterialRequisitionViewModel
                {
                    FleetNo = vehicleRes.Data.FleetNumber,
                    Supervisor = $"{supervisorRes.FirstName} {supervisorRes.LastName}",
                    SpareParts = g.ToList()
                }
            ).ToList();

            if (result.Any())
            {
                foreach (var item in result)
                {
                    foreach (var item2 in item.SpareParts)
                    {

                        var inventoryManagerResult = InventoryManager.Get(item2.FK_Inventory_ID);
                        if (inventoryManagerResult.IsSucceeded && inventoryManagerResult.Data != null)
                        {
                            item2.SparePart_Name = inventoryManagerResult.Data.Name_EN;
                            item2.SerialNumber = inventoryManagerResult.Data.SerialNumber;
                            item2.Description = inventoryManagerResult.Data.Description;
                            item2.Cost = inventoryManagerResult.Data.Cost;
                            item2.SparePart_Name = inventoryManagerResult.Data.Name_EN;
                        }
                    }
                }
            }

            return ProcessResultViewModelHelper.Succedded(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">spare part request id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetById/{id}")]
        public ProcessResultViewModel<InternalRequestViewModel> GetById(int id)
        {

            var oResult = manger.Get(id);
            var repairRequestRes = RepairRequestManager.Get(oResult.Data.FK_RepairRequest_ID);
            var jobCardRes = JobCardManager.Get(repairRequestRes.Data.FK_JobCard_ID);
            var supervisorRes = UserManager.Get(repairRequestRes.Data.FK_User_ID);
            var resulttest = _processResultMapper.Map<SparePartsRequest, SparePartsRequestViewModel>(oResult);

            var result = new InternalRequestViewModel();

            if (jobCardRes.Data != null && supervisorRes != null && resulttest != null)
            {
                result = new InternalRequestViewModel
                {
                    Customer = jobCardRes.Data.Customer_Type,
                    RequestedBy = $"{supervisorRes.Result.FirstName} {supervisorRes.Result.LastName}",
                    SpareParts = resulttest.Data
                };
            }

            if (result != null)
            {
                var inventoryManagerResult = InventoryManager.Get(result.SpareParts.FK_Inventory_ID);

                if (inventoryManagerResult.IsSucceeded && inventoryManagerResult.Data != null)
                {
                    result.SpareParts.SparePart_Name = inventoryManagerResult.Data.Name_EN;
                    result.SpareParts.SerialNumber = inventoryManagerResult.Data.SerialNumber;
                    result.SpareParts.Description = inventoryManagerResult.Data.Description;
                    result.SpareParts.Cost = inventoryManagerResult.Data.Cost;
                    result.SpareParts.SparePart_Name = inventoryManagerResult.Data.Name_EN;
                    result.SpareParts.Unit = inventoryManagerResult.Data.Unit;
                }
            }

            return ProcessResultViewModelHelper.Succedded(result);
        }

        [Route("GetNonApproved")]
        [HttpGet]
        public ProcessResultViewModel<List<SparePartsRequestViewModel>> GetNonApproved()
        {
            var entityResult = _manager.GetNonApproved();

            var result =
                _processResultMapper.Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(entityResult);

            if (result.Data.Any())
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
                result.IsSucceeded = BindRepairRequest(result.Data);
            }

            return result;
        }

        [Route("GetApproved")]
        [HttpGet]
        public ProcessResultViewModel<List<SparePartsRequestViewModel>> GetApproved()
        {

            var entityResult = _manager.GetApproved();
            var result =
                _processResultMapper.Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(entityResult);

            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }

            return result;
        }

        [HttpGet]
        [Route("GetByJobCardID/{id}")]
        public ProcessResultViewModel<List<SparePartsRequestViewModel>> GetByJobCardID(int id)
        {
            var repairRes = RepairRequestManager.GetByJobCardID(id).Data.ToList();
            List<int?> repairRequestIDs = null;

            foreach (var item in repairRes)
            {
                repairRequestIDs.Add(item.Id);
            }

            var Result = manger.GetByRepairRequestIDs(repairRequestIDs);
            var result = _processResultMapper.Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(Result);

            if (result.Data.Any())
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }

            return result;
        }

        [HttpGet]
        [Route("GetAllInternal")]
        public ProcessResultViewModel<List<SparePartsRequestViewModel>> GetAllInternal()
        {
            var Result = base.Get();

            List<RepairRequest> repairRequestList = new List<RepairRequest>();

            foreach (var item in Result.Data)
            {
                var repairRes = RepairRequestManager.Get(item.FK_RepairRequest_ID);

                if (repairRes.Data != null && !repairRes.Data.External.Value)
                    repairRequestList.Add(repairRes.Data);
            }

            List<int?> repairRequestIds = new List<int?>();

            foreach (var item in repairRequestList)
            {
                repairRequestIds.Add(item.Id);
            }

            var entityResult = manger.GetByRepairRequestIDs(repairRequestIds);

            var result =
                _processResultMapper.Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(entityResult);

            if (result.Data.Any())
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }

            return result;
        }

        [Route("ApproveSparePart")]
        [HttpPut]
        public ProcessResultViewModel<bool> ApproveSparePart([FromQuery] int sparePartId, bool Approve)
        {
            var entityResult = _manager.ApproveSparePart(sparePartId, Approve);
            var result = _processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [HttpGet]
        [Route("SearchSparePartsRequests")]
        public ProcessResultViewModel<List<SparePartsRequestViewModel>> SearchSparePartsRequests(
            [FromQuery] int? inventoryId, DateTime? startDate, DateTime? endDate)
        {
            var entityResult = _manager.SearchSparePartsRequests(inventoryId, startDate, endDate);

            var result =
                _processResultMapper.Map<List<SparePartsRequest>, List<SparePartsRequestViewModel>>(entityResult);

            if (result.Data.Any())
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartData(result.Data);
            }

            return result;
        }

        public bool bindTechnicianName(List<SparePartsRequestViewModel> data)
        {
            bool result = false;

            foreach (var item in data)
            {
                var technicianResult = TechnicianManager.Get(item.FK_Technician_ID);
                if (technicianResult.IsSucceeded && technicianResult.Data != null)
                {
                    item.TechnicianEmploye_Id = technicianResult.Data.Employe_Id;
                    item.Technician = technicianResult.Data;

                    result = true;
                }
            }
            return result;
        }

        public bool bindSparePartData(List<SparePartsRequestViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var inventoryManagerResult = InventoryManager.Get(item.FK_Inventory_ID);
                if (inventoryManagerResult.IsSucceeded && inventoryManagerResult.Data != null)
                {
                    item.SerialNumber = inventoryManagerResult.Data.SerialNumber;
                    item.Description = inventoryManagerResult.Data.Description;
                    item.Cost = inventoryManagerResult.Data.Cost;
                    item.SparePart_Name = inventoryManagerResult.Data.Name_EN;
                    result = true;
                }
            }
            return result;
        }

        public bool BindRepairRequest(List<SparePartsRequestViewModel> data)
        {
            bool result = false;

            foreach (var item in data)
            {
                var repairRequestResult = RepairRequestManager.Get(item.FK_RepairRequest_ID);

                if (repairRequestResult.IsSucceeded && repairRequestResult.Data != null)
                {
                    item.RepairRequest = new RepairRequest();
                    item.RepairRequest.FK_JobCard_ID = repairRequestResult.Data.FK_JobCard_ID;
                    result = true;
                }
            }

            return result;
        }

        public bool bindTechnicianName(SparePartsRequestViewModel data)
        {
            bool result = false;
            var technicianResult = TechnicianManager.Get(data.FK_Technician_ID);
            if (technicianResult.IsSucceeded && technicianResult.Data != null)
            {
                data.TechnicianEmploye_Id = technicianResult.Data.Employe_Id;
                result = true;
            }
            return result;
        }

        public bool bindSparePartData(SparePartsRequestViewModel data)
        {
            bool result = false;
            var inventoryManagerResult = InventoryManager.Get(data.FK_Inventory_ID);
            if (inventoryManagerResult.IsSucceeded && inventoryManagerResult.Data != null)
            {
                data.SparePart_Name = inventoryManagerResult.Data.Name_EN;
                data.SerialNumber = inventoryManagerResult.Data.SerialNumber;
                data.Description = inventoryManagerResult.Data.Description;
                data.Cost = inventoryManagerResult.Data.Cost;
                data.SparePart_Name = inventoryManagerResult.Data.Name_EN;

                result = true;
            }
            return result;
        }
    }
}