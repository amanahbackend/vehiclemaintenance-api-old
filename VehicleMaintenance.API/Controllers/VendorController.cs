﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Vendor")]
    public class VendorController : BaseController<IVendorManager, Vendor, VendorViewModel>
    {
        IServiceProvider _serviceprovider;
        IVendorManager manager;
        IProcessResultMapper processResultMapper;
        public VendorController(IServiceProvider serviceprovider, IVendorManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) 
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        private IFileManager Filemanager
        {
            get
            {
                return _serviceprovider.GetService<IFileManager>();
            }
        }
        private IRepairRequestManager Technicianmanager
        {
            get
            {
                return _serviceprovider.GetService<IRepairRequestManager>();
            }
        }
        private FileController FileController
        {
            get
            {
                return _serviceprovider.GetService<FileController>();
            }
        }

        public override ProcessResultViewModel<VendorViewModel> Get(int Id)
        {
            var result = base.Get(Id);
            if (result.Data != null)
            {
                bindFiles(result.Data);
            }
            return result;
        }
        public bool bindFiles(List<VendorViewModel> data)
        {
            bool result = false;
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    var FilesResult = FileController.GetByVendorID(item.Id);
                    if (FilesResult.IsSucceeded && FilesResult.Data != null)
                    {
                        item.Files = FilesResult.Data.ToList();
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
        public bool bindFiles(VendorViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var FilesResult = FileController.GetByVendorID(data.Id);
                if (FilesResult.IsSucceeded && FilesResult.Data != null)
                {
                    data.Files = FilesResult.Data.ToList();
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

    }
}