﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Weekend")]
    public class WeekendController : BaseController<IWeekendManager, Weekend, WeekendViewModel>
    {
        IWeekendManager manager;
        IProcessResultMapper processResultMapper;
        public WeekendController(IWeekendManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        [Route("GetLast/{date}")]
        [HttpGet()]
        public ProcessResultViewModel<List<WeekendViewModel>> GetLast([FromRoute]DateTime date)
        {
            var entityResult = manager.GetLast(date);
            return processResultMapper.Map<List<Weekend>, List<WeekendViewModel>>(entityResult);
        }
    }

}