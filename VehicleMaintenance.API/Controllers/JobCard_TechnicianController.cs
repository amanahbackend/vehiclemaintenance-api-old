﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/JobCard_Technician")]
    public class JobCard_TechnicianController : BaseController<IJobCard_TechnicianManager, JobCardTechnician,
        JobCard_TechnicianViewModel>
    {
        IServiceProvider _serviceprovider;
        IJobCard_TechnicianManager manager;
        IProcessResultMapper processResultMapper;

        public JobCard_TechnicianController(IServiceProvider serviceprovider, IJobCard_TechnicianManager _manager,
            IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        private ITechnicianManager TechnicianManager
        {
            get { return _serviceprovider.GetService<ITechnicianManager>(); }
        }

        public override ProcessResultViewModel<List<JobCard_TechnicianViewModel>> Get()
        {
            var result = base.Get();
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
            }
            return result;
        }

        public bool bindTechnicianName(List<JobCard_TechnicianViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var technicianResult = TechnicianManager.Get(item.FK_Technician_ID);
                if (technicianResult.IsSucceeded && technicianResult.Data != null)
                {
                    item.TechnicianEmploye_Id = technicianResult.Data.Employe_Id;
                    result = true;
                }
            }
            return result;
        }

        [HttpGet]
        [Route("GetByJobCardID/{id}")]
        public ProcessResultViewModel<List<JobCard_TechnicianViewModel>> GetByJobCardID(int id)
        {
            var Result = manger.GetByJobCardID(id);
            var result = processResultMapper.Map<List<JobCardTechnician>, List<JobCard_TechnicianViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
            }
            return result;
        }
    }
}