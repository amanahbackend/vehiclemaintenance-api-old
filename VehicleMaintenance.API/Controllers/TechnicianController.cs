﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Technician")]
    public class TechnicianController : BaseController<ITechnicianManager, Technician, TechnicianViewModel>
    {
        ITechnicianManager manager;
        IProcessResultMapper processResultMapper;
        public TechnicianController(ITechnicianManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        [Route("SearchByName_EmployeId")]
        [HttpGet()]
        public ProcessResultViewModel<List<TechnicianViewModel>> SearchByName_EmployeId([FromQuery]string word)
        {
            var entityResult = manager.SearchByName_EmployeId(word);
            var result = processResultMapper.Map<List<Technician>, List<TechnicianViewModel>>(entityResult);
            return result;
        }

    }
}