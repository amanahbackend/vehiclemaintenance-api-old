﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;
using Utilites.UploadFile;
using Microsoft.Extensions.DependencyInjection;
using VehicleMaintenance.models.Settings;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/File")]
    public class FileController : BaseController<IFileManager, File, FileViewModel>
    {
        VehicleAppSettings settings;
        IHostingEnvironment hostingEnv;
        IUploadFileManager fileManager;
        IFileManager manager;
        IServiceProvider _serviceprovider;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IHttpContextAccessor httpContextAccessor;
        public FileController(IServiceProvider serviceprovider, IUploadFileManager _fileManager, IHostingEnvironment _hostingEnv, IOptions<VehicleAppSettings> _settings, IFileManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IHttpContextAccessor _httpContextAccessor) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            settings = _settings.Value;
            hostingEnv = _hostingEnv;
            fileManager = _fileManager;
            manager = _manager;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            httpContextAccessor = _httpContextAccessor;
            _serviceprovider = serviceprovider;
        }

        private IVendorManager Vendormanager
        {
            get
            {
                return _serviceprovider.GetService<IVendorManager>();
            }
        }
        private IJobCardManager JobCardManager
        {
            get
            {
                return _serviceprovider.GetService<IJobCardManager>();
            }
        }
        private IRepairRequestManager RepairRequestManager
        {
            get
            {
                return _serviceprovider.GetService<IRepairRequestManager>();
            }
        }
        private IVehicleManager VehicleManager
        {
            get
            {
                return _serviceprovider.GetService<IVehicleManager>();
            }
        }

        public ProcessResultViewModel<FileViewModel> SubmitFile([FromBody]FileViewModel model)
        {
            var result = base.Post(model);
            if (result != null && result.IsSucceeded)
            {
                result.Data.File = model.File;
                if (settings.RepairFilePath == null)
                {
                    settings.RepairFilePath = "OSP-Work-order";
                }
                var path = "";
                if(result.Data.FK_RepairRequest_ID==0)
                {
                     path = hostingEnv.WebRootPath + "/" + settings.RepairFilePath + "/" + result.Data.FK_JobCard_ID;
                }
                else
                {
                     path = hostingEnv.WebRootPath + "/" + settings.RepairFilePath + "/" + result.Data.FK_RepairRequest_ID;
                }
                result.Data.File.FileName = result.Data.FileName;
                if (result.Data.File.FileName ==null)
                {
                    result.Data.FileName = model.FileName;
                }
                var fileResult = fileManager.AddFile(result.Data.File, path);
                if (fileResult.IsSucceeded)
                {
                    result.Data.FilePath = $"{path}/{result.Data.File.FileName}";
                    result.Data = BindFilesURL(result.Data);

                    var updateResult = base.Put(result.Data);
                    
                }
            }
            return result;
        }
        [HttpPost]
        [Route("SubmitFile")]
        public ProcessResultViewModel<List<FileViewModel>> SubmitFile([FromBody]List<FileViewModel> model)
        {
            List<FileViewModel> data = new List<FileViewModel>();
            foreach (var item in model)
            {
                var fileResult = SubmitFile(item);
                if (fileResult.IsSucceeded)
                {
                    data.Add(fileResult.Data);
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<FileViewModel, List<FileViewModel>>(fileResult);

                }

            }
            return ProcessResultViewModelHelper.Succedded(data);
        }

        public bool bindVendor_Name(List<FileViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var VendorResult = Vendormanager.Get(item.FK_Vendor_ID);
                if (VendorResult.IsSucceeded && VendorResult.Data != null)
                {
                    item.Vendor_Name = VendorResult.Data.Name;
                    result = true;
                }
            }

            return result;
        }
        public bool bindVendor_Name(FileViewModel data)
        {
                bool result = false;
                var VendorResult = Vendormanager.Get(data.FK_Vendor_ID);
                if (VendorResult.IsSucceeded && VendorResult.Data != null)
                {
                    data.Vendor_Name = VendorResult.Data.Name;
                    result = true;
                }

           return result;
        }
        public bool bindVendor_Address(List<FileViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var VendorResult = Vendormanager.Get(item.FK_Vendor_ID);
                if (VendorResult.IsSucceeded && VendorResult.Data != null)
                {
                    item.Vendor_Address = VendorResult.Data.Address;
                    result = true;
                }
            }

            return result;
        }
        public bool bindVendor_Address(FileViewModel data)
        {
            bool result = false;
            var VendorResult = Vendormanager.Get(data.FK_Vendor_ID);
            if (VendorResult.IsSucceeded && VendorResult.Data != null)
            {
                data.Vendor_Address = VendorResult.Data.Address;
                result = true;
            }

            return result;
        }
        private List<FileViewModel> BindFilesURL(List<FileViewModel> model)
        {
            foreach (var item in model)
            {
                BindFilesURL(item);
            }
            return model;
        }
        private FileViewModel BindFilesURL(FileViewModel model)
        {
          var req=  httpContextAccessor.HttpContext.Request     ;
            string baseURl = $"{req.Scheme}://{req.Host}{req.PathBase}";

            if (model.FilePath != null)
            {
                model.FilePath = model.FilePath.Replace('\\', '/');
                var segments = model.FilePath.Split('/');
                model.FilePath = model.FilePath.Remove(0, segments[1].Length + 1).Remove(0, segments[2].Length + 2).Remove(0, segments[3].Length + 3).Remove(0, segments[4].Length + 1);
                model.FileURL = $"{baseURl}/{model.FilePath}";
            }
            return model;
        }
        public bool bindvehicleData(List<FileViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var repairrequestResult = RepairRequestManager.Get(item.FK_RepairRequest_ID);
                if (repairrequestResult.IsSucceeded && repairrequestResult.Data != null)
                {
                    var jobcardResult = JobCardManager.Get(repairrequestResult.Data.FK_JobCard_ID);
                    item.RepairRequest_Cost = repairrequestResult.Data.Cost;
                    if (jobcardResult.IsSucceeded && jobcardResult.Data != null)
                    {
                        var vehicleResult = VehicleManager.Get(jobcardResult.Data.FK_Vehicle_ID);
                        if (vehicleResult.IsSucceeded && vehicleResult.Data != null)
                        {
                            item.VehicleNumberPlate = vehicleResult.Data.VehicleNumberPlate;
                            item.FleetNumber = vehicleResult.Data.FleetNumber;
                            
                        }
                    }
                    result = true;
                }
            }
            return result;
        }
        public bool bindvehicleData(FileViewModel data)
        {
            bool result = false;
            var repairrequestResult = RepairRequestManager.Get(data.FK_RepairRequest_ID);
            if (repairrequestResult.IsSucceeded && repairrequestResult.Data != null)
            {
                var jobcardResult = JobCardManager.Get(repairrequestResult.Data.FK_JobCard_ID);
                if (jobcardResult.IsSucceeded && jobcardResult.Data != null)
                {
                    var vehicleResult = VehicleManager.Get(jobcardResult.Data.FK_Vehicle_ID);
                    if (vehicleResult.IsSucceeded && vehicleResult.Data != null)
                    {
                        data.VehicleNumberPlate = vehicleResult.Data.VehicleNumberPlate;
                        data.FleetNumber = vehicleResult.Data.FleetNumber;
                    }
                }
                result = true;
            }
            return result;
        }

        public ProcessResultViewModel<List<FileViewModel>> GetByRepairRequestID(int repairId)
        {
            var entityResult = manager.GetByRepairRequestID(repairId);
            var result = processResultMapper.Map<List<File>, List<FileViewModel>>(entityResult);
            if (result.Data.Count()>0)
            {
                result.IsSucceeded = bindVendor_Name(result.Data);
                result.IsSucceeded = bindVendor_Address(result.Data);
            }
            return result;
        }

        [HttpGet]
        [Route("GetByRepairRequestIDandChoosedVendor/{repairId}")]
        public ProcessResultViewModel<List<FileViewModel>> GetByRepairRequestIDandChoosedVendor(int repairId)
        {
            var entityResult = manager.GetByRepairRequestIDandChoosedVendor(repairId);
            var result = processResultMapper.Map<List<File>, List<FileViewModel>>(entityResult);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindVendor_Name(result.Data);
                result.IsSucceeded = bindVendor_Address(result.Data);
                result.IsSucceeded = bindvehicleData(result.Data);
            }
            return result;
        }

        [Route("UpdateChoosedVendor")]
        public ProcessResultViewModel<bool> UpdateChoosedVendor([FromQuery]int repairId, int FK_Vendor_ID)
        {
            var entityResult = manager.UpdateChoosedVendor(repairId, FK_Vendor_ID);
            var result = processResultMapper.Map<bool, bool>(entityResult);
            
            return result;
        }

        [HttpGet]
        public ProcessResultViewModel<List<FileViewModel>> GetByVendorID(int vendorId)
        {
            var entityResult = manager.GetByVendorID(vendorId);
            var result = processResultMapper.Map<List<File>, List<FileViewModel>>(entityResult);
            if (result.Data.Count() > 0)
            {
                bindVendor_Name(result.Data);
                bindVendor_Address(result.Data);
                bindvehicleData(result.Data);
            }
            return result;
        }
    }
}