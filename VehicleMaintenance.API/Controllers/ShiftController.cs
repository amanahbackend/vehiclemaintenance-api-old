﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.Models.Entities.Common;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Shift")]
    public class ShiftController : BaseController
    {
        private readonly IShiftManager _shiftManager;

        public ShiftController(IShiftManager manager, IVehicleManager vehicleManager,
            IProcessResultMapper processResultMapper)
        {
            ProcessResultMapper = processResultMapper;
            _shiftManager = manager;
        }

        [HttpGet]
        [Route("GetAll")]
        public ProcessResultViewModel<List<ShiftViewModel>> Get()
        {
            var entityResult = _shiftManager.GetList();
            return ProcessResultMapper.Map<List<Shift>, List<ShiftViewModel>>(entityResult);
        }
    }
}
