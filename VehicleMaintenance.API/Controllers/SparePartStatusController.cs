﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/SparePartStatus")]
    public class SparePartStatusController : BaseController<ISparePartStatusManager, SparePartStatus, SparePartStatusViewModel>
    {
        public SparePartStatusController(ISparePartStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) 
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }
    }
}