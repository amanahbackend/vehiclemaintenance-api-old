﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.BLL.Manager;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/RepairRequestType")]
    public class RepairRequestTypeController : BaseController<IRepairRequestTypeManager, RepairRequestType, RepairRequestTypeViewModel>
    {
        public RepairRequestTypeController(IRepairRequestTypeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }
    }
}