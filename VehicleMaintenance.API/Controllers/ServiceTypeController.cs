﻿using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.Models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/ServiceType")]
    public class ServiceTypeController : BaseController<IServiceTypeManager, ServiceType, ServiceTypeViewModel>
    {
        private readonly IServiceTypeManager _manager;
        private readonly IProcessResultMapper _processResultMapper;

        public ServiceTypeController(IServiceTypeManager manager, IMapper mapper,
            IProcessResultMapper processResultMapper, IProcessResultPaginatedMapper processResultPaginatedMapper) :
            base(manager, mapper, processResultMapper, processResultPaginatedMapper)
        {
            _manager = manager;
            _processResultMapper = processResultMapper;
        }

        [HttpGet]
        [Route("GetByMaintenanceType/{type}")]
        public ProcessResultViewModel<List<ServiceTypeViewModel>> GetByMaintenanceType([FromRoute] bool type)
        {
            var entityResult = _manager.GetByMaintenanceType(type);
            return _processResultMapper.Map<List<ServiceType>, List<ServiceTypeViewModel>>(entityResult);
        }
    }
}
