using AutoMapper;
using Dispatching.Identity.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace Dispatching.Identity.API.Controllers
{
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private IRoleManager _RoleManager;

        public RoleController(IRoleManager RoleManager)
        {
            _RoleManager = RoleManager;
        }

        [HttpPost, Route("Add")]
        public async Task<ProcessResultViewModel<RoleViewModel>> Add([FromBody] RoleViewModel roleViewModel)
        {
            ProcessResultViewModel<RoleViewModel> result = null;
            try
            {
                var role = Mapper.Map<RoleViewModel, Role>(roleViewModel);
                role = await _RoleManager.AddRoleAsync(role);
                if (role != null)
                {
                    roleViewModel = Mapper.Map<Role, RoleViewModel>(role);
                    result = ProcessResultViewModelHelper.Succedded(roleViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<RoleViewModel>(null, "Couldn't create role");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<RoleViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete/{roleName}")]
        public async Task<ProcessResultViewModel<bool>> Delete([FromRoute]string roleName)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeleted = await _RoleManager.Delete(roleName);
                result = ProcessResultViewModelHelper.Succedded(isDeleted);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Update")]
        public async Task<ProcessResultViewModel<RoleViewModel>> Update([FromBody] RoleViewModel roleViewModel)
        {
            ProcessResultViewModel<RoleViewModel> result = null;
            try
            {
                var role = Mapper.Map<RoleViewModel, Role>(roleViewModel);
                role = await _RoleManager.Update(role);
                if (role != null)
                {
                    roleViewModel = Mapper.Map<Role, RoleViewModel>(role);
                    result = ProcessResultViewModelHelper.Succedded(roleViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<RoleViewModel>(null, "Role can not be updated");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<RoleViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAll")]
        public ProcessResultViewModel<List<RoleViewModel>> GetAll()
        {
            ProcessResultViewModel<List<RoleViewModel>> result = null;
            try
            {
                var roles = _RoleManager.GetAll();
                var roleViewModels = Mapper.Map<List<Role>, List<RoleViewModel>>(roles);
                result = ProcessResultViewModelHelper.Succedded(roleViewModels);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<RoleViewModel>>(null, ex.Message);
            }
            return result;
        }

    }
}
