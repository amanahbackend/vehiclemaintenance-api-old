﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Vehicle")]
    public class VehicleController : BaseController<IVehicleManager, Vehicle, VehicleViewModel>
    {
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IVehicleManager _manager;

        public VehicleController(IVehicleManager manager, IMapper mapper, IProcessResultMapper processResultMapper,
            IProcessResultPaginatedMapper processResultPaginatedMapper) : base(manager, mapper, processResultMapper,
            processResultPaginatedMapper)
        {
            _processResultMapper = processResultMapper;
            _manager = manager;
        }

        /// <summary>
        /// Add Vehicle.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ProcessResultViewModel<VehicleViewModel> Post([FromBody] VehicleViewModel model)
        {
            // Confirm that the vehicle plate no doesn't exist.
            var plateNoExists = _manager.IsPlateNoExists(model.VehicleNumberPlate, null);

            if (plateNoExists.Data)
            {
                return ProcessResultViewModelHelper.Failed<VehicleViewModel>(null, "Vehicle plate number already exists");
            }

            // Confirm that the vehicle plate no doesn't exist.
            var fleetNoExists = _manager.IsFleetExists(model.FleetNumber, null);

            if (fleetNoExists.Data)
            {
                return ProcessResultViewModelHelper.Failed<VehicleViewModel>(null, "Vehicle fleet number already exists");
            }

            // Fix purchase date
            model.PurchaseDate = FixPurchaseDate(model.PurchaseDate);

            return base.Post(model);
        }

        /// <summary>
        /// Update Vchicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ProcessResultViewModel<bool> Put([FromBody] VehicleViewModel model)
        {
            // Confirm that the vehicle plate no doesn't exist.
            var plateNoExists = _manager.IsPlateNoExists(model.VehicleNumberPlate, model.Id);

            if (plateNoExists.Data)
            {
                return ProcessResultViewModelHelper.Failed(false, "Vehicle plate number already exists");
            }

            // Confirm that the vehicle plate no doesn't exist.
            var fleetNoExists = _manager.IsFleetExists(model.FleetNumber, model.Id);

            if (fleetNoExists.Data)
            {
                return ProcessResultViewModelHelper.Failed(false, "Vehicle fleet number already exists");
            }

            // Fix purchase date
            model.PurchaseDate = FixPurchaseDate(model.PurchaseDate);

            return base.Put(model);
        }

        private static DateTime FixPurchaseDate(DateTime modelPurchaseDate)
        {
            if (modelPurchaseDate.Year < 100)
            {
                return new DateTime(1900 + modelPurchaseDate.Year, modelPurchaseDate.Month, modelPurchaseDate.Day);
            }

            return modelPurchaseDate;
        }

        [HttpGet]
        [Route("GetVehiclesPerPage")]
        public ProcessResultViewModel<List<VehicleViewModel>> GetVehiclesPerPage(int pageNo, int pageSize)
        {
            var result = _manager.GetVehiclesPerPage(pageNo, pageSize);
            return _processResultMapper.Map<List<Vehicle>, List<VehicleViewModel>>(result);
        }

        [Route("Search/{Word}")]
        [HttpGet]
        public ProcessResultViewModel<List<VehicleViewModel>> Search([FromRoute] string word)
        {
            var entityResult = _manager.Search(word);
            return _processResultMapper.Map<List<Vehicle>, List<VehicleViewModel>>(entityResult);
        }

        [Route("GetByType/{Type}")]
        [HttpGet]
        public ProcessResultViewModel<List<VehicleViewModel>> GetByType([FromRoute] string type)
        {
            var entityResult = _manager.GetByType(type);
            return _processResultMapper.Map<List<Vehicle>, List<VehicleViewModel>>(entityResult);
        }

        [HttpGet]
        [Route("SearchVehiclesRowsCount")]
        public ProcessResultViewModel<int> SearchVehiclesRowsCount(
            [FromQuery] string plateNo, string fleetNo, string model, string vehicleStatus)
        {
            var rowsCount = _manager.SearchVehiclesRowsCount(plateNo, fleetNo, model, vehicleStatus);

            var result = _processResultMapper.Map<int, int>(rowsCount);

            return result;
        }

        [HttpGet]
        [Route("SearchVehicles")]
        public ProcessResultViewModel<List<VehicleViewModel>> SearchVehicles(
            [FromQuery] string plateNo, string fleetNo, string model, string vehicleStatus, int pageNo, int pageSize)
        {
            var entityResult = _manager.SearchVehicles(plateNo, fleetNo, model, vehicleStatus, pageNo, pageSize);
            var result = _processResultMapper.Map<List<Vehicle>, List<VehicleViewModel>>(entityResult);

            return result;
        }

        //[HttpGet]
        //[Route("IsFleetExist/{fleetNo}")]
        //public ProcessResultViewModel<bool> IsFleetExist([FromRoute] string fleetNo)
        //{
        //    var entityResult = _manager.IsFleetExists(fleetNo, null);
        //    return _processResultMapper.Map<bool, bool>(entityResult);
        //}
    }
}