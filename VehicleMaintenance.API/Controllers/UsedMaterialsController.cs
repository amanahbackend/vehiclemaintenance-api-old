﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/UsedMaterials")]
    public class UsedMaterialsController : BaseController<IUsedMaterialsManager, UsedMaterials, UsedMaterialsViewModel>
    {
        IServiceProvider _serviceprovider;
        IUsedMaterialsManager manager;
        IProcessResultMapper processResultMapper;

        public UsedMaterialsController(IServiceProvider serviceprovider, IUsedMaterialsManager _manager,
            IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        private ITechnicianManager TechnicianManager
        {
            get { return _serviceprovider.GetService<ITechnicianManager>(); }
        }

        private IRepairRequestManager RepairRequestManager
        {
            get { return _serviceprovider.GetService<IRepairRequestManager>(); }
        }

        private IInventoryManager InventoryManager
        {
            get { return _serviceprovider.GetService<IInventoryManager>(); }
        }

        private ISparePartStatusManager SparePartStatusManager
        {
            get { return _serviceprovider.GetService<ISparePartStatusManager>(); }
        }

        public override ProcessResultViewModel<List<UsedMaterialsViewModel>> Get()
        {
            var result = base.Get();
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartSerialNumber(result.Data);
                result.IsSucceeded = bindSparePartStatusName(result.Data);

            }

            return result;
        }

        public bool bindTechnicianName(List<UsedMaterialsViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var technicianResult = TechnicianManager.Get(item.FK_Technician_ID);
                if (technicianResult.IsSucceeded && technicianResult.Data != null)
                {
                    item.TechnicianEmploye_Id = technicianResult.Data.Employe_Id;
                    result = true;
                }
            }

            return result;
        }

        public bool bindSparePartStatusName(List<UsedMaterialsViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var sparePartStatusResult = SparePartStatusManager.Get(item.FK_SparePartStatus_ID);
                if (sparePartStatusResult.IsSucceeded && sparePartStatusResult.Data != null)
                {
                    item.SparePartStatus_Name = sparePartStatusResult.Data.StatusName_EN;
                    result = true;
                }
            }

            return result;
        }

        public bool bindSparePartSerialNumber(List<UsedMaterialsViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var inventoryManagerResult = InventoryManager.Get(item.FK_Inventory_ID);
                if (inventoryManagerResult.IsSucceeded && inventoryManagerResult.Data != null)
                {
                    item.SparePart_SerialNumber = inventoryManagerResult.Data.SerialNumber;
                    result = true;
                }
            }

            return result;
        }

        [HttpGet]
        [Route("GetByJobCardID/{id}")]
        public ProcessResultViewModel<List<UsedMaterialsViewModel>> GetByJobCardID(int id)
        {
            var Result = manger.GetByJobCardID(id);
            var result = processResultMapper.Map<List<UsedMaterials>, List<UsedMaterialsViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartSerialNumber(result.Data);
                result.IsSucceeded = bindSparePartStatusName(result.Data);
            }

            return result;
        }

        [HttpGet]
        [Route("GetByRepairRequestID/{id}")]
        public ProcessResultViewModel<List<UsedMaterialsViewModel>> GetByRepairRequestID(int id)
        {
            var Result = manger.GetByRepairRequestID(id);
            var result = processResultMapper.Map<List<UsedMaterials>, List<UsedMaterialsViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartSerialNumber(result.Data);
                result.IsSucceeded = bindSparePartStatusName(result.Data);
            }

            return result;
        }

        [HttpPost]
        [Route("GetByRepairRequestIDs")]
        public ProcessResultViewModel<List<UsedMaterialsViewModel>> GetByRepairRequestIDs([FromBody] List<int?> repairrequestIds)
        {
            var Result = manger.GetByRepairRequestIDs(repairrequestIds);
            var result = processResultMapper.Map<List<UsedMaterials>, List<UsedMaterialsViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartSerialNumber(result.Data);
                result.IsSucceeded = bindSparePartStatusName(result.Data);
            }

            return result;
        }

        [HttpGet, Route("GetAllEquipmentTransfer")]
        public ProcessResultViewModel<List<EquipmentTransferViewModel>> GetAllEquipmentTransfer()
        {
            ProcessResultViewModel<List<EquipmentTransferViewModel>> result;
            try
            {
                var getResult = manager.GetAllEquipmentTransfer();
                var models = mapper.Map<List<EquipmentTransfer>, List<EquipmentTransferViewModel>>(getResult.Data);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<EquipmentTransferViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetEquipmentTransferByDate")]
        public ProcessResultViewModel<List<EquipmentTransferViewModel>> GetEquipmentTransferByDate([FromQuery]DateTime date)
        {
            ProcessResultViewModel<List<EquipmentTransferViewModel>> result;
            try
            {
                var getResult = manager.GetEquipmentTransferByDate(date);
                var models = mapper.Map<List<EquipmentTransfer>, List<EquipmentTransferViewModel>>(getResult.Data);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<EquipmentTransferViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetEquipmentTransferByVehicleFrom")]
        public ProcessResultViewModel<List<EquipmentTransferViewModel>> GetEquipmentTransferByVehicleFrom([FromQuery]string fleetNumber)
        {
            ProcessResultViewModel<List<EquipmentTransferViewModel>> result;
            try
            {
                var getResult = manager.GetEquipmentTransferByVehicleFrom(fleetNumber);
                var models = mapper.Map<List<EquipmentTransfer>, List<EquipmentTransferViewModel>>(getResult.Data);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<EquipmentTransferViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetEquipmentTransferByVehicleTo")]
        public ProcessResultViewModel<List<EquipmentTransferViewModel>> GetEquipmentTransferByVehicleTo([FromQuery]string fleetNumber)
        {
            ProcessResultViewModel<List<EquipmentTransferViewModel>> result;
            try
            {
                var getResult = manager.GetEquipmentTransferByVehicleTo(fleetNumber);
                var models = mapper.Map<List<EquipmentTransfer>, List<EquipmentTransferViewModel>>(getResult.Data);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<EquipmentTransferViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetEquipmentTransferBySparePart")]
        public ProcessResultViewModel<List<EquipmentTransferViewModel>> GetEquipmentTransferBySparePart([FromQuery]int sparePartId)
        {
            ProcessResultViewModel<List<EquipmentTransferViewModel>> result;
            try
            {
                var getResult = manager.GetEquipmentTransferBySparePart(sparePartId);
                var models = mapper.Map<List<EquipmentTransfer>, List<EquipmentTransferViewModel>>(getResult.Data);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<EquipmentTransferViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet]
        [Route("SearchBysparePartSerialNumber_cost")]
        public ProcessResultViewModel<List<UsedMaterialsViewModel>> SearchBysparePartSerialNumber_cost([FromQuery]int repairrequestId, string word)
        {

            var Result = manger.SearchBysparePartSerialNumber_cost(repairrequestId, word);
            var result = processResultMapper.Map<List<UsedMaterials>, List<UsedMaterialsViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartSerialNumber(result.Data);
                result.IsSucceeded = bindSparePartStatusName(result.Data);
            }
            return result;
        }

        [HttpGet]
        [Route("GetByJobCardID2/{id}")]
        public ProcessResultViewModel<List<UsedMaterialsViewModel>> GetByJobCardID2(int id)
        {
            var repairRes = RepairRequestManager.GetByJobCardID(id).Data.ToList();
            List<int?> repairRequestIDs = null;
            foreach (var item in repairRes)
            {
                repairRequestIDs.Add(item.Id);
            }
            
            var Result = manger.GetByRepairRequestIDs(repairRequestIDs);
            var result = processResultMapper.Map<List<UsedMaterials>, List<UsedMaterialsViewModel>>(Result);
            if (result.Data.Count() > 0)
            {
                result.IsSucceeded = bindTechnicianName(result.Data);
                result.IsSucceeded = bindSparePartSerialNumber(result.Data);
                result.IsSucceeded = bindSparePartStatusName(result.Data);
            }

            return result;
        }
    }
}
