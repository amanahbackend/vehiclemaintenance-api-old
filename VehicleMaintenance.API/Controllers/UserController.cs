using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using AutoMapper;
using Dispatching.Identity.API.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using VehicleMaintenance.API.Utilities;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{

    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IUserManager _userManager;
        private readonly IConfigurationRoot _configuration;
        private readonly IPasswordTokenPinManager _passwordTokenPinManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IUploadImageFileManager _imageManager;

        public UserController(IUserManager userManager,
            IHostingEnvironment hostingEnv, IConfigurationRoot configuration,
            IPasswordTokenPinManager passwordTokenPinManager, IPasswordHasher<User> passwordHasher,
            IUploadImageFileManager imageManager)
        {
            _passwordHasher = passwordHasher;
            _passwordTokenPinManager = passwordTokenPinManager;
            _configuration = configuration;
            _hostingEnv = hostingEnv;
            _userManager = userManager;
            _imageManager = imageManager;
        }


        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginViewModel)
        {
            var response = await TokenManager.GetToken(loginViewModel, _userManager, _passwordHasher);

            if (response != null && response.Item2 != null && response.Item1 != null)
            {
                return Ok(new
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(response.Item2),
                    ExpirationTimeUTC = response.Item2.ValidTo,
                    UserName = response.Item1.UserName,
                    Roles = response.Item1.RoleNames,
                    userId = response.Item1.Id
                });
            }

            return Unauthorized();
        }

        [HttpGet, Route("Count")]
        public ProcessResultViewModel<int> Count()
        {
            var entityResult = _userManager.Count();
            if (entityResult.IsSucceeded)
            {
                return ProcessResultViewModelHelper.Succedded(entityResult.Data);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(0, entityResult.Exception.Message);
            }
        }

        [HttpPost, Route("Add")]
        public async Task<ProcessResultViewModel<UserViewModel>> Add([FromBody] UserViewModel userViewModel)
        {
            ProcessResultViewModel<UserViewModel> result;

            try
            {
                if (userViewModel.Picture != null)
                {
                    userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                        userViewModel.UserName, _imageManager);
                }

                User user = Mapper.Map<UserViewModel, User>(userViewModel);
                user = await _userManager.AddUserAsync(user, userViewModel.Password);

                if (user?.Id != null)
                {
                    userViewModel = Mapper.Map<User, UserViewModel>(user);
                    result = ProcessResultViewModelHelper.Succedded(userViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<UserViewModel>(null,
                        "An error occured while adding user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<UserViewModel>(null, ex.Message);
            }

            return result;
        }

        [HttpGet, Route("GetBy")]
        public async Task<ProcessResultViewModel<UserViewModel>> GetBy([FromQuery] string username)
        {
            ProcessResultViewModel<UserViewModel> result;
            try
            {
                User user = await _userManager.GetBy(username);
                if (user != null)
                {
                    //user.Picture = ProfilePictureManager.GetProfilePictureBase64(username, _hostingEnv);
                    var userModel = Mapper.Map<User, UserViewModel>(user);
                    userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);

                    result = ProcessResultViewModelHelper.Succedded(userModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<UserViewModel>(null, "User not found");
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<UserViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Update")]
        public async Task<ProcessResultViewModel<UserViewModel>> Update([FromBody] UserViewModel userViewModel)
        {
            if (userViewModel.Picture != null)
            {
                userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                    userViewModel.UserName, _hostingEnv);
            }

            var user = Mapper.Map<UserViewModel, User>(userViewModel);
            user = await _userManager.UpdateUserAsync(user);

            if (user != null)
            {
                userViewModel = Mapper.Map<User, UserViewModel>(user);
                return ProcessResultViewModelHelper.Succedded(userViewModel);
            }

            return ProcessResultViewModelHelper.Failed<UserViewModel>(null, "An error occured while updating user");
        }

        [HttpPut, Route("UpdateUserPassword")]
        public async Task<ProcessResultViewModel<bool>> UpdateUserPassword([FromBody] UserViewModel userViewModel)
        {
            try
            {
                var user = Mapper.Map<UserViewModel, User>(userViewModel);
                bool isUserPasswordUpdated = await _userManager.UpdateUserPassword(user.Id, userViewModel.Password);

                return ProcessResultViewModelHelper.Succedded(isUserPasswordUpdated);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed(false, "An error occured while updating user password. " + ex.Message);
            }
        }

        [HttpGet, Route("GetAll")]
        public async Task<ProcessResultViewModel<List<UserViewModel>>> GetAll()
        {
            ProcessResultViewModel<List<UserViewModel>> result;
            try
            {
                var users = await _userManager.GetAll();
                if (users.Count > 0)
                {
                    List<UserViewModel> userModels = Mapper.Map<List<User>, List<UserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<UserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<UserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetUserPicture")]
        public ProcessResultViewModel<string> GetUserPicture([FromQuery] string username)
        {
            ProcessResultViewModel<string> result;
            try
            {
                var pictureBase64 = ProfilePictureManager.GetProfilePictureBase64(username, _hostingEnv);

                if (!string.IsNullOrEmpty(pictureBase64))
                {
                    result = ProcessResultViewModelHelper.Succedded(pictureBase64);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<string>(null, "No profile picture found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete/{userName}")]
        public async Task<ProcessResultViewModel<bool>> Delete([FromRoute] string username)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isDeleted = await _userManager.DeleteAsync(username);

                if (isDeleted)
                {
                    //ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("DeleteById")]
        public async Task<ProcessResultViewModel<bool>> DeleteById([FromQuery] string id)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var user = await _userManager.Get(id);
                var username = user?.UserName;
                var isDeleted = await _userManager.DeleteByIdAsunc(id);

                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }

            return result;
        }

        [HttpGet, Route("GetByRole")]
        public async Task<ProcessResultViewModel<List<UserViewModel>>> GetByRole([FromQuery] string role)
        {
            ProcessResultViewModel<List<UserViewModel>> result;
            try
            {
                var users = (List<User>)await _userManager.GetUsersInRole(role);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<User>, List<UserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<UserViewModel>>(null, "No users with this role");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<UserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsEmailExist")]
        public async Task<ProcessResultViewModel<bool>> IsEmailExist([FromQuery] string email)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isEmailExist = await _userManager.IsEmailExistAsync(email);
                result = ProcessResultViewModelHelper.Succedded(isEmailExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsUsernameExist")]
        public async Task<ProcessResultViewModel<bool>> IsUsernameExist([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isExisting = await _userManager.IsUserNameExistAsync(username);
                result = ProcessResultViewModelHelper.Succedded(isExisting);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsPhoneExist")]
        public ProcessResultViewModel<bool> IsPhoneExist([FromQuery] string phone)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isPhoneExist = _userManager.IsPhoneExist(phone);
                result = ProcessResultViewModelHelper.Succedded(isPhoneExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Search")]
        public async Task<ProcessResultViewModel<List<UserViewModel>>> Search([FromQuery] string searchToken)
        {
            ProcessResultViewModel<List<UserViewModel>> result;
            try
            {
                string[] searchFields = _configuration.GetSection("UserSearchFields").Get<string[]>();
                var users = await _userManager.Search(searchToken, searchFields);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<User>, List<UserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<UserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<UserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Get")]
        public async Task<ProcessResultViewModel<UserViewModel>> Get([FromQuery] string id)
        {
            var user = await _userManager.Get(id);

            if (user != null)
            {
                var userModel = Mapper.Map<User, UserViewModel>(user);
                userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);

                return ProcessResultViewModelHelper.Succedded(userModel);
            }

            return ProcessResultViewModelHelper.Failed<UserViewModel>(null, "User can not be found");
        }

        [HttpGet, Route("Deactivate")]
        public async Task<ProcessResultViewModel<bool>> Deactivate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isDeactivated = await _userManager.Deactivate(username);
                result = ProcessResultViewModelHelper.Succedded(isDeactivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Activate")]
        public async Task<ProcessResultViewModel<bool>> Activate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isActivated = await _userManager.Activate(username);
                result = ProcessResultViewModelHelper.Succedded(isActivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetPhoneVerficationToken")]
        public async Task<ProcessResultViewModel<string>> GetPhoneVerficationToken(
            [FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<string> result;
            try
            {
                var token = await _userManager.GeneratePhoneNumberToken(model.Username, model.Phone);
                if (model.CountryCode.ToLower().Equals("kw"))
                {
                    //SmsService.SendKW(token, model.Phone);
                }
                else if (model.CountryCode.ToLower().Equals("uae"))
                {
                    //SmsService.SendUAE(token, model.Phone);
                }
                result = ProcessResultViewModelHelper.Succedded(token);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("IsPhoneVerificationTokenValid")]
        public async Task<ProcessResultViewModel<bool>> IsPhoneVerificationTokenValid(
            [FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isValid = await _userManager.CheckPhoneValidationToken(model.Username, model.Phone, model.Token);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ConfirmPhone")]
        public async Task<ProcessResultViewModel<bool>> ConfirmPhone([FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var confirmed = await _userManager.ConfirmPhone(model.Username, model.Phone, model.Token);
                result = ProcessResultViewModelHelper.Succedded(confirmed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }

            return result;
        }

        [HttpGet, Route("GenerateForgetPasswordToken")]
        public async Task<ProcessResultViewModel<string>> GenerateForgetPasswordToken([FromQuery] string username)
        {
            ProcessResultViewModel<string> result;
            try
            {
                var token = await _userManager.GenerateForgetPasswordToken(username);
                // we will implement send sms with this token to the user phone
                result = ProcessResultViewModelHelper.Succedded(token);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ChangePassword")]
        public async Task<ProcessResultViewModel<bool>> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var changed = await _userManager.ChangePassword(model.Username, model.NewPassword, model.Token);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ResetPassword")]
        public async Task<ProcessResultViewModel<bool>> ResetPassword([FromBody] ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isReset = await _userManager.ResetPassword(model.Username, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(isReset);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsPasswordPinExist")]
        public ProcessResultViewModel<bool> IsPasswordPinExist([FromQuery] string pin)
        {
            ProcessResultViewModel<bool> result;
            try
            {
                var isExist = _passwordTokenPinManager.IsPinExist(pin);
                result = ProcessResultViewModelHelper.Succedded(isExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }

            return result;
        }

        [HttpGet, Route("IsTokenValid")]
        public ProcessResultViewModel<bool> IsTokenValid()
        {
            ProcessResultViewModel<bool> result;

            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                bool isValid = TokenManager.ValidateToken(token);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }

            return result;
        }
    }
}
