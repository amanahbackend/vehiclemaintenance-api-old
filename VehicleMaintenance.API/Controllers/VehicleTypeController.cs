﻿using System.Collections.Generic;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/VehicleType")]
    public class VehicleTypeController : BaseController<IVehicleTypeManager, VehicleType, VehicleTypeViewModel>
    {
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IVehicleTypeManager _manager;

        public VehicleTypeController(IVehicleTypeManager _manager, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this._processResultMapper = _processResultMapper;
            this._manager = _manager;
        }

        [Route("SearchByName")]
        [HttpGet]
        public ProcessResultViewModel<List<VehicleTypeViewModel>> SearchByName([FromQuery]string word)
        {
            var entityResult = _manager.SearchByName(word);
            return _processResultMapper.Map<List<VehicleType>, List<VehicleTypeViewModel>>(entityResult);
        }
    }
}