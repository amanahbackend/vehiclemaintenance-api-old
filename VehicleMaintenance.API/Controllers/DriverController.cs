﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Driver")]
    public class DriverController : BaseController<IDriverManager, Driver, DriverViewModel>
    {
        IProcessResultMapper processResultMapper;
        IDriverManager manager;
        public DriverController(IDriverManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            processResultMapper = _processResultMapper;
            manager = _manager;
        }
        [Route("SearchByName_EmployeeNum")]
        [HttpGet()]
        public ProcessResultViewModel<List<DriverViewModel>> SearchByName_EmployeeNum([FromQuery]string word)
        {
            var entityResult = manager.SearchByName_EmployeeNum(word);
            return processResultMapper.Map<List<Driver>, List<DriverViewModel>>(entityResult);
        }
    }
}