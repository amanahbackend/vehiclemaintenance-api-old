﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.BLL.IManager;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.Common;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace VehicleMaintenance.API.Controllers
{
    [Produces("application/json")]
    [Route("api/JobCard")]
    public class JobCardController : BaseController<IJobCardManager, JobCard, JobCardViewModel>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IJobCardManager _manager;
        private readonly IProcessResultMapper _processResultMapper;
        private IRepairRequestManager RepairRequestManager => _serviceProvider.GetService<IRepairRequestManager>();
        private IShiftManager ShiftManager => _serviceProvider.GetService<IShiftManager>();
        private FileController FileController => _serviceProvider.GetService<FileController>();
        private IVehicleManager VehicleManager => _serviceProvider.GetService<IVehicleManager>();
        private IDriverManager DriverManager => _serviceProvider.GetService<IDriverManager>();
        private IStatusManager StatusManager => _serviceProvider.GetService<IStatusManager>();
        private IServiceTypeManager ServiceTypeManager => _serviceProvider.GetService<IServiceTypeManager>();
        private IUserManager UserManager => _serviceProvider.GetService<IUserManager>();

        public JobCardController(IServiceProvider serviceprovider, IJobCardManager manager, IMapper mapper,
            IProcessResultMapper processResultMapper, IProcessResultPaginatedMapper processResultPaginatedMapper)
            : base(manager, mapper, processResultMapper, processResultPaginatedMapper)
        {
            _serviceProvider = serviceprovider;
            _manager = manager;
            _processResultMapper = processResultMapper;
        }

        // get all 
        public override ProcessResultViewModel<List<JobCardViewModel>> Get()
        {
            var result = base.Get();

            if (result.Data.Any())
            {
                BindVehicleFleetNo(result.Data);
                BindVehiclePlateNo(result.Data);
                BindDriverName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        // get all 
        public ProcessResultViewModel<List<JobCardViewModel>> GetJobCards()
        {
            var result = base.Get();

            if (result.Data.Any())
            {
                BindVehicleFleetNo(result.Data);
                BindVehiclePlateNo(result.Data);
                BindDriverName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [HttpGet]
        [Route("GetAllClosed/{vehicleId?}")]
        public ProcessResultViewModel<List<JobCardViewModel>> GetAllClosed([FromRoute] int vehicleId)
        {
            var entityResult = _manager.GetAllClosed(vehicleId);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehicleFleetNo(result.Data);
                BindVehiclePlateNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [HttpGet]
        [Route("GetAllExceptClosed/{vehicleId?}")]
        public ProcessResultViewModel<List<JobCardViewModel>> GetAllExceptClosed([FromRoute] int vehicleId)
        {
            var entityResult = _manager.GetAllExceptClosed(vehicleId);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehicleFleetNo(result.Data);
                BindVehiclePlateNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [HttpPost]
        [Route("GetAllPaginatedExceptClosed/{vehicleId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<JobCardViewModel>> GetAllPaginatedExceptClosed(
            [FromRoute] int vehicleId, [FromBody] PaginatedItems<JobCard> model = null)
        {
            var entityResult = _manager.GetAllPaginatedExceptClosed(vehicleId, model);

            var result = _processResultMapper
                .Map<PaginatedItems<JobCard>, PaginatedItemsViewModel<JobCardViewModel>>(entityResult);

            if (result.Data.Data.Any())
            {
                BindVehicleFleetNo(result.Data.Data);
                BindVehiclePlateNo(result.Data.Data);
                BindUserName(result.Data.Data);
                BindDriverName(result.Data.Data);
                BindStatusName(result.Data.Data);
                BindServiceTypeName(result.Data.Data);
            }

            return result;
        }

        [Route("GetAllPaginatedClosed/{vehicleId?}")]
        [HttpPost]
        public ProcessResultViewModel<PaginatedItemsViewModel<JobCardViewModel>> GetAllPaginatedClosed(
            [FromRoute] int vehicleId, [FromBody] PaginatedItems<JobCard> model = null)
        {
            var entityResult = _manager.GetAllPaginatedClosed(vehicleId, model);
            var result =
                _processResultMapper
                    .Map<PaginatedItems<JobCard>, PaginatedItemsViewModel<JobCardViewModel>>(entityResult);
            if (result.Data.Data.Any())
            {
                BindVehicleFleetNo(result.Data.Data);
                BindVehiclePlateNo(result.Data.Data);
                BindDriverName(result.Data.Data);
                BindUserName(result.Data.Data);
                BindStatusName(result.Data.Data);
                BindServiceTypeName(result.Data.Data);
            }

            return result;
        }

        [HttpPost]
        [Route("GetAllPaginated")]
        public override ProcessResultViewModel<PaginatedItemsViewModel<JobCardViewModel>> GetAllPaginated(
            [FromBody] PaginatedItemsViewModel<JobCardViewModel> model = null)
        {
            var result = base.GetAllPaginated(model);
            if (result.Data.Data.Any())
            {
                BindVehicleFleetNo(result.Data.Data);
                BindVehiclePlateNo(result.Data.Data);
                BindDriverName(result.Data.Data);
                BindUserName(result.Data.Data);
                BindStatusName(result.Data.Data);
                BindServiceTypeName(result.Data.Data);
            }
            return result;
        }

        [HttpPost]
        [Route("GetAllPaginatedByPredicate")]
        public override ProcessResultViewModel<PaginatedItemsViewModel<JobCardViewModel>> GetAllPaginatedByPredicate(
            [FromBody] PaginatedItemsViewModel<JobCardViewModel> model = null,
            Expression<Func<JobCard, bool>> predicate = null)
        {
            var result = base.GetAllPaginatedByPredicate(model, predicate);
            if (result.Data.Data.Any())
            {
                BindVehicleFleetNo(result.Data.Data);
                BindVehiclePlateNo(result.Data.Data);
                BindDriverName(result.Data.Data);
                BindUserName(result.Data.Data);
                BindStatusName(result.Data.Data);
                BindServiceTypeName(result.Data.Data);
            }
            return result;
        }

        // Get
        public override ProcessResultViewModel<JobCardViewModel> Get(int id)
        {
            var result = base.Get(id);

            if (result.Data != null)
            {
                BindVehicleFleetNo(result.Data);
                BindVehiclePlateNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
                BindVehiclePlateNo(result.Data);
            }
            return result;
        }

        public override ProcessResultViewModel<List<JobCardViewModel>> GetByIds([FromBody] List<int> ids)
        {
            var result = base.GetByIds(ids);

            if (result.Data != null)
            {
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindStatusName(result.Data);
                BindUserName(result.Data);
                BindServiceTypeName(result.Data);
                BindVehiclePlateNo(result.Data);
            }

            return result;
        }

        /// <summary>
        /// Add Job Card. AddJobCard.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ProcessResultViewModel<JobCardViewModel> Post([FromBody] JobCardViewModel model)
        {
            if (model != null)
            {
                if (model.FK_Vehicle_ID == 0 && model.FleetNumber != null)
                {
                    model.FK_Vehicle_ID = VehicleManager.GetAll().Data.Find(x => x.FleetNumber == model.FleetNumber).Id;
                }

                var getResult = ServiceTypeManager.Get(model.FK_ServiceType_ID);

                if (getResult.IsSucceeded && getResult.Data != null)
                {
                    bool approved = getResult.Data.Need_Approve.Value;

                    if (approved)
                    {
                        var statusRes = StatusManager.GetAllQuerable().Data
                            .FirstOrDefault(x => x.StatusName_EN.ToLower() == "pending approved");

                        if (statusRes != null)
                        {
                            model.FK_Status_ID = statusRes.Id;
                        }
                    }
                    else
                    {
                        var statusRes = StatusManager.GetAllQuerable().Data
                            .FirstOrDefault(x => x.StatusName_EN.ToLower() == "in progress");

                        if (statusRes != null)
                        {
                            model.FK_Status_ID = statusRes.Id;
                        }
                    }

                    var driverRes = DriverManager.GetAll().Data.Find(x => x.Employe_Id == model.DriverEmploye_Id);

                    if (driverRes != null)
                    {
                        model.FK_Driver_ID = driverRes.Id;
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed(model,
                        $"There is no service type with Id {model.FK_ServiceType_ID}");
                }
            }

            UpdateOdometerOfVehicle(model.Odometer, model.FK_Vehicle_ID);

            // Set DateInShiftId and DateOutShiftId
            model.DateInShiftId = GetShiftIdOfDateTime(model.Date_In);
            model.DateOutShiftId = GetShiftIdOfDateTime(model.Date_Out);

            var result = base.Post(model);

            if (result.IsSucceeded && result.Data != null)
            {
                if (model.Files != null)
                {
                    model.Files.ForEach(x => x.FK_JobCard_ID = result.Data.Id);
                    var fileRes = FileController.SubmitFile(model.Files.ToList());

                    if (fileRes.IsSucceeded && fileRes.Data != null)
                    {
                        result.Data.Files = fileRes.Data;
                    }
                    else
                    {
                        return ProcessResultViewModelHelper
                            .MapToProcessResultViewModel<List<FileViewModel>, JobCardViewModel>(fileRes);
                    }
                }
            }

            return result;
        }

        [HttpPost]
        [Route("AddwithApprove")]
        public ProcessResultViewModel<JobCardViewModel> PostWithApproved([FromBody] JobCardViewModel model)
        {
            if (model != null)
            {
                if (model.FK_Vehicle_ID == 0 && model.FleetNumber != null)
                {
                    model.FK_Vehicle_ID = VehicleManager.GetAll().Data.Find(x => x.FleetNumber == model.FleetNumber).Id;
                }

                var statusRes = StatusManager.GetAllQuerable().Data
                    .FirstOrDefault(x => x.StatusName_EN.ToLower() == "in progress");
                model.FK_Status_ID = statusRes.Id;

                var driverRes = DriverManager.GetAll().Data.Find(x => x.Employe_Id == model.DriverEmploye_Id);
                if (driverRes != null)
                {
                    model.FK_Driver_ID = driverRes.Id;
                }
            }

            UpdateOdometerOfVehicle(model.Odometer, model.FK_Vehicle_ID);

            // Set DateInShiftId and DateOutShiftId
            model.DateInShiftId = GetShiftIdOfDateTime(model.Date_In);
            model.DateOutShiftId = GetShiftIdOfDateTime(model.Date_Out);

            var result = base.Post(model);

            if (result.IsSucceeded && result.Data != null)
            {
                if (model.Files != null)
                {
                    model.Files.ForEach(x => x.FK_JobCard_ID = result.Data.Id);
                    var fileRes = FileController.SubmitFile(model.Files.ToList());
                    if (fileRes.IsSucceeded && fileRes.Data != null)
                    {
                        result.Data.Files = fileRes.Data;
                    }
                    else
                    {
                        return ProcessResultViewModelHelper
                            .MapToProcessResultViewModel<List<FileViewModel>, JobCardViewModel>(fileRes);
                    }
                }
            }

            return result;
        }

        private int? GetShiftIdOfDateTime(DateTime? dateTime)
        {
            if (dateTime == null)
            {
                return null;
            }

            TimeSpan timeSpan = new TimeSpan(dateTime.Value.Hour, dateTime.Value.Minute, 0);

            Shift shift = ShiftManager.GetAllQuerable().Data.FirstOrDefault(
                obj => timeSpan >= obj.StartTime && timeSpan <= obj.EndTime && obj.IsDeleted == false);

            int shiftId = shift?.Id ?? 3; // Shift 3 has some exceptions. Start time=23:00:00; EndTime=06:59

            return shiftId;
        }
        
        [HttpPost]
        [Route("AddKCC_JobCard")]
        public ProcessResultViewModel<JobCardViewModel> PostKCC_JobCard([FromBody] JobCardViewModel model)
        {
            if (model != null)
            {
                if (model.FK_Vehicle_ID == 0 && model.FleetNumber != null)
                {
                    model.FK_Vehicle_ID = VehicleManager.GetAll().Data.Find(x => x.FleetNumber == model.FleetNumber).Id;
                }

                var statusRes = StatusManager.GetAllQuerable().Data
                    .FirstOrDefault(x => x.StatusName_EN.ToLower() == "in progress");

                model.FK_Status_ID = statusRes.Id;

                var driverRes = DriverManager.GetAll().Data.Find(x => x.Employe_Id == model.DriverEmploye_Id);
                if (driverRes != null)
                {
                    model.FK_Driver_ID = driverRes.Id;
                }
            }

            UpdateOdometerOfVehicle(model.Odometer, model.FK_Vehicle_ID);

            var result = base.Post(model);

            if (result.IsSucceeded && result.Data != null)
            {
                if (model.Files != null)
                {
                    model.Files.ForEach(x => x.FK_JobCard_ID = result.Data.Id);
                    var fileRes = FileController.SubmitFile(model.Files.ToList());
                    if (fileRes.IsSucceeded && fileRes.Data != null)
                    {
                        result.Data.Files = fileRes.Data;
                    }
                    else
                    {
                        return ProcessResultViewModelHelper
                            .MapToProcessResultViewModel<List<FileViewModel>, JobCardViewModel>(fileRes);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Update Job Card. UpdateJobCard.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ProcessResultViewModel<bool> Put([FromBody] JobCardViewModel model)
        {
            ProcessResultViewModel<bool> result;

            if (model != null)
            {
                if (model.FK_Vehicle_ID == 0 && model.FleetNumber != null)
                {
                    model.FK_Vehicle_ID = VehicleManager.GetAll().Data.Find(x => x.FleetNumber == model.FleetNumber).Id;
                }

                var driverRes = DriverManager.GetAll().Data.Find(x => x.Employe_Id == model.DriverEmploye_Id);

                if (driverRes != null)
                {
                    model.FK_Driver_ID = driverRes.Id;
                }

                // Not to close job card if it has some unclosed repair requests.
                if (model.FK_Status_ID == (int)EnumJobCardStatus.Closed) // Closed.
                {
                    // Get all non-deleted repaire requests for this job card.
                    int repairRequestsCount = RepairRequestManager.GetAllQuerable().Data.Count(
                        obj => obj.FK_JobCard_ID == model.Id && obj.IsDeleted == false);

                    // Get all non-deleted & completed repaire requests for this job card.
                    int completedRepairRequestsCount = RepairRequestManager.GetAllQuerable().Data.Count(
                        obj => obj.FK_JobCard_ID == model.Id
                               && obj.FK_RepairRequestStatus_ID == (int)EnumRepairRequestStatus.Closed // Closed.
                               && obj.IsDeleted == false);

                    if (repairRequestsCount != completedRepairRequestsCount)
                    {
                        result = new ProcessResultViewModel<bool>
                        {
                            IsSucceeded = false
                        };

                        return result;
                    }
                }
            }

            UpdateOdometerOfVehicle(model.Odometer, model.FK_Vehicle_ID);

            result = base.Put(model);

            return result;
        }

        [HttpGet]
        [Route("GetChart")]
        public ProcessResultViewModel<List<ChartViewModel>> GetChart()
        {
            var entityRes = _manager.GetChart();
            return _processResultMapper.Map<List<ChartViewModel>, List<ChartViewModel>>(entityRes);
        }

        public bool BindVehicleFleetNo(List<JobCardViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var vehicleResult = VehicleManager.Get(item.FK_Vehicle_ID);
                if (vehicleResult.IsSucceeded && vehicleResult.Data != null)
                {
                    item.FleetNumber = vehicleResult.Data.FleetNumber;
                    result = true;
                }
            }

            return result;
        }

        public bool BindVehiclePlateNo(List<JobCardViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var vehicleResult = VehicleManager.Get(item.FK_Vehicle_ID);
                if (vehicleResult.IsSucceeded && vehicleResult.Data != null)
                {
                    item.VehicleNumberPlate = vehicleResult.Data.VehicleNumberPlate;
                    result = true;
                }
            }

            return result;
        }

        public bool BindDriverName(List<JobCardViewModel> data)
        {
            bool result = false;

            foreach (var item in data)
            {
                var driverResult = DriverManager.Get(item.FK_Driver_ID);

                if (driverResult.IsSucceeded && driverResult.Data != null)
                {
                    item.DriverEmploye_Id = driverResult.Data.Employe_Id;
                    item.DriverName = $"{driverResult.Data.FirstName} {driverResult.Data.LastName}";

                    result = true;
                }
            }

            return result;
        }

        public bool BindStatusName(List<JobCardViewModel> data)
        {
            bool result = false;

            foreach (var item in data)
            {
                var statusResult = StatusManager.Get(item.FK_Status_ID);

                if (statusResult.IsSucceeded && statusResult.Data != null)
                {
                    item.StatusName = statusResult.Data.StatusName_EN;
                    result = true;
                }
            }

            return result;
        }

        public bool BindUserName(List<JobCardViewModel> data)
        {
            bool result = false;

            if (data.Any())
            {
                foreach (var item in data)
                {
                    var userNameResult = UserManager.Get(item.FK_User_ID);

                    if (userNameResult.Result != null)
                    {
                        item.User_Name = $"{userNameResult.Result.FirstName} {userNameResult.Result.LastName}";
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        public bool BindServiceTypeName(List<JobCardViewModel> data)
        {
            bool result = false;
            foreach (var item in data)
            {
                var serviceTypeResult = ServiceTypeManager.Get(item.FK_ServiceType_ID);
                if (serviceTypeResult.IsSucceeded && serviceTypeResult.Data != null)
                {
                    item.ServiceTypeName = serviceTypeResult.Data.ServiceTypeName_EN;
                    result = true;
                }
            }
            return result;
        }

        public bool BindVehicleFleetNo(JobCardViewModel data)
        {
            bool result = false;

            var vehicleResult = VehicleManager.Get(data.FK_Vehicle_ID);
            if (vehicleResult.IsSucceeded && vehicleResult.Data != null)
            {
                data.FleetNumber = vehicleResult.Data.FleetNumber;
                result = true;
            }
            return result;
        }

        public bool BindVehiclePlateNo(JobCardViewModel data)
        {
            bool result = false;
            var vehicleResult = VehicleManager.Get(data.FK_Vehicle_ID);
            if (vehicleResult.IsSucceeded && vehicleResult.Data != null)
            {
                data.VehicleNumberPlate = vehicleResult.Data.VehicleNumberPlate;
                result = true;
            }
            return result;
        }

        public bool BindDriverName(JobCardViewModel data)
        {
            bool result = false;
            var driverResult = DriverManager.Get(data.FK_Driver_ID);

            if (driverResult.IsSucceeded && driverResult.Data != null)
            {
                data.DriverEmploye_Id = driverResult.Data.Employe_Id;
                data.DriverName = $"{driverResult.Data.FirstName} {driverResult.Data.LastName}";
                result = true;
            }

            return result;
        }

        public bool BindStatusName(JobCardViewModel data)
        {
            bool result = false;
            var statusResult = StatusManager.Get(data.FK_Status_ID);
            if (statusResult.IsSucceeded && statusResult.Data != null)
            {
                data.StatusName = statusResult.Data.StatusName_EN;
                result = true;
            }
            return result;
        }

        public bool BindServiceTypeName(JobCardViewModel data)
        {
            bool result = false;
            var serviceTypeResult = ServiceTypeManager.Get(data.FK_ServiceType_ID);
            if (serviceTypeResult.IsSucceeded && serviceTypeResult.Data != null)
            {
                data.ServiceTypeName = serviceTypeResult.Data.ServiceTypeName_EN;
                result = true;
            }
            return result;
        }

        public bool BindUserName(JobCardViewModel data)
        {
            bool result = false;
            if (data != null)
            {
                var userNameResult = UserManager.Get(data.FK_User_ID);

                if (userNameResult.Result != null)
                {
                    data.User_Name = $"{userNameResult.Result.FirstName} {userNameResult.Result.LastName}";
                    result = true;
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        // get by maintenance type
        [Route("GetByMaintenanceType/{type}")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByMaintenanceType([FromRoute] bool type)
        {
            var entityResult = _manager.GetByMaintenanceType(type);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);
            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }
            return result;
        }

        // get by created date
        [Route("GetByCreatedDate")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByCreatedDate([FromQuery] string fleetNumber)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;

            DateTime createdDate = DateTime.ParseExact(
                DateTime.Today.ToString("yyyy-MM-dd hh:mm:s.ffffff"),
                "yyyy-MM-dd hh:mm:s.ffffff", provider);

            var vehicleId = VehicleManager.GetAll().Data.Find(x => x.FleetNumber == fleetNumber).Id;
            var entityResult = _manager.GetByCreatedDate(createdDate, vehicleId);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindStatusName(result.Data);
                BindUserName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [Route("GetByMaintenanceAndserviceAndvehicleId")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByMaintenanceAndserviceAndvehicleId(
            [FromQuery] int serviceType, bool maintenanceType, int vehicleId)
        {

            var entityResult = _manager.GetByMaintenanceAndserviceAndvehicleId(serviceType, maintenanceType, vehicleId);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);
            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }
            return result;
        }

        [Route("IsVehicleAudoMeterValid")]
        [HttpPost]
        public ProcessResultViewModel<bool> IsVehicleAudoMeterValid(
            [FromBody] VehicleAudoMeterViewModelValidation model)
        {
            var entityResult = _manager.IsValidVehicleOdoMeter(model.CurrentAudoMeter, model.VehicleId);
            return _processResultMapper.Map<bool, bool>(entityResult);
        }

        [Route("CanAddJobCard/{VehicleId}")]
        [HttpGet]
        public ProcessResultViewModel<bool> CanAddJobCard([FromRoute] int vehicleId)
        {
            var entityResult = _manager.CanAddJobCard(vehicleId);
            return _processResultMapper.Map<bool, bool>(entityResult);
        }

        #region Get Daily Receipt / Brought Forward

        // Mohammed Osman
        [HttpPost]
        [Route("GetDailyReceiptReport")]
        public ProcessResultViewModel<List<JobCardViewModel>> GetDailyReceiptReport(
            [FromBody] DailyServiceSearchParams model)
        {
            ProcessResult<List<JobCard>> entityResult = _manager.GetDailyReceiptReport(model.VehiclePlateNo, model.StartDate, model.EndDate);

            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            return result;
        }

        //// Marked for deletion
        //[Route("GetDailyBroughtReceipt")]
        //[HttpPost]
        //public ProcessResultViewModel<PaginatedItems<JobCardViewModel>> GetDailyBroughtReceipt(
        //    [FromBody] DailyReceiptViewModel model)
        //{
        //    ProcessResult<PaginatedItems<JobCard>> entityResult = model != null
        //        ? _manager.GetDailyBroughtReceipt(model.date, model.paginatedItems)
        //        : _manager.GetDailyBroughtReceipt(null, null);

        //    var result =
        //        _processResultMapper.Map<PaginatedItems<JobCard>, PaginatedItems<JobCardViewModel>>(entityResult);

        //    if (result.IsSucceeded)
        //    {
        //        BindVehicleFleetNo(result.Data.Data);
        //        BindVehiclePlateNo(result.Data.Data);
        //        BindUserName(result.Data.Data);
        //        BindServiceTypeName(result.Data.Data);
        //    }

        //    return result;
        //}

        //// Marked for deletion
        //[Route("GetDailyReceipt")]
        //[HttpPost]
        //public ProcessResultViewModel<PaginatedItems<JobCardViewModel>> GetDailyReceipt(
        //    [FromBody] DailyReceiptViewModel model)
        //{
        //    ProcessResult<PaginatedItems<JobCard>> entityResult = null;
        //    if (model != null)
        //    {
        //        if (model.paginatedItems != null)
        //        {
        //            entityResult = _manager.GetDailyReceipt(model.date, model.paginatedItems);
        //        }
        //    }
        //    else
        //    {
        //        entityResult = _manager.GetDailyReceipt(null, null);
        //    }

        //    var result =
        //        _processResultMapper.Map<PaginatedItems<JobCard>, PaginatedItems<JobCardViewModel>>(entityResult);

        //    if (result.IsSucceeded)
        //    {
        //        BindVehicleFleetNo(result.Data.Data);
        //        BindVehiclePlateNo(result.Data.Data);
        //        BindServiceTypeName(result.Data.Data);
        //    }
        //    return result;
        //}

        //// Marked for deletion
        //[Route("GetDailyReceiptExceptBrought")]
        //[HttpPost]
        //public ProcessResultViewModel<PaginatedItems<JobCardViewModel>> GetDailyReceiptExceptBrought(
        //    [FromBody] DailyReceiptViewModel model)
        //{
        //    ProcessResult<PaginatedItems<JobCard>> entityResult = null;
        //    if (model != null)
        //    {
        //        if (model.paginatedItems != null)
        //        {
        //            entityResult = _manager.GetDailyReceiptExceptBrought(model.date, model.paginatedItems);
        //        }
        //    }
        //    else
        //    {
        //        entityResult = _manager.GetDailyReceiptExceptBrought(null, null);
        //    }
        //    var result =
        //        _processResultMapper.Map<PaginatedItems<JobCard>, PaginatedItems<JobCardViewModel>>(entityResult);

        //    if (result.IsSucceeded)
        //    {
        //        BindVehicleFleetNo(result.Data.Data);
        //        BindVehiclePlateNo(result.Data.Data);
        //        BindServiceTypeName(result.Data.Data);
        //    }

        //    return result;
        //}

        //// Marked for deletion
        //[Route("GetDailyWorkOfGarage/{date?}")]
        //[HttpGet]
        //public ProcessResultViewModel<List<DailyGarageWorkViewModel>> GetDailyWorkOfGarage([FromRoute] DateTime? date)
        //{
        //    var entityResult = _manager.GetDailyWorkOfGarage(date);
        //    var result = entityResult.Data.GroupBy(gp => gp.FleetNumber, (key, g) => new DailyGarageWorkViewModel
        //    {
        //        FleetNo = key,
        //        SpareParts = g.Select(usdM => usdM.Inventory).ToList()
        //    }
        //    ).ToList();
        //    return ProcessResultViewModelHelper.Succedded(result);
        //}

        #endregion Get Daily Receipt / Brought Forward


        [Route("GetNonApprovedJobcards")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> GetNonApprovedJobcards()
        {

            var entityResult = _manager.GetNonApprovedJobcards();
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);
            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }
            return result;
        }

        [Route("GetByIdsAndMaintenanceType")]
        [HttpPost]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByIdsAndMaintenanceType([FromBody] List<int> ids,
            bool maintenanceType)
        {
            var entityResult = _manager.GetByIdsAndMaintenanceType(ids, maintenanceType);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [Route("GetServiceCountReport")]
        [HttpGet]
        public List<Resulttest2> GetServiceCountReport([FromQuery] string fleetNumber)
        {
            var serviceTypesResult = ServiceTypeManager.GetAll().Data.ToList();
            var vehicleId = VehicleManager.GetAll().Data.FirstOrDefault(x => x.FleetNumber == fleetNumber).Id;

            List<Resulttest> resultSet1 = new List<Resulttest>();

            var jobCardsList = _manager.GetAll().Data.Where(x => x.FK_Vehicle_ID == vehicleId)
                .GroupBy(
                    x => new
                    {
                        x.CreatedDate.Date,
                        x.FK_ServiceType_ID
                    },
                    (key, g) => new
                    {
                        key.Date,
                        Value = new
                        {
                            key.FK_ServiceType_ID,
                            Count = g.Count()
                        }
                    }).ToList();

            foreach (var item in jobCardsList)
            {
                var resultWithDate = resultSet1.FirstOrDefault(x => x.Date.Equals(item.Date));

                if (resultWithDate != null)
                {
                    resultWithDate.Value.Add(new Valuetest
                    {
                        FK_ServiceType_ID = item.Value.FK_ServiceType_ID.Value,
                        Count = item.Value.Count
                    });
                }
                else
                {
                    resultSet1.Add(new Resulttest
                    {
                        Date = item.Date,

                        Value = new List<Valuetest>
                        {
                            new Valuetest
                            {
                                FK_ServiceType_ID = item.Value.FK_ServiceType_ID.Value,
                                Count = item.Value.Count
                            }
                        }
                    });
                }
            }

            foreach (Resulttest result in resultSet1)
            {
                List<int> fkServiceTypeIDs = new List<int>();

                foreach (Valuetest item in result.Value.OrderBy(x => x.FK_ServiceType_ID).ToList())
                {
                    fkServiceTypeIDs.Add(item.FK_ServiceType_ID);
                }

                foreach (var servicetype in serviceTypesResult.OrderBy(x => x.Id))
                {
                    if (!fkServiceTypeIDs.Contains(servicetype.Id))
                    {
                        var resultWithDate = resultSet1.FirstOrDefault(x => x.Date.Equals(result.Date));
                        resultWithDate.Value.Add(new Valuetest { FK_ServiceType_ID = servicetype.Id, Count = 0 });
                    }
                }
            }

            List<Resulttest2> resultSet2 = new List<Resulttest2>();

            foreach (Resulttest result in resultSet1)
            {
                foreach (var servicetype in serviceTypesResult.OrderBy(x => x.Id))
                {
                    foreach (Valuetest item in result.Value.OrderBy(x => x.FK_ServiceType_ID).ToList())
                    {
                        if (servicetype.Id == item.FK_ServiceType_ID)
                        {
                            var resultWithDate = resultSet2.FirstOrDefault(x => x.Date.Equals(result.Date));

                            if (resultWithDate != null)
                            {
                                resultWithDate.Value.Add(new Valuetest2
                                {
                                    FK_ServiceType_ID = item.FK_ServiceType_ID,
                                    ServiceTypeName_En = servicetype.ServiceTypeName_EN,
                                    ServiceTypeName_AR = servicetype.ServiceTypeName_AR,
                                    Count = item.Count
                                });
                            }
                            else
                            {
                                resultSet2.Add(new Resulttest2
                                {
                                    Date = result.Date,
                                    Value = new List<Valuetest2>
                                    {
                                        new Valuetest2
                                        {
                                            FK_ServiceType_ID = item.FK_ServiceType_ID,
                                            ServiceTypeName_En = servicetype.ServiceTypeName_EN,
                                            ServiceTypeName_AR = servicetype.ServiceTypeName_AR,
                                            Count = item.Count
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            }

            return resultSet2;
        }

        [Route("GetByVehicle_IDandOpen/{vehicle_ID}")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByVehicle_IDandOpen([FromRoute] int vehicle_ID)
        {
            var entityResult = _manager.GetByVehicle_IDandOpen(vehicle_ID);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [HttpGet]
        [Route("GetByVehicle_ID/{vehicle_ID}")]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByVehicle_ID([FromRoute] int vehicle_ID)
        {
            var entityResult = _manager.GetByVehicle_ID(vehicle_ID);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [Route("SearchByID_CustomerType_Desc")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> SearchByID_CustomerType_Desc([FromQuery] int vehicleId,
            string word)
        {
            var entityResult = _manager.SearchByID_CustomerType_Desc(vehicleId, word);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [Route("SearchActiveJobCards")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> SearchActiveJobCards([FromQuery] int vehicleId,
            string word)
        {
            var entityResult = _manager.SearchActiveJobCards(vehicleId, word);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindStatusName(result.Data);
                BindUserName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [Route("SearchDeActiveJobCards")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> SearchDeActiveJobCards([FromQuery] int vehicleId,
            string word)
        {
            var entityResult = _manager.SearchDeActiveJobCards(vehicleId, word);
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [Route("GetByOpenStatus")]
        [HttpGet]
        public ProcessResultViewModel<List<JobCardViewModel>> GetByOpenStatus()
        {
            var entityResult = _manager.GetByOpenStatus();
            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        private void UpdateOdometerOfVehicle(double? odoMeter, int vehicleId)
        {
            var entityResult = VehicleManager.Get(vehicleId);

            if (entityResult.Data != null)
            {
                entityResult.Data.Odometer = odoMeter;
            }

            //var result = _processResultMapper.Map<Vehicle, VehicleViewModel>(entityResult);

            //return result;
        }

        [HttpGet]
        [Route("SearchJobCards")]
        public ProcessResultViewModel<List<JobCardViewModel>> GetJobCardReport(
            [FromQuery] string customerType, DateTime? dateIn, DateTime? dateOut,
            int? serviceTypeId, int? jobCardStatusId, string vehiclePlateNo,
            string vehicleFleetNo, int? jobCardId)
        {
            var entityResult = _manager.SearchJobCards(customerType, dateIn, dateOut,
                serviceTypeId, jobCardStatusId, vehiclePlateNo, vehicleFleetNo, jobCardId);

            var result = _processResultMapper.Map<List<JobCard>, List<JobCardViewModel>>(entityResult);

            if (result.Data.Any())
            {
                BindVehiclePlateNo(result.Data);
                BindVehicleFleetNo(result.Data);
                BindDriverName(result.Data);
                BindUserName(result.Data);
                BindStatusName(result.Data);
                BindServiceTypeName(result.Data);
            }

            return result;
        }

        [HttpGet, Route("Count")]
        public ProcessResultViewModel<int> Count()
        {
            var entityResult = _manager.Count();

            if (entityResult.IsSucceeded)
            {
                return ProcessResultViewModelHelper.Succedded(entityResult.Data);
            }

            return ProcessResultViewModelHelper.Failed(0, entityResult.Exception.Message);
        }

        [HttpPut]
        [Route("ApproveJobCard")]
        public ProcessResultViewModel<bool> ApproveJobCard([FromQuery] int jobCardId, bool approve)
        {
            var entityResult = _manager.ApproveJobCard(jobCardId, approve);
            var result = _processResultMapper.Map<bool, bool>(entityResult);
            return result;
        }
    }

    public class Resulttest
    {
        public DateTime Date { get; set; }
        public List<Valuetest> Value { get; set; }
    }

    public class Valuetest
    {
        public int FK_ServiceType_ID { get; set; }
        public int Count { get; set; }
    }

    public class Resulttest2
    {
        public DateTime Date { get; set; }
        public List<Valuetest2> Value { get; set; }
    }

    public class Valuetest2
    {
        public int FK_ServiceType_ID { get; set; }
        public string ServiceTypeName_En { get; set; }
        public string ServiceTypeName_AR { get; set; }
        public int Count { get; set; }
    }
}