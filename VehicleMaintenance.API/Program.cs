﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using VehicleMaintenance.models.Context;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VehicleMaintenance.API.Seed;
using VehicleMaintenance.models.Settings;

namespace VehicleMaintenance.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args)
             .MigrateDbContext<VehicleMaintenanceDbContext>((context, services) =>
             {
                 var logger = services.GetService<ILogger<VehicleMaintenanceDbContextSeed>>();
                 var env = services.GetService<IHostingEnvironment>();
                 var settings = services.GetService<IOptions<VehicleAppSettings>>();
                 new VehicleMaintenanceDbContextSeed()
                    .SeedAsync(services, settings, context, env, logger)
                 .Wait();
             }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .Build();
    }
}
