﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleMaintenance.API.ViewModel
{
    public class VehicleAudoMeterViewModelValidation
    {
        public double CurrentAudoMeter { get; set; }
        public int VehicleId { get; set; }
    }
}
