﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class TechnicianViewModel : RepoistryBaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Specialty { get; set; }
        public string Employe_Id { get; set; }
        public double HourRating { get; set; }
        public bool Available { get; set; }

    }
}
