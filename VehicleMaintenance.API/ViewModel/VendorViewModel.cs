﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class VendorViewModel : RepoistryBaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public List<FileViewModel> Files { get; set; }
    }
}
