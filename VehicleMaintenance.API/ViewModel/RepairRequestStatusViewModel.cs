﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class RepairRequestStatusViewModel : RepoistryBaseEntity
    {
        public string RepairRequestStatusName_AR { get; set; }
        public string RepairRequestStatusName_EN { get; set; }
    }
}
