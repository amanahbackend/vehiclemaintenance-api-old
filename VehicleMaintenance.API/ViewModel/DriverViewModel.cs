﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class DriverViewModel : RepoistryBaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Title { get; set; }
        public string DateOfBirth { get; set; }
        public string SSN { get; set; }
        public DateTime LicenseIssuedDate { get; set; }
        public string LicenseIssuedState { get; set; }
        public string LicenseNumber { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public bool Active { get; set; }
        public string Employe_Id { get; set; }

    }
}
