﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Utilites.PaginatedItems;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class DailyReceiptViewModel : RepoistryBaseEntity
    {
        public PaginatedItems<JobCard> paginatedItems { get; set; }
        public DateTime? date { get; set; }
    }
}
