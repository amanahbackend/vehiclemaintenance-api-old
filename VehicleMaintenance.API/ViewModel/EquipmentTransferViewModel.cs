﻿using System;
namespace VehicleMaintenance.API.ViewModel
{
    public class EquipmentTransferViewModel
    {
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public string Description { get; set; }
        public string TransferFromFleetNumber { get; set; }
        public string TransferToFleetNumber { get; set; }
        public int? RepairRequestId { get; set; }
        public int? JobCardId { get; set; }
        public DateTime Date { get; set; }
        public string Capacity { get; set; }
    }
}
