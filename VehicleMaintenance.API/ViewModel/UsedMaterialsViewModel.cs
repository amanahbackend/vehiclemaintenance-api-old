﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.API.ViewModel
{
    public class UsedMaterialsViewModel : RepoistryBaseEntity
    {
        public int? FK_JobCard_ID { get; set; }
        public int FK_Technician_ID { get; set; }
        public int Amount { get; set; }
        public int FK_Inventory_ID { get; set; }
        public string TechnicianEmploye_Id { get; set; }
        public string SparePart_SerialNumber { get; set; }
        public double cost { get; set; }
        public int FK_SparePartStatus_ID { get; set; }
        public string SparePartStatus_Name { get; set; }
        public string FleetNumber { get; set; }
        public int? FK_RepairRequest_ID { get; set; }
        public RepairRequest RepairRequest { get; set; }
        public SparePartStatus SparePartStatus { get; set; }
        public Vehicle Vehicle { get; set; }
        public Inventory Inventory { get; set; }
        public JobCard JobCard { get; set; }
        public Technician Technician { get; set; }

        public int? FK_TranferFromVehicle_Id { get; set; }
        public Vehicle TransferFromVehicle { get; set; }
        public string Capacity { get; set; }
    }
}
