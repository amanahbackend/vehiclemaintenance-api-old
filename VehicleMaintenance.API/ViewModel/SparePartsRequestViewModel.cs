﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.ViewModel
{
    public class SparePartsRequestViewModel : RepoistryBaseEntity
    {
        public int FK_RepairRequest_ID { get; set; }

        public string RepairRequestJobCardId => RepairRequest != null ? Convert.ToString(RepairRequest.FK_JobCard_ID) : string.Empty;

        public RepairRequest RepairRequest { get; set; }

        public int FK_Technician_ID { get; set; }

        public string Description { get; set; }
        public string FleetNumber { get; set; }
        public int Cost { get; set; }
        public decimal? Amount { get; set; }
        public int? Quantity { get; set; }

        public string SerialNumber { get; set; }
        public string Source { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual int FK_CreatedBy_Id { get; set; }
        public string Unit { get; set; }
        public string Comments { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int FK_Inventory_ID { get; set; }
        public string TechnicianEmploye_Id { get; set; }
        public string SparePart_Name { get; set; }
        public int FK_Status_ID { get; set; }
        public Status Status { get; set; }
        public Inventory Inventory { get; set; }
        public JobCard JobCard { get; set; }
        public Technician Technician { get; set; }

    }
}
