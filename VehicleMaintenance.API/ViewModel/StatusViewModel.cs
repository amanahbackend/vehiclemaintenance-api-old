﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class StatusViewModel : RepoistryBaseEntity
    {
        public string StatusName_AR { get; set; }
        public string StatusName_EN { get; set; }
    }
}
