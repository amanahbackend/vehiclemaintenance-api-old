using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.API.ViewModel
{
    public class ChangePasswordViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }
        public string Token { get; set; }
    }
}
