﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class ServiceTypeViewModel : RepoistryBaseEntity
    {
        public string ServiceTypeName_AR { get; set; }
        public string ServiceTypeName_EN { get; set; }
        public bool Maintenance_Type { get; set; }
        public bool Need_Approve { get; set; }
        public int Period { get; set; }
    }
}
