﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.ViewModel
{
    public class EquipmentTransferHistoryViewModel : RepoistryBaseEntity
    {
        public int FK_RepairRequest_ID { get; set; }
        public int FK_User_ID { get; set; }
        public User User { get; set; }
        public RepairRequest RepairRequest { get; set; }
    }
}
