﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class EquipmentTransferStatusViewModel : RepoistryBaseEntity
    {
        public string EquipmentTransferStatusName_AR { get; set; }
        public string EquipmentTransferStatusName_EN { get; set; }
    }
}
