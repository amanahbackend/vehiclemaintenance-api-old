﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    public class InternalRequestViewModel
    {
        public string RequestedBy { get; set; }
        public string Customer { get; set; }
        public SparePartsRequestViewModel SpareParts { get; set; }
    }
}
