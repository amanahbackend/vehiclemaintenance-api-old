﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.API.ViewModel;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.Controllers
{
    public class MaterialRequisitionViewModel
    {
        public string FleetNo { get; set; }
        public string Supervisor { get; set; }
        public List<SparePartsRequestViewModel> SpareParts { get; set; }
    }
}
