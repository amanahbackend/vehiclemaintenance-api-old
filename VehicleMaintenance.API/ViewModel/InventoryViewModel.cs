﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class InventoryViewModel : RepoistryBaseEntity
    {
        public string Name_ER { get; set; }
        public string Name_EN { get; set; }
        public string SerialNumber { get; set; }
        public string Case_AR { get; set; }
        public string Case_EN { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public string Unit { get; set; }
    }
}
