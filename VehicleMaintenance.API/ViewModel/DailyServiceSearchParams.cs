﻿using System;

namespace VehicleMaintenance.API.ViewModel
{
    public class DailyServiceSearchParams
    {
        public string VehiclePlateNo { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
