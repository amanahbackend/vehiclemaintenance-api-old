﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class JobCard_TechnicianViewModel : RepoistryBaseEntity
    {
        public int FK_Technician_ID { get; set; }
        public int FK_JobCard_ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string TechnicianEmploye_Id { get; set; }
        public Technician Technician { get; set; }
        public JobCard JobCard { get; set; }
    }
}
