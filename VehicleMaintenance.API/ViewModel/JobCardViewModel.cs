﻿using System;
using System.Collections.Generic;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.API.ViewModel
{
    public class JobCardViewModel : RepoistryBaseEntity
    {
        public string Customer_Type { get; set; }
        public DateTime Date_In { get; set; }
        public DateTime Date_Out { get; set; }
        public int? DateInShiftId { get; set; }
        public int? DateOutShiftId { get; set; }

        public string Desc { get; set; }
        public string User_Name { get; set; }
        public string FK_User_ID { get; set; }
        public int FK_Vehicle_ID { get; set; }
        public string FleetNumber { get; set; }
        public string DriverEmploye_Id { get; set; }
        public string ServiceTypeName { get; set; }
        public string DriverName { get; set; }
        public string StatusName { get; set; }
        public string Comment { get; set; }
        public int? FK_Driver_ID { get; set; }
        public int? FK_ServiceType_ID { get; set; }
        public bool Maintenance_Type { get; set; }
        public string VehicleNumberPlate { get; set; }
        public int FK_Status_ID { get; set; }
        public double? Odometer { get; set; }
        public double? Hourmeter { get; set; }
        public List<int> FK_File_IDs { get; set; }
        public List<String> FK_File_URLs { get; set; }
        public Status Status { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public Vehicle vehicle { get; set; }
        public Driver Driver { get; set; }
        public User User { get; set; }
        public ServiceType ServiceType { get; set; }
        public List<RepairRequestViewModel> RepairRequests { get; set; }
        public List<FileViewModel> Files { get; set; }

        public int? Sr { get; set; }

        public string DailyServiceDesc { get; set; }
    }
}
