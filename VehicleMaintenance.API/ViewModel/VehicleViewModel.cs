﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.API.ViewModel
{
    public class VehicleViewModel : RepoistryBaseEntity
    {
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string KiloMeters { get; set; }
        public string VIN { get; set; }

        public int? VehicleTypeId { get; set; }
        public VehicleType VehicleType { get; set; }
        public string ToolDescription { get; set; }

        public string Status { get; set; }
        public string Remarks { get; set; }
        public string ChassisNumber { get; set; }
        public double? Odometer { get; set; }
        public string VehicleNumberPlate { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int? FuelConsumption { get; set; }
        public string WorkIdleTime { get; set; }
        public string FleetNumber { get; set; }

    }
}
