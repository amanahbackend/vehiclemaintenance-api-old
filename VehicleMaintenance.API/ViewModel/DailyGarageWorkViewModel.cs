﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class DailyGarageWorkViewModel 
    {
        public string  FleetNo { get; set; }
        public List<Inventory> SpareParts { get; set; }
    }
}
