﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.API.ViewModel
{
    public class VehicleTypeViewModel : RepoistryBaseEntity
    {
        public string Name { get; set; }

        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
