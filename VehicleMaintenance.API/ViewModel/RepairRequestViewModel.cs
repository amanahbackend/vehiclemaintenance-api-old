﻿using System;
using System.Collections.Generic;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.ViewModel
{
    public class RepairRequestViewModel : RepoistryBaseEntity
    {
        public string ServiceType_Name { get; set; }
        public string FK_User_ID { get; set; }
        public string User_Name { get; set; }
        public int FK_RepairRequestType_ID { get; set; }
        public string RepairRequestType_Name { get; set; }
        public int FK_JobCard_ID { get; set; }
        public int FK_RepairRequestStatus_ID { get; set; }
        public string RepairRequestStatus_Name { get; set; }
        public bool Maintenance_Type { get; set; }
        public List<int> FK_File_IDs { get; set; }
        public List<String> FK_File_URLs { get; set; }
        public int FK_ServiceType_ID { get; set; }
        
        public bool External { get; set; }
        public double Duration { get; set; }
        public string Comment { get; set; }
        public virtual int FK_CreatedBy_Id { get; set; }

        public double Cost { get; set; }
        public Driver Driver { get; set; }
        public User User { get; set; }
        public RepairRequestType RepairRequestType { get; set; }
        public RepairRequestStatus RepairRequestStatus { get; set; }
        public JobCard JobCard { get; set; }
        public List<FileViewModel> Files { get; set; }
        public ServiceType ServiceType { get; set; }

    }
}
