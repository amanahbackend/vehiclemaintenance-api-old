﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.ViewModel
{
    public class RepairRequest_TechnicianViewModel : RepoistryBaseEntity
    {
        public int FK_Technician_ID { get; set; }
        public int FK_RepairRequest_ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public virtual DateTime DeletedDate { get; set; }
        public bool Removed { get; set; }

        public string TechnicianEmploye_Id { get; set; }
        public Technician Technician { get; set; }
        public RepairRequest RepairRequest { get; set; }
    }
}
