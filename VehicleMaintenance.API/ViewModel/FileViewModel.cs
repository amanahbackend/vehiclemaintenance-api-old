﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Utilites.UploadFile;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.API.ViewModel
{
    public class FileViewModel : RepoistryBaseEntity
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int FK_RepairRequest_ID { get; set; }
        public double RepairRequest_Cost { get; set; }
        public int FK_JobCard_ID { get; set; }
        public string FileURL { get; set; }
        public int FK_Vendor_ID { get; set; }
        public string Vendor_Name{ get; set; }
        public string Vendor_Address { get; set; }
        public bool Selected { get; set; }
        public string FleetNumber { get; set; }
        public string VehicleNumberPlate { get; set; }
        public Vendor Vendor { get; set; }
        public UploadFile File { get; set; }
        public RepairRequest RepairRequest { get; set; }
        public JobCard JobCard { get; set; }

    }
}
