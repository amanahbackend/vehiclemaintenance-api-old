﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.Drivers
{
    public interface IDriverRepository : IRepository<Driver>
    {
        List<Driver> SearchByWord(string keyword);
    }
}
