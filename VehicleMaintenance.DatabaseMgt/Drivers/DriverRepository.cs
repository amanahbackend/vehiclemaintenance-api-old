﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.Drivers
{
    public class DriverRepository : Repository<Driver>, IDriverRepository
    {
        public DriverRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<Driver> SearchByWord(string keyword)
        {
            List<Driver> lstDrivers = Find(
                obj => obj.RowStatusId != -1 &&
                       (obj.FirstName.StartsWith(keyword) ||
                        obj.LastName.StartsWith(keyword) ||
                        obj.EmployeeId.StartsWith(keyword))).ToList();

            return lstDrivers;
        }
    }
}
