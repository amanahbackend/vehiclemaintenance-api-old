﻿namespace VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses
{
    public class JobCardStatusRepository : Repository<JobCardStatus>, IJobCardStatusRepository
    {
        public JobCardStatusRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
