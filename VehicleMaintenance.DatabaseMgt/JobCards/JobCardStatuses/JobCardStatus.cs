﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses
{
    public class JobCardStatus : BaseEntity
    {
        [StringLength(50)]
        public string NameAr { get; set; }

        [StringLength(50)]
        public string NameEn { get; set; }
    }
}
