﻿using System;
using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.JobCards
{
    public interface IJobCardRepository : IRepository<JobCard>
    {
        List<JobCard> SearchJobCards(string customerName,
            DateTime? dateIn, DateTime? dateOut, int? serviceTypeId, int? jobCardStatusId,
            string vehiclePlateNo, string vehicleFleetNo, int? jobCardId);

        JobCard GetJobCard(int jobCardId);

        List<JobCard> GetJobCards();

        List<JobCard> GetByOpenStatus();

        List<JobCard> GetByOpenStatus(int vehicleId);

        List<JobCard> GetCancelledOrClosedStatuses(int vehicleId);

        List<JobCard> GetNonCancelledNonClosedStatuses(int vehicleId);

        List<JobCard> GetByVehicleId(int vehicleId);

        bool CanAddJobCard(int vehicleId);

        int GetOpenJobCardsCount();

        //List<JobCard> GetDailyReceiptReport(string vehiclePlateNo, DateTime? startDate, DateTime? endDate);
    }
}
