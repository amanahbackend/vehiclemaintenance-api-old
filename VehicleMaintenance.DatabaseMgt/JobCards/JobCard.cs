﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.DatabaseMgt.JobCards
{
    public class JobCard : BaseEntity
    {
        public DateTime? DateIn { get; set; }

        public DateTime? DateOut { get; set; }

        public int? DateInShiftId { get; set; }
        public int? DateOutShiftId { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        public int? VehicleId { get; set; }
        public virtual Vehicle Vehicle {get; set; }

        public int? DriverId { get; set; }
        public virtual Driver Driver { get; set; }
        
        [StringLength(200)]
        public string Comment { get; set; }
        
        public int? JobCardStatusId { get; set; }
        public virtual JobCardStatus JobCardStatus { get; set; }

        public double? Odometer { get; set; }
        public double? Hourmeter { get; set; }
        
        public virtual List<RepairRequest> RepairRequests { get; set; }

        public virtual ICollection<UploadedFile> Files { get; set; }
    }

    //public class DailyServiceSearchParams
    //{
    //    public string VehiclePlateNo { get; set; }

    //    public DateTime? StartDate { get; set; }

    //    public DateTime? EndDate { get; set; }
    //}

    public enum EnumJobCardStatus
    {
        InProgress = 2,
        Closed = 3,
        Cancelled = 4
    }
}
