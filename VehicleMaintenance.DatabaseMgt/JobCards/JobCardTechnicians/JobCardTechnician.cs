﻿using System;
using VehicleMaintenance.DatabaseMgt.Technicians;

namespace VehicleMaintenance.DatabaseMgt.JobCards.JobCardTechnicians
{
   public class JobCardTechnician : BaseEntity
    {
        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }

        public int? JobCardId { get; set; }
        public virtual JobCard JobCard { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }
    }
}
