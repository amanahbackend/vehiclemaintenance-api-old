﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.UploadedFiles
{
    public interface IUploadedFileRepository : IRepository<UploadedFile>
    {
        List<UploadedFile> GetUploadedFilesByJobCard(int jobCardId);
    }
}
