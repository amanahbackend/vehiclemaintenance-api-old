﻿using System;

namespace VehicleMaintenance.DatabaseMgt.Common
{
    public class Weekend : BaseEntity
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
