﻿namespace VehicleMaintenance.DatabaseMgt.Common.Vendors
{
    public interface IVendorRepository : IRepository<Vendor>
    {
    }
}
