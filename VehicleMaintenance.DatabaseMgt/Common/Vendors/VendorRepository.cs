﻿namespace VehicleMaintenance.DatabaseMgt.Common.Vendors
{
    public class VendorRepository : Repository<Vendor>, IVendorRepository
    {
        public VendorRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
