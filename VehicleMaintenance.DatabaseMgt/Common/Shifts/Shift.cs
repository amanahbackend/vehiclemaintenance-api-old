﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Common.Shifts
{
    public class Shift : BaseEntity
    {
        [StringLength(100)]
        public string Name { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }
    }
}
