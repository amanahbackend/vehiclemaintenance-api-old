﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.Common.Shifts
{
    public interface IShiftRepository : IRepository<Shift>
    {
        List<Shift> GetList();
    }
}
