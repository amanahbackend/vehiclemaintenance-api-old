﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.Common.Shifts
{
    public class ShiftRepository : Repository<Shift>, IShiftRepository
    {
        public ShiftRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<Shift> GetList()
        {
           List<Shift> lstShifts =  Find(obj => obj.RowStatusId != -1).ToList();

            return lstShifts;
        }
    }
}
