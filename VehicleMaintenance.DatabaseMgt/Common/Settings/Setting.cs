﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Common.Settings
{
    public class Setting : BaseEntity
    {
        public Setting()
        {
            DataType = EnumDataType.String;
        }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Value { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(50)]
        public string Category { get; set; }

        public EnumDataType DataType { get; set; }
    }

    public enum EnumDataType
    {
        String = 0,
        Int = 1,
        Decimal = 2,
        Bool = 3,
        DateTime = 4,
        Date = 5
    }
}
