﻿namespace VehicleMaintenance.DatabaseMgt.Common.Settings
{
    public interface ISettingRepository : IRepository<Setting>
    {
        string WebsiteUrl { get; }

        string WebsitePhysicalPath { get; }

        string ApiServiceUrl { get; }

        string ApiServiceRootPath { get; }
    }
}