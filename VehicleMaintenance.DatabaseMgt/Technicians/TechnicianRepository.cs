﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.Technicians
{
    public class TechnicianRepository : Repository<Technician>, ITechnicianRepository
    {
        public TechnicianRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<Technician> SearchByWord(string keyword)
        {
            List<Technician> lsTechnicians = Find(
                obj => obj.RowStatusId != -1 &&
                       (obj.FirstName.StartsWith(keyword) ||
                        obj.LastName.StartsWith(keyword) ||
                        obj.EmployeeId.StartsWith(keyword))).ToList();

            return lsTechnicians;
        }
    }
}
