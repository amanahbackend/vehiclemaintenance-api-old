﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.Technicians
{
    public interface ITechnicianRepository : IRepository<Technician>
    {
        List<Technician> SearchByWord(string keyword);
    }
}
