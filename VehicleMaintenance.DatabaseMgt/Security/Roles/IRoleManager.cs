﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VehicleMaintenance.DatabaseMgt.Security.Roles
{
    public interface IRoleManager
    {
        Task<ApplicationRole> AddRoleAsync(ApplicationRole Role);
        Task AddRolesAsync(List<ApplicationRole> Roles);
        Task<bool> Delete(string roleName);
        Task<ApplicationRole> Update(ApplicationRole role);
        List<ApplicationRole> GetAll();
        Task<ApplicationRole> GetByName(string roleName);
    }
}
