﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace VehicleMaintenance.DatabaseMgt.Security.Users
{
    public interface IUserRepository
    {
        Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password);
        Task<bool> AddUserToRolesAsync(ApplicationUser user);
        Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users);
        Task<ApplicationUser> GetBy(string username);
        Task<IList<string>> GetRolesAsync(ApplicationUser user);
        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsEmailExistAsync(string email);
        Task<IList<ApplicationUser>> GetUsersInRole(string roleName);
        Task<bool> IsUserInRole(string userName, string roleName);
        Task<List<ApplicationUser>> GetAll();
        ProcessResult<int> Count();
        Task<bool> DeleteAsync(ApplicationUser user);
        Task<bool> DeleteByIdAsunc(string id);
        Task<ApplicationUser> UpdateUserAsync(ApplicationUser user);
        Task<bool> DeleteAsync(string username);
        bool IsPhoneExist(string phone);
        Task<List<ApplicationUser>> Search(string searchToken, string[] searchFields);
        Task<ApplicationUser> Get(string id);
        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);

        Task<bool> Deactivate(string username);
        Task<bool> IsUserDeactivated(string username);
        Task<bool> Activate(string username);

        Task<string> GenerateForgetPasswordToken(string username);
        Task<bool> ChangePassword(string username, string newPassword, string changePasswordToken);
        Task<bool> ResetPassword(string username, string newPassword);
        Task<bool> UpdateUserPassword(string userId, string newPassword);
        Task<string> GeneratePhoneNumberToken(string username, string phone);
        Task<bool> CheckPhoneValidationToken(string username, string phone, string token);
        Task<bool> IsPhoneConfirmed(string username);
        Task<bool> ConfirmPhone(string username, string phone, string token);

        Task<bool> SetAuthentiacationToken(ApplicationUser user, string token);
        Task<bool> RemoveAuthenticationToken(ApplicationUser user);
        Task<string> GetAuthenticationToken(ApplicationUser user);
    }
}
