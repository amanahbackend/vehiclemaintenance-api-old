using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Security
{
    public class PasswordTokenPin : BaseEntity
    {
        [StringLength(200)]
        public string Token { get; set; }

        [StringLength(50)]
        public string Pin { get; set; }
    }
}
