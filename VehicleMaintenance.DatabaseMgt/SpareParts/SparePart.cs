﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VehicleMaintenance.DatabaseMgt.SpareParts
{
    public class SparePart : BaseEntity
    {
        [StringLength(20)]
        public string SerialNumber { get; set; }

        [StringLength(100)]
        public string NameAr { get; set; }

        [StringLength(100)]
        public string NameEn { get; set; }

        [DefaultValue(false)]
        public bool? NeedApprove { get; set; }

        [StringLength(50)]
        public string Category { get; set; }

        [StringLength(50)]
        public string CaseAr { get; set; }

        [StringLength(50)]
        public string CaseEn { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [Column(TypeName = "decimal(18, 4)")]
        public decimal? Cost { get; set; }

        [StringLength(20)]
        public string Unit { get; set; }
    }
}
