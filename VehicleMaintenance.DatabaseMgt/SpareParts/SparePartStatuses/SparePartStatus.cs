﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses
{
    public class SparePartStatus : BaseEntity
    {
        [StringLength(50)]
        public string NameAr { get; set; }

        [StringLength(50)]
        public string NameEn { get; set; }
    }
}
