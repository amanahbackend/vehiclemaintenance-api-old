﻿namespace VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses
{
    public class SparePartStatusRepository : Repository<SparePartStatus>, ISparePartStatusRepository
    {
        public SparePartStatusRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
