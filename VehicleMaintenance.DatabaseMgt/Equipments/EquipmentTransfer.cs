﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Equipments
{
    public class EquipmentTransfer
    {
        [StringLength(100)]
        public string NameEn { get; set; }

        [StringLength(100)]
        public string NameAr { get; set; }

        [StringLength(50)]
        public string SerialNumber { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(50)]
        public string TransferFromFleetNumber { get; set; }

        [StringLength(50)]
        public string TransferToFleetNumber { get; set; }

        public int? RepairRequestId { get; set; }
        public int? JobCardId { get; set; }
        public DateTime Date { get; set; }

        [StringLength(50)]
        public string Capacity { get; set; }
    }
}
