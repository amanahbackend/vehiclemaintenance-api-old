﻿using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.Security.Users;

namespace VehicleMaintenance.DatabaseMgt.Equipments
{
    public class EquipmentTransferHistory : BaseEntity
    {
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }
    }
}
