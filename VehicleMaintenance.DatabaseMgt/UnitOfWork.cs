﻿using VehicleMaintenance.DatabaseMgt.Common.Settings;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.DatabaseMgt.Vehicles;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;

namespace VehicleMaintenance.DatabaseMgt
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly VehicleMaintenanceDbContext _context;

        private IDriverRepository _drivers;

        private IVehicleRepository _vehicles;
        private IVehicleTypeRepository _vehicleTypes;

        private IJobCardRepository _jobCards;
        private IJobCardStatusRepository _jobCardStatuses;

        private IRepairRequestRepository _repairRequests;
        private IRepairRequestTypeRepository _repairRequestTypes;
        private IRepairRequestStatusRepository _repairRequestStatuses;
        private IRepairRequestSparePartRepository _repairRequestSpareParts;
        private IRepairRequestMaterialRepository _repairRequestMaterials;
        private IRepairRequestVendorRepository _repairRequestVendors;

        private ISparePartDeliveryOrderRepository _sparePartDeliveryOrders;
        private ISparePartDeliveryOrderDetailsRepository _sparePartDeliveryOrderDetails;

        private ITechnicianRepository _technicians;
        private IServiceTypeRepository _serviceTypes;

        private ISparePartRepository _spareParts;
        private ISparePartStatusRepository _sparePartStatuses;

        private IShiftRepository _shifts;
        private IVendorRepository _vendors;
        private ISettingRepository _settings;
        private IUploadedFileRepository _uploadedFiles;

        public UnitOfWork(VehicleMaintenanceDbContext context)
        {
            _context = context;
        }

        public IDriverRepository Drivers => _drivers ?? (_drivers = new DriverRepository(_context));

        public IVehicleRepository Vehicles => _vehicles ?? (_vehicles = new VehicleRepository(_context));
        public IVehicleTypeRepository VehicleTypes => _vehicleTypes ?? (_vehicleTypes = new VehicleTypeRepository(_context));

        public IJobCardRepository JobCards => _jobCards ?? (_jobCards = new JobCardRepository(_context));
        public IJobCardStatusRepository JobCardStatuses => _jobCardStatuses ?? (_jobCardStatuses = new JobCardStatusRepository(_context));

        public IRepairRequestRepository RepairRequests =>
            _repairRequests ?? (_repairRequests = new RepairRequestRepository(_context));

        public IRepairRequestTypeRepository RepairRequestTypes =>
            _repairRequestTypes ?? (_repairRequestTypes = new RepairRequestTypeRepository(_context));

        public IRepairRequestStatusRepository RepairRequestStatuses =>
            _repairRequestStatuses ?? (_repairRequestStatuses = new RepairRequestStatusRepository(_context));

        public IRepairRequestSparePartRepository RepairRequestSpareParts =>
            _repairRequestSpareParts ?? (_repairRequestSpareParts = new RepairRequestSparePartRepository(_context));
        
        public IRepairRequestMaterialRepository RepairRequestMaterials =>
            _repairRequestMaterials ?? (_repairRequestMaterials = new RepairRequestMaterialRepository(_context));

        public IRepairRequestVendorRepository RepairRequestVendors =>
            _repairRequestVendors ?? (_repairRequestVendors = new RepairRequestVendorRepository(_context));

        public ISparePartDeliveryOrderRepository SparePartDeliveryOrders =>
            _sparePartDeliveryOrders ?? (_sparePartDeliveryOrders = new SparePartDeliveryOrderRepository(_context));

        public ISparePartDeliveryOrderDetailsRepository SparePartDeliveryOrderDetails =>
            _sparePartDeliveryOrderDetails ?? (_sparePartDeliveryOrderDetails = new SparePartDeliveryOrderDetailsRepository(_context));

        public ITechnicianRepository Technicians => _technicians ?? (_technicians = new TechnicianRepository(_context));

        public IServiceTypeRepository ServiceTypes => _serviceTypes ?? (_serviceTypes = new ServiceTypeRepository(_context));

        public ISparePartRepository SpareParts => _spareParts ?? (_spareParts = new SparePartRepository(_context));
        public ISparePartStatusRepository SparePartStatuses => _sparePartStatuses ?? (_sparePartStatuses = new SparePartStatusRepository(_context));

        public IShiftRepository Shifts => _shifts ?? (_shifts = new ShiftRepository(_context));
        public IVendorRepository Vendors => _vendors ?? (_vendors = new VendorRepository(_context));
        public ISettingRepository Settings => _settings ?? (_settings = new SettingRepository(_context));
        public IUploadedFileRepository UploadedFiles => _uploadedFiles ?? (_uploadedFiles = new UploadedFileRepository(_context));
    }
}
