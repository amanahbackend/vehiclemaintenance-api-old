﻿namespace VehicleMaintenance.DatabaseMgt.ServiceTypes
{
    public interface IServiceTypeRepository : IRepository<ServiceType>
    {
    }
}
