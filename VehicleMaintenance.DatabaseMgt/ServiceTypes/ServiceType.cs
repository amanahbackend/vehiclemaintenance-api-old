﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.ServiceTypes
{
    public class ServiceType : BaseEntity
    {
        [StringLength(100)]
        public string NameAr { get; set; }

        [StringLength(100)]
        public string NameEn { get; set; }       

        public int Period { get; set; }
    }
}
