﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.ServiceTypes
{
    public class ServiceTypeRepository : Repository<ServiceType>, IServiceTypeRepository
    {
        public ServiceTypeRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
