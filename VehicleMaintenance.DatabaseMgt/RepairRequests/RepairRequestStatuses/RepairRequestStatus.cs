﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses
{
   public class RepairRequestStatus :BaseEntity
    {
        [StringLength(100)]
        public string NameAr { get; set; }

        [StringLength(100)]
        public string NameEn { get; set; }
    }
}
