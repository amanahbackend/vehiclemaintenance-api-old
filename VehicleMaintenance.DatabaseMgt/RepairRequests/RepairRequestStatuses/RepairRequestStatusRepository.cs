﻿namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses
{
    public class RepairRequestStatusRepository : Repository<RepairRequestStatus>, IRepairRequestStatusRepository
    {
        public RepairRequestStatusRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
