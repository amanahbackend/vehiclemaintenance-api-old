﻿using System;
using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts
{
    public interface IRepairRequestSparePartRepository : IRepository<RepairRequestSparePart>
    {
        RepairRequestSparePart GetRepairRequestSparePart(int id);

        List<RepairRequestSparePart> GetByRepairRequest(int repairRequestId, int rowStatusId);

        List<RepairRequestSparePart> GetPendingRepairRequestSpareParts();

        List<RepairRequestSparePart> Search(int? sparePartId, DateTime? startDate, DateTime? endDate,
            string vehiclePlateNo, string vehicleFleetNo, int? jobCardId, int? jobCardStatusId);

        List<RepairRequestSparePart> GetSparePartsOfJobCard(int jobCardId);

        List<RepairRequestSparePart> GetUndeliveredSpareParts(int? jobCardId, DateTime? startDate, DateTime? endDate);
        
        List<RepairRequestSparePart> GetTechniciansWorkReport(int? technicianId, DateTime? startDate, DateTime? endDate);
    }
}
