﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts
{
    public class RepairRequestSparePart : BaseEntity
    {
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }

        /// <summary>
        /// Database De-normalization.
        /// </summary>
        public int? JobCardId { get; set; }
        public virtual JobCard JobCard { get; set; }

        /// <summary>
        /// Database De-normalization.
        /// </summary>
        public int? VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }

        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }

        /// <summary>
        /// DeliveredQuantity should be less than or equal Quantity.
        /// </summary>
        public int? DeliveredQuantity { get; set; }

        public int? Quantity { get; set; }

        [Column(TypeName = "decimal(18, 4)")]
        public decimal? Amount { get; set; }

        [StringLength(100)]
        public string Source { get; set; }

        public int? KiloMetersChangeSparePart { get; set; }

        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }
        public DateTime? TechnicianStartDate { get; set; }
        public DateTime? TechnicianEndDate { get; set; }
        
        [StringLength(200)]
        public string Comments { get; set; }
    }
}
