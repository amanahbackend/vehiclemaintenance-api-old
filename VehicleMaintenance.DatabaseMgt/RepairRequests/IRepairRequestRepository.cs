﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests
{
    public interface IRepairRequestRepository : IRepository<RepairRequest>
    {
        RepairRequest GetRepairRequest(int repairRequestId);

        List<RepairRequest> GetByJobCardId(int jobCardId);
        
        bool AllRepairRequestsAreClosedForJobCard(int jobCardId);

        //int GetRepairRequestsCountOfJobCard(int jobCardId);

        //int GetClosedRepairRequestsCountOfJobCard(int jobCardId);

        List<RepairRequest> GetPendingRepairRequests();

        int GetOpenRepairRequestsCount();
    }
}
