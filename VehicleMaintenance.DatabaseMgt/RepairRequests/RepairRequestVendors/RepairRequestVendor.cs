﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors
{
    [Route("api/RepairRequestVendor")]
    [Produces("application/json")]
    public class RepairRequestVendor : BaseEntity
    {
        [Required]
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }

        [Required]
        public int? VendorId { get; set; }
        public virtual Vendor Vendor { get; set; }

        public virtual ICollection<UploadedFile> Files { get; set; }
    }
}
