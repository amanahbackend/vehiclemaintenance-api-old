﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors
{
    public class RepairRequestVendorRepository : Repository<RepairRequestVendor>, IRepairRequestVendorRepository
    {
        public RepairRequestVendorRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<RepairRequestVendor> GetByRepairRequest(int repairRequestId)
        {
            List<RepairRequestVendor> lstRepairRequestVendors = GetQueryable()
                .Include("Files").Include("Vendor")
                .Where(obj => obj.RepairRequestId == repairRequestId &&
                              obj.RowStatusId != (int) EnumRowStatus.Deleted).ToList();

            return lstRepairRequestVendors;
        }
    }
}
