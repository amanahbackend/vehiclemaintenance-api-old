﻿namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes
{
    public class RepairRequestTypeRepository : Repository<RepairRequestType>, IRepairRequestTypeRepository
    {
        public RepairRequestTypeRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
