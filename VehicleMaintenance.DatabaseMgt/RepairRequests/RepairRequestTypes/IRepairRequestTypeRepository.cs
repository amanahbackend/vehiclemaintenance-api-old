﻿namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes
{
    public interface IRepairRequestTypeRepository : IRepository<RepairRequestType>
    {
    }
}
