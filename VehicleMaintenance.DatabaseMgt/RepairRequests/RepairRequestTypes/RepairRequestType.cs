﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes
{
    public class RepairRequestType : BaseEntity
    {
        [StringLength(100)]
        public string NameAr { get; set; }

        [StringLength(100)]
        public string NameEn { get; set; }

        [DefaultValue(false)]
        public bool? NeedApprove { get; set; }
    }
}
