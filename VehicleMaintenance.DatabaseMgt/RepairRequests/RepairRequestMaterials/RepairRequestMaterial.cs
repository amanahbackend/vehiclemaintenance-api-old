﻿using System.ComponentModel.DataAnnotations.Schema;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials
{
    public class RepairRequestMaterial : BaseEntity
    {
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }

        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }

        public int? SparePartStatusId { get; set; }
        public virtual SparePartStatus SparePartStatus { get; set; }

        public int? Quantity { get; set; }

        [Column(TypeName = "decimal(18, 4)")]
        public decimal? Cost { get; set; }

        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }
    }
}
