﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials
{
    public interface IRepairRequestMaterialRepository : IRepository<RepairRequestMaterial>
    {
        List<RepairRequestMaterial> GetByRepairRequestId(int repairRequestId);
    }
}
