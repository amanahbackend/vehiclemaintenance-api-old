﻿using System;
using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder
{
    public interface ISparePartDeliveryOrderRepository : IRepository<SparePartDeliveryOrder>
    {
        List<SparePartDeliveryOrder> Search(int? deliveryOrderId, DateTime? startDate, DateTime? endDate);
    }
}
