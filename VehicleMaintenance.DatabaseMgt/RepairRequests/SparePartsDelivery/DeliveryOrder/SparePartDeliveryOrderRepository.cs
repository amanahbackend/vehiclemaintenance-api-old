﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder
{
    public class SparePartDeliveryOrderRepository : Repository<SparePartDeliveryOrder>, ISparePartDeliveryOrderRepository
    {
        public SparePartDeliveryOrderRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<SparePartDeliveryOrder> Search(int? deliveryOrderId, DateTime? startDate, DateTime? endDate)
        {
            var iqSparePartsDeliveryOrders = GetQueryable()
                .Where(obj => obj.RowStatusId == (int)EnumRowStatus.Active);

            if (deliveryOrderId != null && deliveryOrderId.Value > 0)
            {
                iqSparePartsDeliveryOrders = iqSparePartsDeliveryOrders.Where(obj => obj.Id == deliveryOrderId);
            }

            if (startDate != null)
            {
                iqSparePartsDeliveryOrders = iqSparePartsDeliveryOrders.Where(obj => obj.CreationDate.Value.Date >= startDate);
            }

            if (endDate != null)
            {
                iqSparePartsDeliveryOrders = iqSparePartsDeliveryOrders.Where(obj => obj.CreationDate.Value.Date <= endDate);
            }

            List<SparePartDeliveryOrder> result = iqSparePartsDeliveryOrders.OrderByDescending(obj => obj.Id).ToList();

            return result;
        }
    }
}
