﻿using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder;
using VehicleMaintenance.DatabaseMgt.SpareParts;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails
{
    public class SparePartDeliveryOrderDetails : BaseEntity
    {
        public int? SparePartDeliveryOrderId { get; set; }
        public virtual SparePartDeliveryOrder SparePartDeliveryOrder { get; set; }

        public int? RepairRequestSparePartId { get; set; }
        public virtual RepairRequestSparePart RepairRequestSparePart { get; set; }

        /// <summary>
        /// This property should equal RepairRequestSparePart.SparePartId.
        /// The property is denormalized to prevent joining with RepairRequestSparePart.
        /// </summary>
        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }

        public int? DeliveredQty { get; set; }
    }
}
