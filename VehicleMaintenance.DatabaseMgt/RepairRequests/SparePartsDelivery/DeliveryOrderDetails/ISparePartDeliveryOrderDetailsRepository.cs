﻿namespace VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails
{
    public interface ISparePartDeliveryOrderDetailsRepository : IRepository<SparePartDeliveryOrderDetails>
    {
    }
}
