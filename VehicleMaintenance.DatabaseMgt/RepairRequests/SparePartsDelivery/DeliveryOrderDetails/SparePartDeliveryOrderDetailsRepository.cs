﻿namespace VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails
{
    public class SparePartDeliveryOrderDetailsRepository : Repository<SparePartDeliveryOrderDetails>, ISparePartDeliveryOrderDetailsRepository
    {
        public SparePartDeliveryOrderDetailsRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
