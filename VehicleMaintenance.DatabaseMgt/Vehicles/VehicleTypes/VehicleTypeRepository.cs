﻿namespace VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes
{
    public class VehicleTypeRepository : Repository<VehicleType>, IVehicleTypeRepository
    {
        public VehicleTypeRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }
    }
}
