﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
   public interface IJunkUser : IIdentityBaseEntity
    {
         string FirstName { get; set; }
         string LastName { get; set; }
         string Phone1 { get; set; }
         string Phone2 { get; set; }
         string PicturePath { get; set; }
         bool Deactivated { get; set; }
    }
}
