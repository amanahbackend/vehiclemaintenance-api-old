﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.Models.IEntities.Common
{
    public interface IShift : IBaseEntity
    {
        string Name { get; set; }

        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }
    }
}
