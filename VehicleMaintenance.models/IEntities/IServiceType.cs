﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.Models.IEntities
{
    public interface IServiceType : IBaseEntity
    {
        string ServiceTypeName_AR { get; set; }
        string ServiceTypeName_EN { get; set; }
        bool? Maintenance_Type { get; set; }
        bool? Need_Approve { get; set; }
        int Period { get; set; }
    }
}
