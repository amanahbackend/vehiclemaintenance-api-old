﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
    public interface IDriver : IIdentityBaseEntity
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Address { get; set; }
        string PhoneNumber { get; set; }
        string Title { get; set; }
        string DateOfBirth { get; set; }
        string SSN { get; set; }
        DateTime LicenseIssuedDate { get; set; }
        string LicenseIssuedState { get; set; }
        string LicenseNumber { get; set; }
        string Gender { get; set; }
        string MaritalStatus { get; set; }
        bool Active { get; set; }
        string Employe_Id { get; set; }

    }
}
