﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.Models.IEntities.Vehicles
{
    public interface IVehicle : IBaseEntity
    {
        string Year { get; set; }
        string Make { get; set; }  // manufacturer
        string Model { get; set; }  // the specific type of car
        string Color { get; set; }
        string KiloMeters { get; set; }
        string VIN { get; set; }  //vehicle identification number

        int? VehicleTypeId { get; set; }
        VehicleType VehicleType { get; set; }

        string ToolDescription { get; set; }
        string Status { get; set; }
        string Remarks { get; set; }
        string ChassisNumber { get; set; }
        double? Odometer { get; set; }
        string FleetNumber { get; set; }
        string VehicleNumberPlate { get; set; }
        DateTime? PurchaseDate { get; set; }
        int? FuelConsumption { get; set; }
        string WorkIdleTime { get; set; }
    }
}
