﻿using System.Collections.Generic;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.Models.IEntities.RepairRequests
{
    public interface IRepairRequest :IBaseEntity
    {
       string FK_User_ID { get; set; }
       int? FK_RepairRequestType_ID { get; set; }
       int? FK_JobCard_ID { get; set; }
       int? FK_RepairRequestStatus_ID { get; set; }
       List<int> FK_File_IDs { get; set; }
       bool? Maintenance_Type { get; set; }
       int? FK_ServiceType_ID { get; set; }
       double Duration { get; set; }
       double Cost { get; set; }
       bool? External { get; set; }
       string Comment { get; set; }

       Driver Driver { get; set; }
       
       ServiceType ServiceType { get; set; }
       User User { get; set; }
       RepairRequestType RepairRequestType { get; set; }
       RepairRequestStatus RepairRequestStatus { get; set; }
       JobCard JobCard { get; set; }
       ICollection<File> Files { get; set; }

    }
}
