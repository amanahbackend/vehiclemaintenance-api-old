﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
    public interface IRepairRequestType  :IBaseEntity
    {
        string RepairRequestTypeName_AR { get; set; }
        string RepairRequestTypeName_EN { get; set; }
    }
}
