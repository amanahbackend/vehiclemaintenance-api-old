﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.IEntities
{
    public interface IRepairRequestTechnician : IIdentityBaseEntity
    {
        int? FK_Technician_ID { get; set; }
        int? FK_RepairRequest_ID { get; set; }
        DateTime StartTime { get; set; }
        bool? Removed { get; set; }

        DateTime EndTime { get; set; }
        Technician Technician { get; set; }
        RepairRequest RepairRequest { get; set; }
    }
}
