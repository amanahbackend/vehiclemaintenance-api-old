﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
    public interface IRepairRequestStatus :IBaseEntity
    {
        string RepairRequestStatusName_AR { get; set; }
        string RepairRequestStatusName_EN { get; set; }
    }
}
