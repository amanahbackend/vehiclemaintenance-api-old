﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleMaintenance.models.IEntities
{
   public interface IPasswordTokenPin
    {
        int Id { get; set; }
        string Token { get; set; }
        string Pin { get; set; }
    }
}
