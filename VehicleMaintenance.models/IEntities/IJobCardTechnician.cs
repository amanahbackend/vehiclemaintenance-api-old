﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.IEntities
{
    public interface IJobCardTechnician : IIdentityBaseEntity
    {
        int? FK_Technician_ID { get; set; }
        int? FK_JobCard_ID { get; set; }
        DateTime StartTime { get; set; }
        DateTime EndTime { get; set; }
        Technician Technician { get; set; }
        JobCard JobCard { get; set; }
    }
}
