﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.IEntities
{
    public interface IFile :IBaseEntity
    {
        string FileName { get; set; }
        string FilePath { get; set; }
        string FileURL { get; set; }
        int? FK_RepairRequest_ID { get; set; }
        int FK_Vendor_ID { get; set; }
        bool? Selected { get; set; }
        int? FK_JobCard_ID { get; set; }

        Vendor Vendor { get; set; }
        RepairRequest RepairRequest { get; set; }
        JobCard JobCard { get; set; }
    }
}
