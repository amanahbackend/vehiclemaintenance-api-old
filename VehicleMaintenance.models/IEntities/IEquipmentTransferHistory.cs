﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.IEntities
{
    public interface IEquipmentTransferHistory:IBaseEntity
    {
        int? FK_RepairRequest_ID { get; set; }
        int? FK_User_ID { get; set; }
        User User { get; set; }
        RepairRequest RepairRequest { get; set; }
    }
}
