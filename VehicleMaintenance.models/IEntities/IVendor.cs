﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
   public interface IVendor : IBaseEntity
    {
        string Name { get; set; }
        string Address { get; set; }

    }
}
