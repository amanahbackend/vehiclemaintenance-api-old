﻿using System;
using System.Collections.Generic;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.models.IEntities
{
    public interface IJobCard  :IBaseEntity
    {
        string Customer_Type { get; set; }
        int? FK_ServiceType_ID { get; set; }
        DateTime Date_In { get; set; }
        DateTime Date_Out { get; set; }

        int? DateInShiftId { get; set; }
        int? DateOutShiftId { get; set; }

        string Desc { get; set; }
        bool? Maintenance_Type { get; set; }
        int? FK_Vehicle_ID { get; set; }
        int? FK_Driver_ID { get; set; }
        int? FK_Status_ID { get; set; }
        string Comment { get; set; }
        double? Odometer { get; set; }
        double? Hourmeter { get; set; }
        List<int> FK_File_IDs { get; set; }
        Vehicle vehicle { get; set; }
        Driver Driver { get; set; }
        ServiceType ServiceType { get; set; }
        Status Status { get; set; }
        ICollection<File> Files { get; set; }
    }
}
