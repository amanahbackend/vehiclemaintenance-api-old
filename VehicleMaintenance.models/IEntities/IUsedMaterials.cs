﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.models.IEntities
{
    public interface IUsedMaterials : IBaseEntity
    {
        int? FK_JobCard_ID { get; set; }
        int? FK_RepairRequest_ID { get; set; }
        int? FK_Technician_ID { get; set; }
        int Amount { get; set; }
        double cost { get; set; }
        int? FK_Inventory_ID { get; set; }
        int? FK_SparePartStatus_ID { get; set; }
        string FleetNumber { get; set; }
        SparePartStatus SparePartStatus { get; set; }
        Vehicle Vehicle { get; set; }
        Inventory Inventory { get; set; }
        JobCard JobCard { get; set; }
        Technician Technician { get; set; }
        RepairRequest RepairRequest { get; set; }
    }
}
