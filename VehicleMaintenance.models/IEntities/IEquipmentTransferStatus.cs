﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
    public interface IEquipmentTransferStatus :IBaseEntity
    {
        string EquipmentTransferStatusName_AR { get; set; }
        string EquipmentTransferStatusName_EN { get; set; }
    }
}
