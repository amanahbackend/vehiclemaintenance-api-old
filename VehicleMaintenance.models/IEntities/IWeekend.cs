﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
   public interface IWeekend : IIdentityBaseEntity
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }
}
