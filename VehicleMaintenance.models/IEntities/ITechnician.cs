﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
    public interface ITechnician : IBaseEntity
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Address { get; set; }
        string PhoneNumber { get; set; }
        string Specialty { get; set; }
        string Employe_Id { get; set; }
        double HourRating { get; set; }
        bool? Available { get; set; }
    }
}
