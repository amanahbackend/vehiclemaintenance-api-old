﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
   public interface IStatus : IBaseEntity
    {
        string StatusName_AR { get; set; }
        string StatusName_EN { get; set; }
    }
}
