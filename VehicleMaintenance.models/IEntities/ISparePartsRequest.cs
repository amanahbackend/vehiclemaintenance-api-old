﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.IEntities
{
    public interface ISparePartsRequest : IBaseEntity
    {
        int? FK_RepairRequest_ID { get; set; }
        int? FK_Technician_ID { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        int? FK_Status_ID { get; set; }
        string Source { get; set; }
        string Comments { get; set; }

        Status Status { get; set; }
        decimal? Amount { get; set; }
        int? Quantity { get; set; }

        int? FK_Inventory_ID { get; set; }
        Inventory Inventory { get; set; }
        JobCard JobCard { get; set; }
        Technician Technician { get; set; }
        RepairRequest RepairRequest { get; set; }
    }
}
