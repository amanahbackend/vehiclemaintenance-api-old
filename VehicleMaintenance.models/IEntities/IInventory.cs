﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace VehicleMaintenance.models.IEntities
{
  public interface IInventory : IBaseEntity
    {
        string Name_ER { get; set; }
        string Name_EN { get; set; }
        string SerialNumber { get; set; }
        string Case_AR { get; set; }
        string Case_EN { get; set; }
        string Model { get; set; }
        string Make { get; set; }
        string Description { get; set; }
        int Cost { get; set; }
        string Unit { get; set; }

    }
}
