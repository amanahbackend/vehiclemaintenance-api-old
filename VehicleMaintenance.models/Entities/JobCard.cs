﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.models.Entities
{
    public class JobCard : BaseEntity, IJobCard
    {
        [StringLength(100)]
        public string Customer_Type { get; set; }        //plant

        public DateTime Date_In { get; set; }
        public DateTime Date_Out { get; set; }

        public int? DateInShiftId { get; set; }
        public int? DateOutShiftId { get; set; }

        [StringLength(200)]
        public string Desc { get; set; }

        public bool? Maintenance_Type { get; set; } //work order type
        public int? FK_Vehicle_ID { get; set; }
        public int? FK_Driver_ID { get; set; }
        public int? FK_ServiceType_ID { get; set; }

        [StringLength(200)]
        public string Comment { get; set; }

        public int? FK_Status_ID { get; set; }
        public double? Odometer { get; set; }
        public double? Hourmeter { get; set; }

        [StringLength(50)]
        public string FK_User_ID { get; set; }

        [NotMapped]
        public virtual User User { get; set; }

        [NotMapped]
        public virtual List<int> FK_File_IDs { get; set; }

        [NotMapped]
        public virtual Status Status { get; set; }

        [NotMapped]
        public virtual Vehicle vehicle { get; set; }

        [NotMapped]
        public virtual Driver Driver { get; set; }

        [NotMapped]
        public virtual ServiceType ServiceType { get; set; }

        [NotMapped]
        public virtual List<RepairRequest> RepairRequests { get; set; }

        [NotMapped]
        public virtual ICollection<File> Files { get; set; }

        [NotMapped]
        public int? Sr { get; set; }

        [NotMapped]
        public string DailyServiceDesc { get; set; }
    }

    public enum EnumJobCardStatus
    {
        InProgress = 2,
        Closed = 3,
        Cancelled = 4
    }
}
