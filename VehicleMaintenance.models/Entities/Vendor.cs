﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
   public class Vendor : BaseEntity, IVendor, IBaseEntity
    {
        [StringLength(100)]
        public string Name { get; set; }

        public string Address { get; set; }
    }
}
