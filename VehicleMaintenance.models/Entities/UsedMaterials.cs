﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.models.Entities
{
   public class UsedMaterials : BaseEntity, IUsedMaterials
    {
        public int? FK_JobCard_ID { get; set; }
        public int? FK_Technician_ID { get; set; }
        public int Amount { get; set; }
        public  double cost { get; set; }
        public int? FK_Inventory_ID { get; set; }
        public int? FK_SparePartStatus_ID { get; set; }

        [StringLength(50)]
        public string FleetNumber { get; set; }

        public int? FK_RepairRequest_ID { get; set; }

        [NotMapped]
        public RepairRequest RepairRequest { get; set; }
        [NotMapped]
        public SparePartStatus SparePartStatus { get; set; }

        [NotMapped]
        public Vehicle Vehicle { get; set; }

        [NotMapped]
        public Inventory Inventory { get; set; }
        
        [NotMapped]
        public JobCard JobCard { get; set; }

        [NotMapped]
        public Technician Technician { get; set; }


        public int? FK_TranferFromVehicle_Id { get; set; }
        [ForeignKey("FK_TranferFromVehicle_Id")]
        public Vehicle TransferFromVehicle { get; set; }

        [StringLength(50)]
        public string Capacity { get; set; }
    }
}
