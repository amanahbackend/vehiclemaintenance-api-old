﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace VehicleMaintenance.Models.Entities.Vehicles
{
    public class VehicleType : BaseEntity
    {
        [StringLength(100)]
        public string Name { get; set; }
    }
}
