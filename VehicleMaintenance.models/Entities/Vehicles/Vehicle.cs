﻿using System;
using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.Models.IEntities.Vehicles;

namespace VehicleMaintenance.Models.Entities.Vehicles
{
    public class Vehicle : BaseEntity, IVehicle
    {
        [StringLength(20)]
        public string Year { get; set; }

        [StringLength(100)]
        public string Make { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [StringLength(50)]
        public string KiloMeters { get; set; }

        [StringLength(50)]
        public string VIN { get; set; }

        public int? VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }

        [StringLength(500)]
        public string ToolDescription { get; set; }

        //public int? Quantity { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        [StringLength(50)]
        public string ChassisNumber { get; set; }

        public double? Odometer { get; set; }

        [StringLength(50)]
        public string FleetNumber { get; set; }

        [StringLength(50)]
        public string VehicleNumberPlate { get; set; }
        public DateTime? PurchaseDate { get; set; }

        public int? FuelConsumption { get; set; }

        [StringLength(50)]
        public string WorkIdleTime { get; set; }
    }
}
