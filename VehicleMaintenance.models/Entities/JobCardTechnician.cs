﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
   public class JobCardTechnician : BaseEntity, IJobCardTechnician
    {
        public int? FK_Technician_ID { get; set; }
        public int? FK_JobCard_ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        [NotMapped]
        public Technician Technician { get; set; }
        [NotMapped]
        public JobCard JobCard { get; set; }
    }
}
