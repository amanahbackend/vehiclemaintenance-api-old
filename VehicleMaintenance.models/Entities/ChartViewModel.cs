﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.models.Entities
{
    public class ChartViewModel
    {
        [StringLength(100)]
        public string NameEN { get; set; }

        [StringLength(100)]
        public string NameAR { get; set; }

        public int Count { get; set; }
        public int Id { get; set; }
    }
}
