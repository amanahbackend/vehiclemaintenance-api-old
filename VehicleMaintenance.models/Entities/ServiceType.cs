﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.Models.IEntities;

namespace VehicleMaintenance.Models.Entities
{
   public class ServiceType: BaseEntity , IServiceType
    {
        [StringLength(100)]
        public string ServiceTypeName_AR { get; set; }

        [StringLength(100)]
        public string ServiceTypeName_EN { get; set; }
        public bool? Maintenance_Type { get; set; }
        public bool? Need_Approve { get; set; }
        public int Period { get; set; }
    }
}
