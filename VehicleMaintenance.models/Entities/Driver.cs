﻿using System;
using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
    public class Driver  : BaseEntity, IDriver
    {
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(50)]
        public string DateOfBirth { get; set; }

        [StringLength(50)]
        public string SSN { get; set; }

        public DateTime LicenseIssuedDate { get; set; }

        [StringLength(50)]
        public string LicenseIssuedState { get; set; }

        [StringLength(50)]
        public string LicenseNumber { get; set; }

        [StringLength(20)]
        public string Gender { get; set; }

        [StringLength(50)]
        public string MaritalStatus { get; set; }

        public bool Active { get; set; }

        [StringLength(50)]
        public string Employe_Id { get; set; }

    }
}
