﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
   public class Technician : BaseEntity, ITechnician, IBaseEntity
    {
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Specialty { get; set; }

        [StringLength(50)]
        public string Employe_Id { get; set; }

        public double HourRating { get; set; }
        public bool? Available { get; set; }
    }
}
