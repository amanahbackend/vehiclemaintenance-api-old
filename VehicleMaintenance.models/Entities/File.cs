﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.Entities
{
    public class File : BaseEntity, IFile
    {
        [StringLength(50)]
        public string FileName { get; set; }

        [StringLength(100)]
        public string FilePath { get; set; }

        [StringLength(100)]
        public string FileURL { get; set; }

        public int? FK_RepairRequest_ID { get; set; }
        public int? FK_JobCard_ID { get; set; }
        public int FK_Vendor_ID { get; set; }
        public bool? Selected { get; set; }

        [NotMapped]
        public Vendor Vendor { get; set; }

        [NotMapped]
        public RepairRequest RepairRequest { get; set; }

        [NotMapped]
        public JobCard JobCard { get; set; }
    }
}
