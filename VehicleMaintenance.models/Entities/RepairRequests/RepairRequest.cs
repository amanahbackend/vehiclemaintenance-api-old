﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.IEntities.RepairRequests;

namespace VehicleMaintenance.Models.Entities.RepairRequests
{
    public class RepairRequest : BaseEntity, IRepairRequest
    {
        [StringLength(50)]
        public string FK_User_ID { get; set; }
        public int? FK_RepairRequestType_ID { get; set; }
        public int? FK_JobCard_ID { get; set; }
        public int? FK_RepairRequestStatus_ID { get; set; }
        public bool? Maintenance_Type { get; set; }
        public int? FK_ServiceType_ID { get; set; }

        public double Duration { get; set; }
        public double Cost { get; set; }
        public bool? External { get; set; }

        [StringLength(200)]
        public string Comment { get; set; }

        [NotMapped]
        public virtual Driver Driver { get; set; }

        [NotMapped]
        public virtual ServiceType ServiceType { get; set; }

        [NotMapped]
        public virtual List<int> FK_File_IDs { get; set; }

        [NotMapped]
        public virtual User User { get; set; }

        [NotMapped]
        public virtual RepairRequestType RepairRequestType { get; set; }

        [NotMapped]
        public virtual RepairRequestStatus RepairRequestStatus { get; set; }

        [NotMapped]
        public virtual JobCard JobCard { get; set; }

        [NotMapped]
        public virtual ICollection<File> Files { get; set; }
    }

    public enum EnumRepairRequestStatus
    {
        Closed = 4
    }
}
