﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.Models.Entities.RepairRequests
{
    public class RepairRequestType : BaseEntity, IRepairRequestType
    {
        [StringLength(100)]
        public string RepairRequestTypeName_AR { get; set; }

        [StringLength(100)]
        public string RepairRequestTypeName_EN { get; set; }
    }
}
