﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.Models.Entities.RepairRequests
{
   public class RepairRequestTechnician : BaseEntity, IRepairRequestTechnician
    {
        public int? FK_Technician_ID { get; set; }
        public int? FK_RepairRequest_ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool? Removed { get; set; }

        [NotMapped]
        public virtual Technician Technician { get; set; }

        [NotMapped]
        public virtual RepairRequest RepairRequest { get; set; }
    }
}
