﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.Models.Entities.RepairRequests
{
   public class RepairRequestStatus :BaseEntity , IRepairRequestStatus
    {
        [StringLength(100)]
        public string RepairRequestStatusName_AR { get; set; }

        [StringLength(100)]
        public string RepairRequestStatusName_EN { get; set; }
    }
}
