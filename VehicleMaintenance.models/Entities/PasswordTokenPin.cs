using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
    public class PasswordTokenPin : IPasswordTokenPin
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string Token { get; set; }

        [StringLength(50)]
        public string Pin { get; set; }
    }
}
