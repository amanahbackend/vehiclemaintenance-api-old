﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
   public class Inventory : BaseEntity, IInventory
    {
        [StringLength(100)]
        public string Name_ER { get; set; }

        [StringLength(100)]
        public string Name_EN { get; set; }

        [StringLength(50)]
        public string SerialNumber { get; set; }

        [StringLength(100)]
        public string Case_AR { get; set; }

        [StringLength(100)]
        public string Case_EN { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public int Cost { get; set; }

        [StringLength(50)]
        public string Unit { get; set; }
    }
}
