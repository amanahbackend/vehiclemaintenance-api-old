﻿using System;
using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.AspNetCore.Identity;

namespace VehicleMaintenance.models.Entities
{
    public class Role : IdentityRole, IIdentityBaseEntity
    {

        public Role() : base()
        {

        }
        public Role(string roleName) : base(roleName)
        {

        }

        [StringLength(50)]
        public string FK_CreatedBy_Id { get; set; }

        [StringLength(50)]
        public string FK_UpdatedBy_Id { get; set; }

        [StringLength(50)]
        public string FK_DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}
