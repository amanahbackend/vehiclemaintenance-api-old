﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
    public class Status : BaseEntity, IStatus
    {
        [StringLength(50)]
        public string StatusName_AR { get; set; }

        [StringLength(50)]
        public string StatusName_EN { get; set; }
    }
}
