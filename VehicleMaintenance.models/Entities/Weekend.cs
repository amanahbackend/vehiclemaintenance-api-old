﻿using System;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
    public class Weekend : BaseEntity, IWeekend
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
