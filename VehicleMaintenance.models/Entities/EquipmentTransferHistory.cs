﻿using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.Entities
{
    public class EquipmentTransferHistory : BaseEntity, IEquipmentTransferHistory
    {
        public int? FK_RepairRequest_ID { get; set; }
        public int? FK_User_ID { get; set; }

        [NotMapped]
        public User User { get; set; }

        [NotMapped]
        public RepairRequest RepairRequest { get; set; }
    }
}
