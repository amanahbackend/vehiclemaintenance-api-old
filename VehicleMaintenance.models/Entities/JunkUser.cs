using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
    public class JunkUser : IdentityUser , IJunkUser
    {
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string PicturePath { get; set; }

        [StringLength(50)]
        public string FK_CreatedBy_Id { get; set; }

        [StringLength(50)]
        public string FK_UpdatedBy_Id { get; set; }

        [StringLength(50)]
        public string FK_DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool Deactivated { get; set; }
    }
}
