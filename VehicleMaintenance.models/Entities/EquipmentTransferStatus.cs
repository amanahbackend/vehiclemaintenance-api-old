﻿using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;

namespace VehicleMaintenance.models.Entities
{
   public class EquipmentTransferStatus  : BaseEntity , IEquipmentTransferStatus
    {
        [StringLength(100)]
        public string EquipmentTransferStatusName_AR { get; set; }

        [StringLength(100)]
        public string EquipmentTransferStatusName_EN { get; set; }
    }
}
