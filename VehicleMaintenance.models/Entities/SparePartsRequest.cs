﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using VehicleMaintenance.models.IEntities;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.Entities
{
  public  class SparePartsRequest : BaseEntity, ISparePartsRequest
    {
        public int? FK_RepairRequest_ID { get; set; }
        public int? FK_Technician_ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal? Amount { get; set; }
        public int? Quantity { get; set; }

        public int? FK_Status_ID { get; set; }
        public Status Status { get; set; }

        [StringLength(100)]
        public string Source { get; set; }

        [StringLength(200)]
        public string Comments { get; set; }

        public int? FK_Inventory_ID { get; set; }
        [NotMapped]
        public Inventory Inventory { get; set; }
        [NotMapped]
        public JobCard JobCard { get; set; }
        [NotMapped]
        public Technician Technician { get; set; }

        [NotMapped]
        public RepairRequest RepairRequest { get; set; }
    }
}
