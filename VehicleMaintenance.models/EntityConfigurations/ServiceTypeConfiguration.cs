﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.Models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class ServiceTypeConfiguration : BaseEntityTypeConfiguration<ServiceType>, IEntityTypeConfiguration<ServiceType>
    {
        public override void Configure(EntityTypeBuilder<ServiceType> serviceTypeConfiguration)
        {
            base.Configure(serviceTypeConfiguration);

            serviceTypeConfiguration.ToTable("ServiceType");
            serviceTypeConfiguration.HasKey(o => o.Id);
            serviceTypeConfiguration.Property(o => o.ServiceTypeName_AR).IsRequired();
            serviceTypeConfiguration.Property(o => o.ServiceTypeName_EN).IsRequired();
            serviceTypeConfiguration.Property(o => o.Need_Approve).IsRequired().HasDefaultValue(false);
            serviceTypeConfiguration.Property(o => o.Maintenance_Type).IsRequired().HasDefaultValue(false);
            serviceTypeConfiguration.Property(o => o.Period).IsRequired();
        }
    }
}
