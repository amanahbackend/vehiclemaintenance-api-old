﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
    class EquipmentTransferHistoryTypeConfiguration : BaseEntityTypeConfiguration<EquipmentTransferHistory>, IEntityTypeConfiguration<EquipmentTransferHistory>
    {
        public override void Configure(EntityTypeBuilder<EquipmentTransferHistory> equipmentTransferHistoryConfiguration)
        {
            base.Configure(equipmentTransferHistoryConfiguration);

            equipmentTransferHistoryConfiguration.ToTable("EquipmentTransferHistory");
            equipmentTransferHistoryConfiguration.HasKey(o => o.Id);
            //equipmentTransferHistoryConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("EquipmentTransferHistoryseq");
            equipmentTransferHistoryConfiguration.Property(o => o.FK_RepairRequest_ID).IsRequired(false);
            equipmentTransferHistoryConfiguration.Property(o => o.FK_User_ID).IsRequired(false);
        } 
    }
}
