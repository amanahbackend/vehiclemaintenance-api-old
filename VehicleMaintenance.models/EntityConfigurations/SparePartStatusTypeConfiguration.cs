﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public  class SparePartStatusTypeConfiguration : BaseEntityTypeConfiguration<SparePartStatus>, IEntityTypeConfiguration<SparePartStatus>
    {
        public override void Configure(EntityTypeBuilder<SparePartStatus> sparePartStatusConfiguration)
        {
            base.Configure(sparePartStatusConfiguration);

            sparePartStatusConfiguration.ToTable("SparePartStatus");
            sparePartStatusConfiguration.HasKey(o => o.Id);
            //sparePartStatusConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("SparePartStatusseq");
            sparePartStatusConfiguration.Property(o => o.StatusName_AR).IsRequired();
            sparePartStatusConfiguration.Property(o => o.StatusName_EN).IsRequired();

        }
    }
}
