﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class FileTypeConfiguration : BaseEntityTypeConfiguration<File>, IEntityTypeConfiguration<File>
    {
        public override void Configure(EntityTypeBuilder<File> fileConfiguration)
        {
            base.Configure(fileConfiguration);

            fileConfiguration.ToTable("File");
            fileConfiguration.HasKey(o => o.Id);
            // fileConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Fileseq");
            fileConfiguration.Property(o => o.FileName).IsRequired(false);
            fileConfiguration.Property(o => o.FilePath).IsRequired(false);
            fileConfiguration.Property(o => o.FileURL).IsRequired(false);
            fileConfiguration.Property(o => o.Selected).IsRequired().HasDefaultValue(false);
            fileConfiguration.Property(o => o.FK_RepairRequest_ID).IsRequired(false);
            fileConfiguration.Property(o => o.FK_JobCard_ID).IsRequired(false);

        }
    }
}
