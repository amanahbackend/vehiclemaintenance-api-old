﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public class EquipmentTransferStatusTypeConfiguration : BaseEntityTypeConfiguration<EquipmentTransferStatus>, IEntityTypeConfiguration<EquipmentTransferStatus>
    {
        public override void Configure(EntityTypeBuilder<EquipmentTransferStatus> equipmentTransferStatusConfiguration)
        {
            base.Configure(equipmentTransferStatusConfiguration);

            equipmentTransferStatusConfiguration.ToTable("EquipmentTransferStatus");
            equipmentTransferStatusConfiguration.HasKey(o => o.Id);
            // equipmentTransferStatusConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("EquipmentTransferStatusseq");
            equipmentTransferStatusConfiguration.Property(o => o.EquipmentTransferStatusName_AR).IsRequired();
            equipmentTransferStatusConfiguration.Property(o => o.EquipmentTransferStatusName_EN).IsRequired();
        } 
    }
}
