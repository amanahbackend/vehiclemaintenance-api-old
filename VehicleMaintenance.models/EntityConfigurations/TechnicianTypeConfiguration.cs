﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public class TechnicianTypeConfiguration : BaseEntityTypeConfiguration<Technician>, IEntityTypeConfiguration<Technician>
    {
        public override void Configure(EntityTypeBuilder<Technician> technicianConfiguration)
        {
            base.Configure(technicianConfiguration);

            technicianConfiguration.ToTable("Technician");
            technicianConfiguration.HasKey(o => o.Id);
            //technicianConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Technicianseq");
            technicianConfiguration.Property(o => o.FirstName).IsRequired();
            technicianConfiguration.Property(o => o.LastName).IsRequired();
            technicianConfiguration.Property(o => o.PhoneNumber).IsRequired();
            technicianConfiguration.Property(o => o.Address).IsRequired();
            technicianConfiguration.Property(o => o.Specialty).IsRequired();
            technicianConfiguration.Property(o => o.Employe_Id).IsRequired();
            technicianConfiguration.Property(o => o.HourRating).IsRequired();
            technicianConfiguration.Property(o => o.Available).IsRequired().HasDefaultValue(false);
        }
    }
}
