﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public class WeekendTypeConfiguration : BaseEntityTypeConfiguration<Weekend>, IEntityTypeConfiguration<Weekend>
    {
        public override void Configure(EntityTypeBuilder<Weekend> weekendConfiguration)
        {
            base.Configure(weekendConfiguration);

            weekendConfiguration.ToTable("Weekend");
            weekendConfiguration.HasKey(o => o.Id);
            //weekendConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Weekendseq");
            weekendConfiguration.Property(o => o.StartDate).IsRequired();
            weekendConfiguration.Property(o => o.EndDate).IsRequired();         

        }
    }
}
