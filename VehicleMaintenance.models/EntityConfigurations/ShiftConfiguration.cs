﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.Models.Entities.Common;

namespace VehicleMaintenance.Models.EntityConfigurations
{
    public class ShiftConfiguration : BaseEntityTypeConfiguration<Shift>, IEntityTypeConfiguration<Shift>
    {
        public override void Configure(EntityTypeBuilder<Shift> shiftConfiguration)
        {
            base.Configure(shiftConfiguration);

            shiftConfiguration.ToTable("Shift");
            shiftConfiguration.HasKey(o => o.Id);
        }
    }
}
