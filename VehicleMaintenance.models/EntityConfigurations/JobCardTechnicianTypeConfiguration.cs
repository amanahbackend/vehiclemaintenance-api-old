﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class JobCardTechnicianTypeConfiguration : BaseEntityTypeConfiguration<JobCardTechnician>, IEntityTypeConfiguration<JobCardTechnician>
    {
        public override void Configure(EntityTypeBuilder<JobCardTechnician> jobCardTechnicianTypeConfiguration)
        {
            base.Configure(jobCardTechnicianTypeConfiguration);

            jobCardTechnicianTypeConfiguration.ToTable("JobCardTechnician");
            jobCardTechnicianTypeConfiguration.HasKey(o => o.Id);
            //JobCard_TechnicianTypeConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("JobCard_Technicianseq");
            jobCardTechnicianTypeConfiguration.Property(o => o.FK_JobCard_ID).IsRequired(false);
            jobCardTechnicianTypeConfiguration.Property(o => o.FK_Technician_ID).IsRequired(false);
            jobCardTechnicianTypeConfiguration.Property(o => o.StartTime).IsRequired();
            jobCardTechnicianTypeConfiguration.Property(o => o.EndTime).IsRequired();

            jobCardTechnicianTypeConfiguration.Ignore(o => o.JobCard);
            jobCardTechnicianTypeConfiguration.Ignore(o => o.Technician);
        }
    }
}