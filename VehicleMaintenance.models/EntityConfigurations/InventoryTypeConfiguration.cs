﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class InventoryTypeConfiguration : BaseEntityTypeConfiguration<Inventory>, IEntityTypeConfiguration<Inventory>
    {
        public override void Configure(EntityTypeBuilder<Inventory> inventoryConfiguration)
        {
            base.Configure(inventoryConfiguration);

            inventoryConfiguration.ToTable("Inventory");
            inventoryConfiguration.HasKey(o => o.Id);
            //inventoryConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Inventoryseq");
            inventoryConfiguration.Property(o => o.Name_ER).IsRequired();
            inventoryConfiguration.Property(o => o.Name_EN).IsRequired();
            inventoryConfiguration.Property(o => o.SerialNumber).IsRequired();
            inventoryConfiguration.Property(o => o.Case_AR).IsRequired();
            inventoryConfiguration.Property(o => o.Case_EN).IsRequired();
            inventoryConfiguration.Property(o => o.Model).IsRequired();
            inventoryConfiguration.Property(o => o.Make).IsRequired();
            inventoryConfiguration.Property(o => o.Description).IsRequired(false);
            inventoryConfiguration.Property(o => o.Cost).HasDefaultValue(10);
            inventoryConfiguration.Property(o => o.Unit).IsRequired(false);
        }
    }
}
