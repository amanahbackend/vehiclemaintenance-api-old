﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public  class StatusTypeConfiguration : BaseEntityTypeConfiguration<Status>, IEntityTypeConfiguration<Status>
    {
        public override void Configure(EntityTypeBuilder<Status> statusConfiguration)
        {
            base.Configure(statusConfiguration);

            statusConfiguration.ToTable("Status");
            statusConfiguration.HasKey(o => o.Id);
            //statusConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Statusseq");
            statusConfiguration.Property(o => o.StatusName_AR).IsRequired();
            statusConfiguration.Property(o => o.StatusName_EN).IsRequired();

        }
    }
}
