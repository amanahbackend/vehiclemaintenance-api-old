﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
  public  class VendorTypeConfiguration : BaseEntityTypeConfiguration<Vendor>, IEntityTypeConfiguration<Vendor>
    {
        public override void Configure(EntityTypeBuilder<Vendor> sparePartsRequestTypeConfiguration)
        {
            base.Configure(sparePartsRequestTypeConfiguration);

            sparePartsRequestTypeConfiguration.ToTable("Vendor");
            sparePartsRequestTypeConfiguration.HasKey(o => o.Id);
            //sparePartsRequestTypeConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Vendorseq");
            sparePartsRequestTypeConfiguration.Property(o => o.Name).IsRequired();
            sparePartsRequestTypeConfiguration.Property(o => o.Address).IsRequired(false);

        }
    }
}
