﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class RepairRequestTechnicianConfiguration : BaseEntityTypeConfiguration<RepairRequestTechnician>, IEntityTypeConfiguration<RepairRequestTechnician>
    {
        public override void Configure(EntityTypeBuilder<RepairRequestTechnician> repairRequestTechnicianTypeConfiguration)
        {
            base.Configure(repairRequestTechnicianTypeConfiguration);

            repairRequestTechnicianTypeConfiguration.ToTable("RepairRequestTechnician");
            repairRequestTechnicianTypeConfiguration.HasKey(o => o.Id);
            repairRequestTechnicianTypeConfiguration.Property(o => o.FK_RepairRequest_ID).IsRequired(false);
            repairRequestTechnicianTypeConfiguration.Property(o => o.FK_Technician_ID).IsRequired(false);
            repairRequestTechnicianTypeConfiguration.Property(o => o.StartTime).IsRequired();
            repairRequestTechnicianTypeConfiguration.Property(o => o.EndTime).IsRequired();
            repairRequestTechnicianTypeConfiguration.Property(o => o.Removed).IsRequired().HasDefaultValue(false);

            repairRequestTechnicianTypeConfiguration.Ignore(o => o.RepairRequest);
            repairRequestTechnicianTypeConfiguration.Ignore(o => o.Technician);
        }
    }
}