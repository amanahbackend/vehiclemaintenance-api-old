﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.models.EntityConfigurations
{
    class VehicleTypeConfiguration : BaseEntityTypeConfiguration<VehicleType>
    {
        public override void Configure(EntityTypeBuilder<VehicleType> vehicleTypeConfiguration)
        {
            base.Configure(vehicleTypeConfiguration);

            vehicleTypeConfiguration.ToTable("VehicleType");
            vehicleTypeConfiguration.HasKey(o => o.Id);

            //vehicleTypeConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("VehicleTypeSeq");
            vehicleTypeConfiguration.Property(o => o.Name).HasMaxLength(100).IsRequired();
            vehicleTypeConfiguration.Ignore(o => o.CurrentUserId);
            vehicleTypeConfiguration.Property(o => o.DeletedDate).IsRequired(false);
            vehicleTypeConfiguration.Property(o => o.FK_CreatedBy_Id).HasMaxLength(100);
            vehicleTypeConfiguration.Property(o => o.FK_DeletedBy_Id).HasMaxLength(100);
            vehicleTypeConfiguration.Property(o => o.FK_UpdatedBy_Id).HasMaxLength(100);
        }
    }
}
