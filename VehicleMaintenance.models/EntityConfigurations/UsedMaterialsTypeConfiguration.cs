﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class UsedMaterialsTypeConfiguration : BaseEntityTypeConfiguration<UsedMaterials>, IEntityTypeConfiguration<UsedMaterials>
    {
        public override void Configure(EntityTypeBuilder<UsedMaterials> sparePartsRequestTypeConfiguration)
        {
            base.Configure(sparePartsRequestTypeConfiguration);

            sparePartsRequestTypeConfiguration.ToTable("UsedMaterials");
            sparePartsRequestTypeConfiguration.HasKey(o => o.Id);
            //sparePartsRequestTypeConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("UsedMaterialsseq");
            sparePartsRequestTypeConfiguration.Property(o => o.FK_JobCard_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.FK_RepairRequest_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.FK_Technician_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.Amount).IsRequired();
            sparePartsRequestTypeConfiguration.Property(o => o.FK_Inventory_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.cost).HasDefaultValue(20).IsRequired();
            sparePartsRequestTypeConfiguration.Property(o => o.FK_SparePartStatus_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.FleetNumber).IsRequired(false);

            sparePartsRequestTypeConfiguration.Ignore(o => o.RepairRequest);
            sparePartsRequestTypeConfiguration.Ignore(o => o.Inventory);
            sparePartsRequestTypeConfiguration.Ignore(o => o.JobCard);
            sparePartsRequestTypeConfiguration.Ignore(o => o.Technician);
        }
    }
}
