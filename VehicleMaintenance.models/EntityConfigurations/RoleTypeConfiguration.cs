using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using VehicleMaintenance.models.Entities;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class RoleTypeConfiguration
        : IdentityBaseEntityTypeConfiguration<Role>, IEntityTypeConfiguration<Role>
    {
        public new void Configure(EntityTypeBuilder<Role> builder)
        {
            base.Configure(builder);
            //builder.Property(r => r.CreatedDate).IsRequired();
            //builder.Property(r => r.DeletedDate).IsRequired();
            //builder.Property(r => r.UpdatedDate).IsRequired();
            //builder.Property(r => r.FK_UpdatedBy_Id).IsRequired(false);
            //builder.Property(r => r.FK_DeletedBy_Id).IsRequired(false);
            //builder.Property(r => r.FK_CreatedBy_Id).IsRequired(false);
            //builder.Property(r => r.IsDeleted).IsRequired(true);
        }
    }
}
