﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public class DriverTypeConfiguration : BaseEntityTypeConfiguration<Driver>, IEntityTypeConfiguration<Driver>
    {
        public override void Configure(EntityTypeBuilder<Driver> driverConfiguration)
        {
            base.Configure(driverConfiguration);

            driverConfiguration.ToTable("Driver");
            driverConfiguration.HasKey(o => o.Id);
            //driverConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Driverseq");
            driverConfiguration.Property(o => o.FirstName).IsRequired();
            driverConfiguration.Property(o => o.LastName).IsRequired();
            driverConfiguration.Property(o => o.Address).IsRequired();
            driverConfiguration.Property(o => o.Active).IsRequired();
            driverConfiguration.Property(o => o.DateOfBirth).IsRequired();
            driverConfiguration.Property(o => o.Gender).IsRequired(false);
            driverConfiguration.Property(o => o.LicenseIssuedDate).IsRequired();
            driverConfiguration.Property(o => o.LicenseIssuedState).IsRequired();
            driverConfiguration.Property(o => o.LicenseNumber).IsRequired();
            driverConfiguration.Property(o => o.MaritalStatus).IsRequired(false);
            driverConfiguration.Property(o => o.PhoneNumber).IsRequired();
            driverConfiguration.Property(o => o.SSN).IsRequired();
            driverConfiguration.Property(o => o.Title).IsRequired();
            driverConfiguration.Property(o => o.Employe_Id).IsRequired();

        }
    }
}
