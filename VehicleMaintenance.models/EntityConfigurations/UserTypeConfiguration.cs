using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using VehicleMaintenance.models.Entities;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class UserTypeConfiguration
        : IdentityBaseEntityTypeConfiguration<User>, IEntityTypeConfiguration<User>
    {
        public new void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.FirstName).IsRequired(true);
            builder.Property(u => u.LastName).IsRequired(true);
            builder.Property(u => u.Phone1).IsRequired(true);
            builder.Property(u => u.Phone2).IsRequired(false);
            builder.Property(u => u.PicturePath).IsRequired(false);

        }
    }
}
