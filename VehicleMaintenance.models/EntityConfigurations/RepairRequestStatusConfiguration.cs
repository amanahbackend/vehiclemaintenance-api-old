﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class RepairRequestStatusConfiguration : BaseEntityTypeConfiguration<RepairRequestStatus>, IEntityTypeConfiguration<RepairRequestStatus>
    {
        public override void Configure(EntityTypeBuilder<RepairRequestStatus> repairRequestStatusConfiguration)
        {
            base.Configure(repairRequestStatusConfiguration);

            repairRequestStatusConfiguration.ToTable("RepairRequestStatus");
            repairRequestStatusConfiguration.HasKey(o => o.Id);
            repairRequestStatusConfiguration.Property(o => o.RepairRequestStatusName_AR).IsRequired();
            repairRequestStatusConfiguration.Property(o => o.RepairRequestStatusName_EN).IsRequired();

        }
    }
}
