﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.Models.Entities.Vehicles;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class VehicleConfiguration : BaseEntityTypeConfiguration<Vehicle>, IEntityTypeConfiguration<Vehicle>
    {
        public override void Configure(EntityTypeBuilder<Vehicle> vehicleConfiguration)
        {
            base.Configure(vehicleConfiguration);

            vehicleConfiguration.ToTable("Vehicle");
            vehicleConfiguration.HasKey(o => o.Id);

            //vehicleConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Vehicleseq");

            vehicleConfiguration.Property(o => o.KiloMeters).IsRequired(false);
            vehicleConfiguration.Property(o => o.Color).IsRequired(false);
            vehicleConfiguration.Property(o => o.Model).IsRequired();
            vehicleConfiguration.Property(o => o.Make).IsRequired(false);
            vehicleConfiguration.Property(o => o.PurchaseDate).IsRequired(false);
            vehicleConfiguration.Property(o => o.VehicleNumberPlate).IsRequired(false);
            vehicleConfiguration.Property(o => o.VIN).IsRequired(false);
            vehicleConfiguration.Property(o => o.WorkIdleTime).IsRequired(false);
            vehicleConfiguration.Property(o => o.FuelConsumption).IsRequired(false);
            vehicleConfiguration.Property(o => o.Year).IsRequired(false);
            vehicleConfiguration.Property(o => o.FleetNumber).IsRequired();
            vehicleConfiguration.Property(o => o.ToolDescription).IsRequired(false);
            vehicleConfiguration.Property(o => o.Status).IsRequired(false);
            vehicleConfiguration.Property(o => o.Remarks).IsRequired(false);
            vehicleConfiguration.Property(o => o.ChassisNumber).IsRequired(false);
            vehicleConfiguration.Property(o => o.Odometer).IsRequired(false);
        }
    }
}
