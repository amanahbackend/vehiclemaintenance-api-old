﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public class JobCardConfiguration : BaseEntityTypeConfiguration<JobCard>, IEntityTypeConfiguration<JobCard>
    {
        public override void Configure(EntityTypeBuilder<JobCard> jobCardConfiguration)
        {
            base.Configure(jobCardConfiguration);

            jobCardConfiguration.ToTable("JobCard");
            jobCardConfiguration.HasKey(o => o.Id);
            //JobCardConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("JobCardseq");
            jobCardConfiguration.Property(o => o.Customer_Type).IsRequired();
            jobCardConfiguration.Property(o => o.Date_In).IsRequired();
            jobCardConfiguration.Property(o => o.Date_Out).IsRequired();
            jobCardConfiguration.Property(o => o.Desc).IsRequired(false);
            jobCardConfiguration.Property(o => o.FK_ServiceType_ID).IsRequired(false);
            jobCardConfiguration.Property(o => o.FK_Driver_ID).IsRequired(false);
            jobCardConfiguration.Property(o => o.FK_Vehicle_ID).IsRequired();
            jobCardConfiguration.Property(o => o.Maintenance_Type).IsRequired().HasDefaultValue(false);
            jobCardConfiguration.Property(o => o.FK_Status_ID).IsRequired(false);
            jobCardConfiguration.Property(o => o.Odometer).IsRequired(false);
            jobCardConfiguration.Property(o => o.Hourmeter).IsRequired(false);
            jobCardConfiguration.Property(o => o.Comment).IsRequired(false);
            jobCardConfiguration.Ignore(o => o.Driver);
            jobCardConfiguration.Ignore(o => o.vehicle);

        }
    }
}
