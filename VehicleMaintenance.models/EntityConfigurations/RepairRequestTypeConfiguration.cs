﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.EntityConfigurations
{
   public class RepairRequestTypeConfiguration : BaseEntityTypeConfiguration<RepairRequestType>, IEntityTypeConfiguration<RepairRequestType>
    {
        public override void Configure(EntityTypeBuilder<RepairRequestType> repairRequestTypeConfiguration)
        {
            base.Configure(repairRequestTypeConfiguration);

            repairRequestTypeConfiguration.ToTable("RepairRequestType");
            repairRequestTypeConfiguration.HasKey(o => o.Id);
            repairRequestTypeConfiguration.Property(o => o.RepairRequestTypeName_AR).IsRequired();
            repairRequestTypeConfiguration.Property(o => o.RepairRequestTypeName_EN).IsRequired();
           
        }
    }
}
