﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.EntityConfigurations
{
  public class SparePartsRequestConfiguration : BaseEntityTypeConfiguration<SparePartsRequest>, IEntityTypeConfiguration<SparePartsRequest>
    {
        public override void Configure(EntityTypeBuilder<SparePartsRequest> sparePartsRequestTypeConfiguration)
    {
            base.Configure(sparePartsRequestTypeConfiguration);

            sparePartsRequestTypeConfiguration.ToTable("SparePartsRequest");
            sparePartsRequestTypeConfiguration.HasKey(o => o.Id);
            sparePartsRequestTypeConfiguration.Property(o => o.FK_RepairRequest_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.FK_Technician_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.StartDate).IsRequired();
            sparePartsRequestTypeConfiguration.Property(o => o.EndDate).IsRequired();
            sparePartsRequestTypeConfiguration.Property(o => o.Amount).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.FK_Inventory_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.FK_Status_ID).IsRequired(false);
            sparePartsRequestTypeConfiguration.Property(o => o.Comments).IsRequired(false);

            sparePartsRequestTypeConfiguration.Ignore(o => o.Status);
            sparePartsRequestTypeConfiguration.Ignore(o => o.Inventory);
            sparePartsRequestTypeConfiguration.Ignore(o => o.JobCard);
            sparePartsRequestTypeConfiguration.Ignore(o => o.Technician);
        }
    }
}
