﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleMaintenance.Models.Entities.RepairRequests;

namespace VehicleMaintenance.models.EntityConfigurations
{
    public class RepairRequestConfiguration : BaseEntityTypeConfiguration<RepairRequest>, IEntityTypeConfiguration<RepairRequest>
    {
        public override void Configure(EntityTypeBuilder<RepairRequest> repairRequestConfiguration)
        {
            base.Configure(repairRequestConfiguration);

            repairRequestConfiguration.ToTable("RepairRequest");
            repairRequestConfiguration.HasKey(o => o.Id);
            repairRequestConfiguration.Property(o => o.FK_User_ID).IsRequired(false);
            repairRequestConfiguration.Property(o => o.FK_RepairRequestType_ID).IsRequired(false);
            repairRequestConfiguration.Property(o => o.FK_JobCard_ID).IsRequired(false);
            repairRequestConfiguration.Property(o => o.FK_RepairRequestStatus_ID).IsRequired(false);
            repairRequestConfiguration.Property(o => o.Maintenance_Type).IsRequired().HasDefaultValue(false);
            repairRequestConfiguration.Property(o => o.FK_ServiceType_ID).IsRequired(false);
            repairRequestConfiguration.Property(o => o.Duration).IsRequired();
            repairRequestConfiguration.Property(o => o.Cost).IsRequired();
            repairRequestConfiguration.Property(o => o.External).IsRequired().HasDefaultValue(false);
            repairRequestConfiguration.Property(o => o.Comment).IsRequired(false);
        }
    }
}
