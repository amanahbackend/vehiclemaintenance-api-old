﻿using System.Collections.Generic;
using VehicleMaintenance.models.Entities;

namespace VehicleMaintenance.models.Settings
{
    public class VehicleAppSettings
    {
        public string RepairFilePath{ get; set; }
        public string AudoMeterInvalidMessage { get; set; }
        public string ExistInGarageStatus{ get; set; }
        public string PendingApprovedStatus { get; set; }
        public double AudoMeterFactorChange { get; set; }
        public string TransferdSparePartStatus  { get; set; }
        public string ClosedStatus { get; set; }
        public string CancelledStatus { get; set; }
        public List<Status> JobCardStatus { get; set; }
        public List<SparePartStatus> SparePartStatus { get; set; }
    }
}
