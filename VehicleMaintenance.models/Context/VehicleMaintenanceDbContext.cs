﻿using Dispatching.Identity.Models.EntitiesConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VehicleMaintenance.models.Entities;
using VehicleMaintenance.models.EntityConfigurations;
using VehicleMaintenance.Models.Entities;
using VehicleMaintenance.Models.Entities.Common;
using VehicleMaintenance.Models.Entities.RepairRequests;
using VehicleMaintenance.Models.Entities.Vehicles;
using VehicleMaintenance.Models.EntityConfigurations;

namespace VehicleMaintenance.models.Context
{
    public class VehicleMaintenanceDbContext : IdentityDbContext<User, Role, string>
    {
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<JobCard> JobCards { get; set; }
        public DbSet<Technician> Technicians { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleType> VehicleType { get; set; }
        public DbSet<JunkUser> JunkUsers { get; set; }
        public DbSet<PasswordTokenPin> PasswordTokenPins { get; set; }
        public DbSet<EquipmentTransferHistory> EquipmentTransferHistorys { get; set; }
        public DbSet<EquipmentTransferStatus> EquipmentTransferStatuss { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<RepairRequest> RepairRequests { get; set; }
        public DbSet<RepairRequestStatus> RepairRequestStatus { get; set; }
        public DbSet<RepairRequestType> RepairRequestTypes { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<SparePartsRequest> SparePartsRequests { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<JobCardTechnician> JobCard_Technicians { get; set; }
        public DbSet<RepairRequestTechnician> RepairRequest_Technicians { get; set; }
        public DbSet<Inventory> Inventorys { get; set; }
        public DbSet<UsedMaterials> UsedMaterials { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<SparePartStatus> SparePartStatus { get; set; }
        public DbSet<Weekend> Weekend { get; set; }

        public DbSet<Shift> Shift { get; set; }

        public VehicleMaintenanceDbContext(DbContextOptions<VehicleMaintenanceDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DriverTypeConfiguration());
            modelBuilder.ApplyConfiguration(new JobCardConfiguration());
            modelBuilder.ApplyConfiguration(new RoleTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TechnicianTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleTypeConfiguration());
            modelBuilder.ApplyConfiguration(new JunkUserTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EquipmentTransferHistoryTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EquipmentTransferStatusTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FileTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RepairRequestStatusConfiguration());
            modelBuilder.ApplyConfiguration(new RepairRequestConfiguration());
            modelBuilder.ApplyConfiguration(new RepairRequestTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ServiceTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PasswordTokenPinTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StatusTypeConfiguration());
            modelBuilder.ApplyConfiguration(new InventoryTypeConfiguration());
            modelBuilder.ApplyConfiguration(new JobCardTechnicianTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RepairRequestTechnicianConfiguration());
            modelBuilder.ApplyConfiguration(new SparePartsRequestConfiguration());
            modelBuilder.ApplyConfiguration(new UsedMaterialsTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VendorTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SparePartStatusTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WeekendTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ShiftConfiguration());

        }
    }
}
